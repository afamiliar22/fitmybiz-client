import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/follow_model.dart';

import 'package:fitmybiz/models/keyword_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'package:fitmybiz/routes/searchView/search_view_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class SearchViewCubit extends BaseCubit<SearchViewState> {
  /// {@macro counter_cubit}
  SearchViewCubit() : super(InitialState());
  final searchDebouncer = Debouncer(milliseconds: 500);
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  int page = 1;
  List<ContentsModel> contents = [];

  String query = '';
  void init() {
    emit(InitialState());
  }

  void setQuery(String str) {
    this.query = str;
  }

  onRefresh() {
    this.page = 1;
    this.getSearchImageContent().then((contents) {
      this.contents = contents;
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    }).catchError((e) {
      this.contents = [];
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    });
  }

  onLoading() {
    this.page = this.page + 1;
    this.getSearchImageContent().then((contents) {
      this.contents.addAll(contents);
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      if (contents.length == 0) {
        this.refreshController.loadNoData();
      } else {
        this.refreshController.loadComplete();
      }
    }).catchError((e) {
      this.refreshController.loadComplete();
    });
  }

  void getKeyword() {
    String method = 'get';
    String path = '/api/v1/recommend/keyword';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<KeywordModel> keywords = (response.data as List)
          .map((record) => KeywordModel.fromJson(record))
          .toList();

      emit(GetKeywords(true, keywords));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getRecommendUser(double lat, double lng) {
    String method = 'get';
    String path = '/api/v1/user/recommend';

    emit(LoadingState());
    Geolocator.getLastKnownPosition().then((value) {
      Map<String, dynamic> data = {};

      if (value != null) {
        data = {
          "lat": value.latitude.toString(),
          "lng": value.longitude.toString()
        };
      }
      // Map<String, dynamic> data = {"lat": "37.6052809", "lng": "127.0246305"};
      this.network.requestWithAuth(method, path, data).then((response) {
        List<UserModel> recommendUsers = (response.data['data'] as List)
            .map((record) => UserModel.fromJson(record))
            .toList();

        emit(GetRecommendUser(true, recommendUsers));
      }).catchError((e) {
        emit(ErrorState());
      });
    });
  }

  void getLikingImageContents() {
    String method = 'get';
    String path = '/api/v1/content/image_content/list';
    Map<String, dynamic> data = {"user_taste": "1"};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      List<ContentsModel> contents = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();

      emit(GetLikingContents(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future<List<ContentsModel>> getSearchImageContent() {
    String method = 'get';
    String path = '/api/v1/search/image_content';
    Map<String, dynamic> data = {
      "query": this.query,
      'take': '20',
      'page': this.page.toString()
    };

    return this.network.requestWithAuth(method, path, data).then((response) {
      List<ContentsModel> result = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();

      return Future.value(result);
    });
  }

  void getSearchUser(String name, String take) {
    String method = 'get';
    String path = '/api/v1/user/search';
    Map<String, dynamic> data = {"nickname": name, 'take': take};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      List<UserModel> users = (response.data['data'] as List)
          .map((record) => UserModel.fromJson(record))
          .toList();

      emit(GetSearchUser(true, users));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void putUpdateFollow(String userNo) {
    String method = 'put';
    String path = '/api/v1/user/$userNo/follow';

    // emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      this.property.meProfileSubject.add(true);
      this.facebook.addEvent(FacebookEventType.following);
      FollowModel follow = FollowModel.fromJson(response.data);

      emit(PutUpdateFollow(true, follow));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
