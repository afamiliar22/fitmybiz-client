import 'dart:developer';

import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'image_source_model.dart';

class FollowModel {
  final String followerNo;
  final String userNo;
  final String followingUserNo;
  final String state;
  final String nickname;
  final ImageSourceModel profileImage;

  final bool isFollow;

  FollowModel({
    this.followerNo,
    this.userNo,
    this.followingUserNo,
    this.state,
    this.nickname,
    this.profileImage,
    this.isFollow,
  });

  factory FollowModel.fromJson(Map<String, dynamic> json) {
    return FollowModel(
      followerNo: json['follow_no'].toString(),
      userNo: json['user_no'].toString(),
      followingUserNo: json['following_user_no'].toString(),
      state: json['state'].toString(),
      nickname: json['nickname'].toString(),
      profileImage:
          json.containsKey('profile_image') && json['profile_image'] != null
              ? ImageSourceModel.fromJson(json['profile_image'])
              : null,
      isFollow: json['is_follow'],
    );
  }
}
