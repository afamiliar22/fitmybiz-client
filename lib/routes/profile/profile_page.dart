import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_text_field2.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/area_code_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/profile/profile_state.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/taste_temp/taste_page.dart';
import 'package:fitmybiz/themes/text_style_theme.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'profile_cubit.dart';

class ProfilePage extends StatelessWidget {
  final bool isReset;
  ProfilePage(
    this.isReset,
  );
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => ProfileCubit(),
        ),
      ],
      child: _View(this.isReset),
    );
  }
}

class _View extends BaseRoute {
  final bool isReset;
  _View(this.isReset);
  @override
  _ViewState createState() => _ViewState(this.isReset);
}

class _ViewState extends BaseRouteState {
  bool isReset;

  _ViewState(this.isReset);

  AreaCodeModel areaCode;
  List<Map<String, dynamic>> areas = [];
  UserModel user;
  TextEditingController nicknameController = TextEditingController();
  TextEditingController introduceController = TextEditingController();

  ProfileCubit cubit;

  //
  List<CategoryModel> categories = List<CategoryModel>();

  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<ProfileCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
  }

  @override
  Widget build(BuildContext context) {
    Widget avatarImage;
    if (cubit.imageBytes != null) {
      avatarImage = Image.memory(cubit.imageBytes, fit: BoxFit.cover);
    } else if (this.user != null && this.user.profileImage != null) {
      // avatarImage = FadeInImage.memoryNetwork(placeholder: kTransparentImage, image: this.user.profileImage.l, fit: BoxFit.cover);
      avatarImage = CustomCachedNetworkImage(
        preloadImage: this.user.profileImage.s,
        image: this.user.profileImage.s,
      );
    } else {
      avatarImage =
          Image.asset('assets/images/userProfileImg.png', fit: BoxFit.cover);
    }

    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case GetCategory:
            categories = state.categories;

            this.indicator.hide();

            break;

          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            if (state.message != null) {
              this.showToast(state.message);
            }
            break;
          case CompleteState:
            this.indicator.hide();
            if (this.isReset == true) {
              this
                  .navigator
                  .replaceRoute(TastePage(categories, true, this.user.userNo));
              return;
              this.navigator.resetRoute(SplashPage());
            } else {
              this.navigator.popRoute(null);
            }

            break;
          case LoadedUserState:
            this.indicator.hide();
            this.user = state.user;
            this.nicknameController.text = this.user.nickname;
            this.introduceController.text = this.user.introduce;
            setState(() {});
            break;
          case LoadedAreaState:
            this.indicator.hide();
            this.areaCode = state.areaCode;
            setState(() {});
            break;
          case LoadedUserAreaState:
            this.indicator.hide();
            this.areas = state.areas;
            setState(() {});
            break;
        }
      },
      child: PlatformScaffold(
        onPressed: () {
          FocusScope.of(context).unfocus();
        },
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('프로필 설정'),
        trailing: true
            ? CustomContainer(
                child: Text(
                  '다음',
                  style: this.nicknameController.text.isNotEmpty
                      ? TextStyleTheme.txtBtnNextEnable
                      : TextStyleTheme.txtBtnNextDisable,
                ),
                onPressed: () => context.bloc<ProfileCubit>().updateUser(),
              )
            : FlatButton(
                padding: EdgeInsets.only(right: 24),
                child: Text(
                  '다음',
                  style: this.nicknameController.text.isNotEmpty
                      ? TextStyleTheme.txtBtnNextEnable
                      : TextStyleTheme.txtBtnNextDisable,
                ),
                onPressed: () => context.bloc<ProfileCubit>().updateUser(),
              ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  padding: EdgeInsets.all(24),
                  children: <Widget>[
                    Center(
                      child: Stack(
                        children: [
                          CustomContainer(
                            child: CustomAvatar(
                              image: avatarImage,
                              size: 100,
                              radius: 40,
                            ),
                            onPressed: () {
                              this.showImagePicker().then((value) {
                                this.cubit.imageBytesChanged(value);
                                setState(() {});
                              });
                            },
                          ),
                          Positioned(
                            child: CustomContainer(
                              backgroundColor: Color.fromRGBO(0, 0, 0, 0.3),
                              borderRadius: [20, 20, 20, 20],
                              padding: EdgeInsets.all(6),
                              child: Image.asset('assets/images/icnsCamera.png',
                                  fit: BoxFit.cover, width: 24, height: 24),
                            ),
                            bottom: 0,
                            right: 0,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 36),
                    Text('닉네임', style: TextStyleTheme.textFieldLabel),
                    SizedBox(height: 8),
                    CustomTextField2(
                      controller: this.nicknameController,
                      placeholder: "닉네임을 입력해주세요",
                      placeholderStyle: TextStyleTheme.placeholder,
                      onChanged: (text) {
                        setState(() {});
                        return context
                            .bloc<ProfileCubit>()
                            .nicknameChanged(text);
                      },
                    ),
                    SizedBox(height: 36),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 10),
                            Text('활동 지역 설정',
                                style: TextStyleTheme.textFieldLabel),
                            SizedBox(height: 10),
                            Text(
                              '여러분께 꼭 맞는 추천을 위해 쓰이며\n최대 3개의 지역까지 설정 가능합니다',
                              style: TextStyleTheme.common.copyWith(
                                  fontSize: 13,
                                  color: Colors.grey.withOpacity(0.5)),
                            ),
                          ],
                        ),
                        Spacer(),
                        CustomButton(
                          text: Text('+ 지역 추가',
                              style: TextStyleTheme.common.copyWith(
                                  fontSize: 13, color: ColorTheme.textLevel5)),
                          padding:
                              EdgeInsets.symmetric(vertical: 6, horizontal: 12),
                          borderColor: ColorTheme.textLevel5,
                          borderWidth: 1,
                          borderRadius: 8,
                          onPressed: () {
                            if (this.areas.length < 3) {
                              this.areas.add({});
                              setState(() {});
                            } else {
                              this.showToast('활동 지역은 최대 3개의 지역까지 설정 가능합니다');
                            }
                          },
                        )
                      ],
                    ),
                    SizedBox(height: 20),
                    ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return this.buildAreaCell(index);
                      },
                      itemCount: this.areas.length,
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
              /*
              CustomContainer(
                margin: EdgeInsets.all(25),
                child: PrimaryButton(
                  text: Text('완료'),
                  onPressed: () => context.bloc<ProfileCubit>().updateUser(),
                ),
              ),
              */
            ],
          ),
        ),
      ),
    );
  }

  Widget buildAreaCell(int index) {
    Map<String, dynamic> area = this.areas[index];
    List<dynamic> selectGungu = this.areaCode.gungu[area['areaName1']];
    if (selectGungu != null && selectGungu.length <= 1) {
      area['areaName2'] = area['areaName1'];
      area['code'] = selectGungu[0]['code'];
    }
    String areaName1 = area['areaName1'] ?? '서울특별시';
    String areaName2 = area['areaName2'] ?? '종로구';

    Color areaColor1 = area['areaName1'] != null
        ? Colors.black
        : Color.fromRGBO(205, 210, 216, 1.0);
    Color areaColor2 = area['areaName2'] != null
        ? Colors.black
        : Color.fromRGBO(205, 210, 216, 1.0);

    Widget actionButton;

    if (true ? area['areaName2'] == null : area['userActiveAreaNo'] == null) {
      actionButton = CustomContainer(
        backgroundColor: Color.fromRGBO(205, 210, 216, 1.0),
        width: 44,
        height: 44,
        borderRadius: [15, 15, 15, 15],
        padding: EdgeInsets.all(8),
        child: Image.asset('assets/images/icnsLocationHunt.png',
            width: 32, height: 32, color: Colors.white),
        onPressed: () {
          context.bloc<ProfileCubit>().selectCurrentLocation(index);
        },
      );
    } else {
      actionButton = CustomContainer(
        backgroundColor: ColorTheme.primary,
        width: 44,
        height: 44,
        borderRadius: [15, 15, 15, 15],
        padding: EdgeInsets.all(8),
        child: Center(
          child: CustomContainer(
              width: 16,
              height: 4,
              backgroundColor: Colors.white,
              borderRadius: [8, 8, 8, 8]),
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (dialogContext) {
              return Dialog(
                child: CustomContainer(
                  padding: EdgeInsets.all(25),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        '활동 지역 삭제',
                        style: TextStyleTheme.common.copyWith(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(height: 16),
                      Text(
                        '선택하신 활동 지역을 삭제하시겠습니까?',
                        style: TextStyleTheme.common.copyWith(
                            fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 40),
                      Row(
                        children: [
                          Expanded(
                            child: CustomButton(
                              text: Text(
                                '취소',
                                style: TextStyleTheme.common.copyWith(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                              ),
                              onPressed: () {
                                Navigator.of(dialogContext).pop();
                              },
                            ),
                          ),
                          Expanded(
                            child: CustomButton(
                              text: Text(
                                '삭제',
                                style: TextStyleTheme.common.copyWith(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: ColorTheme.primary),
                              ),
                              onPressed: () {
                                area['areaName1'] = null;
                                area['areaName2'] = null;
                                if (area['userActiveAreaNo'] != null)
                                  context
                                      .bloc<ProfileCubit>()
                                      .deleteUserArea(area['userActiveAreaNo']);
                                setState(() {});
                                Navigator.of(dialogContext).pop();
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            },
          );
        },
      );
    }

    return CustomContainer(
      margin: EdgeInsets.symmetric(vertical: 6),
      child: Row(
        children: [
          Expanded(
            child: CustomContainer(
              backgroundColor: Color.fromRGBO(244, 246, 248, 1.0),
              padding: EdgeInsets.only(top: 8, bottom: 8, left: 12, right: 8),
              borderRadius: [15, 15, 15, 15],
              child: Row(
                children: [
                  Expanded(
                    child: Text(areaName1,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyleTheme.common
                            .copyWith(fontSize: 15, color: areaColor1)),
                  ),
                  Image.asset('assets/images/icnsDown.png',
                      width: 32, height: 32),
                ],
              ),
              onPressed: () {
                this.showArea1Dialog(area);
              },
            ),
          ),
          SizedBox(width: 12),
          if (selectGungu == null || selectGungu.length > 1)
            Expanded(
              child: CustomContainer(
                backgroundColor: Color.fromRGBO(244, 246, 248, 1.0),
                padding: EdgeInsets.only(top: 8, bottom: 8, left: 12, right: 8),
                borderRadius: [15, 15, 15, 15],
                child: Row(
                  children: [
                    Expanded(
                      child: Text(areaName2,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyleTheme.common
                              .copyWith(fontSize: 15, color: areaColor2)),
                    ),
                    Image.asset('assets/images/icnsDown.png',
                        width: 32, height: 32),
                  ],
                ),
                onPressed: () {
                  if (area['areaName1'] != null) {
                    this.showArea2Dialog(area);
                  } else {
                    this.showToast('시도를 먼저 선택해주세요');
                  }
                },
              ),
            ),
          if (selectGungu == null || selectGungu.length > 1)
            SizedBox(width: 12),
          actionButton
        ],
      ),
    );
  }

  void showArea1Dialog(Map<String, dynamic> area) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.all(8),
                  child: Text(
                    '시도 선택',
                    style: TextStyleTheme.common
                        .copyWith(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: this.areaCode.si.length,
                    itemBuilder: (context, index) {
                      return CustomContainer(
                        child: CupertinoButton(
                          padding: EdgeInsets.all(16),
                          child: Text(
                            this.areaCode.si[index],
                            textAlign: TextAlign.start,
                          ),
                          onPressed: () {
                            area['areaName1'] = this.areaCode.si[index];
                            area['areaName2'] = null;
                            area['isUpdate'] = true;
                            area['code'] = null;

                            setState(() {});
                            Navigator.of(context).pop();
                          },
                        ),
                        borderColor: Colors.grey.withOpacity(0.25),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showArea2Dialog(Map<String, dynamic> area) {
    String areaName1 = area['areaName1'];

    List<dynamic> gungu = this.areaCode.gungu[areaName1];

    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.all(8),
                  child: Text(
                    '군구 선택',
                    style: TextStyleTheme.common
                        .copyWith(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: gungu.length,
                    itemBuilder: (context, index) {
                      return CustomContainer(
                        child: CupertinoButton(
                          padding: EdgeInsets.all(16),
                          child: Text(
                            gungu[index]['name'] ?? '-',
                            textAlign: TextAlign.start,
                          ),
                          onPressed: () {
                            area['areaName2'] = gungu[index]['name'];
                            area['code'] = gungu[index]['code'];
                            area['isUpdate'] = true;
                            setState(() {});
                            Navigator.of(context).pop();
                          },
                        ),
                        borderColor: Colors.grey.withOpacity(0.25),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
