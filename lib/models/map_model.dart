import 'dart:developer';

import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'image_source_model.dart';

class MapModel {
  final String contentNo;

  final double lat;
  final double lng;
  final String imageContentNoList;
  final int tempMarkerIcon;
  final String categoryNoList;

  MapModel(
      {this.contentNo,
      this.lat,
      this.lng,
      this.imageContentNoList,
      this.tempMarkerIcon,
      this.categoryNoList});

  factory MapModel.fromJson(Map<String, dynamic> json) {
    return MapModel(
      contentNo: json['content_no'].toString(),
      lat: json['lat'],
      lng: json['lng'],
      imageContentNoList: json['image_content_no_list'],
      tempMarkerIcon: json['temp_marker_icon'],
      categoryNoList: json['category_no_list'],
    );
  }
}
