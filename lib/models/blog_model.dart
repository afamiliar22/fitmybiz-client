class BlogModel {
  String blogname;
  String contents;
  String datetime;
  String thumbnail;
  String title;
  String url;

  BlogModel(
      {this.blogname,
      this.contents,
      this.datetime,
      this.thumbnail,
      this.title,
      this.url});

  BlogModel.fromJson(Map<String, dynamic> json) {
    blogname = json['blogname'];
    contents = json['contents'];
    datetime = json['datetime'];
    thumbnail = json['thumbnail'];
    title = json['title'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['blogname'] = this.blogname;
    data['contents'] = this.contents;
    data['datetime'] = this.datetime;
    data['thumbnail'] = this.thumbnail;
    data['title'] = this.title;
    data['url'] = this.url;
    return data;
  }
}
