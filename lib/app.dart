import 'package:bot_toast/bot_toast.dart';
import 'package:fitmybiz/temp_routes/home.dart';
import 'package:fitmybiz/temp_routes/tabs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:kakao_flutter_sdk/all.dart';
import 'package:fitmybiz/components/platform_app.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:easy_localization/easy_localization.dart';

import 'base_route.dart';

/// {@template counter_app}
/// A [MaterialApp] which sets the `home` to [CounterPage].
/// {@endtemplate}
class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    KakaoContext.clientId = "46c49ac8e5f404e286f2cdea5e5ee53d";
    final botToastBuilder = BotToastInit();
    return PlatformApp(
      localizationDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      navigatorObservers: [BotToastNavigatorObserver()],
      builder: (BuildContext context, Widget child) {
        child = FlutterEasyLoading(
          child: child,
        );
        child = botToastBuilder(context, child);
        return child;
      },
      home: SplashPage(),
    );
  }
}

class WillPopScopeDemo extends BaseRoute {
  @override
  WillPopScopeDemoState createState() => WillPopScopeDemoState();
}

class WillPopScopeDemoState extends BaseRouteState {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: this.onWillPop,
      child: Scaffold(
        appBar: AppBar(title: Text("WillPopScope Demo")),
        body: Center(
          child: Text("Tap back button to leave this page"),
        ),
      ),
    );
  }
}
