import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/splash/splash_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class SplashCubit extends BaseCubit<SplashState> {
  /// {@macro counter_cubit}
  SplashCubit() : super(InitialState());

  void init() {
    this.facebook.addEvent(FacebookEventType.installLaunchApp);
    this.property.isSignIn.then((value) {
      if (value == false) {
        emit(AuthState(false));
      } else {
        String method = 'get';
        String path = '/api/v1/user/me';
        this.network.requestWithAuth(method, path, null).then((response) {
          this.property.user = UserModel.fromJson(response.data);
          emit(AuthState(true));
        }).catchError((e) {
          emit(AuthState(false));
        });
      }
    });
  }

  void getCategories(int categoryNo, String categoryName, int limit, int page) {
    String method = 'get';
    String path = '/api/v1/content/category/list';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<CategoryModel> category = (response.data['data'] as List)
          .map((record) => CategoryModel.fromJson(record))
          .toList();

      emit(GetCategory(true, category));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
