import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/notice_model.dart';
import 'package:fitmybiz/extensions/extensions.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'notice_detail_cubit.dart';

class NoticeDetailPage extends StatelessWidget {
  final NoticeModel notice;
  NoticeDetailPage(this.notice);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => NoticeDetailCubit(),
      child: _View(this.notice),
    );
  }
}

class _View extends BaseRoute {
  final NoticeModel notice;
  _View(this.notice);
  @override
  _ViewState createState() => _ViewState(this.notice);
}

class _ViewState extends BaseRouteState {
  NoticeModel notice;
  _ViewState(this.notice);

  NoticeDetailCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<NoticeDetailCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<NoticeDetailCubit>(),
      listener: (context, state) {},
      child: PlatformScaffold(
        title: Text(''),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
            children: [
              Text(
                notice.noticeTitle,
                style: TextStyleTheme.common.copyWith(
                  fontSize: 22,
                  color: ColorTheme.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 15),
              Text(
                notice.createdAt.toDateString() +
                    ' ' +
                    notice.createdAt.toTimeString(),
                style: TextStyleTheme.common.copyWith(
                  fontSize: 13,
                  color: Color.fromRGBO(173, 181, 190, 1.0),
                ),
              ),
              SizedBox(height: 15),
              Divider(height: 0.5, thickness: 0.5),
              SizedBox(height: 20),
              Text(
                notice.noticeContent,
                style: TextStyleTheme.common.copyWith(
                  fontSize: 15,
                  color: ColorTheme.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
