import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CustomSmartRefresher {
  static CustomHeader customHeader() {
    return CustomHeader(
      builder: (BuildContext context, RefreshStatus mode) {
        Widget body = CupertinoActivityIndicator();
        return Container(
          height: 48.0,
          child: Center(child: body),
        );
      },
    );
  }

  static CustomFooter customFooter() {
    return CustomFooter(
      builder: (BuildContext context, LoadStatus mode) {
        Widget body;

        if (mode == LoadStatus.idle || mode == LoadStatus.noMore) {
          body = null;
        } else {
          body = CupertinoActivityIndicator();
        }
        return Container(
          height: 48.0,
          child: Center(child: body),
        );
      },
    );
  }
}
