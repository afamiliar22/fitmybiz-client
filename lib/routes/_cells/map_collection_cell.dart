import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:fitmybiz/extensions/extensions.dart';

class MapCollectionCell extends StatelessWidget {
  final CollectionModel collection;
  final VoidCallback onPressed;
  final VoidCallback onPressedMore;
  final bool isEditMode;
  final bool isSelect;
  final bool isForDelete;
  MapCollectionCell(
      {this.collection,
      this.isEditMode,
      this.isSelect,
      this.onPressed,
      @required this.isForDelete,
      this.onPressedMore});

  @override
  Widget build(BuildContext context) {
    Widget squareWidget;
    Widget radio;
    if (this.isEditMode == true) {
      radio = Icon(
        Icons.radio_button_unchecked,
        color: Color.fromRGBO(205, 210, 216, 1),
      );

      Color coverColor = Colors.transparent;
      if (this.isSelect == true) {
        radio = Icon(
          Icons.radio_button_checked,
          color: ColorTheme.primary2,
        );
        coverColor = ColorTheme.primary.withOpacity(0.3);
      }
      squareWidget = Stack(
        children: [
          Positioned.fill(child: this.buildCollectionImages(collection)),
          //Positioned(top: 12, left: 12, child: radio),
          this.isSelect == true
              ? CustomContainer(
                  child: Center(
                      child: Image.asset(
                    'assets/images/icnsCheck.png',
                    color: Colors.white,
                    width: 28,
                    height: 28,
                  )),
                  backgroundColor: coverColor,
                  borderRadius: [12, 12, 12, 12],
                )
              : Container()
        ],
      );
    } else {
      squareWidget = this.buildCollectionImages(collection);
    }

    return CustomContainer(
      height: 60,
      onPressed: onPressed,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 60,
            height: 60,
            child: squareWidget,
          ),
          SizedBox(width: 15),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(collection.name,
                      style: TextStyle(
                        fontSize: 16,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis),
                ],
              ),
              SizedBox(height: 2),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${collection.itemCount}개의 장소',
                    style: TextStyleTheme.common
                        .copyWith(fontSize: 14, color: ColorTheme.grayLv1),
                  ),
                ],
              )
            ],
          ),
          Spacer(),
          isForDelete ? radio : Container()
        ],
      ),
    );
  }

  Widget buildMore() {
    return CustomButton(
      image: Image.asset(
        'assets/images/iconMore.png',
        color: ColorTheme.black,
        height: 20,
        width: 20,
      ),
      onPressed: this.onPressedMore,
    );
  }

  Widget buildCollectionImages(CollectionModel collection) {
    if (collection.collectionItems.length >= 1) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: CustomCachedNetworkImage(
          preloadImage: collection.collectionItems[0].sm,
          image: collection.collectionItems[0].sm,
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Container(color: ColorTheme.grayBg),
      );
    }
  }
}
