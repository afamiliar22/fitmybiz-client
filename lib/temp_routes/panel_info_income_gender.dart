import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class PanelInfoGenderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('패널 정보입력'),
        body: CustomContainer(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '성별을 입력해주세요',
                style: TextStyle(color: ColorTheme.black, fontSize: 16),
              ),
              SizedBox(
                height: 18,
              ),
              Center(
                child: Text(
                  '성별',
                  style: TextStyle(color: ColorTheme.grayLv1),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomContainer(
                    width: 86,
                    height: 41,
                    backgroundColor: ColorTheme.primary2,
                    child: Center(
                      child: Text(
                        '여성',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  CustomContainer(
                    width: 86,
                    height: 41,
                    backgroundColor: ColorTheme.primary2,
                    child: Center(
                      child: Text(
                        '남성',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  Spacer(),
                  CustomContainer(
                    margin: EdgeInsets.all(10),
                    width: 285,
                    height: 40,
                    onPressed: () {},
                    backgroundColor: ColorTheme.primary2,
                    borderRadius: [6, 6, 6, 6],
                    child: Center(
                      child: Text(
                        '다음',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
