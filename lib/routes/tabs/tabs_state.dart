import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/login_model.dart';

abstract class TabsState extends Equatable {}

class InitialState extends TabsState {
  @override
  List<Object> get props => [];
}

class LoadingState extends TabsState {
  @override
  List<Object> get props => [];
}

class CompleteState extends TabsState {
  @override
  List<Object> get props => [];
}

class ErrorState extends TabsState {
  @override
  List<Object> get props => [];
}
