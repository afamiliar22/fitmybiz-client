import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/collection_model.dart';

abstract class SelectCollectionState extends Equatable {}

class InitialState extends SelectCollectionState {
  @override
  List<Object> get props => [];
}

class SetState extends SelectCollectionState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadedState extends SelectCollectionState {
  @override
  List<Object> get props => [];
}

class CompletedState extends SelectCollectionState {
  @override
  List<Object> get props => [];
}

class LoadingState extends SelectCollectionState {
  @override
  List<Object> get props => [];
}

class ToastState extends SelectCollectionState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends SelectCollectionState {
  @override
  List<Object> get props => [];
}

class PostCollectionSet extends SelectCollectionState {
  PostCollectionSet(this.result);

  final bool result;

  @override
  List<Object> get props => [result];
}
