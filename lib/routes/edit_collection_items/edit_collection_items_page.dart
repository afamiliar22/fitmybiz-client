import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_item_model.dart';
import 'package:fitmybiz/routes/_cells/collection_item_cell.dart';
import 'package:fitmybiz/routes/_cells/list_collection_item_cell.dart';
import 'package:fitmybiz/routes/select_collection/select_collection_page.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'edit_collection_items_cubit.dart';
import 'edit_collection_items_state.dart';

class EditCollectionItemsPage extends StatelessWidget {
  final String collectionNo;
  final List<CollectionItemModel> collectionItems;
  EditCollectionItemsPage(this.collectionNo, this.collectionItems);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => EditCollectionItemsCubit(),
      child: _View(this.collectionNo, this.collectionItems),
    );
  }
}

class _View extends BaseRoute {
  final String collectionNo;
  final List<CollectionItemModel> collectionItems;
  _View(this.collectionNo, this.collectionItems);

  @override
  _ViewState createState() =>
      _ViewState(this.collectionNo, this.collectionItems);
}

class _ViewState extends BaseRouteState {
  String collectionNo;
  List<CollectionItemModel> collectionItems;
  _ViewState(this.collectionNo, this.collectionItems);

  EditCollectionItemsCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<EditCollectionItemsCubit>();
    this.cubit.collectionNoChanged(this.collectionNo);
    this.cubit.collectionItemsChanged(this.collectionItems);
  }

  @override
  Widget build(BuildContext context) {
    Widget selectButton = CustomContainer(
      backgroundColor: Colors.white,
      borderRadius: [10, 10, 10, 10],
      borderWidth: 1.0,
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
      child: Text(
        '전체 선택',
        style: TextStyleTheme.common
            .copyWith(fontSize: 16, color: ColorTheme.primary2),
      ),
      onPressed: () {
        this.cubit.selectedCollectionItems = this
            .cubit
            .collectionItems
            .map((e) => e.collectionItemNo.toString())
            .toSet();
        setState(() {});
      },
    );

    Widget deselectButton = CustomContainer(
      backgroundColor: Colors.white,
      borderRadius: [10, 10, 10, 10],
      borderColor: Colors.white,
      borderWidth: 1.0,
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
      child: Text(
        '선택 해제',
        style: TextStyleTheme.common
            .copyWith(fontSize: 16, color: ColorTheme.grayLv1),
      ),
      onPressed: () {
        this.cubit.selectedCollectionItems = Set<String>();
        setState(() {});
      },
    );

    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case CompletedState:
            this.navigator.popRoute(true);
            break;
          case LoadedState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        body: SafeArea(
          child: Column(
            children: [
              CustomContainer(
                height: 48,
                child: Row(
                  children: [
                    SizedBox(width: 25),
                    IconButton(
                      padding: EdgeInsets.all(8),
                      icon: Image.asset('assets/images/icnsClose.png',
                          color: Colors.black, width: 28, height: 28),
                      onPressed: () {
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                      },
                    ),
                    Spacer(),
                    Text('장소 정리', style: TextStyleTheme.subtitle),
                    Spacer(),
                    if (this.cubit.selectedCollectionItems.length !=
                        this.cubit.collectionItems.length)
                      selectButton,
                    if (this.cubit.selectedCollectionItems.length ==
                        this.cubit.collectionItems.length)
                      deselectButton,
                    SizedBox(width: 25),
                  ],
                ),
              ),
              Expanded(
                child: Stack(
                  children: [
                    buildCollections(this.collectionItems),
                    Positioned(
                      bottom: 24,
                      left: 0,
                      right: 0,
                      child: Row(
                        children: [
                          Spacer(),
                          CustomContainer(
                            shadowColor: ColorTheme.primary.withOpacity(.5),
                            backgroundColor: ColorTheme.primary,
                            borderRadius: [40, 40, 40, 40],
                            padding: EdgeInsets.symmetric(
                                vertical: 20, horizontal: 30),
                            child: Row(
                              children: [
                                GestureDetector(
                                  child: Image.asset(
                                      'assets/images/icnsMove.png',
                                      width: 28,
                                      height: 28,
                                      color: Colors.white),
                                  onTap: () {
                                    if (this
                                            .cubit
                                            .selectedCollectionItems
                                            .length <=
                                        0) {
                                      this.showToast('복사할 장소를 선택해주세요.');
                                      return;
                                    }
                                    this
                                        .navigator
                                        .pushRoute(SelectCollectionPage(
                                          selectedItems: this
                                              .cubit
                                              .collectionItems
                                              .where((element) => this
                                                  .cubit
                                                  .selectedCollectionItems
                                                  .contains(
                                                      element.collectionItemNo))
                                              .toList(),
                                        ))
                                        .then((value) {
                                      return;
                                      if (value == null) return;
                                      List<CollectionItemModel>
                                          selectedCollections = this
                                              .cubit
                                              .collectionItems
                                              .where((element) => this
                                                  .cubit
                                                  .selectedCollectionItems
                                                  .contains(
                                                      element.collectionItemNo))
                                              .toList();
                                      this.cubit.copyCollectionItem(
                                          value.toString(),
                                          selectedCollections
                                              .map((e) => e.imageContentNo)
                                              .join(','));
                                    });
                                    //
                                  },
                                ),
                                SizedBox(width: 30),
                                GestureDetector(
                                  child: Image.asset(
                                      'assets/images/icnsDelete2.png',
                                      width: 28,
                                      height: 28,
                                      color: Colors.white),
                                  onTap: () {
                                    if (this
                                            .cubit
                                            .selectedCollectionItems
                                            .length <=
                                        0) {
                                      this.showToast('삭제할 장소를 선택해주세요.');
                                      return;
                                    }
                                    this
                                        .showConfirm(
                                            title: '정말 삭제하시겠습니까?',
                                            description: '삭제하신 장소는 복구되지 않아요',
                                            positiveButtonText: '삭제',
                                            negativeButtonText: '취소')
                                        .then((value) {
                                      if (value == true) {
                                        this.cubit.deleteCollectionItem(
                                            this.cubit.collectionNo.toString(),
                                            this
                                                .cubit
                                                .selectedCollectionItems
                                                .toList()
                                                .join(','));
                                      }
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildCollections(List<CollectionItemModel> collectionItems) {
    return true
        ? ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 25, vertical: 12),
            shrinkWrap: true,
            primary: false,
            itemCount: this.cubit.collectionItems.length,
            itemBuilder: (BuildContext context, int index) {
              CollectionItemModel collectionItem =
                  this.cubit.collectionItems[index];
              return ListCollectionItemCell(
                collectionItem: collectionItem,
                isEditMode: true,
                isSelect: this
                    .cubit
                    .selectedCollectionItems
                    .contains(collectionItem.collectionItemNo),
                onPressed: () {
                  if (this
                      .cubit
                      .selectedCollectionItems
                      .contains(collectionItem.collectionItemNo)) {
                    this
                        .cubit
                        .selectedCollectionItems
                        .remove(collectionItem.collectionItemNo);
                  } else {
                    this
                        .cubit
                        .selectedCollectionItems
                        .add(collectionItem.collectionItemNo);
                  }
                  setState(() {});
                },
              );
            },
          )
        : StaggeredGridView.countBuilder(
            padding: EdgeInsets.symmetric(horizontal: 25, vertical: 12),
            shrinkWrap: true,
            primary: false,
            crossAxisCount: 3,
            itemCount: this.cubit.collectionItems.length,
            itemBuilder: (BuildContext context, int index) {
              CollectionItemModel collectionItem =
                  this.cubit.collectionItems[index];
              return CollectionItemCell(
                collectionItem: collectionItem,
                isEditMode: true,
                isSelect: this
                    .cubit
                    .selectedCollectionItems
                    .contains(collectionItem.collectionItemNo),
                onPressed: () {
                  if (this
                      .cubit
                      .selectedCollectionItems
                      .contains(collectionItem.collectionItemNo)) {
                    this
                        .cubit
                        .selectedCollectionItems
                        .remove(collectionItem.collectionItemNo);
                  } else {
                    this
                        .cubit
                        .selectedCollectionItems
                        .add(collectionItem.collectionItemNo);
                  }
                  setState(() {});
                },
              );
            },
            staggeredTileBuilder: (index) => StaggeredTile.count(
                (index % 7 == 0) ? 2 : 1, (index % 7 == 0) ? 2 : 1),
            mainAxisSpacing: 8.0,
            crossAxisSpacing: 8.0,
          );
  }
}
