import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/term_model.dart';
import 'package:fitmybiz/routes/auth/term/term_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class TermCubit extends BaseCubit<TermState> {
  /// {@macro counter_cubit}
  TermCubit() : super(InitialState());

  void updateUserTerm(List<int> termNo) {
    String method = 'post';
    String path = '/api/v1/user/term';
    Map data = {"term_no": termNo};

    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      emit(CompleteState());
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
