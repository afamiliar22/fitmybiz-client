import 'dart:developer';

import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'image_source_model.dart';

class QuestionsModel {
  final String personalityTestQuestionNo;
  final String imageContentUuid;
  final String contentNo;
  final String reviewNo;
  final ImageSourceModel imageSrc;
  final String question;
  final String description;
  final String resourceUrl;
  final String userNo;
  final double lat;
  final double lng;
  final String state;
  final bool isFollow;
  final bool isZzim;
  final bool secret;

  final String formattedAddress;
  final String formattedPhoneNumber;

  final List<CategoryModel> categories;
  final UserModel userInfo;
  final QuestionsModel contentInfo;
  final String providerName;
  final String authorName;

  QuestionsModel(
      {this.personalityTestQuestionNo,
      this.imageContentUuid,
      this.contentNo,
      this.reviewNo,
      this.imageSrc,
      this.question,
      this.description,
      this.resourceUrl,
      this.userNo,
      this.lat,
      this.lng,
      this.state,
      this.secret,
      this.isFollow,
      this.isZzim,
      this.formattedAddress,
      this.formattedPhoneNumber,
      this.categories,
      this.userInfo,
      this.contentInfo,
      this.providerName,
      this.authorName});

  factory QuestionsModel.fromJson(Map<String, dynamic> json) {
    List<CategoryModel> a =
        json.containsKey('categories') && json['categories'] != null
            ? (json['categories'] as List)
                .map((value) => CategoryModel.fromJson(value))
                .toList()
            : [];

    return QuestionsModel(
      personalityTestQuestionNo:
          json['personality_test_question_no'].toString(),
      imageContentUuid: json['image_content_uuid'],
      contentNo: json['content_no'].toString(),
      reviewNo: json['review_no'].toString(),
      imageSrc: json.containsKey('image_src') && json['image_src'] != null
          ? ImageSourceModel.fromJson(json['image_src'])
          : null,
      question: json['question'],
      description: json['description'],
      resourceUrl: json['resource_url'],
      userNo: json['user_no'].toString(),
      lat: json['lat'],
      lng: json['lng'],
      state: json['state'],
      isFollow: json['is_follow'] == 1,
      isZzim: json['is_zzim'],
      secret: json['secret'],
      formattedAddress: json['formatted_address'].toString(),
      formattedPhoneNumber: json['formatted_phone_number'].toString(),
      categories: a,
      userInfo: json.containsKey('user_info') && json['user_info'] != null
          ? UserModel.fromJson(json['user_info'])
          : null,
      contentInfo:
          json.containsKey('content_info') && json['content_info'] != null
              ? QuestionsModel.fromJson(json['content_info'])
              : null,
      providerName: json['provider_name'],
      authorName: json['author_name'],
    );
  }
}
