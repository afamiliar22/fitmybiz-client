import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';
import 'package:fitmybiz/tools/network_client.dart';

import 'reset_password_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class ResetPasswordCubit extends BaseCubit<ResetPasswordState> {
  /// {@macro counter_cubit}
  ResetPasswordCubit() : super(InitialState());

  String email = '';
  String passwordToken = '';
  String password = '';
  String password2 = '';

  void init() {
    emit(InitialState());
  }

  void emailChanged(String email) {
    this.email = email;
  }

  void passwordTokenChanged(String passwordToken) {
    this.passwordToken = passwordToken;
  }

  void passwordChanged(String password) {
    this.password = password;
  }

  void password2Changed(String password2) {
    this.password2 = password2;
  }

  void resetPassword() {
    if (this.password.isEmpty) {
      emit(ToastState('비밀번호를 입력해주세요'));
      emit(ErrorState());
      return;
    }
    if (this.password2.isEmpty) {
      emit(ToastState('비밀번호를 한번 더 입력해주세요'));
      emit(ErrorState());
      return;
    }
    if (this.password != this.password2) {
      emit(ToastState('비밀번호와 비밀번호 확인이 일치하지 않습니다.'));
      emit(ErrorState());
      return;
    }
    String method = 'post';
    String path = '/api/v1/user/password/reset';
    Map data = {
      "email": this.email,
      'password': this.password,
      'password_token': this.passwordToken
    };
    emit(LoadingState());
    this.network.requestWithoutAuth(method, path, data).then((response) {
      emit(ToastState('비밀번호 재설정이 완료되었습니다.'));
      emit(CompletedState());
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
