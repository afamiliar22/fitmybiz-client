import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_models/notice.dart';
import 'package:fitmybiz/temp_routes/panel_info_job.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:intl/intl.dart';

class NoticePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  bool _value = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              this.navigator.popRoute(null);
            },
          ),
          title: Text('공지사항'),
          body: CustomContainer(
            child: ListView.builder(
              itemBuilder: (context, index) {
                return buildItem(index);
              },
            ),
          )),
    );
  }

  Widget buildItem(int index) {
    var data = NoticeModel(
        createdAt: 1610699148192,
        title: "서버점검 예정입니다.",
        startDate: "22일 03시",
        endDate: "22일 05시",
        subtitle: ("""
                      개인정보수집 및 이용동의 약관
“안전결제 프리미엄 서비스(이하, ‘서비스’라 합니다.)”의 서비스 제공과 관련하여 “브이피 주식회사(이하, ‘회사’라 합니다.)”가 본인으로부터 취득한 개인정보는 통신비밀보호법, 전기통신사업법, 정보통신망 이용촉진 및 정보보호 등에 관한 법률 등 정보통신서비스제공자가 준수하여야 할 관련 법령상의 개인정보보호 규정에 따라 제3자에 제공하거나 영업목적으로 이용 시 동의를 받아야 하는 정보입니다. 이에 본인은, 귀사가 본인으로부터 취득한 개인정보를 아래와 같은 목적으로 이용하거나 제휴업체에게 제공하는데 동의합니다. 회사는 본 서비스를 가입자에게 제공하기 위해 다음과 같이 개인정보를 수집/이용합니다.

1. 개인정보수집 및 이용
① 수집항목
- 휴대폰정보, 단말기 모델명, 서비스 가입/해지일자, 서비스 가입 및 실행이력, 카드번호, 카드 유효기간
② 이용목적
- 서비스 제공에 관한 계약 이행 및 서비스 제"""));
    return CustomContainer(
      child: ExpansionTile(
        title: Text(DateFormat('yyyy.MM.dd')
            .format(DateTime.fromMillisecondsSinceEpoch(data.createdAt))),
        subtitle: Text('${data.startDate} ~ ${data.endDate} ${data.title}'),
        children: [
          CustomContainer(
            child: Text(data.subtitle),
            backgroundColor: ColorTheme.grayBg,
          )
        ],
      ),
    );
  }
}
