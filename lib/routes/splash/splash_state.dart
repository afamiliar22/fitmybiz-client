import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/category_model.dart';

abstract class SplashState extends Equatable {}

class InitialState extends SplashState {
  @override
  List<Object> get props => [];
}

class AuthState extends SplashState {
  AuthState(this.result);

  final bool result;

  @override
  List<Object> get props => [result];
}

class LoadingState extends SplashState {
  @override
  List<Object> get props => [];
}

class ErrorState extends SplashState {
  @override
  List<Object> get props => [];
}

class GetCategory extends SplashState {
  GetCategory(this.result, this.categories);

  final bool result;
  final List<CategoryModel> categories;

  @override
  List<Object> get props => [result, categories];
}
