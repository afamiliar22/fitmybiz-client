import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/contents_model.dart';

abstract class SearchState extends Equatable {}

class InitialState extends SearchState {
  @override
  List<Object> get props => [];
}

class LoadingState extends SearchState {
  @override
  List<Object> get props => [];
}

class ErrorState extends SearchState {
  @override
  List<Object> get props => [];
}

class GetBanner extends SearchState {
  GetBanner(this.result, this.banners);

  final bool result;
  final List<BannerModel> banners;

  @override
  List<Object> get props => [result, banners];
}

class GetContents extends SearchState {
  GetContents(this.result, this.contents);

  final bool result;
  final List<ContentsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class RefreshRadius extends SearchState {
  RefreshRadius(this.radius);

  final double radius;

  @override
  List<Object> get props => [radius];
}
