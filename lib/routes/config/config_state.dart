import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';

abstract class ConfigState extends Equatable {}

class InitialState extends ConfigState {
  @override
  List<Object> get props => [];
}

class LoadingState extends ConfigState {
  @override
  List<Object> get props => [];
}

class ErrorState extends ConfigState {
  @override
  List<Object> get props => [];
}

class CompleteDeleteUserState extends ConfigState {
  @override
  List<Object> get props => [];
}

class ToastState extends ConfigState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class GetCategory extends ConfigState {
  GetCategory(this.result, this.categories);

  final bool result;
  final List<CategoryModel> categories;

  @override
  List<Object> get props => [result, categories];
}
