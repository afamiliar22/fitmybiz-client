import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/link_data_model.dart';

abstract class UpdatePlaceState extends Equatable {}

class InitialState extends UpdatePlaceState {
  @override
  List<Object> get props => [];
}

class SetState extends UpdatePlaceState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadedState extends UpdatePlaceState {
  @override
  List<Object> get props => [];
}

class ToastState extends UpdatePlaceState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class CompletedState extends UpdatePlaceState {
  @override
  List<Object> get props => [];
}

class FailedLoadState extends UpdatePlaceState {
  @override
  List<Object> get props => [];
}

class LoadingState extends UpdatePlaceState {
  @override
  List<Object> get props => [];
}

class LoadedLinkImageState extends UpdatePlaceState {
  LoadedLinkImageState(this.linkData);

  final LinkDataModel linkData;

  @override
  List<Object> get props => [linkData];
}

class ErrorState extends UpdatePlaceState {
  @override
  List<Object> get props => [];
}
