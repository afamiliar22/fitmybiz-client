import 'dart:io';

import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_search_bar.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/liking/liking_page.dart';
import 'package:fitmybiz/routes/me/me_cubit.dart';
import 'package:fitmybiz/routes/taste_temp/taste_cubit.dart';
import 'package:fitmybiz/routes/views/place_range_sheet.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'contentsView/taste_content_view.dart';
import 'taste_state.dart';
//
import 'dart:async';

class TastePage extends StatelessWidget {
  final List<CategoryModel> categories;
  final bool isReset;
  final String userNo;
  TastePage(this.categories, this.isReset, this.userNo);
  // const SearchPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => TasteCubit(),
        ),
        BlocProvider(
          create: (BuildContext context) => MeCubit(),
        ),
      ],
      child: _View(this.categories, this.isReset, this.userNo),
    );
  }
}

class _View extends BaseRoute {
  final List<CategoryModel> categories;
  final bool isReset;
  final String userNo;
  _View(this.categories, this.isReset, this.userNo);
  @override
  _ViewState createState() =>
      _ViewState(this.categories, this.isReset, this.userNo);
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  List<CategoryModel> categories = List<CategoryModel>();
  bool isReset;
  double radius;
  String userNo;

  List<CollectionModel> collections;
  _ViewState(this.categories, this.isReset, this.userNo);
  final TextEditingController textEditingController = TextEditingController();
  final searchDebouncer = Debouncer(milliseconds: 500);
  bool isFocus = false;
  String searchText;

  List<ContentsModel> contents = List<ContentsModel>();
  List<BannerModel> banners = List<BannerModel>();
  int tabIndex = 0;
  TabController tabController;

  bool isSearchBar = true;

  final pageIndexNotifier = ValueNotifier<int>(0);

  List<Widget> pages = [];

  //
  //MeCubit cubit;
  TasteCubit cubit;
  var zzimItems = <ContentsModel>[];

  UserModel user;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.tabController = TabController(length: pages.length + 1, vsync: this);
    this.tabController.addListener(() {
      setState(() {
        this.tabIndex = this.tabController.index;
      });
    });
    //
    //this.cubit = context.bloc<MeCubit>();
    this.cubit = context.bloc<TasteCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    context.bloc<TasteCubit>().getBanner();
    this.cubit.getCollection(this.userNo);
    this.cubit.selectUser();
    //this.cubit.init();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<TasteCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();

            break;
          case SelectUser:
            this.user = state.user;
            setState(() {});
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case PostCollectionSet:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case GetCollection:
            this.collections = state.collections;
            print('collectons: ${this.collections}');
            setState(() {});
            break;
          case GetBanner:
            this.indicator.hide();
            this.banners = state.banners;
            if (pages != null)
              pages.add(TasteContentView(
                  null, TasteContentType.all, this.isReset, onZzim: (value) {
                ContentsModel contents = value[0];
                zzimItems = value[1];
                if (zzimItems.contains(contents)) {
                  context
                      .bloc<TasteCubit>()
                      .postCollectionSet(contents.imageContentNo, '');
                } else {
                  if (this.collections != null) {
                    context.bloc<TasteCubit>().postCollectionSet(
                        contents.imageContentNo,
                        '${this.collections.first.collectionNo}');
                    return;
                    print(
                        '${contents.imageContentNo}, ${this.collections.first.collectionNo}');
                  }
                }
                setState(() {});
              }));

            setState(() {});
            break;
        }
      },
      child: PlatformScaffold(
        leading: CustomContainer(
          onPressed: () {
            this.navigator.popRoute(null);
          },
          margin: EdgeInsets.only(left: 20),
          width: 28,
          height: 28,
          child: Image.asset('assets/images/icnsBack.png'),
        ),
        title: Text.rich(
          TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: "${this.zzimItems?.length ?? 0}",
                style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    color: ColorTheme.black),
              ),
              TextSpan(
                  text: " / 5",
                  style: TextStyle(
                      fontSize: 26, color: ColorTheme.grayPlaceholder)),
            ],
          ),
        ),
        body: SafeArea(
          child: NestedScrollView(
            headerSliverBuilder: (context, innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  backgroundColor: Colors.white,
                  //title: searchBar(),
                  // floating 설정. SliverAppBar는 스크롤 다운되면 화면 위로 사라짐.
                  // true: 스크롤 업 하면 앱바가 바로 나타남. false: 리스트 최 상단에서 스크롤 업 할 때에만 앱바가 나타남
                  floating: false,

                  pinned: false,
                  bottom: PreferredSize(
                      child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 25),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Spacer(),
                                  Image.asset(
                                    'assets/images/elementButtonsBookmark.png',
                                    width: 112,
                                    height: 113,
                                    fit: BoxFit.cover,
                                  )
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: "${this.user?.nickname ?? ''}",
                                          style: TextStyle(
                                              fontSize: 26,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        TextSpan(
                                            text: " 님의\n취향에 딱 맞는 곳들을\n찜! 해보세요",
                                            style: TextStyle(fontSize: 26)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )),
                      preferredSize: Size.fromHeight(220 - kToolbarHeight)),
                ),
                /*
                SliverPersistentHeader(
                  delegate: _SliverAppBarDelegate(this.getHorizontalMenu()),
                  pinned: true,
                ),
                */
              ];
            },
            body: this.tabBarView(),
          ),
        ),
      ),
    );
  }

  Widget tab(String title, int index) {
    return true
        ? Tab(
            child: Container(
              child: Text(
                title,
                style: this.tabIndex == index
                    ? TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold)
                    : TextStyle(color: Colors.grey[400], fontSize: 16),
              ),
            ),
          )
        : Material(
            color: Colors.transparent,
            child: Tab(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /*
            Container(
              width: 6,
              height: 6,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: this.tabIndex == index
                    ? ColorTheme.primary
                    : Colors.transparent,
              ),
              margin: EdgeInsets.only(
                  top: this.tabIndex == index ? 4 : 10, left: 5.5),
            ),
            */
                  if (Platform.isIOS) SizedBox(height: 4),
                  Container(
                    child: Text(
                      title,
                      style: this.tabIndex == index
                          ? TextStyle(
                              color: true ? Colors.white : Colors.black,
                              fontSize: true ? 16 : 22,
                              fontWeight: FontWeight.bold)
                          : TextStyle(color: Colors.grey[400], fontSize: 15),
                    ),
                  )
                ],
              ),
            ),
          );
  }

  tabBarView() {
    return Stack(
      children: [
        TabBarView(
          controller: this.tabController,
          children: pages.isEmpty ? [Container()] : pages,
        ),
        /*
              Column(
                children: [
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: FutureBuilder(
                            future: SharedPreferences.getInstance(),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                SharedPreferences prefs = snapshot.data;
                                radius = prefs.getDouble('radius') ?? 0.0;
                                return FloatingActionButton.extended(
                                    heroTag: null,
                                    icon: Icon(Icons.arrow_drop_down),
                                    backgroundColor: ColorTheme.black,
                                    onPressed: () {
                                      showModalBottomSheet(
                                          backgroundColor: Colors.transparent,
                                          context: context,
                                          builder: (context) {
                                            return PlaceRangeSheet(
                                              onPressed: (value) {
                                                radius = value;
                                                this
                                                    .searchCubit
                                                    .refreshRadius(radius);
                                                Navigator.of(context).pop();
                                              },
                                            );
                                          });
                                    },
                                    label: Text(radius == 0.0
                                        ? '전체'
                                        : '내 주변 $radius KM'));
                              } else {
                                return FloatingActionButton.extended(
                                    heroTag: null,
                                    icon: Icon(Icons.arrow_drop_down),
                                    backgroundColor: ColorTheme.black,
                                    onPressed: () {
                                      showModalBottomSheet(
                                          backgroundColor: Colors.transparent,
                                          context: context,
                                          builder: (context) {
                                            return PlaceRangeSheet(
                                              onPressed: (value) {
                                                Navigator.of(context).pop();
                                              },
                                            );
                                          });
                                    },
                                    label: Text('내 주변 5 KM'));
                              }
                            }),
                      )
                    ],
                  )
                ],
              )
              */
      ],
    );
  }

  PageViewIndicator _buildIndicator() {
    return PageViewIndicator(
      pageIndexNotifier: pageIndexNotifier,
      length: 3,
      normalBuilder: (animationController, index) => Circle(
        size: 8.0,
        color: Colors.black87,
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController,
          curve: Curves.ease,
        ),
        child: Circle(size: 10.0, color: Colors.white),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
