import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/models/collection_item_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:fitmybiz/extensions/extensions.dart';

class CollectionItemCell extends StatelessWidget {
  final CollectionItemModel collectionItem;
  final VoidCallback onPressed;
  final VoidCallback onPressedMore;
  final bool isEditMode;
  final bool isSelect;
  CollectionItemCell(
      {this.collectionItem,
      this.isEditMode,
      this.isSelect,
      this.onPressed,
      this.onPressedMore});

  @override
  Widget build(BuildContext context) {
    if (this.isEditMode == true) {
      Widget check = Container();
      Widget radio = Icon(
        Icons.radio_button_unchecked,
        color: Color.fromRGBO(205, 210, 216, 1),
      );

      Color coverColor = Colors.transparent;
      if (this.isSelect) {
        check = Icon(
          (Icons.check),
          color: Colors.white,
        );
        radio = Icon(
          Icons.radio_button_checked,
          color: ColorTheme.primary2,
        );
        coverColor = ColorTheme.primary.withOpacity(0.3);
      }

      return Stack(
        fit: StackFit.expand,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CustomCachedNetworkImage(
              preloadImage: collectionItem.imageSrc.sm,
              image: collectionItem.imageSrc.sm,
            ),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.black12, Colors.transparent, Colors.black12],
              ),
            ),
          ),
          Column(
            children: [
              Spacer(),
              Container(
                margin: EdgeInsets.only(left: 8, right: 8, bottom: 8),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(right: 2),
                        child: Text(
                          collectionItem.title ?? '',
                          style: TextStyle(color: Colors.white),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          CustomContainer(
            backgroundColor: coverColor,
            borderRadius: [20, 20, 20, 20],
            onPressed: this.onPressed,
          ),
          Positioned.fill(
              //top: 12,
              //left: 12,
              child: true
                  ? Align(
                      alignment: Alignment.center,
                      child: Center(
                        child: check,
                      ),
                    )
                  : radio),
        ],
      );
    } else {
      return Stack(
        fit: StackFit.expand,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CustomCachedNetworkImage(
              preloadImage: this.collectionItem.imageSrc.sm,
              image: this.collectionItem.imageSrc.sm,
            ),
          ),
          GestureDetector(
            onTap: this.onPressed,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.black12, Colors.transparent, Colors.black12],
                ),
              ),
            ),
          ),
          /*
          Column(
            children: [
              Spacer(),
              Container(
                margin: EdgeInsets.only(left: 8, right: 0, bottom: 8),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(right: 2),
                        child: Text(
                          this.collectionItem.title ?? '',
                          style: TextStyle(color: Colors.white),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                    ),
                    CustomButton(
                      image: Image.asset(
                        'assets/images/iconMore.png',
                        height: 20,
                      ),
                      onPressed: this.onPressedMore,
                    )
                  ],
                ),
              )
            ],
          )
          */
        ],
      );
    }
  }
}
