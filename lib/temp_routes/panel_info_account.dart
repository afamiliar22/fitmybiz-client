import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/panel_info_job.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class PanelInfoAccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('계좌번호 변경'),
        body: CustomContainer(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '거주하시는 곳의 동읍면 까지의 주소를 알려주세요',
                style: TextStyle(color: ColorTheme.black, fontSize: 16),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                '주소입력',
                style: TextStyle(color: ColorTheme.grayLv1),
              ),
              SizedBox(
                height: 7,
              ),
              Row(
                children: [
                  Expanded(
                    child: CustomContainer(
                      height: 41,
                      child: TextFormField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.verified_user),
                            hintText: '동읍면을 입력해주세요',
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: ColorTheme.grayLv1),
                                borderRadius: BorderRadius.circular(5))),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  CustomContainer(
                    width: 66,
                    height: 41,
                    backgroundColor: ColorTheme.primary2,
                    child: Center(
                      child: Text(
                        '검색',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    borderRadius: [6, 6, 6, 6],
                  )
                ],
              ),
              SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  Spacer(),
                  CustomContainer(
                    margin: EdgeInsets.all(10),
                    width: 285,
                    height: 40,
                    onPressed: () {
                      this.navigator.pushRoute(PanelInfoJobPage());
                    },
                    backgroundColor: ColorTheme.primary2,
                    borderRadius: [6, 6, 6, 6],
                    child: Center(
                      child: Text(
                        '다음',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
