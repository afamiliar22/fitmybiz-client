import 'dart:developer';

class TermModel {
  final int termNo;
  final String termTitle;
  final String urlSrc;
  final bool necessary;

  TermModel({this.termNo, this.termTitle, this.urlSrc, this.necessary});

  factory TermModel.fromJson(Map<String, dynamic> json) {
    return TermModel(
      termNo: json['term_no'],
      termTitle: json['term_title'],
      urlSrc: json['url_src'],
      necessary: json['necessary'],
    );
  }
}
