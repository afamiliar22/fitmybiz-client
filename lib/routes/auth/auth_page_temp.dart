import 'dart:io';

import 'package:fitmybiz/temp_routes/tabs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/auth/auth_cubit.dart';
import 'package:fitmybiz/routes/auth/auth_page.dart';
import 'package:fitmybiz/routes/auth/auth_state.dart';
import 'package:fitmybiz/routes/auth/sign_email/sign_email_page_temp.dart';
import 'package:fitmybiz/routes/splash/splash_cubit.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/auth/term/term_page.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'login_email/login_email_page.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AuthCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return BlocListener(
      cubit: context.bloc<AuthCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case SignedState:
            this.indicator.hide();
            context.bloc<AuthCubit>().checkUserTerm();
            this.navigator.resetRoute(TabsPage());
            break;
          case CheckedTermState:
            this.indicator.hide();
            if (state.result == true) {
              this.property.isSignIn = true;
              this.navigator.resetRoute(SplashPage());
            } else {
              this.navigator.pushRoute(TermPage(
                    state.terms,
                  ));
            }

            break;
        }
      },
      child: PlatformScaffold(
          onWillPop: this.onWillPop,
          body: SafeArea(
            top: true,
            child: Container(
              //margin: EdgeInsets.symmetric(horizontal: 20),
              child: ListView(
                //mainAxisSize: MainAxisSize.min,
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: kToolbarHeight,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Image.asset(
                      'assets/illusts/illustsBgLogin.png',
                      width: 107,
                      height: 55,
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  /*
                  Center(
                    child: Text.rich(
                      TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: "여기저기 흩어져있는 핫플들\n",
                            style: TextStyle(fontSize: 28),
                          ),
                          TextSpan(
                              text: "모두 모아 바로 여기서,",
                              style: TextStyle(fontSize: 28)),
                          TextSpan(
                              text: " 찜!",
                              style: TextStyle(
                                  fontSize: 28,
                                  color: ColorTheme.primary2,
                                  fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                  ),
                  */
                  SizedBox(height: 45),
                  CustomContainer(
                    width: width,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      decoration: InputDecoration(
                          hintText: '아이디를 입력해주세요',
                          border: OutlineInputBorder(),
                          prefixIcon: SizedBox(
                            width: 17,
                            height: 12,
                            child: Image.asset(
                              'assets/images/icnsEmail.png',
                            ),
                          )),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomContainer(
                    width: width,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      decoration: InputDecoration(
                          hintText: '비밀번호를 입력해주세요',
                          border: OutlineInputBorder(),
                          prefixIcon: SizedBox(
                            width: 17,
                            height: 12,
                            child: Image.asset(
                              'assets/images/icnsLock.png',
                            ),
                          )),
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Divider(
                          color: ColorTheme.grayDisable,
                          thickness: 1.0,
                        ),
                      ),
                      Container(
                        width: 200 * MediaQuery.of(context).size.width / 375,
                        height: 57 * MediaQuery.of(context).size.height / 568,
                        child: Image.asset('assets/images/elementGroup.png'),
                      ),
                      Expanded(
                        child: Divider(
                          color: ColorTheme.grayDisable,
                          thickness: 1.0,
                        ),
                      ),
                    ],
                  ),

                  //SizedBox(height: 11),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomContainer(
                        margin: EdgeInsets.symmetric(horizontal: 40),
                        backgroundColor: Color.fromRGBO(254, 207, 54, 1.0),
                        width: width,
                        height: 40,
                        borderRadius: [6, 6, 6, 6],
                        padding: EdgeInsets.all(11),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/icnsKakao.png',
                              width: 24,
                              height: 24,
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            Text('카카오로 로그인')
                          ],
                        ),
                        onPressed: () {
                          context.bloc<AuthCubit>().signInKakao();
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CustomContainer(
                        margin: EdgeInsets.symmetric(horizontal: 40),
                        backgroundColor: Color(0xff1eb900),
                        width: width,
                        height: 40,
                        borderRadius: [6, 6, 6, 6],
                        padding: EdgeInsets.all(11),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/icnsKakao.png',
                              width: 24,
                              height: 24,
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            Text(
                              '네이버로 로그인',
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          context.bloc<AuthCubit>().signInKakao();
                        },
                      ),
                      Platform.isAndroid
                          ? CustomContainer(
                              backgroundColor: Colors.black,
                              width: 60,
                              height: 60,
                              borderRadius: [30, 30, 30, 30],
                              padding: EdgeInsets.all(18),
                              child: Image.asset(
                                'assets/images/icnsApple.png',
                                width: 24,
                                height: 24,
                              ),
                              onPressed: () {
                                context.bloc<AuthCubit>().signInApple();
                              },
                            )
                          : CustomContainer(
                              borderColor: ColorTheme.grayBg,
                              backgroundColor: Colors.white,
                              width: 60,
                              height: 60,
                              borderRadius: [30, 30, 30, 30],
                              padding: EdgeInsets.all(18),
                              child: FlutterLogo(),
                              onPressed: () {
                                context.bloc<AuthCubit>().signInGoogle();
                              },
                            ),
                      if (Platform.isIOS)
                        SizedBox(
                          height: 20,
                        ),
                      CustomContainer(
                        backgroundColor: ColorTheme.primary2,
                        width: 60,
                        height: 60,
                        borderRadius: [30, 30, 30, 30],
                        padding: EdgeInsets.all(18),
                        child: Image.asset(
                          'assets/images/icnsEmail.png',
                          width: 24,
                          height: 24,
                        ),
                        onPressed: () {
                          this.navigator.pushRoute(SignEmailPage());
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      OutlineButton(
                        padding: EdgeInsets.fromLTRB(14, 9, 14, 9),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        borderSide: BorderSide(color: ColorTheme.grayLv2),
                        child: Text('이미 찜 계정이 있으신가요?',
                            textAlign: TextAlign.end,
                            style: TextStyleTheme.common.copyWith(
                                color: ColorTheme.grayLv2, fontSize: 14)),
                        onPressed: () =>
                            this.navigator.pushRoute(LoginEmailPage()),
                      )
                    ],
                  ),
                  SizedBox(height: 8),
                ],
              ),
            ),
          )),
    );
  }
}
