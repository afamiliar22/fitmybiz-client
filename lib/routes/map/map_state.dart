import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/map_model.dart';
import 'package:fitmybiz/models/user_model.dart';

abstract class MapState extends Equatable {}

class InitialState extends MapState {
  @override
  List<Object> get props => [];
}

class LoadingState extends MapState {
  @override
  List<Object> get props => [];
}

class SetState extends MapState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class ErrorState extends MapState {
  @override
  List<Object> get props => [];
}

class GetCollectionSaved extends MapState {
  GetCollectionSaved(this.result, this.collections);

  final bool result;
  final List<CollectionModel> collections;

  @override
  List<Object> get props => [result, collections];
}

class GetCollection extends MapState {
  GetCollection(this.result, this.collections);

  final bool result;
  final List<CollectionModel> collections;

  @override
  List<Object> get props => [result, collections];
}

class GetUser extends MapState {
  GetUser(this.result);

  final UserModel result;

  @override
  List<Object> get props => [result];
}

class GetCollectionMap extends MapState {
  GetCollectionMap(this.result, this.maps);

  final bool result;
  final List<MapModel> maps;

  @override
  List<Object> get props => [result, maps];
}

class GetUserMap extends MapState {
  GetUserMap(this.result, this.maps);

  final bool result;
  final List<MapModel> maps;

  @override
  List<Object> get props => [result, maps];
}

class GetImageContents extends MapState {
  GetImageContents(this.result, this.contents);

  final bool result;
  final List<ContentsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class GetCategory extends MapState {
  GetCategory(this.result, this.categories);

  final bool result;
  final List<CategoryModel> categories;

  @override
  List<Object> get props => [result, categories];
}
