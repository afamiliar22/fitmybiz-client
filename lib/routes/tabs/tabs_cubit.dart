import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';

import 'tabs_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class TabsCubit extends BaseCubit<TabsState> {
  /// {@macro counter_cubit}
  TabsCubit() : super(InitialState());

  void tokenChanged(String token) {
    print('token:' + token);
    String method = 'post';
    String path = '/api/v1/user/device/token';
    Map<String, dynamic> data = {"device_token": token, "machine_type": 2};
    this.network.requestWithAuth(method, path, data).then((value) {
      print('success');
    }).catchError((e) {
      print('failure');
      print(e);
    });
  }
}
