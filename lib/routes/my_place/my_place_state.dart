import 'package:equatable/equatable.dart';

abstract class MyPlaceState extends Equatable {}

class InitialState extends MyPlaceState {
  @override
  List<Object> get props => [];
}

class LoadingState extends MyPlaceState {
  @override
  List<Object> get props => [];
}

class SetState extends MyPlaceState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadedState extends MyPlaceState {
  @override
  List<Object> get props => [];
}

class ToastState extends MyPlaceState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends MyPlaceState {
  @override
  List<Object> get props => [];
}
