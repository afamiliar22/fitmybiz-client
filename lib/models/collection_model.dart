import 'dart:developer';

import 'package:intl/intl.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'image_source_model.dart';

class CollectionModel {
  final int collectionNo;
  final String collectionId;
  final int userNo;
  final int itemCount;
  final int memberCount;
  final String name;
  final bool secret;
  final bool isHome;
  final String state;
  final bool isSaved;
  final ImageSourceModel imageSrc;
  final List<ImageSourceModel> collectionItems;
  final DateTime updatedAt;

  CollectionModel({
    this.collectionNo,
    this.collectionId,
    this.userNo,
    this.itemCount,
    this.memberCount,
    this.name,
    this.secret,
    this.isHome,
    this.state,
    this.isSaved,
    this.imageSrc,
    this.collectionItems,
    this.updatedAt,
  });

  factory CollectionModel.fromJson(Map<String, dynamic> json) {
    List<ImageSourceModel> collectionItems =
        json.containsKey('collection_items')
            ? (json['collection_items'] as List)
                .map((value) => ImageSourceModel.fromJson(value['image_src']))
                .toList()
            : [];
    return CollectionModel(
      collectionNo: json['collection_no'],
      collectionId: json['collection_id'],
      userNo: json['user_no'],
      itemCount: json['item_count'],
      memberCount: json['member_count'],
      name: json['name'],
      secret: json['secret'] == 1,
      isHome: json['is_home'] == 1,
      state: json['state'].toString(),
      isSaved: json['is_saved'] == 1,
      imageSrc: json.containsKey('image_src') && json['image_src'] != null
          ? ImageSourceModel.fromJson(json['image_src'])
          : null,
      collectionItems: collectionItems,
      updatedAt: json.containsKey('updated_at')
          ? DateFormat("yyyy-MM-dd HH:mm:ss").parse(json['updated_at'], true)
          : null,
    );
  }
}
