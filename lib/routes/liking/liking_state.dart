import 'package:equatable/equatable.dart';

abstract class LikingState extends Equatable {}

class InitialState extends LikingState {
  @override
  List<Object> get props => [];
}

class LoadingState extends LikingState {
  @override
  List<Object> get props => [];
}

class ErrorState extends LikingState {
  @override
  List<Object> get props => [];
}

class SetState extends LikingState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}
