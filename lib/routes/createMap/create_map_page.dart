import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_text_field2.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/createMap/create_map_cubit.dart';
import 'package:fitmybiz/routes/createMap/create_map_state.dart';

import 'package:fitmybiz/themes/themes.dart';

class CreateMapPage extends StatelessWidget {
  // const PlaceDetailPage({Key key}) : super(key: key);

  CreateMapPage();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CreateMapCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  _View();
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  _ViewState();

  TextEditingController controller;
  String collectionNo = '';
  List<bool> arrIsSelected = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = TextEditingController();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<CreateMapCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();

            break;

          case ToastState:
            this.showToast(state.message);
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;

          case PostCollection:
            this.indicator.hide();
            this.navigator.popRoute('refresh');

            break;
        }
      },
      child: PlatformScaffold(
          title: Text('지도 만들기'),
          leading: IconButton(
            padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
            icon: Image.asset('assets/images/icnsBack.png',
                color: Colors.black, width: 28, height: 28),
            onPressed: () {
              this.navigator.popRoute(null);
            },
          ),
          trailing: true
              ? CustomContainer(
                  padding:
                      EdgeInsets.only(left: 0, top: 8, right: 24, bottom: 8),
                  child: Text(
                    '완료',
                    style: TextStyle(
                        color: this.controller.text.isEmpty
                            ? ColorTheme.grayDisable
                            : ColorTheme.black),
                  ),
                  onPressed: () => context
                      .bloc<CreateMapCubit>()
                      .postCollection(controller.text, true),
                )
              : FlatButton(
                  padding:
                      EdgeInsets.only(left: 0, top: 8, right: 12, bottom: 8),
                  child: Text(
                    '완료',
                    style: TextStyle(
                        color: this.controller.text.isEmpty
                            ? ColorTheme.grayDisable
                            : ColorTheme.black),
                  ),
                  onPressed: () => context
                      .bloc<CreateMapCubit>()
                      .postCollection(controller.text, true),
                ),
          body: SafeArea(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Text(
                  "지도 만들기",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
              */
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Text(
                  "지도 제목",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(138, 148, 159, 1)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                child: CustomTextField2(
                  controller: this.controller,
                  placeholder: "지도 제목을 입력 해 주세요.",
                  placeholderStyle: TextStyleTheme.placeholder,
                  textStyle: TextStyle(fontSize: 14),
                ),
              ),
              /*
              Spacer(),
              Container(
                margin: EdgeInsets.all(20),
                child: PrimaryButton(
                  text: Text('완료'),
                  onPressed: () => context
                      .bloc<CreateMapCubit>()
                      .postCollection(controller.text, true),
                ),
              )
              */
            ],
          ))),
    );
  }
}
