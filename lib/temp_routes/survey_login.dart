import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_models/survey_item.dart';
import 'package:fitmybiz/temp_routes/survey_intro.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SurveyLoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return PlatformScaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Column(
                  children: [
                    CustomContainer(
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      backgroundColor: ColorTheme.primary2,
                      width: width,
                      height: (width / 359) * 134,
                      borderRadius: [0, 0, 15, 15],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                const TextSpan(
                                  text: '회원가입하고 설문조사 참여하면,\n',
                                  style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 12,
                                      color: Colors.white),
                                ),
                                TextSpan(
                                  text: '총 60,000P를 적립 가능!',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: ColorTheme.white),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 13,
                    ),
                  ],
                ),
                Positioned.fill(
                  child: Column(
                    children: [
                      Spacer(),
                      Row(
                        children: [
                          Spacer(),
                          CustomContainer(
                            borderRadius: [6, 6, 6, 6],
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 8),
                            backgroundColor: ColorTheme.primary3,
                            child: Row(
                              children: [
                                Text(
                                  '회원가입',
                                  style: TextStyle(
                                      color: ColorTheme.white, fontSize: 12),
                                ),
                                Image.asset(
                                  'assets/images/icnsNext.png',
                                  width: 16,
                                  height: 16,
                                  color: ColorTheme.white,
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 27,
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
            CustomContainer(
              child: Column(
                children: [
                  CustomContainer(
                    child: Row(
                      children: [
                        Text(
                          '비회원 참여 가능 설문',
                          style: TextStyle(
                              color: ColorTheme.grayLv1, fontSize: 12),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(22),
                itemBuilder: (context, index) {
                  return buildItem(index);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildItem(index) {
    var data = SurveyItemModel(
        duration: 1,
        point: 100,
        contentType: "law data",
        participantCount: 1234,
        title: "고양이 집사 이용성 테스트",
        subtitle: "고양이를 키우는 사람들이 가장 이상적으로 생각하는 화장실 모양");
    return CustomContainer(
      padding: EdgeInsets.symmetric(vertical: 11),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 9,
          ),
          Text(
            data.title,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            data.subtitle,
            style: TextStyle(color: ColorTheme.grayLv1),
          ),
          SizedBox(
            height: 14,
          ),
          Row(
            children: [
              CustomContainer(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '예상 시간',
                      style: TextStyle(color: ColorTheme.grayLv1),
                    ),
                    Text(
                      '${data.duration} 분',
                      style: TextStyle(
                          color: ColorTheme.primary2,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Spacer(),
              CustomContainer(
                child: Text(
                  '가입시 포인트 지급',
                  style: TextStyle(color: ColorTheme.grayLv1),
                ),
              ),
              Spacer(),
              CustomContainer(
                onPressed: () {
                  this.navigator.pushRoute(SurveyIntroPage());
                },
                borderRadius: [6, 6, 6, 6],
                backgroundColor: ColorTheme.primary,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                child: Text(
                  '참여하기',
                  style: TextStyle(color: ColorTheme.white),
                ),
              )
            ],
          ),
          SizedBox(
            height: 18,
          ),
          Divider(thickness: 1),
        ],
      ),
    );
  }
}
