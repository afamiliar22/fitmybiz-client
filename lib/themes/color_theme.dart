import 'package:flutter/material.dart';

class ColorTheme {
  static Color white = Color.fromRGBO(255, 255, 255, 1.0);
  static Color primary = Color(0xff005bab);
  static Color primary2 = Color(0xffec6c00);
  static Color primary3 = Color(0xfff5ab00);
  static Color textLevel5 = Color.fromRGBO(138, 148, 159, 1.0);
  static Color point = Color.fromRGBO(255, 203, 82, 1.0);
  static Color point2 = Color.fromRGBO(104, 117, 252, 1.0);

  static Color grayLv1 = Color.fromRGBO(138, 148, 159, 1.0);
  static Color grayLv2 = Color.fromRGBO(173, 181, 190, 1.0);
  static Color grayPlaceholder = Color.fromRGBO(205, 210, 216, 1.0);
  static Color grayDisable = Color.fromRGBO(228, 230, 233, 1.0);
  static Color grayBg = Color.fromRGBO(244, 246, 248, 1.0);

  static Color black = Color.fromRGBO(31, 31, 36, 1.0);
  static Color red = Color.fromRGBO(255, 30, 64, 1.0);

  static Color blackShadow = Color.fromRGBO(0, 0, 0, .7);

  static Color colorActivity = Color.fromRGBO(238, 94, 100, 1);
  static Color colorTrip = Color.fromRGBO(0, 197, 139, 1);
  static Color colorCafe = Color.fromRGBO(251, 201, 90, .9);
  static Color colorRestuarant = Color.fromRGBO(255, 157, 81, 1);
  static Color colorShopping = Color.fromRGBO(74, 169, 225, 1);
  static Color colorReports = Color.fromRGBO(198, 147, 198, 1);
  static Color colorBar = Color.fromRGBO(110, 110, 255, 1);
  static Color colorAccommodation = Color.fromRGBO(172, 121, 101, 1);
  static Color colorEtc = Color.fromRGBO(79, 91, 103, 1);
  static List categoryColors = [
    colorRestuarant,
    colorCafe,
    colorBar,
    colorShopping,
    colorTrip,
    colorActivity,
    colorReports,
    colorAccommodation,
    colorEtc,
  ];
}
