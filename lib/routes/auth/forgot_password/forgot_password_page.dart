import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_text_field2.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/routes/auth/auth_page.dart';
import 'package:fitmybiz/routes/auth/reset_password/reset_password_page.dart';
import 'package:fitmybiz/routes/splash/splash_cubit.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/tabs/tabs_page.dart';
import 'package:fitmybiz/routes/auth/term/term_page.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/indicator_client.dart';
import 'package:fitmybiz/tools/network_client.dart';
import 'package:validators/validators.dart';

import 'forgot_password_cubit.dart';
import 'forgot_password_state.dart';

class ForgotPasswordPage extends StatelessWidget {
  const ForgotPasswordPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => ForgotPasswordCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  ForgotPasswordCubit cubit;
  TextEditingController _emailEditingController = TextEditingController();
  TextEditingController _validEditingController = TextEditingController();
  bool isSent = false;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<ForgotPasswordCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
  }

  @override
  Widget build(BuildContext context) {
    Color verifyCodeButtonColor = Color.fromRGBO(228, 230, 233, 1.0);
    if (isEmail(this.cubit.email)) {
      verifyCodeButtonColor = ColorTheme.primary;
    }
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case CompletedState:
            this.navigator.pushRoute(
                ResetPasswordPage(this.cubit.email, this.cubit.passwordToken));
            break;
          case RequestedState:
            this.indicator.hide();
            setState(() {});
            break;
          case SetState:
            setState(() {});
            break;
          case ToastState:
            if (state.message == '인증번호가 전송되었습니다.') isSent = true;
            setState(() {});
            this.showToast(state.message);
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
        }
      },
      child: PlatformScaffold(
        title: Text('비밀번호 재설정'),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        trailing: CustomContainer(
          margin: EdgeInsets.only(right: 24),
          onPressed: () {},
          child: Text(
            '완료',
            style: TextStyle(
                color: _emailEditingController.text.isNotEmpty ||
                        _validEditingController.text.isNotEmpty
                    ? ColorTheme.primary
                    : ColorTheme.grayDisable),
          ),
        ),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              Text('이메일', style: TextStyleTheme.textFieldLabel),
              SizedBox(height: 8),
              CustomTextField2(
                controller: _emailEditingController,
                placeholder: "이메일을 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                inputType: TextInputType.emailAddress,
                suffix: CupertinoButton(
                  padding: EdgeInsets.all(0),
                  child: isSent
                      ? CustomContainer(
                          onPressed: () {
                            _emailEditingController.clear();
                            isSent = false;
                            setState(() {});
                            this
                                .cubit
                                .emailChanged(_emailEditingController.text);
                          },
                          width: 28,
                          height: 28,
                          margin: EdgeInsets.all(4),
                          child: _emailEditingController.text.isNotEmpty
                              ? Image.asset('assets/images/icnsDelete3.png')
                              : null,
                        )
                      : Text('인증번호 보내기',
                          style: TextStyleTheme.common.copyWith(
                              fontSize: 16,
                              color: verifyCodeButtonColor,
                              fontWeight: FontWeight.w600)),
                  onPressed: () => this.cubit.requestVerifyCode(),
                ),
                onChanged: (text) => this.cubit.emailChanged(text),
              ),
              SizedBox(height: 40),
              Text('인증번호', style: TextStyleTheme.textFieldLabel),
              SizedBox(height: 8),
              CustomTextField2(
                suffix: CupertinoButton(
                  padding: EdgeInsets.all(0),
                  child: isSent
                      ? Text('인증번호 다시 보내기',
                          style: TextStyleTheme.common.copyWith(
                              fontSize: 16,
                              color: ColorTheme.primary,
                              fontWeight: FontWeight.w600))
                      : Container(),
                  onPressed: () => this.cubit.requestVerifyCode(),
                ),
                controller: _validEditingController,
                placeholder: "인증번호를 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                onChanged: (text) => this.cubit.verifyCodeChanged(text),
              ),
              SizedBox(height: 90),
              /*
              PrimaryButton(
                text: Text('인증 완료'),
                onPressed: this.cubit.verifyCode.isEmpty ? null : this.cubit.completeVerifyCode,
              ),
              */
            ],
          ),
        ),
      ),
    );
  }
}
