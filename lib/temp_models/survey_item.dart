class SurveyItemModel {
  String contentType;
  int participantCount;
  String title;
  String subtitle;
  int duration;
  int point;

  SurveyItemModel(
      {this.contentType,
      this.participantCount,
      this.title,
      this.subtitle,
      this.duration,
      this.point});

  SurveyItemModel.fromJson(Map<String, dynamic> json) {
    contentType = json['content_type'];
    participantCount = json['participant_count'];
    title = json['title'];
    subtitle = json['subtitle'];
    duration = json['duration'];
    point = json['point'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content_type'] = this.contentType;
    data['participant_count'] = this.participantCount;
    data['title'] = this.title;
    data['subtitle'] = this.subtitle;
    data['duration'] = this.duration;
    data['point'] = this.point;
    return data;
  }
}
