import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_search_bar.dart';
import 'package:fitmybiz/components/custom_smart_refresher.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/liking/likingSearchUser/liking_search_user_page.dart';
import 'package:fitmybiz/routes/liking/zzimWithUser/liking_zzim_page.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'liking_cubit.dart';
import 'liking_state.dart';

class LikingPage extends StatelessWidget {
  const LikingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LikingCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  final TextEditingController textEditingController = TextEditingController();
  final searchDebouncer = Debouncer(milliseconds: 500);
  bool isFocus = false;
  String searchText;
  LikingCubit cubit;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.cubit = context.bloc<LikingCubit>();
    // this.searchDebouncer.run(() => searchPlace());
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
    cubit.setUserNo(this.property.user.userNo);
    this.textEditingController.text = "";
    // this.cubit.getSearchFollowing();
    this.cubit.onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        cubit: this.cubit,
        listener: (context, state) {
          switch (state.runtimeType) {
            case InitialState:
              this.indicator.hide();

              break;

            case ErrorState:
              this.indicator.hide();
              break;
            case LoadingState:
              this.indicator.show();
              break;
            case SetState:
              this.indicator.hide();
              setState(() {});
              break;
          }
        },
        child: PlatformScaffold(
          onPressed: () {
            FocusManager.instance.primaryFocus.unfocus();
            this.isFocus = false;
            setState(() {});
          },
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                searchBar(),
                gridView(),
              ],
            ),
          ),
        ));
  }

  Widget searchBar() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Row(
        children: [
          CustomButton(
            image: Image.asset(
              'assets/images/icnsClose.png',
              width: 28,
              height: 28,
            ),
            onPressed: () {
              this.navigator.popRoute(null);
            },
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: CustomSearchBar(
              isFocus: isFocus,
              controller: this.textEditingController,
              placeholder: "팔로우 중인 유저 닉네임 검색",
              btnSearch: CustomContainer(
                child: Image.asset('assets/images/icnsClose.png',
                    color: Colors.black, width: 28, height: 28),
                onPressed: () {
                  this.navigator.popRoute(null);
                },
              ),
              onEnter: () {
                setState(() {
                  this.isFocus = true;
                });
              },
              onLeave: () {
                setState(() {
                  this.isFocus = false;
                });
              },
              onSubmitted: (str) {
                this.navigator.pushRoute(LikingSearchUserPage(str));
                setState(() {
                  this.isFocus = false;
                });
              },
              onValueChange: (value) {
                this.searchText = value;

                // if (this.searchText.length > 1) {
                //   context.bloc<LikingCubit>().searchChanged(this.searchText, '20');
                // }
                setState(() {});
              },
              onClear: () {
                setState(() {});
              },
            ),
          )
        ],
      ),
    );
  }

  guideBanner() {
    return Container(
      width: MediaQuery.of(context).size.width - 50,
      height: 60,
      margin: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: ColorTheme.primary),
      child: Center(
        child: Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 20),
          child: Text(
            "가이드배너",
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget emptyView() {
    String title = "";
    String desc = "";

    title = "팔로잉 리스트가 없어요.";
    desc = "나와 비슷한 취향의 유저를 찾아 팔로우해보세요.";

    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 100),
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              desc,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  gridView() {
    return this.cubit.follows == null
        ? Container()
        : this.cubit.follows.length == 0
            ? emptyView()
            : Expanded(
                child: RefreshConfiguration(
                  enableLoadingWhenNoData: false,
                  footerTriggerDistance: 200,
                  child: SmartRefresher(
                    controller: this.cubit.refreshController,
                    enablePullDown: true,
                    enablePullUp: true,
                    header: CustomSmartRefresher.customHeader(),
                    footer: CustomSmartRefresher.customFooter(),
                    onRefresh: this.cubit.onRefresh,
                    onLoading: this.cubit.onLoading,
                    child: GridView.builder(
                      padding:
                          EdgeInsets.symmetric(horizontal: 32, vertical: 20),
                      // shrinkWrap: true,
                      // primary: false,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          childAspectRatio: 0.7,
                          crossAxisSpacing: 30,
                          mainAxisSpacing: 20),
                      itemBuilder: ((BuildContext c, int i) {
                        return CustomContainer(
                          child: Column(
                            children: [
                              Container(
                                width: 84,
                                height: 84,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(42),
                                  child: CustomCachedNetworkImage(
                                    preloadImage: this
                                                .cubit
                                                .follows[i]
                                                .profileImage ==
                                            null
                                        ? ''
                                        : this.cubit.follows[i].profileImage.s,
                                    image: this.cubit.follows[i].profileImage ==
                                            null
                                        ? ''
                                        : this.cubit.follows[i].profileImage.s,
                                  ),
                                ),
                              ),
                              Container(
                                // margin: EdgeInsets.only(top: 4),
                                child: Text(this.cubit.follows[i].nickname,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15)),
                              ),
                            ],
                          ),
                          onPressed: () {
                            this.navigator.pushRoute(LikingZzimPage(
                                this.cubit.follows[i].followingUserNo));
                          },
                        );
                      }),
                      itemCount: this.cubit.follows.length,
                    ),
                  ),
                ),
              );
  }
}
