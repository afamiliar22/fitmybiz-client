import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_models/survey_item.dart';
import 'package:fitmybiz/temp_routes/my_login_change.dart';
import 'package:fitmybiz/temp_routes/notice.dart';
import 'package:fitmybiz/temp_routes/panel_info_account.dart';
import 'package:fitmybiz/temp_routes/point.dart';
import 'package:fitmybiz/temp_routes/settings.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyLoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return PlatformScaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Column(
                  children: [
                    CustomContainer(
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      backgroundColor: ColorTheme.primary3,
                      width: width,
                      height: (width / 359) * 134,
                      borderRadius: [0, 0, 15, 15],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              CustomContainer(
                                borderRadius: [6, 6, 6, 6],
                                child: Image.asset(
                                  'assets/images/icnsMypage.png',
                                  width: 26,
                                  height: 26,
                                ),
                              ),
                              SizedBox(
                                width: 1,
                              ),
                              CustomContainer(
                                borderRadius: [6, 6, 6, 6],
                                child: Text('김수윤님 반갑습니다',
                                    style: TextStyle(
                                      color: ColorTheme.black,
                                    )),
                              ),
                              Spacer(),
                              CustomContainer(
                                onPressed: () {
                                  this.navigator.pushRoute(MyLoginChangePage());
                                },
                                borderRadius: [6, 6, 6, 6],
                                child: Text('내 정보',
                                    style: TextStyle(
                                      color: ColorTheme.white,
                                    )),
                                backgroundColor: ColorTheme.primary,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 13, vertical: 8),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 32,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Text.rich(
                                  TextSpan(
                                    children: <InlineSpan>[
                                      const TextSpan(
                                        text: '누적 포인트,\n',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 12,
                                            color: Colors.white),
                                      ),
                                      TextSpan(
                                        text: '총 60,000P',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: ColorTheme.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              VerticalDivider(
                                thickness: 2,
                                color: Colors.white,
                              ),
                              Expanded(
                                child: Text.rich(
                                  TextSpan(
                                    children: <InlineSpan>[
                                      const TextSpan(
                                        text: '가용 포인트,\n',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 12,
                                            color: Colors.white),
                                      ),
                                      TextSpan(
                                        text: '총 60,000P',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: ColorTheme.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 13,
                          ),
                          CustomContainer(
                            onPressed: () {
                              this.navigator.pushRoute(PointPage());
                            },
                            width: width,
                            child: Center(
                              child: Text(
                                '포인트 상세',
                                style: TextStyle(
                                    color: ColorTheme.primary, fontSize: 12),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 13,
                    ),
                  ],
                ),
              ],
            ),
            CustomContainer(
              child: Column(
                children: [
                  CustomContainer(
                    child: Row(
                      children: [
                        Text(
                          '내가 참여한 설문조사',
                          style: TextStyle(
                              color: ColorTheme.grayLv1, fontSize: 12),
                        ),
                        Spacer(),
                        Text(
                          '전체보기',
                          style: TextStyle(
                              color: ColorTheme.primary,
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            ),
            Flexible(
              child: ListView.builder(
                itemCount: 2,
                padding: EdgeInsets.all(22),
                itemBuilder: (context, index) {
                  return buildItem(index);
                },
              ),
            ),
            CustomContainer(
              onPressed: () {
                this.navigator.pushRoute(NoticePage());
              },
              padding: EdgeInsets.symmetric(horizontal: 34),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(CupertinoIcons.bell),
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: Text('공지사항'),
                      ),
                      Icon(CupertinoIcons.arrow_right),
                    ],
                  ),
                  Divider()
                ],
              ),
            ),
            CustomContainer(
              onPressed: () {
                this.navigator.pushRoute(SettingsPage());
              },
              padding: EdgeInsets.symmetric(horizontal: 34),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(CupertinoIcons.settings),
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: Text('설정'),
                      ),
                      Icon(CupertinoIcons.arrow_right),
                    ],
                  ),
                  Divider()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildItem(index) {
    var data = SurveyItemModel(
        duration: 1,
        point: 100,
        contentType: "law data",
        participantCount: 1234,
        title: "고양이 집사 이용성 테스트",
        subtitle: "고양이를 키우는 사람들이 가장 이상적으로 생각하는 화장실 모양");
    return CustomContainer(
      padding: EdgeInsets.symmetric(vertical: 11),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 9,
          ),
          Text(
            data.title,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            data.subtitle,
            style: TextStyle(color: ColorTheme.grayLv1),
          ),
          SizedBox(
            height: 14,
          ),
          Row(
            children: [
              CustomContainer(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '예상 시간',
                      style: TextStyle(color: ColorTheme.grayLv1),
                    ),
                    Text(
                      '${data.duration} 분',
                      style: TextStyle(
                          color: ColorTheme.primary2,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Spacer(),
              CustomContainer(
                child: Text(
                  '가입시 포인트 지급',
                  style: TextStyle(color: ColorTheme.grayLv1),
                ),
              ),
              Spacer(),
              CustomContainer(
                borderRadius: [6, 6, 6, 6],
                backgroundColor: ColorTheme.primary,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                child: Text(
                  '참여하기',
                  style: TextStyle(color: ColorTheme.white),
                ),
              )
            ],
          ),
          SizedBox(
            height: 18,
          ),
          Divider(thickness: 1),
        ],
      ),
    );
  }
}
