import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TextStyleTheme {
  static TextStyle common = TextStyle();
  static TextStyle textFieldLabel = common.copyWith(
      color: Color.fromRGBO(133, 148, 159, 1.0),
      fontSize: 15,
      fontWeight: FontWeight.w600);
  static TextStyle textFieldValue =
      common.copyWith(color: Color.fromRGBO(31, 31, 36, 1.0), fontSize: 16);
  static TextStyle description =
      common.copyWith(color: Color.fromRGBO(205, 210, 216, 1.0), fontSize: 13);
  static TextStyle placeholder =
      common.copyWith(color: Color.fromRGBO(205, 210, 216, 1.0), fontSize: 16);
  static TextStyle subtitle = common.copyWith(
      color: Color.fromRGBO(31, 31, 36, 1.0),
      fontSize: 18,
      fontWeight: FontWeight.w700);
  static TextStyle txtBtnNextEnable = common.copyWith(
      color: Color.fromRGBO(255, 30, 64, 1.0),
      fontSize: 16,
      fontWeight: FontWeight.w700);
  static TextStyle txtBtnNextDisable = common.copyWith(
      color: Color.fromRGBO(228, 230, 233, 1.0),
      fontSize: 16,
      fontWeight: FontWeight.w700);
}
