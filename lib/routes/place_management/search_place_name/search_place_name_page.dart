import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/google_place_model.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'search_place_name_cubit.dart';
import 'search_place_name_state.dart';

class SearchPlaceNamePage extends StatelessWidget {
  const SearchPlaceNamePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SearchPlaceNameCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  final TextEditingController textEditingController = TextEditingController();
  final ExpandableController expandableController = ExpandableController();
  final FocusNode focusNode = FocusNode();

  List<GooglePlaceModel> places = [];
  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);

    this.expandableController.addListener(() {
      if (this.expandableController.value == false) {
        FocusScope.of(context).unfocus();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<SearchPlaceNameCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case LoadingState:
            break;
          case GetPlace:
            this.places = state.places;
            setState(() {});
            // this.navigator.resetRoute(TabsPage());
            break;
        }
      },
      child: PlatformScaffold(
        title: Text(
          '장소명 검색',
          style: TextStyle(fontSize: 18),
        ),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        /*
        trailing: IconButton(
          padding: EdgeInsets.only(left: 0, top: 8, right: 12, bottom: 8),
          icon: Image.asset('assets/images/icnsClose.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
        ),
        */
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SafeArea(
              bottom: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /*
                  SizedBox(height: 16),
                  CustomContainer(
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    child: Text(
                      '장소명으로 검색하기',
                      style: TextStyleTheme.subtitle,
                    ),
                  ),
                  */
                  SizedBox(height: 20),
                  CustomContainer(
                    borderRadius: [8, 8, 8, 8],
                    shadowColor: Colors.grey.withOpacity(0.1),
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    padding: EdgeInsets.symmetric(horizontal: 14, vertical: 4),
                    backgroundColor: Colors.white,
                    child: Row(
                      children: [
                        Image.asset('assets/images/icnsSearch.png',
                            fit: BoxFit.cover,
                            color: Colors.grey,
                            width: 24,
                            height: 24),
                        SizedBox(width: 8),
                        Expanded(
                          child: CupertinoTextField(
                            focusNode: this.focusNode,
                            controller: this.textEditingController,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(8), // if you need this
                            ),
                            onChanged: (text) => context
                                .bloc<SearchPlaceNameCubit>()
                                .searchChanged(text),
                            placeholder: "장소명, 상호명 등을 입력하세요",
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(0),
                itemBuilder: (context, index) {
                  GooglePlaceModel place = this.places[index];
                  return CupertinoButton(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(place.name,
                              style: TextStyleTheme.common.copyWith(
                                  fontSize: 15,
                                  color: ColorTheme.black,
                                  fontWeight: FontWeight.w600)),
                          SizedBox(height: 4),
                          Text(place.formattedAddress,
                              style: TextStyleTheme.common.copyWith(
                                  fontSize: 13,
                                  color: ColorTheme.grayPlaceholder)),
                        ],
                      ),
                    ),
                    onPressed: () {
                      this.navigator.popRoute(place);
                    },
                  );
                },
                itemCount: this.places.length,
              ),
            ),
            SizedBox(height: 8),
            CustomContainer(
              backgroundColor: ColorTheme.grayBg,
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
              width: double.infinity,
              child: SafeArea(
                top: false,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('찾으시는 장소가 없나요?',
                        style: TextStyleTheme.common.copyWith(
                            fontSize: 15,
                            color: ColorTheme.black,
                            fontWeight: FontWeight.w600)),
                    SizedBox(height: 4),
                    Text('근처 장소를 선택하시고 자유 메모를 활용해보세요.',
                        style: TextStyleTheme.common.copyWith(
                            fontSize: 15,
                            color: ColorTheme.black,
                            fontWeight: FontWeight.w600)),
                    SizedBox(height: 10),
                    Wrap(
                      children: [
                        Text('ex) 용산역 검색',
                            style: TextStyleTheme.common.copyWith(
                                fontSize: 13, color: ColorTheme.grayLv1)),
                        Image.asset('assets/images/icnsNext.png',
                            color: Colors.grey, width: 16, height: 16),
                        Text('선택',
                            style: TextStyleTheme.common.copyWith(
                                fontSize: 13, color: ColorTheme.grayLv1)),
                        Image.asset('assets/images/icnsNext.png',
                            color: Colors.grey, width: 16, height: 16),
                        Text('자유메모 : 7번 출구 직진 100미터',
                            style: TextStyleTheme.common.copyWith(
                                fontSize: 13, color: ColorTheme.grayLv1)),
                      ],
                    ),
                    SizedBox(height: 16),
                    Row(
                      children: [
                        Spacer(),
                        CustomButton(
                          backgroundColor: Color.fromRGBO(254, 211, 74, 1.0),
                          padding:
                              EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                          borderRadius: 30,
                          text: Text('카카오톡 문의',
                              style: TextStyleTheme.common.copyWith(
                                  fontSize: 15,
                                  color: ColorTheme.black,
                                  fontWeight: FontWeight.w600)),
                          onPressed: () {
                            String url = "https://pf.kakao.com/_esUeC";
                            this.launch.moveToBrowser(url);
                          },
                        )
                      ],
                    ),
                    SizedBox(height: 8),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
