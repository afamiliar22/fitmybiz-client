import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';

import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_smart_refresher.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/myMap/my_map_page.dart';
import 'package:fitmybiz/routes/my_place/my_place_page.dart';

import 'package:fitmybiz/routes/notification/notification_cubit.dart';
import 'package:fitmybiz/routes/notification/notification_state.dart';
import 'package:fitmybiz/extensions/extensions.dart';
import 'package:fitmybiz/routes/user/user_page.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NotificationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => NotificationCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  _View();
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  _ViewState();

  String titleText;
  NotificationCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<NotificationCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);

    this.cubit.setUSerNo(this.property.user.userNo);
    // this.cubit.getNotification();
    this.cubit.onRefresh();
    setState(() {});
  }

  List<String> areas = [];
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<NotificationCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text(
          '알림',
          style: TextStyle(
              fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),
        ),
        body: SafeArea(
          child: this.cubit.alarms == null
              ? Container()
              : this.cubit.alarms.length == 0
                  ? emptyView()
                  : Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                      child: RefreshConfiguration(
                        enableLoadingWhenNoData: false,
                        footerTriggerDistance: 200,
                        child: SmartRefresher(
                          controller: this.cubit.refreshController,
                          enablePullDown: true,
                          enablePullUp: true,
                          header: CustomSmartRefresher.customHeader(),
                          footer: CustomSmartRefresher.customFooter(),
                          onRefresh: this.cubit.onRefresh,
                          onLoading: this.cubit.onLoading,
                          child: ListView.builder(
                            itemBuilder: ((BuildContext c, int i) {
                              return CustomContainer(
                                  onPressed: () {
                                    if (this.cubit.alarms[i].alarmDivision ==
                                        '10000') {
                                      this.navigator.pushRoute(UserPage(this
                                          .cubit
                                          .alarms[i]
                                          .sendUserInfo
                                          .userNo));
                                    } else if (this
                                            .cubit
                                            .alarms[i]
                                            .alarmDivision ==
                                        '20000') {
                                      this.navigator.pushRoute(MyPlacePage(
                                            this.cubit.alarms[i].collectionNo,
                                            '새로운 장소',
                                            false,
                                            true,
                                          ));
                                    } else if (this
                                            .cubit
                                            .alarms[i]
                                            .alarmDivision ==
                                        '30000') {
                                      this.navigator.pushRoute(
                                          MyMapPage(this.property.user.userNo));
                                    }
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    // color: Colors.red,
                                    height: 84,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 60,
                                          height: 60,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 12),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(12),
                                            child: this
                                                        .cubit
                                                        .alarms[i]
                                                        .sendUserInfo
                                                        .profileImage ==
                                                    null
                                                ? Container(
                                                    // margin: EdgeInsets.only(top: 8, bottom: 8),
                                                    child: ClipRRect(
                                                      // borderRadius: BorderRadius.circular(22),
                                                      child: Image.asset(
                                                        'assets/images/userProfileImg.png',
                                                        fit: BoxFit.cover,
                                                        height: 60,
                                                        width: 60,
                                                      ),
                                                    ),
                                                  )
                                                : CustomCachedNetworkImage(
                                                    preloadImage: this
                                                        .cubit
                                                        .alarms[i]
                                                        .sendUserInfo
                                                        .profileImage
                                                        .sm,
                                                    image: this
                                                        .cubit
                                                        .alarms[i]
                                                        .sendUserInfo
                                                        .profileImage
                                                        .sm,
                                                  ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              width: this
                                                          .cubit
                                                          .alarms[i]
                                                          .alarmDivision ==
                                                      '10000'
                                                  ? MediaQuery.of(context)
                                                          .size
                                                          .width -
                                                      200
                                                  : MediaQuery.of(context)
                                                          .size
                                                          .width -
                                                      138,
                                              margin: EdgeInsets.only(
                                                  left: 16, right: 8),
                                              child: Text(
                                                  this
                                                      .cubit
                                                      .alarms[i]
                                                      .alarmText,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Color.fromRGBO(
                                                          31, 31, 36, 1))),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 16),
                                              child: Text(
                                                  this
                                                      .cubit
                                                      .alarms[i]
                                                      .createdAt
                                                      .toBeforeString(),
                                                  style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          138, 148, 159, 1))),
                                            )
                                          ],
                                        ),
                                        this.cubit.alarms[i].alarmDivision ==
                                                '10000'
                                            ? this.cubit.selectedFollows[i]
                                                ? Container(
                                                    // margin: EdgeInsets.only(right: 15),
                                                    child: CustomContainer(
                                                    // height: 28,
                                                    backgroundColor:
                                                        Colors.white,
                                                    borderRadius: [
                                                      10,
                                                      10,
                                                      10,
                                                      10
                                                    ],
                                                    borderWidth: 1,
                                                    borderColor: Color.fromRGBO(
                                                        173, 181, 190, 1),
                                                    child: Container(
                                                      margin:
                                                          EdgeInsets.symmetric(
                                                              vertical: 8,
                                                              horizontal: 16),
                                                      child: Text(
                                                        "팔로잉",
                                                        style: TextStyle(
                                                            color:
                                                                Color.fromRGBO(
                                                                    173,
                                                                    181,
                                                                    190,
                                                                    1),
                                                            fontSize: 13),
                                                      ),
                                                    ),
                                                    onPressed: () {
                                                      this.cubit.selectedFollows[
                                                              i] =
                                                          !this
                                                              .cubit
                                                              .selectedFollows[i];
                                                      setState(() {});

                                                      this
                                                          .cubit
                                                          .putUpdateFollow(this
                                                              .cubit
                                                              .alarms[i]
                                                              .sendUserInfo
                                                              .userNo);
                                                    },
                                                  ))
                                                : Container(
                                                    // margin: EdgeInsets.only(right: 15),
                                                    child: CustomContainer(
                                                      // height: 28,
                                                      backgroundColor:
                                                          Color.fromRGBO(
                                                              31, 31, 36, 1),
                                                      borderRadius: [
                                                        10,
                                                        10,
                                                        10,
                                                        10
                                                      ],
                                                      borderColor:
                                                          Color.fromRGBO(
                                                              31, 31, 36, 1),
                                                      child: Container(
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                vertical: 8,
                                                                horizontal: 16),
                                                        child: Text(
                                                          "팔로우",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 13),
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        this.cubit.selectedFollows[
                                                                i] =
                                                            !this
                                                                .cubit
                                                                .selectedFollows[i];
                                                        setState(() {});
                                                        this
                                                            .cubit
                                                            .putUpdateFollow(this
                                                                .cubit
                                                                .alarms[i]
                                                                .sendUserInfo
                                                                .userNo);
                                                      },
                                                    ),
                                                  )
                                            : Container()
                                      ],
                                    ),
                                  ));
                            }),
                            itemCount: this.cubit.alarms.length,
                          ),
                        ),
                      ),
                    ),
        ),
      ),
    );
  }

  Widget emptyView() {
    String title = "";
    String desc = "";

    title = "알림이 없어요.";
    desc = "";

    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 100),
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              desc,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
