import 'dart:developer';

import 'package:intl/intl.dart';
import 'package:fitmybiz/models/image_source_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'contents_model.dart';

class AlarmModel {
  final String alarmNo;
  final String targetUserNo;
  final String sendUserNo;
  final String alarmTitle;
  final String alarmText;
  final String collectionNo;
  final String imageContentNo;
  final String alarmDivision;

  final UserModel sendUserInfo;
  final DateTime createdAt;

  AlarmModel(
      {this.alarmNo,
      this.targetUserNo,
      this.sendUserNo,
      this.alarmTitle,
      this.alarmText,
      this.collectionNo,
      this.imageContentNo,
      this.alarmDivision,
      this.sendUserInfo,
      this.createdAt});

  factory AlarmModel.fromJson(Map<String, dynamic> json) {
    return AlarmModel(
      alarmNo: json['alarm_no'].toString(),
      targetUserNo: json['target_user_no'].toString(),
      sendUserNo: json['send_user_no'].toString(),
      alarmTitle: json['alarm_title'].toString(),
      alarmText: json['alarm_text'].toString(),
      collectionNo: json['collection_no'].toString(),
      imageContentNo: json['image_content_no'].toString(),
      alarmDivision: json['alarm_division'].toString(),
      sendUserInfo:
          json.containsKey('send_user_info') && json['send_user_info'] != null
              ? UserModel.fromJson(json['send_user_info'])
              : null,
      createdAt: json.containsKey('created_at')
          ? DateFormat("yyyy-MM-dd HH:mm:ss").parse(json['created_at'], true)
          : null,
    );
  }
}
