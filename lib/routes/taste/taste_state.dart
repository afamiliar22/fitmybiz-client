import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/contents_model.dart';

abstract class TasteState extends Equatable {}

class InitialState extends TasteState {
  @override
  List<Object> get props => [];
}

class LoadingState extends TasteState {
  @override
  List<Object> get props => [];
}

class ErrorState extends TasteState {
  @override
  List<Object> get props => [];
}

class GetContents extends TasteState {
  GetContents(this.result, this.contents);

  final bool result;
  final List<ContentsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class CompleteState extends TasteState {
  @override
  List<Object> get props => [];
}

class GetCategory extends TasteState {
  GetCategory(this.result, this.categories);

  final bool result;
  final List<CategoryModel> categories;

  @override
  List<Object> get props => [result, categories];
}

class PostCollectionSet extends TasteState {
  PostCollectionSet(this.result);

  final bool result;

  @override
  List<Object> get props => [result];
}
