import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/point_request.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class PointPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  //
  HDTRefreshController _hdtRefreshController = HDTRefreshController();

  static const int sortName = 0;
  static const int sortStatus = 1;
  bool isAscending = true;
  int sortType = sortName;

  @override
  void initState() {
    user.initData(100);
    super.initState();
  }

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  List<DataColumn> _getColumns() {
    List<DataColumn> dataColumn = [];
    for (var item in [1, 2, 3, 4]) {
      dataColumn.add(DataColumn(
          label: Text('i'),
          tooltip: 'i',
          numeric: true,
          onSort: (index, isSort) {}));
    }

    return dataColumn;
  }

  //
  Widget _getBodyWidget() {
    return Container(
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 100,
        rightHandSideColumnWidth: 600,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount: user.userInfo.length,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
        leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
        rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
        enablePullToRefresh: true,
        refreshIndicator: const WaterDropHeader(),
        refreshIndicatorHeight: 60,
        onRefresh: () async {
          //Do sth
          await Future.delayed(const Duration(milliseconds: 500));
          _hdtRefreshController.refreshCompleted();
        },
        htdRefreshController: _hdtRefreshController,
      ),
      height: MediaQuery.of(context).size.height,
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      FlatButton(
        padding: EdgeInsets.all(0),
        child: _getTitleItemWidget(
            'Name' + (sortType == sortName ? (isAscending ? '↓' : '↑') : ''),
            100),
        onPressed: () {
          sortType = sortName;
          isAscending = !isAscending;
          user.sortName(isAscending);
          setState(() {});
        },
      ),
      FlatButton(
        padding: EdgeInsets.all(0),
        child: _getTitleItemWidget(
            'Status' +
                (sortType == sortStatus ? (isAscending ? '↓' : '↑') : ''),
            100),
        onPressed: () {
          sortType = sortStatus;
          isAscending = !isAscending;
          user.sortStatus(isAscending);
          setState(() {});
        },
      ),
      _getTitleItemWidget('Phone', 200),
      _getTitleItemWidget('Register', 100),
      _getTitleItemWidget('Termination', 200),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
      width: width,
      height: 56,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(user.userInfo[index].name),
      width: 100,
      height: 52,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Icon(
                  user.userInfo[index].status
                      ? Icons.notifications_off
                      : Icons.notifications_active,
                  color:
                      user.userInfo[index].status ? Colors.red : Colors.green),
              Text(user.userInfo[index].status ? 'Disabled' : 'Active')
            ],
          ),
          width: 100,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(user.userInfo[index].phone),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(user.userInfo[index].registerDate),
          width: 100,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(user.userInfo[index].terminationDate),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('포인트'),
        body: Column(
          children: [
            Stack(
              children: [
                Column(
                  children: [
                    CustomContainer(
                      padding:
                          EdgeInsets.symmetric(horizontal: 25, vertical: 12),
                      width: width,
                      //height: (width / 359) * 134,
                      borderRadius: [0, 0, 15, 15],
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '오늘 출금 예정 2000P',
                            style: TextStyle(color: ColorTheme.primary2),
                          ),
                          SizedBox(
                            width: 6,
                          ),
                          Text(
                            '취소하기',
                            style: TextStyle(
                                color: ColorTheme.primary,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    CustomContainer(
                      margin: EdgeInsets.symmetric(horizontal: 18),
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      backgroundColor: ColorTheme.primary2,
                      width: width,
                      height: (width / 323) * 73,
                      borderRadius: [7, 7, 7, 7],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Text.rich(
                                  TextSpan(
                                    children: <InlineSpan>[
                                      const TextSpan(
                                        text: '누적 포인트,\n',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 12,
                                            color: Colors.white),
                                      ),
                                      TextSpan(
                                        text: '총 60,000P',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: ColorTheme.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              VerticalDivider(
                                thickness: 2,
                                color: Colors.white,
                              ),
                              Expanded(
                                child: Text.rich(
                                  TextSpan(
                                    children: <InlineSpan>[
                                      const TextSpan(
                                        text: '가용 포인트,\n',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 12,
                                            color: Colors.white),
                                      ),
                                      TextSpan(
                                        text: '총 60,000P',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: ColorTheme.white),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 13,
                    ),
                  ],
                ),
                Positioned.fill(
                  child: Column(
                    children: [
                      Spacer(),
                      CustomContainer(
                        onPressed: () {
                          this.navigator.pushRoute(PointRequestPage());
                        },
                        borderRadius: [6, 6, 6, 6],
                        width: 75,
                        height: 30,
                        child: Center(
                          child: Text(
                            '출금신청',
                            style: TextStyle(color: ColorTheme.white),
                          ),
                        ),
                        backgroundColor: ColorTheme.primary,
                      )
                    ],
                  ),
                )
              ],
            ),
            CustomContainer(
              child: Column(
                children: [
                  CustomContainer(
                    child: Row(
                      children: [
                        CustomContainer(
                          padding:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          child: Row(
                            children: [
                              Icon(Icons.date_range),
                              Text('2020.05.03')
                            ],
                          ),
                          borderRadius: [6, 6, 6, 6],
                          borderColor: ColorTheme.grayLv1,
                          borderWidth: 1,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        CustomContainer(
                          padding:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          child: Row(
                            children: [
                              Icon(Icons.date_range),
                              Text('2020.05.03')
                            ],
                          ),
                          borderRadius: [6, 6, 6, 6],
                          borderColor: ColorTheme.grayLv1,
                          borderWidth: 1,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CustomContainer(
                          backgroundColor: ColorTheme.primary,
                          borderRadius: [6, 6, 6, 6],
                          padding:
                              EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                          child: Text(
                            '기간조회',
                            style: TextStyle(
                                color: ColorTheme.white,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            ),
            Expanded(
              child: _getBodyWidget(),
            ),
          ],
        ),
      ),
    );
  }
}

User user = User();

class User {
  List<UserInfo> userInfo = [];

  void initData(int size) {
    for (int i = 0; i < size; i++) {
      userInfo.add(UserInfo(
          "User_$i", i % 3 == 0, '+001 9999 9999', '2019-01-01', 'N/A'));
    }
  }

  ///
  /// Single sort, sort Name's id
  void sortName(bool isAscending) {
    userInfo.sort((a, b) {
      int aId = int.tryParse(a.name.replaceFirst('User_', ''));
      int bId = int.tryParse(b.name.replaceFirst('User_', ''));
      return (aId - bId) * (isAscending ? 1 : -1);
    });
  }

  ///
  /// sort with Status and Name as the 2nd Sort
  void sortStatus(bool isAscending) {
    userInfo.sort((a, b) {
      if (a.status == b.status) {
        int aId = int.tryParse(a.name.replaceFirst('User_', ''));
        int bId = int.tryParse(b.name.replaceFirst('User_', ''));
        return (aId - bId);
      } else if (a.status) {
        return isAscending ? 1 : -1;
      } else {
        return isAscending ? -1 : 1;
      }
    });
  }
}

class UserInfo {
  String name;
  bool status;
  String phone;
  String registerDate;
  String terminationDate;

  UserInfo(this.name, this.status, this.phone, this.registerDate,
      this.terminationDate);
}
