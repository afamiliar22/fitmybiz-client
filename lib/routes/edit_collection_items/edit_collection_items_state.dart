import 'package:equatable/equatable.dart';

abstract class EditCollectionItemsState extends Equatable {}

class InitialState extends EditCollectionItemsState {
  @override
  List<Object> get props => [];
}

class SetState extends EditCollectionItemsState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadedState extends EditCollectionItemsState {
  @override
  List<Object> get props => [];
}

class CompletedState extends EditCollectionItemsState {
  @override
  List<Object> get props => [];
}

class LoadingState extends EditCollectionItemsState {
  @override
  List<Object> get props => [];
}

class ToastState extends EditCollectionItemsState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends EditCollectionItemsState {
  @override
  List<Object> get props => [];
}
