import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/routes/taste/taste_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class TasteCubit extends BaseCubit<TasteState> {
  /// {@macro counter_cubit}
  TasteCubit() : super(InitialState());

  void init() {
    emit(InitialState());
  }

  void getImageContents(double lat, double lng) {
    String method = 'get';
    String path = '/api/v1/content/image_content/list?recommend=1';

    emit(LoadingState());
    Map<String, dynamic> data = {'lat': lat.toString(), 'lng': lng.toString()};

    this
        .network
        .requestWithAuth(method, path, lat != null ? data : data)
        .then((response) {
      List<ContentsModel> contents = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();

      emit(GetContents(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getCategories(int categoryNo, String categoryName, int limit, int page) {
    String method = 'get';
    String path = '/api/v1/content/category/list';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<CategoryModel> category = (response.data['data'] as List)
          .map((record) => CategoryModel.fromJson(record))
          .toList();

      emit(GetCategory(true, category));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void postCollectionSet(String imgContentNo, String collectionNo) {
    String method = 'post';
    String path = '/api/v1/collection/set';

    Map data = {
      "image_content_no": imgContentNo.toString(),
      "collection_no": collectionNo
    };
    if (collectionNo == null) {
      data = {"image_content_no": imgContentNo.toString()};
    }
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.facebook.addEvent(FacebookEventType.wishImageContent);

      this.property.meCollectionSubject.add(true);

      emit(PostCollectionSet(true));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
