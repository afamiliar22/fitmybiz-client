import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/point_account.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class PointRequestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  bool isInfoFill = false;

  @override
  void initState() {
    super.initState();
  }

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  //

  Widget buildUserInfo() {
    return Column(
      children: [
        Row(
          children: [
            Spacer(),
            CustomContainer(
              onPressed: () {
                this.navigator.pushRoute(PointAccountPage());
              },
              child: Text(
                '변경하기',
                style: TextStyle(color: ColorTheme.primary),
              ),
            ),
          ],
        ),
        Row(
          children: [
            CustomContainer(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: Row(
                children: [
                  CustomContainer(
                    child: Text(
                      '예금주',
                      style: TextStyle(
                          color: ColorTheme.primary2,
                          fontWeight: FontWeight.bold),
                    ),
                    width: 80,
                  ),
                  Text('김수윤')
                ],
              ),
              borderRadius: [6, 6, 6, 6],
              borderWidth: 1,
            ),
          ],
        ),
        Row(
          children: [
            CustomContainer(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: Row(
                children: [
                  CustomContainer(
                    child: Text(
                      '은행명',
                      style: TextStyle(
                          color: ColorTheme.primary2,
                          fontWeight: FontWeight.bold),
                    ),
                    width: 80,
                  ),
                  Text('국민은행')
                ],
              ),
              borderRadius: [6, 6, 6, 6],
              borderWidth: 1,
            ),
          ],
        ),
        Row(
          children: [
            CustomContainer(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: Row(
                children: [
                  CustomContainer(
                    child: Text(
                      '계좌번호',
                      style: TextStyle(
                          color: ColorTheme.primary2,
                          fontWeight: FontWeight.bold),
                    ),
                    width: 80,
                  ),
                  Text('933848398204')
                ],
              ),
              borderRadius: [6, 6, 6, 6],
              borderWidth: 1,
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('포인트 출금 신청'),
        body: ListView(
          children: [
            Column(
              children: [
                CustomContainer(
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 12),
                  width: width,
                  //height: (width / 359) * 134,
                  borderRadius: [0, 0, 15, 15],
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '출금할 포인트',
                        style: TextStyle(
                            color: ColorTheme.black,
                            fontWeight: FontWeight.normal),
                      ),
                      Spacer(),
                      Text(
                        '출금 가능 포인트     100,000  P',
                        style: TextStyle(color: ColorTheme.primary2),
                      ),
                    ],
                  ),
                ),
                CustomContainer(
                  margin: EdgeInsets.symmetric(horizontal: 18),
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  backgroundColor: ColorTheme.white,
                  width: width,
                  height: (width / 297) * 44,
                  borderRadius: [7, 7, 7, 7],
                  borderColor: ColorTheme.grayLv1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text.rich(
                        TextSpan(
                          children: <InlineSpan>[
                            const TextSpan(
                              text: '1000',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 18,
                                  color: Colors.black),
                            ),
                            TextSpan(
                              text: 'P',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: ColorTheme.primary2),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 17,
                ),
              ],
            ),
            CustomContainer(
              onPressed: () {
                setState(() {
                  isInfoFill = !isInfoFill;
                });
              },
              child: Column(
                children: [
                  Container(
                    width: width,
                    decoration: DottedDecoration(),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  isInfoFill
                      ? CustomContainer(
                          height: 126,
                          child: Center(
                            child: Text('통장정보 등록하기'),
                          ),
                        )
                      : buildUserInfo(),
                  SizedBox(
                    height: 24,
                  ),
                  Container(
                    width: width,
                    decoration: DottedDecoration(),
                  ),
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            ),
            Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  '약관 확인',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                Spacer(),
                Radio(
                  value: true,
                  onChanged: (value) {},
                  groupValue: true,
                ),
                Text('전체동의'),
                SizedBox(
                  width: 14,
                )
              ],
            ),
            Expanded(
              child: ExpansionTile(
                title: Text(
                  '정보수집 이용약관 (필수)',
                  style: TextStyle(fontSize: 14),
                ),
                trailing: IconButton(
                  iconSize: 24,
                  color: ColorTheme.primary2,
                  icon: Icon(Icons.check_circle),
                  onPressed: () {},
                ),
                children: [
                  CustomContainer(
                    backgroundColor: ColorTheme.grayBg,
                    child: SingleChildScrollView(
                      child: Text(
                        '''
                    개인정보수집 및 이용동의 약관
“안전결제 프리미엄 서비스(이하, ‘서비스’라 합니다.)”의 서비스 제공과 관련하여 “브이피 주식회사(이하, ‘회사’라 합니다.)”가 본인으로부터 취득한 개인정보는 통신비밀보호법, 전기통신사업법, 정보통신망 이용촉진 및 정보보호 등에 관한 법률 등 정보통신서비스제공자가 준수하여야 할 관련 법령상의 개인정보보호 규정에 따라 제3자에 제공하거나 영업목적으로 이용 시 동의를 받아야 하는 정보입니다. 이에 본인은, 귀사가 본인으로부터 취득한 개인정보를 아래와 같은 목적으로 이용하거나 제휴업체에게 제공하는데 동의합니다. 회사는 본 서비스를 가입자에게 제공하기 위해 다음과 같이 개인정보를 수집/이용합니다.

1. 개인정보수집 및 이용
① 수집항목
- 휴대폰정보, 단말기 모델명, 서비스 가입/해지일자, 서비스 가입 및 실행이력, 카드번호, 카드 유효기간
② 이용목적
- 서비스 제공에 관한 계약 이행 및 서비스 제
                    ''',
                        textAlign: TextAlign.start,
                      ),
                    ),
                  )
                ],
              ),
            ),
            CustomContainer(
              margin: EdgeInsets.all(10),
              width: 285,
              height: 40,
              child: CustomButton(
                onPressed: () {},
                backgroundColor: ColorTheme.primary2,
                borderRadius: 6,
                text: Text(
                  '출금 신청',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
