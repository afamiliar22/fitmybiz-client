//import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

enum FacebookEventType {
  installLaunchApp,
  viewImageContent,
  wishImageContent,
  searchTogetherWish,
  addPlaceLink,
  addPlacePicture,
  addMap,
  following
}

class FacebookClient {
  FacebookClient._initialize() {
    print('FacebookClient Initialize');
  }

  static final FacebookClient _instance = FacebookClient._initialize();

  static FacebookClient get shared => _instance;

  //FacebookAppEvents facebookAppEvents = FacebookAppEvents();

  addEvent(FacebookEventType type) {
    print(type.toString());
    String text = type.toString().split('.')[1];
    RegExp exp = RegExp(r'(?<=[a-z])[A-Z]');
    String eventName = text
        .replaceAllMapped(exp, (Match m) => ('_' + m.group(0)))
        .toLowerCase();
    print(eventName);
    //this.facebookAppEvents.logEvent(name: eventName);
  }
}
