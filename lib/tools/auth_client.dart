import 'package:kakao_flutter_sdk/all.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AuthClient {
  AuthClient._initialize() {
    print('AuthClient Initialize');
  }

  static final AuthClient _instance = AuthClient._initialize();

  static AuthClient get shared => _instance;

  Future<Map<String, String>> signInWithApple() {
    return SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      webAuthenticationOptions: WebAuthenticationOptions(
        clientId: 'com.example.fitmybiz.serviceid',
        redirectUri: Uri.parse(
          'https://app.zzieut.com/apple/auth',
        ),
      ),
    ).then((credential) {
      Map<String, String> data = {
        'authorizationCode': credential.authorizationCode,
        'identityToken': credential.identityToken
      };
      return Future.value(data);
    });
  }

  Future<Map<String, String>> signInWithGoogle() {
    return SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      webAuthenticationOptions: WebAuthenticationOptions(
        clientId: 'com.example.fitmybiz.serviceid',
        redirectUri: Uri.parse(
          'https://app.zzieut.com/apple/auth',
        ),
      ),
    ).then((credential) {
      Map<String, String> data = {
        'authorizationCode': credential.authorizationCode,
        'identityToken': credential.identityToken
      };
      return Future.value(data);
    });
  }

  Future<Map<String, String>> signInWithKakao() {
    return isKakaoTalkInstalled().then((isInstalled) {
      if (isInstalled) {
        return AuthCodeClient.instance.requestWithTalk();
      } else {
        return AuthCodeClient.instance.request();
      }
    }).then((code) {
      return AuthApi.instance.issueAccessToken(code);
    }).then((token) {
      AccessTokenStore.instance.toStore(token);
      Map<String, String> data = {'accessToken': token.accessToken};
      return Future.value(data);
    });
  }
}
