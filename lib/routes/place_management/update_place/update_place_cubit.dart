import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/contents_model.dart';

import 'update_place_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class UpdatePlaceCubit extends BaseCubit<UpdatePlaceState> {
  /// {@macro counter_cubit}
  UpdatePlaceCubit() : super(InitialState());

  String description = '';
  bool secret = false;
  ContentsModel contents;
  String imageContentNo;

  final TextEditingController descriptionEditingController =
      TextEditingController();
  void init() {}

  void imageContentNoChanged(String imageContentNo) {
    this.imageContentNo = imageContentNo;
    String method = 'get';
    String path = '/api/v1/content/image_content/list';
    Map<String, dynamic> data = {
      "image_content_no": this.imageContentNo,
      "more_field": "categories,contents"
    };
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      List<ContentsModel> contents = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();
      if (contents.length == 1) {
        this.contents = contents[0];
        this.description = this.contents.description;
        this.descriptionEditingController.text = this.contents.description;
        this.secret = this.contents.secret;
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
      } else {
        emit(FailedLoadState());
      }
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void requestUpdateContent() {
    bool secret = this.secret;
    String description = this.description;

    String method = 'put';
    String path = '/api/v1/content/image_content/${this.imageContentNo}';
    // String path = '/file';
    Map<String, dynamic> data = {'description': description, 'secret': secret};

    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.property.meCollectionSubject.add(true);
      emit(CompletedState());
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void descriptionChanged(String description) {
    this.description = description;
  }

  void secretChanged(bool secret) {
    this.secret = secret;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }
}
