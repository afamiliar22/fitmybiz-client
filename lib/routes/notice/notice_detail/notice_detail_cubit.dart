import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';

import 'notice_detail_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class NoticeDetailCubit extends BaseCubit<NoticeDetailState> {
  /// {@macro counter_cubit}
  NoticeDetailCubit() : super(InitialState());

  void init() {}
}
