import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/questions_model.dart';

abstract class CharacterTestState extends Equatable {}

class InitialState extends CharacterTestState {
  @override
  List<Object> get props => [];
}

class LoadingState extends CharacterTestState {
  @override
  List<Object> get props => [];
}

class ErrorState extends CharacterTestState {
  @override
  List<Object> get props => [];
}

class GetQuestions extends CharacterTestState {
  GetQuestions(this.result, this.contents);

  final bool result;
  final List<QuestionsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class GetBanner extends CharacterTestState {
  GetBanner(this.result, this.banners);

  final bool result;
  final List<BannerModel> banners;

  @override
  List<Object> get props => [result, banners];
}

class CompletedState extends CharacterTestState {
  @override
  List<Object> get props => [];
}
