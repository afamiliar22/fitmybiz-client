import 'package:flutter/material.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:photo_view/photo_view.dart';

class CustomImageView extends BaseRoute {
  final String url;
  final String title;
  CustomImageView(this.url, this.title);
  @override
  CustomImageViewState createState() =>
      CustomImageViewState(this.url, this.title);
}

class CustomImageViewState extends BaseRouteState {
  String url;
  String title;
  CustomImageViewState(this.url, this.title);

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return PlatformScaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          ClipRRect(
            child: PhotoView(
                imageProvider: NetworkImage(url),
                minScale: PhotoViewComputedScale.contained),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: SafeArea(
              child: Row(
                children: [
                  IconButton(
                    padding:
                        EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
                    icon: Image.asset('assets/images/icnsBack.png',
                        color: Colors.white),
                    onPressed: () {
                      this.navigator.popRoute(null);
                    },
                  ),
                  Text(this.title ?? ''),
                  Spacer(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
