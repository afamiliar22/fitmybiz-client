import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/routes/createMap/create_map_page.dart';

import 'package:fitmybiz/routes/place_detail/zzim/place_zzim_cubit.dart';
import 'package:fitmybiz/routes/place_detail/zzim/place_zzim_state.dart';
import 'package:fitmybiz/themes/themes.dart';

class PlaceZzimPage extends StatelessWidget {
  // const PlaceDetailPage({Key key}) : super(key: key);

  final String imageContentNo;
  PlaceZzimPage(this.imageContentNo);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => PlaceZzimCubit(),
      child: _View(this.imageContentNo),
    );
  }
}

class _View extends BaseRoute {
  final String imageContentNo;
  _View(this.imageContentNo);
  @override
  _ViewState createState() => _ViewState(this.imageContentNo);
}

class _ViewState extends BaseRouteState {
  String imageContentNo;
  _ViewState(this.imageContentNo);

  List<CollectionModel> collections;

  String collectionNo = '';
  List<bool> arrIsSelected = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);

    context.bloc<PlaceZzimCubit>().getCollectionSaved(this.imageContentNo);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<PlaceZzimCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();

            break;

          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;

          case GetCollections:
            this.indicator.hide();
            this.collections = state.collections;
            arrIsSelected.clear();
            for (int i = 0; i < this.collections.length; i += 1) {
              if (this.collections[i].isSaved == true) {
                this.arrIsSelected.add(true);
              } else {
                this.arrIsSelected.add(false);
              }
            }

            setState(() {});
            break;
          case PostCollectionSet:
            this.indicator.hide();

            this.navigator.popRoute('refresh');

            setState(() {});
            break;
        }
      },
      child: this.collections == null
          ? Container()
          : PlatformScaffold(
              body: SafeArea(
                  child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                  child: Row(
                    children: [
                      CustomButton(
                        image: Image.asset('assets/images/icnsClose.png',
                            color: Colors.black, width: 28, height: 28),
                        onPressed: () {
                          this.navigator.popRoute(null);
                        },
                      ),
                      Spacer(),
                      Text(
                        "장소 찜하기",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      CustomButton(
                        text: Text(
                          '완료',
                          style: TextStyle(color: ColorTheme.red),
                        ),
                        onPressed: () {
                          this.collectionNo = '';
                          for (int i = 0;
                              i < this.arrIsSelected.length;
                              i += 1) {
                            if (this.arrIsSelected[i] == true) {
                              this.collectionNo +=
                                  "${this.collections[i].collectionNo},";
                            }
                          }

                          if (this.collectionNo.length > 0) {
                            this.collectionNo = this
                                .collectionNo
                                .substring(0, this.collectionNo.length - 1);

                            context.bloc<PlaceZzimCubit>().postCollectionSet(
                                this.imageContentNo, this.collectionNo);
                          } else {
                            //선택 안했을 때
                            context
                                .bloc<PlaceZzimCubit>()
                                .postCollectionSet(this.imageContentNo, null);
                          }
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                  child: Text(
                    "지도를 선택해주세요. (중복선택 가능)",
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(138, 148, 159, 1)),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            ListView.builder(
                                shrinkWrap: true,
                                primary: false,
                                itemBuilder: ((BuildContext c, int i) {
                                  CollectionModel collection =
                                      this.collections[i];
                                  Widget imageWidget;
                                  if (collection.imageSrc != null) {
                                    // imageWidget = FadeInImage.memoryNetwork(placeholder: kTransparentImage, image: collection.imageSrc.s, fit: BoxFit.cover);
                                    imageWidget = CustomCachedNetworkImage(
                                      preloadImage: collection.imageSrc.sm,
                                      image: collection.imageSrc.sm,
                                    );
                                  } else {
                                    imageWidget =
                                        Container(color: ColorTheme.grayBg);
                                  }

                                  return CustomContainer(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                            margin: EdgeInsets.symmetric(
                                                vertical: 8),
                                            width: 60,
                                            height: 60,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: Stack(
                                                children: [
                                                  imageWidget,
                                                  this.arrIsSelected[i]
                                                      ? Container(
                                                          color: ColorTheme
                                                              .primary2
                                                              .withOpacity(.5),
                                                          child: Center(
                                                            child: Icon(
                                                              Icons.check,
                                                              size: 24,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          ),
                                                        )
                                                      : Container()
                                                ],
                                              ),
                                            )),
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 20, horizontal: 16),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                child: Text(
                                                  collection.name,
                                                  style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 4,
                                              ),
                                              Container(
                                                child: Text(
                                                  "${collection.itemCount}개의 장소",
                                                  style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          138, 148, 159, 1)),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Spacer(),
                                        Container(
                                            child: this.arrIsSelected[i]
                                                ? Icon(
                                                    Icons.radio_button_checked,
                                                    color: ColorTheme.primary2,
                                                  )
                                                : Icon(
                                                    Icons
                                                        .radio_button_unchecked,
                                                    color: Color.fromRGBO(
                                                        205, 210, 216, 1),
                                                  )),
                                      ],
                                    ),
                                    onPressed: () {
                                      this.arrIsSelected[i] =
                                          !this.arrIsSelected[i];
                                      setState(() {});
                                    },
                                  );
                                }),
                                itemCount: this.collections.length),
                          ],
                        ),
                      ),
                      Positioned(
                        bottom: 24,
                        left: 0,
                        right: 0,
                        child: Row(
                          children: [
                            Spacer(),
                            CustomContainer(
                                shadowColor: ColorTheme.black.withOpacity(.2),
                                backgroundColor: ColorTheme.black,
                                borderRadius: [40, 40, 40, 40],
                                padding: EdgeInsets.symmetric(
                                    vertical: 20, horizontal: 30),
                                child: GestureDetector(
                                  child: Row(
                                    children: [
                                      Image.asset(
                                          'assets/images/icnsAddMaps.png',
                                          width: 28,
                                          height: 28,
                                          color: Colors.white),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Text(
                                        '새 지도 만들기',
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      )
                                    ],
                                  ),
                                  onTap: () {
                                    this
                                        .navigator
                                        .pushRoute(CreateMapPage())
                                        .then((value) {
                                      if (value == 'refresh') {
                                        context
                                            .bloc<PlaceZzimCubit>()
                                            .getCollectionSaved(
                                                this.imageContentNo);
                                      }
                                    });
                                    /*
                                    if (this
                                            .cubit
                                            .selectedCollectionItems
                                            .length <=
                                        0) {
                                      this.showToast('복사할 장소를 선택해주세요.');
                                      return;
                                    }
                                    this
                                        .navigator
                                        .pushRoute(SelectCollectionPage())
                                        .then((value) {
                                      if (value == null) return;
                                      List<CollectionItemModel>
                                          selectedCollections = this
                                              .cubit
                                              .collectionItems
                                              .where((element) => this
                                                  .cubit
                                                  .selectedCollectionItems
                                                  .contains(
                                                      element.collectionItemNo))
                                              .toList();
                                      this.cubit.copyCollectionItem(
                                          value.toString(),
                                          selectedCollections
                                              .map((e) => e.imageContentNo)
                                              .join(','));
                                    });
                                    */
                                    //
                                  },
                                )),
                            Spacer(),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ))),
    );
  }
}
