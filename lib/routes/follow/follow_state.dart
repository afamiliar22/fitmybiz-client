import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/follow_model.dart';

abstract class FollowState extends Equatable {}

class InitialState extends FollowState {
  @override
  List<Object> get props => [];
}

class LoadingState extends FollowState {
  @override
  List<Object> get props => [];
}

class SetState extends FollowState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class ToastState extends FollowState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends FollowState {
  @override
  List<Object> get props => [];
}

class GetSearchFollower extends FollowState {
  GetSearchFollower(this.result, this.follows);

  final bool result;
  final List<FollowModel> follows;

  @override
  List<Object> get props => [result, follows];
}
