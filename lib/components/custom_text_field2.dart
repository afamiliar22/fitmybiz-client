import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextField2 extends StatelessWidget {
  final TextEditingController controller;
  final double height;
  final Color activeColor;
  final Color inactiveColor;
  final Function(String) onChanged;
  final String placeholder;
  final TextStyle placeholderStyle;
  final TextStyle textStyle;
  final Widget suffix;
  final bool secure;
  final TextInputType inputType;
  CustomTextField2(
      {this.controller,
      this.height,
      this.activeColor,
      this.inactiveColor,
      this.onChanged,
      this.placeholder,
      this.suffix,
      this.textStyle,
      this.secure,
      this.inputType,
      this.placeholderStyle});

  @override
  Widget build(BuildContext context) {
    Color activeColor = this.activeColor ?? Colors.black;
    Color inactiveColor = this.inactiveColor ?? Colors.grey.withOpacity(0.5);

    return TextField(
      style: this.textStyle,
      keyboardType: this.inputType,
      obscureText: this.secure ?? false,
      textInputAction: TextInputAction.newline,
      controller: controller,
      decoration: InputDecoration(
        hintText: this.placeholder,
        hintStyle: this.placeholderStyle,
        suffixIcon: this.suffix,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: inactiveColor),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: activeColor),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: inactiveColor),
        ),
      ),
      onChanged: onChanged,
    );
  }
}
