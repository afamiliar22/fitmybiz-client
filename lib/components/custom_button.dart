import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Image image;
  final Icon icon;
  final bool isRightIcon;
  final Widget text;
  final Color backgroundColor;
  final Color borderColor;
  final double borderWidth;
  final Color shadowColor;
  final double borderRadius;
  final double height;
  final MainAxisAlignment alignment;
  final VoidCallback onPressed;
  final EdgeInsetsGeometry padding;

  CustomButton(
      {this.image,
      this.icon,
      this.isRightIcon,
      this.height,
      this.text,
      this.backgroundColor,
      this.borderColor,
      this.borderWidth,
      this.shadowColor,
      this.borderRadius,
      this.alignment,
      this.onPressed,
      this.padding});

  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];

    if (this.isRightIcon == true) {
      if (this.text != null) {
        children.add(this.text);
      }
      if (this.image != null) {
        if (children.length >= 1) {
          children.add(SizedBox(width: 8));
        }

        children.add(this.image);
      } else if (this.icon != null) {
        if (children.length >= 1) {
          children.add(SizedBox(width: 8));
        }
        children.add(this.icon);
      }
    } else {
      if (this.image != null) {
        children.add(this.image);
      } else if (this.icon != null) {
        children.add(this.icon);
      }

      if (this.text != null) {
        if (children.length >= 1) {
          children.add(SizedBox(width: 8));
        }
        children.add(this.text);
      }
    }

    var activeColor = this.backgroundColor ?? Colors.black;
    var disabledColor = Color.fromARGB(100, activeColor.red, activeColor.green, activeColor.blue);
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: this.borderWidth ?? 0.5, color: this.borderColor ?? Colors.transparent),
        borderRadius: BorderRadius.circular(this.borderRadius ?? 4),
        boxShadow: [
          if (this.shadowColor != null)
            BoxShadow(
              color: this.shadowColor,
              spreadRadius: 0,
              blurRadius: 20,
              offset: Offset(0, 0), // changes position of shadow
            ),
        ],
      ),
      child: CupertinoButton(
        // borderRadius: BorderRadius.circular(this.borderRadius ?? 4),
        minSize: 0,
        padding: this.padding ?? EdgeInsets.zero,
        onPressed: onPressed,
        color: this.backgroundColor,
        disabledColor: disabledColor,
        child: Row(mainAxisAlignment: this.alignment ?? MainAxisAlignment.center, children: children),
      ),
    );
  }
}
