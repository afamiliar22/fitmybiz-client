import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/blog_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/google_place_model.dart';

abstract class PlaceDetailState extends Equatable {}

class InitialState extends PlaceDetailState {
  @override
  List<Object> get props => [];
}

class LoadingState extends PlaceDetailState {
  @override
  List<Object> get props => [];
}

class ErrorState extends PlaceDetailState {
  @override
  List<Object> get props => [];
}

class GetContents extends PlaceDetailState {
  GetContents(this.result, this.contents);

  final bool result;
  final List<ContentsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class GetBlogContents extends PlaceDetailState {
  GetBlogContents(this.result, this.contents);

  final bool result;
  final List<BlogModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class GetGoogleReview extends PlaceDetailState {
  GetGoogleReview(this.result, this.contents);

  final bool result;
  final GooglePlaceModel contents;

  @override
  List<Object> get props => [result, contents];
}

class ToastState extends PlaceDetailState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class SetState extends PlaceDetailState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}
