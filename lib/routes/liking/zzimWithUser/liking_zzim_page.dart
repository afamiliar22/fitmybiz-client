import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/routes/_cells/contents_cell.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';
import 'package:fitmybiz/routes/user/user_page.dart';

import 'liking_zzim_cubit.dart';
import 'liking_zzim_state.dart';

class LikingZzimPage extends StatelessWidget {
  final String someoneNo;
  LikingZzimPage(this.someoneNo);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LikingZzimCubit(),
      child: _View(this.someoneNo),
    );
  }
}

class _View extends BaseRoute {
  final String someoneNo;
  _View(this.someoneNo);
  @override
  _ViewState createState() => _ViewState(this.someoneNo);
}

class _ViewState extends BaseRouteState {
  String someoneNo;
  _ViewState(this.someoneNo);
  final TextEditingController textEditingController = TextEditingController();
  bool isFocus = false;
  String searchText;
  LikingZzimCubit cubit;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.cubit = context.bloc<LikingZzimCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.textEditingController.text = "";
    this.cubit.init();

    this.cubit.selectUser(this.someoneNo);
    this.cubit.getImageContents(this.someoneNo, 1, 10);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        cubit: this.cubit,
        listener: (context, state) {
          switch (state.runtimeType) {
            case InitialState:
              this.indicator.hide();

              break;

            case ToastState:
              this.showToast(state.message);
              break;
            case ErrorState:
              this.indicator.hide();
              break;
            case LoadingState:
              this.indicator.show();
              break;
            case SetState:
              this.indicator.hide();
              setState(() {});
              break;
          }
        },
        child: PlatformScaffold(
          body: SafeArea(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10, right: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          padding: EdgeInsets.only(
                              left: 12, top: 8, right: 0, bottom: 8),
                          icon: Image.asset('assets/images/icnsBack.png',
                              color: Colors.black, width: 28, height: 28),
                          onPressed: () {
                            this.navigator.popRoute(null);
                          },
                        ),
                        Stack(
                          children: [
                            CustomContainer(
                              backgroundColor: Colors.white,
                              width: 44,
                              height: 44,
                              margin: EdgeInsets.only(left: 70),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(22),
                                  child: this.property.user.profileImage == null
                                      ? Image.asset(
                                          "assets/images/userProfileImg.png")
                                      : CustomCachedNetworkImage(
                                          preloadImage:
                                              this.property.user.profileImage.s,
                                          image:
                                              this.property.user.profileImage.s,
                                        )),
                              onPressed: () {
                                this.navigator.pushRoute(
                                    UserPage(this.property.user.userNo));
                              },
                            ),
                            CustomContainer(
                              margin: EdgeInsets.only(left: 35),
                              height: 44,
                              width: 44,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(22),
                                child: this.cubit.other.profileImage == null
                                    ? Image.asset(
                                        "assets/images/userProfileImg.png")
                                    : CustomCachedNetworkImage(
                                        preloadImage:
                                            this.cubit.other.profileImage.s,
                                        image: this.cubit.other.profileImage.s,
                                      ),
                              ),
                              onPressed: () {
                                this.navigator.pushRoute(
                                    UserPage(this.cubit.other.userNo));
                              },
                            ),
                          ],
                        ),
                      ]),
                ),
                Expanded(
                  child: ListView(
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 20),
                            child: Image.asset(
                              'assets/images/elementMarkerZzimFocused.png',
                              height: 60,
                              width: 60,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 20),
                        child: Text.rich(
                          TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: this.cubit.other.nickname ?? '',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 28),
                              ),
                              TextSpan(text: ' '),
                              TextSpan(
                                  text: "님과\n내가 같이 찜한 곳",
                                  style: TextStyle(fontSize: 28)),
                            ],
                          ),
                        ),
                      ),
                      this.cubit.contents.length == 0
                          ? Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 100),
                                child: Column(
                                  children: [
                                    Text(
                                      "같이 찜한 곳이 없어요",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      "프로필에 방문하여 어떤 곳을 찜했는지\n확인해보세요.",
                                      textAlign: TextAlign.center,
                                    )
                                  ],
                                ),
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 16),
                              child: StaggeredGridView.countBuilder(
                                shrinkWrap: true,
                                primary: false,
                                crossAxisCount: 4,
                                itemCount: this.cubit.contents.length,
                                itemBuilder: (BuildContext context, int index) {
                                  ContentsModel contents =
                                      this.cubit.contents[index];
                                  return ContentsCell(
                                    contents: contents,
                                    onPressed: () {
                                      this.navigator.pushRoute(PlaceDetailPage(
                                            contents.imageContentNo,
                                          ));
                                    },
                                    onPressedMore: () {
                                      this.showPlaceMoreSheetOther(
                                          onReport: (value) {
                                        this.cubit.insertImageContentReport(
                                            contents.imageContentNo, value);
                                      });
                                    },
                                  );
                                },
                                staggeredTileBuilder: (int index) =>
                                    StaggeredTile.count(
                                        2,
                                        this
                                                .cubit
                                                .contents[index]
                                                .imageSrc
                                                .height /
                                            this
                                                .cubit
                                                .contents[index]
                                                .imageSrc
                                                .width *
                                            2),
                                mainAxisSpacing: 8.0,
                                crossAxisSpacing: 8.0,
                              ),
                            )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
