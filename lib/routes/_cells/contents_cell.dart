import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/models/collection_item_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:fitmybiz/extensions/extensions.dart';

class ContentsCell extends StatelessWidget {
  final ContentsModel contents;
  final VoidCallback onPressed;
  final VoidCallback onPressedMore;
  final VoidCallback onPressedUser;
  ContentsCell(
      {this.contents, this.onPressed, this.onPressedMore, this.onPressedUser});

  @override
  Widget build(BuildContext context) {
    // onPressed: this.onPressed,

    List<Color> colors = [];
    if (this.contents.userInfo != null) {
      colors = [Colors.black12, Colors.transparent, Colors.black12];
    } else {
      colors = [Colors.black12, Colors.transparent, Colors.black12];
    }

    return Stack(
      fit: StackFit.expand,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: CustomCachedNetworkImage(
            preloadImage: this.contents.imageSrc.sm,
            image: this.contents.imageSrc.sm,
          ),
        ),
        GestureDetector(
          onTap: this.onPressed,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: colors,
              ),
            ),
          ),
        ),
        Column(
          children: [
            if (this.contents.userInfo != null) this.buildUserContainer(),
            Spacer(),
            Container(
              margin: EdgeInsets.only(left: 12, right: 4, bottom: 8),
              child: Row(
                children: [
                  /*
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 2),
                      child: Text(
                        this.contents.title ?? '',
                        style: TextStyleTheme.common.copyWith(color: Colors.white),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    ),
                  ),
                  CustomButton(
                    image: Image.asset(
                      'assets/images/iconMore.png',
                      height: 20,
                      width: 20,
                    ),
                    onPressed: this.onPressedMore,
                  )
                  */
                ],
              ),
            )
          ],
        )
      ],
    );
  }

  Widget buildUserContainer() {
    return CustomContainer(
      onPressed: this.onPressedUser,
      margin: EdgeInsets.all(10),
      child: Row(
        children: [
          Container(
            width: 24,
            height: 24,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: this.contents.userInfo.profileImage == null
                    ? Container()
                    : Image.network(contents.userInfo.profileImage.s)
                // CustomCachedNetworkImage(
                //   preloadImage: contents.userInfo.profileImage.s,
                //   image: contents.userInfo.profileImage.s,
                // ),
                ),
          ),
          SizedBox(width: 6),
          Expanded(
            child: Text(this.contents.userInfo.nickname,
                style: TextStyleTheme.common.copyWith(
                    fontWeight: FontWeight.w700,
                    fontSize: 13,
                    color: Colors.white)),
          ),
        ],
      ),
    );
  }
}
