import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_text_field2.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/themes/themes.dart';

class InsertCollectionSheet extends BaseRoute {
  final Function(String) onCompleted;
  InsertCollectionSheet({this.onCompleted});

  @override
  InsertCollectionSheetState createState() =>
      InsertCollectionSheetState(this.onCompleted);
}

class InsertCollectionSheetState extends BaseRouteState {
  Function(String) onCompleted;
  final TextEditingController textEditingController = TextEditingController();

  InsertCollectionSheetState(this.onCompleted);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Column(
        //mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: kToolbarHeight,
          ),
          CustomContainer(
            padding: EdgeInsets.symmetric(horizontal: 25),
            height: 72,
            child: Row(
              children: [
                GestureDetector(
                  child: Image.asset('assets/images/icnsClose.png',
                      color: Colors.black, width: 28, height: 28),
                  onTap: () => Navigator.of(context).pop(),
                ),
                Spacer(),
                Text('지도 만들기', style: TextStyleTheme.subtitle),
                Spacer(),
                CustomContainer(
                  onPressed: () => complete(),
                  child: Text(
                    '완료',
                    style: TextStyle(
                        color: this.textEditingController.text.isEmpty
                            ? ColorTheme.grayDisable
                            : ColorTheme.primary),
                  ),
                )
              ],
            ),
          ),
          CustomContainer(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('지도 제목', style: TextStyleTheme.textFieldLabel),
                SizedBox(height: 8),
                CustomTextField2(
                  controller: this.textEditingController,
                  placeholderStyle: TextStyleTheme.placeholder,
                  placeholder: "지도의 제목을 입력해주세요",
                  onChanged: (value) {
                    setState(() {});
                  },
                ),
              ],
            ),
          ),
          /*
          SizedBox(height: 60),
          CustomContainer(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: PrimaryButton(
              text: Text('완료'),
              onPressed: () => complete(),
            ),
          ),
          SizedBox(height: 8),
          */
        ],
      ),
    );
  }

  void complete() {
    if (this.textEditingController.text.isEmpty) {
      this.showToast('지도 제목을 입력해주세요');
      return;
    }
    this.onCompleted(this.textEditingController.text);
  }
}
