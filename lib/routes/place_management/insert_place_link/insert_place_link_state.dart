import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/link_data_model.dart';

abstract class InsertPlaceLinkState extends Equatable {}

class InitialState extends InsertPlaceLinkState {
  @override
  List<Object> get props => [];
}

class SetState extends InsertPlaceLinkState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadedState extends InsertPlaceLinkState {
  LoadedState(this.linkData);

  final LinkDataModel linkData;

  @override
  List<Object> get props => [linkData];
}

class LoadingState extends InsertPlaceLinkState {
  @override
  List<Object> get props => [];
}

class ToastState extends InsertPlaceLinkState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class LoadedLinkImageState extends InsertPlaceLinkState {
  LoadedLinkImageState(this.linkData);

  final LinkDataModel linkData;

  @override
  List<Object> get props => [linkData];
}

class ErrorState extends InsertPlaceLinkState {
  @override
  List<Object> get props => [];
}
