import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/follow_model.dart';

abstract class LikingSearchUserState extends Equatable {}

class InitialState extends LikingSearchUserState {
  @override
  List<Object> get props => [];
}

class LoadingState extends LikingSearchUserState {
  @override
  List<Object> get props => [];
}

class SetState extends LikingSearchUserState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class ToastState extends LikingSearchUserState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends LikingSearchUserState {
  @override
  List<Object> get props => [];
}

class GetSearchFollower extends LikingSearchUserState {
  GetSearchFollower(this.result, this.follows);

  final bool result;
  final List<FollowModel> follows;

  @override
  List<Object> get props => [result, follows];
}
