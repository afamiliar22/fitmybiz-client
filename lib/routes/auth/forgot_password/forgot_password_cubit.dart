import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';
import 'package:fitmybiz/tools/network_client.dart';

import 'forgot_password_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class ForgotPasswordCubit extends BaseCubit<ForgotPasswordState> {
  /// {@macro counter_cubit}
  ForgotPasswordCubit() : super(InitialState());

  String email = '';
  String verifyCode = '';
  String passwordToken;

  void init() {
    emit(InitialState());
  }

  void emailChanged(String email) {
    this.email = email;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }

  void verifyCodeChanged(String verifyCode) {
    this.verifyCode = verifyCode;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }

  void requestVerifyCode() {
    if (this.email.isEmpty) {
      emit(ToastState('이메일을 입력해주세요.'));
      emit(ErrorState());
      return;
    }
    String method = 'post';
    String path = '/api/v1/user/password/forget';
    Map data = {"email": this.email};
    emit(LoadingState());
    this.network.requestWithoutAuth(method, path, data).then((response) {
      this.passwordToken = response.data['password_token'];
      emit(ToastState('인증번호가 전송되었습니다.'));
      emit(RequestedState());
    }).catchError((e) {
      if (e is NetworkResponse) {
        String message;
        switch (e.statusCode) {
          case 400:
            message = '잘못된 이메일 형식입니다.';
            break;
          case 404:
            message = '찾을 수 없는 사용자입니다.';
            break;
          default:
            message = '네트워크 오류가 발생하였습니다.';
            break;
        }
        emit(ToastState(message));
      }
      emit(ErrorState());
    });
  }

  void completeVerifyCode() {
    if (this.passwordToken == null) {
      emit(ToastState('인증번호 보내기를 눌러주세요.'));
      emit(ErrorState());
      return;
    }
    if (this.passwordToken != this.verifyCode) {
      emit(ToastState('인증번호가 일치하지 않습니다.'));
      emit(ErrorState());
      return;
    }

    emit(CompletedState());
  }
}
