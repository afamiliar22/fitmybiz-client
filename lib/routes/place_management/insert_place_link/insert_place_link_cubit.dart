import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/link_data_model.dart';
import 'package:validators/validators.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'insert_place_link_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class InsertPlaceLinkCubit extends BaseCubit<InsertPlaceLinkState> {
  /// {@macro counter_cubit}
  InsertPlaceLinkCubit() : super(InitialState());

  String link;
  WebViewController webViewController;
  final TextEditingController textEditingController = TextEditingController();

  void init() {}

  void linkChanged(String link) {
    this.link = link;
    // this.textEditingController.text = link;
  }

  void selectLinkImage() {
    if (isURL(this.link) == false) {
      emit(ToastState('올바르지 않은 링크입니다.'));
      emit(ErrorState());
      return;
    }

    String method = 'get';
    String path = '/api/v1/link/image?source_url=${this.link}';
    // Map<String, dynamic> data = {'source_url': this.link};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      LinkDataModel linkData = LinkDataModel.fromJson(response.data);
      if (linkData.images.length <= 0) {
        emit(ToastState('링크에서 이미지를 찾을 수 없습니다.'));
        emit(ErrorState());
      } else {
        emit(LoadedState(linkData));
      }
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
