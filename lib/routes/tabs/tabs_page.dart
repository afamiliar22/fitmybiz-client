import 'dart:io';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_bottom_tab_bar.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/routes/character/test/character_test_page.dart';
import 'package:fitmybiz/routes/follow/follow_page.dart';
import 'package:fitmybiz/routes/map/map_page.dart';
import 'package:fitmybiz/routes/me/me_page.dart';
import 'package:fitmybiz/routes/place_management/insert_place/insert_place_page.dart';
import 'package:fitmybiz/routes/place_management/insert_place_link/insert_place_link_page.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';
import 'package:fitmybiz/routes/search/search_page.dart';
//import 'package:fitmybiz/routes/taste/search/search_page.dart';
import 'package:fitmybiz/routes/views/insert_place_sheet.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:transparent_image/transparent_image.dart';

import 'package:validators/validators.dart';

import 'tabs_cubit.dart';

class TabsPage extends StatelessWidget {
  final List<CategoryModel> categories;
  // const TabsPage({Key key}) : super(key: key);
  TabsPage(this.categories);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => TabsCubit(),
      child: _View(this.categories),
    );
  }
}

class _View extends BaseRoute {
  final List<CategoryModel> categories;
  _View(this.categories);
  @override
  _ViewState createState() => _ViewState(this.categories);
}

class _ViewState extends BaseRouteState {
  List<CategoryModel> categories;

  bool charCheck = false;
  _ViewState(this.categories);
  TabsCubit cubit;

  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();

    this.cubit = context.bloc<TabsCubit>();
    readLocal();
  }

  readLocal() async {
    prefs = await SharedPreferences.getInstance();
    charCheck = prefs.getBool('charCheck') ?? false;
    if (!charCheck)
      SchedulerBinding.instance.addPostFrameCallback((_) => showDialog(
          context: context,
          builder: (context) => Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      height: 480,
                      child: Stack(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 40,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 40),
                                child: Text(
                                  '나는 어떤 성향일까?',
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(
                                height: 18,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 40),
                                child: Text(
                                  '반가워요! 찜을 시작하기 전\n여러분께 딱 맞는 추천을 해드리기 위해서\n성향 테스트를 준비했어요 :)',
                                  style: TextStyle(fontSize: 15),
                                ),
                              ),
                              Spacer(),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 310,
                                child: Image.asset(
                                  'assets/illusts/illustsBgTasteTest.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Spacer(),
                              Row(
                                children: [
                                  Expanded(
                                      child: CustomContainer(
                                    margin:
                                        EdgeInsets.only(left: 25, bottom: 20),
                                    height: 48,
                                    borderRadius: [15, 15, 15, 15],
                                    backgroundColor: ColorTheme.grayPlaceholder,
                                    child: Center(
                                      child: Text(
                                        '다음에 하기',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      prefs.setBool('charCheck', true);
                                      Navigator.of(context).pop();
                                      Fluttertoast.showToast(
                                          msg:
                                              '마이페이지의 설정 화면에서 언제든지 다시 하실 수 있어요');
                                    },
                                  )),
                                  SizedBox(
                                    width: 11,
                                  ),
                                  Expanded(
                                      child: CustomContainer(
                                    onPressed: () {
                                      prefs.setBool('charCheck', true);
                                      Navigator.of(context).pop();
                                      this
                                          .navigator
                                          .pushRoute(CharacterTestPage());
                                    },
                                    margin:
                                        EdgeInsets.only(right: 25, bottom: 20),
                                    height: 48,
                                    borderRadius: [15, 15, 15, 15],
                                    backgroundColor: ColorTheme.primary,
                                    child: Center(
                                      child: Text(
                                        '검사 시작하기',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                  )),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )));
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
/*
    ReceiveSharingIntent.init();
    // For sharing or opening urls/text coming from outside the app while the app is in the memory

    ReceiveSharingIntent.getTextStream().listen((value) {
      this.processLink(value);
    });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((value) {
      this.processLink(value);
    });
    */

    if (Platform.isIOS) {
      this.firebase.requestNotificationPermission();
    }

    this.firebase.getMessagingToken().then((token) {
      this.cubit.tokenChanged(token);
    });

    this.firebase.configureNotification((data) {
      print('handling view: $data');
      String clickAction = data['click_action'];
      switch (clickAction) {
        case 'ALARM_LIST':
          break;
        case 'HOME_FOLLOWING':
          this.navigator.pushRoute(
              FollowPage(this.property.user.userNo, FollowType.following));
          break;
        default:
          break;
      }
    });
    FirebaseDynamicLinks.instance.getInitialLink().then((data) {
      print('getInitialLink');
      final Uri deepLink = data?.link ?? null;
      processDynamicLink(deepLink);
      setState(() {});
    });
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      print('onLink.onSuccess');
      final Uri deepLink = dynamicLink?.link;
      processDynamicLink(deepLink);
    }, onError: (OnLinkErrorException e) async {
      print('onLink.onError');
      print(e.message);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return PlatformScaffold(
      onWillPop: this.onWillPop,
      body: Stack(
        children: [
          PlatformBottomTabBar(
            selectedColor: ColorTheme.primary,
            children: [
              SearchPage(categories),
              MapPage(isUser: true),
              MePage(),
              Container()
            ],
            tabs: [
              Image.asset('assets/images/homeOn.png', width: 28, height: 28),
              Image.asset('assets/images/maps.png', width: 28, height: 28),
              Image.asset('assets/images/user.png', width: 28, height: 28),
              Image.memory(kTransparentImage),
            ],
            tabs2: [
              Image.asset('assets/images/homeOn.png', width: 28, height: 28),
              Image.asset('assets/images/maps_off.png', width: 28, height: 28),
              Image.asset('assets/images/user_off.png', width: 28, height: 28),
              Image.memory(kTransparentImage),
            ],
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: SafeArea(
              child: CustomContainer(
                onPressed: () {
                  //skip
                },
                width: MediaQuery.of(context).size.width / 4,
                child: Row(
                  children: [
                    Spacer(),
                    CustomContainer(
                      height: 60,
                      width: 60,
                      margin: EdgeInsets.only(right: 25, bottom: 10),
                      shadowColor: ColorTheme.primary.withOpacity(0.3),
                      borderRadius: [30, 30, 30, 30],
                      backgroundColor: ColorTheme.primary,
                      child: Icon(Icons.add, size: 24, color: Colors.white),
                      onPressed: () {
                        this.showInsertPlaceSheet();
                      },
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void showInsertPlaceSheet() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) => InsertPlaceSheet(
        onPressedLink: () {
          Navigator.of(context).pop();
          this.navigator.presentRoute(InsertPlaceLinkPage(null));
        },
        onPressedPhoto: () {
          Navigator.of(context).pop();
          this.showImagePicker().then((value) {
            this.navigator.presentRoute(InsertPlacePage(null, value));
          });
        },
      ),
    );
  }

  void processDynamicLink(Uri link) {
    print('processDynamicLink');
    print(link);
    if (link == null) return;

    if (link.pathSegments.length == 2 && link.pathSegments[0] == 'place') {
      String imageContentNo = link.pathSegments[1];
      this.navigator.pushRoute(PlaceDetailPage(imageContentNo));
    } else if (link.pathSegments.length == 4 && link.pathSegments[0] == 'map') {
      String userNo = link.pathSegments[1];
      String latitude = link.pathSegments[2];
      String longitude = link.pathSegments[3];
      this.navigator.pushRoute(MapPage(
          isUser: false,
          userNo: userNo,
          latitude: latitude,
          longitude: longitude));
    }
  }

  void processLink(String value) {
    if (value != null && isURL(value, requireTld: false)) {
      this.navigator.presentRoute(InsertPlaceLinkPage(value));
    }
  }
}
