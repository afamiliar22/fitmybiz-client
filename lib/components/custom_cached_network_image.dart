import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:shimmer/shimmer.dart';

class CustomCachedNetworkImage extends StatelessWidget {
  final String preloadImage;
  final String image;
  CustomCachedNetworkImage({this.preloadImage, this.image});

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      fit: BoxFit.cover,
      placeholder: (context, url) {
        return CachedNetworkImage(
          fit: BoxFit.cover,
          placeholder: (context, url) {
            return true
                ? Container()
                : Container(
                    color: ColorTheme.grayBg,
                    child: Center(
                      child: CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor: AlwaysStoppedAnimation<Color>(
                              ColorTheme.primary)),
                    ),
                  );
          },
          imageUrl: this.preloadImage,
          errorWidget: (context, url, o) {
            return this.errorWidget();
          },
        );
      },
      imageUrl: this.image,
      errorWidget: (context, url, o) {
        return this.errorWidget();
      },
    );
  }

  Widget errorWidget() {
    return Container(
      color: ColorTheme.grayPlaceholder,
      child: Center(
        child: Icon(
          Icons.warning,
          color: ColorTheme.grayLv1,
        ),
      ),
    );
  }
}
