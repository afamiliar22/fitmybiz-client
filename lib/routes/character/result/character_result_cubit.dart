import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/questions_model.dart';
import 'package:fitmybiz/models/result_model.dart';
import 'package:fitmybiz/routes/character/result/character_result_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class CharacterResultCubit extends BaseCubit<CharacterResultState> {
  /// {@macro counter_cubit}
  CharacterResultCubit() : super(InitialState());

  void init() {
    emit(InitialState());
  }

  void getResult() {
    String method = 'get';
    String path = '/api/v1/personality/me';

    emit(LoadingState());

    this.network.requestWithAuth(method, path, null).then((response) {
      ResultModel contents = ResultModel.fromJson(response.data);
      emit(GetResult(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
