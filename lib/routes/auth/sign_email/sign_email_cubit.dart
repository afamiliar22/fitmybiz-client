import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';
import 'package:fitmybiz/routes/auth/sign_email/sign_email_state.dart';
import 'package:fitmybiz/tools/network_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class SignEmailCubit extends BaseCubit<SignEmailState> {
  /// {@macro counter_cubit}
  SignEmailCubit() : super(InitialState());

  String email = '';
  String password = '';
  String password2 = '';

  void init() {
    emit(InitialState());
  }

  void emailChanged(String email) {
    this.email = email;
  }

  void passwordChanged(String password) {
    this.password = password;
  }

  void password2Changed(String password2) {
    this.password2 = password2;
  }

  void sign() {
    if (this.email.isEmpty) {
      emit(ToastState('이메일을 입력해주세요'));
      return null;
    }
    if (this.password.isEmpty) {
      emit(ToastState('비밀번호를 입력해주세요'));
      return null;
    }
    if (this.password != this.password2) {
      emit(ToastState('비밀번호와 비밀번호 확인이 일치하지 않습니다'));
      return null;
    }
    String method = 'post';
    String path = '/api/v1/user/sign';
    Map data = {"email": this.email, "password": this.password};
    emit(LoadingState());
    this.network.requestWithoutAuth(method, path, data).then((response) {
      LoginModel loginModel = LoginModel.fromJson(response.data);
      this.network.property.accessToken = loginModel.accessToken;
      this.network.property.refreshToken = loginModel.refreshToken;
      emit(SignedState());
    }).catchError((e) {
      if (e is NetworkResponse) {
        String message;
        switch (e.statusCode) {
          case 400:
            message = '잘못된 이메일 형식입니다';
            break;
          case 409:
            message = '이미 가입하신 이메일입니다';
            break;
          default:
            message = '네트워크 오류가 발생하였습니다';
            break;
        }

        emit(ToastState(message));
      }
      emit(ErrorState());
    });
  }

  void checkUserTerm() {
    String method = 'get';
    String path = '/api/v1/user/term';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<TermModel> terms = (response.data['terms'] as List)
          .map((record) => TermModel.fromJson(record))
          .toList();
      emit(CheckedTermState(response.data['check'] == true, terms));
      // emit(CheckedTermState(true, []));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
