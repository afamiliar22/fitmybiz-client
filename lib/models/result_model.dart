class ResultModel {
  Hexaco hexaco;
  Data data;

  ResultModel({this.hexaco, this.data});

  ResultModel.fromJson(Map<String, dynamic> json) {
    hexaco =
        json['hexaco'] != null ? new Hexaco.fromJson(json['hexaco']) : null;
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.hexaco != null) {
      data['hexaco'] = this.hexaco.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Hexaco {
  int h;
  int e;
  int x;
  int a;
  int c;
  int o;

  Hexaco({this.h, this.e, this.x, this.a, this.c, this.o});

  Hexaco.fromJson(Map<String, dynamic> json) {
    h = json['h'];
    e = json['e'];
    x = json['x'];
    a = json['a'];
    c = json['c'];
    o = json['o'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['h'] = this.h;
    data['e'] = this.e;
    data['x'] = this.x;
    data['a'] = this.a;
    data['c'] = this.c;
    data['o'] = this.o;
    return data;
  }
}

class Data {
  int userHexacoNo;
  int userNo;
  int hexacoNo;
  int h;
  int e;
  int x;
  int a;
  int c;
  int o;
  String state;
  String createdAt;
  String updatedAt;
  String hexaco;
  String title;
  String imageUrl;
  String description;
  List<String> tags;
  Null deletedAt;

  Data(
      {this.userHexacoNo,
      this.userNo,
      this.hexacoNo,
      this.h,
      this.e,
      this.x,
      this.a,
      this.c,
      this.o,
      this.state,
      this.createdAt,
      this.updatedAt,
      this.hexaco,
      this.title,
      this.imageUrl,
      this.description,
      this.tags,
      this.deletedAt});

  Data.fromJson(Map<String, dynamic> json) {
    userHexacoNo = json['user_hexaco_no'];
    userNo = json['user_no'];
    hexacoNo = json['hexaco_no'];
    h = json['h'];
    e = json['e'];
    x = json['x'];
    a = json['a'];
    c = json['c'];
    o = json['o'];
    state = json['state'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    hexaco = json['hexaco'];
    title = json['title'];
    imageUrl = json['image_url'];
    description = json['description'];
    tags = json['tags'].cast<String>();
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_hexaco_no'] = this.userHexacoNo;
    data['user_no'] = this.userNo;
    data['hexaco_no'] = this.hexacoNo;
    data['h'] = this.h;
    data['e'] = this.e;
    data['x'] = this.x;
    data['a'] = this.a;
    data['c'] = this.c;
    data['o'] = this.o;
    data['state'] = this.state;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['hexaco'] = this.hexaco;
    data['title'] = this.title;
    data['image_url'] = this.imageUrl;
    data['description'] = this.description;
    data['tags'] = this.tags;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}
