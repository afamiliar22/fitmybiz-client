import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PropertyClient {
  PropertyClient._initialize() {
    print('PropertyClient Initialize');
  }

  static final PropertyClient _instance = PropertyClient._initialize();

  static PropertyClient get shared => _instance;

  Future<SharedPreferences> preferenceInstance =
      SharedPreferences.getInstance();

  UserModel user;
  PublishSubject<bool> meProfileSubject = PublishSubject<bool>();
  PublishSubject<bool> meCollectionSubject = PublishSubject<bool>();

  Future<String> getString(String key) {
    return preferenceInstance.then((preference) {
      String accessToken = preference.getString(key);
      return Future.value(accessToken);
    });
  }

  Future setString(String key, String value) {
    return preferenceInstance.then((preference) {
      preference.setString(key, value);
      return Future.value();
    });
  }

  Future<bool> getBool(String key) {
    return preferenceInstance.then((preference) {
      bool accessToken = preference.getBool(key);
      return Future.value(accessToken);
    });
  }

  Future setBool(String key, bool value) {
    return preferenceInstance.then((preference) {
      preference.setBool(key, value);
      return Future.value();
    });
  }

  Future<List<String>> getStringList(String key) {
    return preferenceInstance.then((preference) {
      List<String> searchText = preference.getStringList(key);
      return Future.value(searchText);
    });
  }

  Future setStringList(String key, List<String> value) {
    return preferenceInstance.then((preference) {
      preference.setStringList(key, value);
      return Future.value();
    });
  }

  Future<bool> get isSignIn => getBool("isSignIn");
  set isSignIn(value) => setBool("isSignIn", value);

  Future<bool> get isGps => getBool("isGps");
  set isGps(value) => setBool("isGps", value);
}
