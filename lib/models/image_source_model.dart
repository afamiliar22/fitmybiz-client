import 'dart:developer';

class ImageSourceModel {
  final String s;
  final String sm;
  final String m;
  final String lm;
  final String l;
  final String o;
  final int width;
  final int height;

  ImageSourceModel({this.s, this.sm, this.m, this.lm, this.l, this.o, this.width, this.height});

  factory ImageSourceModel.fromJson(Map<String, dynamic> json) {
    return ImageSourceModel(
      s: json['s'],
      sm: json['s_m'],
      m: json['m'],
      lm: json['l_m'],
      l: json['l'],
      o: json['o'],
      width: json['width'],
      height: json['height'],
    );
  }
}
