import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/map_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/map/map_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class MapCubit extends BaseCubit<MapState> {
  /// {@macro counter_cubit}
  MapCubit() : super(InitialState());

  String userNo;

  void init() {
    emit(InitialState());

    this.property.meCollectionSubject.listen((value) {
      this.getCollection();
    });
  }

  void userNoChanged(String userNo) {
    this.userNo = userNo;
  }

  Future getCollection() {
    String method = 'get';
    String path = '/api/v1/collection/${this.userNo}';

    return this.network.requestWithAuth(method, path, null).then((response) {
      List<CollectionModel> collections = (response.data['data'] as List)
          .map((record) => CollectionModel.fromJson(record))
          .toList();

      emit(GetCollection(true, collections));
      return Future.value();
    });
  }

  void getMaps(String collectionNo) {
    String method = 'get';
    String path = '/api/v1/map/collection/$collectionNo';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<MapModel> maps = (response.data as List)
          .map((record) => MapModel.fromJson(record))
          .toList();

      emit(GetCollectionMap(true, maps));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getUser() {
    String method = 'get';
    String path = '/api/v1/user/${this.userNo}';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      UserModel user = UserModel.fromJson(response.data);
      emit(GetUser(user));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getUserMaps() {
    String method = 'get';
    String path = '/api/v1/map/user/${this.userNo}';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<MapModel> maps = (response.data as List)
          .map((record) => MapModel.fromJson(record))
          .toList();

      emit(GetUserMap(true, maps));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getImageContents(String contentNo) {
    String method = 'get';
    String path = '/api/v1/content/image_content/list';
    Map<String, dynamic> data = {
      "content_no": contentNo,
      "more_field": "categories,contents"
    };
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      List<ContentsModel> contents = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();

      emit(GetImageContents(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getCategories() {
    String method = 'get';
    String path = '/api/v1/content/category/list';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<CategoryModel> category = (response.data['data'] as List)
          .map((record) => CategoryModel.fromJson(record))
          .toList();

      emit(GetCategory(true, category));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
