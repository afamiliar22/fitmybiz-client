import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_navigator.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/routes/views/report_view.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/firebase_client.dart';
import 'package:fitmybiz/tools/indicator_client.dart';
import 'package:fitmybiz/tools/launch_client.dart';
import 'package:fitmybiz/tools/property_client.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:package_info/package_info.dart';

class BaseRoute extends StatefulWidget {
  @override
  BaseRouteState createState() => BaseRouteState();
}

class BaseRouteState<T extends BaseRoute> extends State<T>
    with WidgetsBindingObserver {
  BaseNavigator navigator;
  IndicatorClient indicator = IndicatorClient.shared;
  PropertyClient property = PropertyClient.shared;
  FirebaseClient firebase = FirebaseClient.shared;
  LaunchClient launch = LaunchClient.shared;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance
        .addPostFrameCallback((_) => afterFirstLayout(context));
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      this.changeStatusBarColor();
    }
  }

  void changeStatusBarColor() {
    //Switcher 설명
    SystemChrome.setApplicationSwitcherDescription(
        ApplicationSwitcherDescription(
            primaryColor: ColorTheme.primary.hashCode, label: '찜'));

    // 상태바 색상 조절
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,
        statusBarBrightness: Brightness.light,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
      ),
    );
  }

  void afterFirstLayout(BuildContext context) {
    this.navigator = BaseNavigator(context);
    this.changeStatusBarColor();
    this.checkSystem();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }

  void checkSystem() {
    Future.wait([
      PackageInfo.fromPlatform(),
      this.firebase.database.reference().child('real-zzim').once()
    ]).then((results) {
      PackageInfo packageInfo = results[0];
      DataSnapshot snapshot = results[1];
      String platform = Platform.isIOS ? 'ios' : 'android';
      List<String> sourceVersionComponents = packageInfo.version.split('.');
      List<String> targetVersionComponents =
          snapshot.value['version'][platform].split('.');
      var emergency = snapshot.value['emergency'][platform];
      bool checkFirst = int.tryParse(sourceVersionComponents[0]) <
          int.tryParse(targetVersionComponents[0]);
      bool checkSecond = int.tryParse(sourceVersionComponents[1]) <
          int.tryParse(targetVersionComponents[1]);
      if (checkFirst || checkSecond) {
        this.showPersistentAlert(
          title: '업데이트 알림',
          content: '더 좋아진 찜 앱을 사용하시기 위해서는 새로운 버전으로 업데이트가 필요합니다.',
          onPressed: () {
            if (Platform.isIOS) {
              String url = "https://apps.apple.com/kr/app/id1451798919";
              this.launch.moveToBrowser(url);
            } else {
              String url =
                  "https://play.google.com/store/apps/details?id=com.example.fitmybiz";
              this.launch.moveToBrowser(url);
            }
          },
        );
      } else if (emergency['state'] == true) {
        String title = emergency['title'];
        String content = emergency['content'];
        this.showPersistentAlert(
          title: title,
          content: content,
          onPressed: () {
            exit(0);
          },
        );
      }
    });
  }

  void showPersistentAlert(
      {String title, String content, VoidCallback onPressed}) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return WillPopScope(
          onWillPop: () {
            // skip
            return Future.value(false);
          },
          child: Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            child: CustomContainer(
              padding: EdgeInsets.all(25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(title,
                      textAlign: TextAlign.center,
                      style: TextStyleTheme.common.copyWith(fontSize: 20)),
                  SizedBox(height: 24),
                  Text(content,
                      textAlign: TextAlign.center,
                      style: TextStyleTheme.common.copyWith(fontSize: 16)),
                  SizedBox(height: 32),
                  PrimaryButton(
                    text: Text('확인'),
                    onPressed: onPressed,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<bool> onWillPop() {
    return this.showConfirm(
        title: '종료 안내',
        description: '정말로 종료하시겠습니까?',
        positiveButtonText: '종료',
        negativeButtonText: '취소');
  }

  Future<Uint8List> showImagePicker() {
    return MultiImagePicker.pickImages(
      maxImages: 1,
      cupertinoOptions: CupertinoOptions(
        autoCloseOnSelectionLimit: true,
      ),
      materialOptions: MaterialOptions(
        statusBarColor: '#${Colors.black.value.toRadixString(16)}',
        actionBarColor: '#${Colors.black.value.toRadixString(16)}',
        actionBarTitleColor: '#${Colors.white.value.toRadixString(16)}',
        actionBarTitle: "앨범",
        allViewTitle: "전체",
        autoCloseOnSelectionLimit: true,
      ),
    ).then((assets) {
      return assets[0].getByteData().then((value) {
        Uint8List data = value.buffer.asUint8List();
        return Future.value(data);
      });
    });
  }

  void showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        backgroundColor: ColorTheme.black,
        textColor: Colors.white);
  }

  Future<bool> showConfirm(
      {String title,
      String description,
      String positiveButtonText,
      String negativeButtonText}) {
    return showModalBottomSheet(
      useRootNavigator: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      context: context,
      builder: (context) {
        return SafeArea(
          top: false,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 30),
              CustomContainer(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child:
                    Center(child: Text(title, style: TextStyleTheme.subtitle)),
              ),
              SizedBox(height: 10),
              CustomContainer(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Center(
                    child: Text(description,
                        style: TextStyleTheme.common
                            .copyWith(fontSize: 15, color: ColorTheme.black))),
              ),
              SizedBox(height: 30),
              Row(
                children: [
                  SizedBox(width: 25),
                  Expanded(
                    child: CustomContainer(
                      child: CustomButton(
                        text: Text(negativeButtonText),
                        padding: EdgeInsets.all(16),
                        borderRadius: 18,
                        backgroundColor: ColorTheme.grayPlaceholder,
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        },
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: CustomContainer(
                      child: CustomButton(
                        text: Text(positiveButtonText),
                        padding: EdgeInsets.all(16),
                        borderRadius: 18,
                        backgroundColor: ColorTheme.primary,
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                      ),
                    ),
                  ),
                  SizedBox(width: 25),
                ],
              ),
              SizedBox(height: 20),
            ],
          ),
        );
      },
    );
  }

  void showPlaceMoreSheetMe({VoidCallback onDelete, VoidCallback onUpdate}) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        actions: <Widget>[
          if (onUpdate != null)
            CupertinoActionSheetAction(
              child: Text('수정'),
              onPressed: () {
                Navigator.pop(context);
                onUpdate();
              },
            ),
          if (onDelete != null)
            CupertinoActionSheetAction(
              isDestructiveAction: true,
              child: Text('삭제'),
              onPressed: () {
                Navigator.pop(context);
                this
                    .showConfirm(
                        title: '정말 삭제하시겠습니까?',
                        description: '삭제하신 장소는 복구되지 않아요',
                        positiveButtonText: '삭제',
                        negativeButtonText: '취소')
                    .then((value) {
                  if (value == true) {
                    onDelete();
                  }
                });
              },
            ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('닫기'),
          isDefaultAction: true,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  void showPlaceMoreSheetOther({Function(String) onReport}) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        actions: <Widget>[
          CupertinoActionSheetAction(
            isDestructiveAction: true,
            child: Text('신고'),
            onPressed: () {
              Navigator.pop(context);
              this.navigator.presentRoute(ReportView()).then((value) {
                if (value == null) return;
                onReport(value);
              });
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('닫기'),
          isDefaultAction: true,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  void showCollectionMoreSheetMe({VoidCallback onDelete}) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        actions: <Widget>[
          CupertinoActionSheetAction(
            isDestructiveAction: true,
            child: Text('삭제'),
            onPressed: () {
              Navigator.pop(context);
              this
                  .showConfirm(
                      title: '정말 삭제하시겠습니까?',
                      description: '삭제하신 지도는 복구되지 않아요',
                      positiveButtonText: '삭제',
                      negativeButtonText: '취소')
                  .then((value) {
                if (value == true) {
                  // this.cubit.deleteCollection(collectionNo);
                  onDelete();
                }
              });
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('닫기'),
          isDefaultAction: true,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  void showCollectionMoreSheetOther({Function(String) onReport}) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        actions: <Widget>[
          CupertinoActionSheetAction(
            isDestructiveAction: true,
            child: Text('신고'),
            onPressed: () {
              Navigator.pop(context);
              this.navigator.presentRoute(ReportView()).then((value) {
                if (value == null) return;
                // this.cubit.insertCollectionReport(collectionNo, value);
                onReport(value);
              });
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('닫기'),
          isDefaultAction: true,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
