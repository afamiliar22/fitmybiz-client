import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_smart_refresher.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/user/user_page.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'follow_cubit.dart';
import 'follow_state.dart';

enum FollowType { user, follower, following }

class FollowPage extends StatelessWidget {
  final String userNo; //유저 검색일 경우 닉네임
  final FollowType type;

  FollowPage(this.userNo, this.type);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => FollowCubit(),
      child: _View(this.userNo, this.type),
    );
  }
}

class _View extends BaseRoute {
  final String userNo;
  final FollowType type;
  _View(this.userNo, this.type);
  @override
  _ViewState createState() => _ViewState(this.userNo, this.type);
}

class _ViewState extends BaseRouteState {
  String userNo;
  FollowType type;
  _ViewState(this.userNo, this.type);

  String titleText;
  FollowCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<FollowCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);

    if (this.type == FollowType.follower) {
      this.titleText = "팔로워";
      this.cubit.setUserNo(this.userNo, this.type);
    } else if (this.type == FollowType.following) {
      this.titleText = "팔로잉";
      this.cubit.setUserNo(this.userNo, this.type);
    } else {
      this.titleText = "유저 검색 결과";
      this.cubit.setNickname(this.userNo);
    }

    this.cubit.onRefresh();
  }

  List<String> areas = [];
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<FollowCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        title: Text(
          this.titleText ?? '',
          style: TextStyle(
              fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        body: SafeArea(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
            child: this.type == FollowType.user
                ? this.cubit.users == null
                    ? Container()
                    : this.cubit.users.length == 0
                        ? this.emptyView()
                        : RefreshConfiguration(
                            enableLoadingWhenNoData: false,
                            footerTriggerDistance: 200,
                            child: SmartRefresher(
                              controller: this.cubit.refreshController,
                              enablePullDown: true,
                              enablePullUp: true,
                              header: CustomSmartRefresher.customHeader(),
                              footer: CustomSmartRefresher.customFooter(),
                              onRefresh: this.cubit.onRefresh,
                              onLoading: this.cubit.onLoading,
                              child: ListView.builder(
                                itemBuilder: ((BuildContext c, int i) {
                                  return CustomContainer(
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    onPressed: () {
                                      this.navigator.pushRoute(
                                          UserPage(this.cubit.users[i].userNo));
                                    },
                                    child: Row(
                                      children: [
                                        this.cubit.users[i].profileImage == null
                                            ? Container(
                                                margin: EdgeInsets.only(
                                                    top: 8, bottom: 8),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(22),
                                                  child: Image.asset(
                                                    'assets/images/userProfileImg.png',
                                                    fit: BoxFit.cover,
                                                    height: 44,
                                                    width: 44,
                                                  ),
                                                ),
                                              )
                                            : Container(
                                                margin: EdgeInsets.only(
                                                    top: 8, bottom: 8),
                                                child: CustomAvatar(
                                                  size: 44,
                                                  image:
                                                      CustomCachedNetworkImage(
                                                    preloadImage: this
                                                        .cubit
                                                        .users[i]
                                                        .profileImage
                                                        .s,
                                                    image: this
                                                        .cubit
                                                        .users[i]
                                                        .profileImage
                                                        .s,
                                                  ),
                                                ),
                                              ),
                                        Container(
                                          margin: EdgeInsets.only(left: 8),
                                          child: Text(
                                            this.cubit.users[i].nickname,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Spacer(),
                                        this.cubit.selectedFollows[i]
                                            ? Container(
                                                child: CustomContainer(
                                                backgroundColor: Colors.white,
                                                borderRadius: [10, 10, 10, 10],
                                                borderWidth: 1,
                                                borderColor: Color.fromRGBO(
                                                    173, 181, 190, 1),
                                                child: Container(
                                                  margin: EdgeInsets.symmetric(
                                                      vertical: 8,
                                                      horizontal: 16),
                                                  child: Text(
                                                    "팔로잉",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            173, 181, 190, 1),
                                                        fontSize: 13),
                                                  ),
                                                ),
                                                onPressed: () {
                                                  this
                                                          .cubit
                                                          .selectedFollows[i] =
                                                      !this
                                                          .cubit
                                                          .selectedFollows[i];
                                                  setState(() {});
                                                  this.cubit.putUpdateFollow(
                                                      this
                                                          .cubit
                                                          .users[i]
                                                          .userNo);
                                                },
                                              ))
                                            : Container(
                                                child: CustomContainer(
                                                  backgroundColor:
                                                      Color.fromRGBO(
                                                          31, 31, 36, 1),
                                                  borderRadius: [
                                                    10,
                                                    10,
                                                    10,
                                                    10
                                                  ],
                                                  borderColor: Color.fromRGBO(
                                                      31, 31, 36, 1),
                                                  child: Container(
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 8,
                                                            horizontal: 16),
                                                    child: Text(
                                                      "팔로우",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 13),
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    this.cubit.selectedFollows[
                                                            i] =
                                                        !this
                                                            .cubit
                                                            .selectedFollows[i];
                                                    setState(() {});
                                                    this.cubit.putUpdateFollow(
                                                        this
                                                            .cubit
                                                            .users[i]
                                                            .userNo);
                                                  },
                                                ),
                                              ),
                                      ],
                                    ),
                                  );
                                }),
                                itemCount: this.cubit.users.length,
                              ),
                            ),
                          )
                : this.cubit.follows == null
                    ? Container()
                    : this.cubit.follows.length == 0
                        ? this.emptyView()
                        : RefreshConfiguration(
                            enableLoadingWhenNoData: false,
                            footerTriggerDistance: 200,
                            child: SmartRefresher(
                              controller: this.cubit.refreshController,
                              enablePullDown: true,
                              enablePullUp: true,
                              header: CustomSmartRefresher.customHeader(),
                              footer: CustomSmartRefresher.customFooter(),
                              onRefresh: this.cubit.onRefresh,
                              onLoading: this.cubit.onLoading,
                              child: ListView.builder(
                                itemBuilder: ((BuildContext c, int i) {
                                  return CustomContainer(
                                    onPressed: () {
                                      if (this.type == FollowType.follower) {
                                        this.navigator.pushRoute(UserPage(
                                            this.cubit.follows[i].userNo));
                                      } else if (this.type ==
                                          FollowType.following) {
                                        this.navigator.pushRoute(UserPage(this
                                            .cubit
                                            .follows[i]
                                            .followingUserNo));
                                      }
                                    },
                                    child: Row(
                                      children: [
                                        this.cubit.follows[i].profileImage ==
                                                null
                                            ? Container(
                                                margin: EdgeInsets.only(
                                                    top: 8, bottom: 8),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(22),
                                                  child: Image.asset(
                                                    'assets/images/userProfileImg.png',
                                                    fit: BoxFit.cover,
                                                    height: 44,
                                                    width: 44,
                                                  ),
                                                ),
                                              )
                                            : Container(
                                                margin: EdgeInsets.only(
                                                    top: 8, bottom: 8),
                                                child: CustomAvatar(
                                                  size: 44,
                                                  image:
                                                      CustomCachedNetworkImage(
                                                    preloadImage: this
                                                        .cubit
                                                        .follows[i]
                                                        .profileImage
                                                        .s,
                                                    image: this
                                                        .cubit
                                                        .follows[i]
                                                        .profileImage
                                                        .s,
                                                  ),
                                                ),
                                              ),
                                        Container(
                                          margin: EdgeInsets.only(left: 8),
                                          child: Text(
                                            this.cubit.follows[i].nickname,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Spacer(),
                                        this.cubit.selectedFollows[i]
                                            ? Container(
                                                child: CustomContainer(
                                                backgroundColor: Colors.white,
                                                borderRadius: [10, 10, 10, 10],
                                                borderWidth: 1,
                                                borderColor: Color.fromRGBO(
                                                    173, 181, 190, 1),
                                                child: Container(
                                                  margin: EdgeInsets.symmetric(
                                                      vertical: 6,
                                                      horizontal: 16),
                                                  child: Text(
                                                    "팔로잉",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            173, 181, 190, 1),
                                                        fontSize: 13),
                                                  ),
                                                ),
                                                onPressed: () {
                                                  this
                                                          .cubit
                                                          .selectedFollows[i] =
                                                      !this
                                                          .cubit
                                                          .selectedFollows[i];
                                                  setState(() {});
                                                  if (this.type ==
                                                      FollowType.follower) {
                                                    this.cubit.putUpdateFollow(
                                                        this
                                                            .cubit
                                                            .follows[i]
                                                            .userNo);
                                                  } else if (this.type ==
                                                      FollowType.following) {
                                                    this.cubit.putUpdateFollow(
                                                        this
                                                            .cubit
                                                            .follows[i]
                                                            .followingUserNo);
                                                  }
                                                },
                                              ))
                                            : Container(
                                                child: CustomContainer(
                                                  backgroundColor:
                                                      Color.fromRGBO(
                                                          31, 31, 36, 1),
                                                  borderRadius: [
                                                    10,
                                                    10,
                                                    10,
                                                    10
                                                  ],
                                                  borderColor: Color.fromRGBO(
                                                      31, 31, 36, 1),
                                                  child: Container(
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 6,
                                                            horizontal: 16),
                                                    child: Text(
                                                      "팔로우",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 13),
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    this.cubit.selectedFollows[
                                                            i] =
                                                        !this
                                                            .cubit
                                                            .selectedFollows[i];
                                                    setState(() {});
                                                    // this.cubit.putUpdateFollow(this.cubit.follows[i].followingUserNo);
                                                    if (this.type ==
                                                        FollowType.follower) {
                                                      this
                                                          .cubit
                                                          .putUpdateFollow(this
                                                              .cubit
                                                              .follows[i]
                                                              .userNo);
                                                    } else if (this.type ==
                                                        FollowType.following) {
                                                      this
                                                          .cubit
                                                          .putUpdateFollow(this
                                                              .cubit
                                                              .follows[i]
                                                              .followingUserNo);
                                                    }
                                                  },
                                                ),
                                              ),
                                      ],
                                    ),
                                  );
                                }),
                                itemCount: this.cubit.follows.length,
                              ),
                            ),
                          ),
          ),
        ),
      ),
    );
  }

  Widget emptyView() {
    String title = "";
    String desc = "";

    if (this.type == FollowType.follower) {
      title = "팔로잉 리스트가 없어요.";
      desc = "";
    } else if (this.type == FollowType.following) {
      title = "팔로잉 리스트가 없어요.";
      desc = "나와 비슷한 취향의 유저를 찾아 팔로우해보세요.";
    } else {}

    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 100),
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              desc,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
