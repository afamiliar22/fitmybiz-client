import 'package:carousel_slider/carousel_slider.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/point.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

class SurveyInfoValidatePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;
  bool isVideo = false;
  int _current = 0;

  @override
  void initState() {
    super.initState();
  }

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      top: true,
      child: PlatformScaffold(
        leading: CustomContainer(
          onPressed: () {
            this.navigator.popRoute(null);
          },
          borderRadius: [6, 6, 6, 6],
          child: Image.asset(
            'assets/images/icnsBack.png',
            width: 26,
            height: 26,
          ),
        ),
        title: Text('패널 정보입력'),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 38),
          child: Column(
            children: [
              SizedBox(
                height: 33,
              ),
              Text('본인인증을 진행해주세요'),
            ],
          ),
        ),
      ),
    );
  }
}
