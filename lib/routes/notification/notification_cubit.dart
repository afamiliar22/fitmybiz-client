import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/alarm_model.dart';
import 'package:fitmybiz/tools/facebook_client.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'notification_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class NotificationCubit extends BaseCubit<NotificationState> {
  /// {@macro counter_cubit}
  NotificationCubit() : super(InitialState());

  List<bool> selectedFollows = [];
  List<AlarmModel> alarms;

  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  int page = 1;
  String userNo = '';

  void init() {
    emit(InitialState());
  }

  void setUSerNo(String userNo) {
    this.userNo = userNo;
  }

  onRefresh() {
    this.page = 1;
    this.selectedFollows.clear();
    this.getNotification().then((alarms) {
      this.alarms = alarms;
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    }).catchError((e) {
      this.alarms = [];
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    });
  }

  onLoading() {
    this.page = this.page + 1;
    this.getNotification().then((alarms) {
      this.alarms.addAll(alarms);
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      if (alarms.length == 0) {
        this.refreshController.loadNoData();
      } else {
        this.refreshController.loadComplete();
      }
    }).catchError((e) {
      this.refreshController.loadComplete();
    });
  }

  Future<List<AlarmModel>> getNotification() {
    String method = 'get';
    String path = '/api/v1/alarm/list';
    Map<String, dynamic> data = {
      'target_user_no': this.userNo,
      'page': this.page.toString(),
      'take': '10'
    };
    // emit(LoadingState());
    return this.network.requestWithAuth(method, path, data).then((response) {
      List<AlarmModel> result = (response.data['data'] as List)
          .map((record) => AlarmModel.fromJson(record))
          .toList();

      for (int i = 0; i < result.length; i += 1) {
        this.selectedFollows.add(result[i].sendUserInfo.isFollow);
      }

      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value(result);
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void putUpdateFollow(String userNo) {
    String method = 'put';
    String path = '/api/v1/user/$userNo/follow';

    // emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      // this.follow = FollowModel.fromJson(response.data);
      this.property.meProfileSubject.add(true);
      this.facebook.addEvent(FacebookEventType.following);
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
