import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'update_place_cubit.dart';
import 'update_place_state.dart';

class UpdatePlacePage extends StatelessWidget {
  final String imageContentNo;
  UpdatePlacePage(this.imageContentNo);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => UpdatePlaceCubit(),
      child: _View(this.imageContentNo),
    );
  }
}

class _View extends BaseRoute {
  final String imageContentNo;
  _View(this.imageContentNo);

  @override
  _ViewState createState() => _ViewState(this.imageContentNo);
}

class _ViewState extends BaseRouteState {
  String imageContentNo;
  _ViewState(this.imageContentNo);

  final FocusNode focusNode = FocusNode();

  WebViewController webViewController;
  UpdatePlaceCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<UpdatePlaceCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.imageContentNoChanged(this.imageContentNo);
  }

  @override
  Widget build(BuildContext context) {
    this.indicator.hide();
    return BlocListener(
      cubit: context.bloc<UpdatePlaceCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case FailedLoadState:
            this.indicator.hide();
            this.showToast('정보를 가져올 수 없습니다.');
            this.navigator.popRoute(null);
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case LoadedState:
            this.indicator.hide();
            setState(() {});
            break;
          case CompletedState:
            this.indicator.hide();
            this.navigator.popRoute(true);
            setState(() {});
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: this.cubit.contents != null ? this.buildScaffold() : Container(),
    );
  }

  Widget buildScaffold() {
    Color focusedColor = ColorTheme.primary;
    Color unfocusedColor = Color.fromRGBO(205, 210, 216, 1.0);

    return PlatformScaffold(
      onPressed: () {
        FocusScope.of(context).unfocus();
      },
      body: SafeArea(
        child: Column(
          children: [
            CustomContainer(
              padding: EdgeInsets.symmetric(vertical: 14, horizontal: 25),
              child: Row(
                children: [
                  Text(
                    '장소 수정',
                    style: TextStyleTheme.subtitle,
                  ),
                  Spacer(),
                  GestureDetector(
                    child: Image.asset('assets/images/icnsClose.png',
                        color: Colors.black, width: 28, height: 28),
                    onTap: () {
                      this.navigator.popRoute(null);
                    },
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                children: [
                  Row(
                    children: [
                      CustomContainer(
                        child: CustomAvatar(
                          image: CustomCachedNetworkImage(
                            preloadImage: this.cubit.contents.imageSrc.sm,
                            image: this.cubit.contents.imageSrc.sm,
                          ),
                          size: 60,
                          radius: 12,
                        ),
                      ),
                      SizedBox(width: 16),
                      Flexible(
                        child: Text(
                          this.cubit.contents.title,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyleTheme.common.copyWith(
                              fontSize: 15,
                              color: Color.fromRGBO(31, 31, 36, 1.0),
                              fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 40),
                  Text('카테고리', style: TextStyleTheme.textFieldLabel),
                  SizedBox(height: 8),
                  CustomContainer(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(this
                            .cubit
                            .contents
                            .categories
                            .map((e) => e.categoryName)
                            .join(', ')),
                        SizedBox(height: 8),
                        Divider(
                            height: 0.5,
                            thickness: 0.5,
                            color: ColorTheme.grayPlaceholder),
                      ],
                    ),
                  ),
                  SizedBox(height: 40),
                  Text('메모', style: TextStyleTheme.textFieldLabel),
                  SizedBox(height: 16),
                  CustomContainer(
                    padding: EdgeInsets.all(20),
                    borderRadius: [12, 12, 12, 12],
                    backgroundColor:
                        Color.fromRGBO(244, 246, 248, 1.0).withOpacity(0.5),
                    child: CupertinoTextField(
                      controller: cubit.descriptionEditingController,
                      padding: EdgeInsets.zero,
                      placeholder: '이 장소에 대해 더 알려주세요',
                      decoration: BoxDecoration(),
                      minLines: 3,
                      maxLines: 3,
                      onChanged: (text) => this.cubit.descriptionChanged(text),
                    ),
                  ),
                  SizedBox(height: 40),
                  Row(
                    children: [
                      Text('공개 범위', style: TextStyleTheme.textFieldLabel),
                      Spacer(),
                      CustomButton(
                        borderRadius: 10,
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 7),
                        borderColor: this.cubit.secret == false
                            ? focusedColor
                            : unfocusedColor,
                        text: Text('전체 공개',
                            style: TextStyleTheme.common.copyWith(
                                fontSize: 13,
                                color: this.cubit.secret == false
                                    ? focusedColor
                                    : unfocusedColor)),
                        onPressed: () => this.cubit.secretChanged(false),
                      ),
                      SizedBox(width: 8),
                      CustomButton(
                        borderRadius: 10,
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 7),
                        borderColor: this.cubit.secret == true
                            ? focusedColor
                            : unfocusedColor,
                        text: Text('나만 보기',
                            style: TextStyleTheme.common.copyWith(
                                fontSize: 13,
                                color: this.cubit.secret == true
                                    ? focusedColor
                                    : unfocusedColor)),
                        onPressed: () => this.cubit.secretChanged(true),
                      ),
                    ],
                  ),
                  SizedBox(height: 40),
                  CustomContainer(
                    backgroundColor: Colors.white,
                    child: PrimaryButton(
                      text: Text('완료'),
                      onPressed: () => this.cubit.requestUpdateContent(),
                    ),
                  ),
                  SizedBox(height: 16),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
