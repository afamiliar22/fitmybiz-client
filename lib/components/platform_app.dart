import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

class PlatformApp extends StatelessWidget {
  final BuildContext context;
  final Widget home;
  final Widget Function(BuildContext, Widget) builder;
  final Iterable<LocalizationsDelegate<dynamic>> localizationDelegates;
  final Iterable<Locale> supportedLocales;
  final List<NavigatorObserver> navigatorObservers;
  final Locale locale;

  PlatformApp(
      {this.context,
      this.localizationDelegates,
      this.supportedLocales,
      this.navigatorObservers,
      this.locale,
      this.home,
      this.builder});

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      return MaterialApp(
        navigatorObservers: this.navigatorObservers,
        debugShowCheckedModeBanner: false,
        localizationsDelegates: this.localizationDelegates,
        supportedLocales: this.supportedLocales,
        locale: this.locale,
        theme: ThemeData(
          accentColor: Colors.black,
          brightness: Brightness.light,
          primaryTextTheme:
              TextTheme(headline6: TextStyle(color: Colors.black)),
          appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            color: Colors.white,
            centerTitle: true,
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            elevation: 0,
          ),
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          scaffoldBackgroundColor: Colors.white,
        ),
        builder: this.builder,
        home: this.home,
      );
    } else {
      return CupertinoApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: this.localizationDelegates,
        supportedLocales: this.supportedLocales,
        locale: this.locale,
        theme: CupertinoThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.black,
        ),
        builder: this.builder,
        home: this.home,
      );
    }
  }
}
