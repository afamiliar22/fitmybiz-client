import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/themes/themes.dart';

class InsertPlaceSheet extends StatelessWidget {
  final VoidCallback onPressedLink;
  final VoidCallback onPressedPhoto;

  InsertPlaceSheet({this.onPressedLink, this.onPressedPhoto});

  @override
  Widget build(BuildContext context) {
    return CustomContainer(
      borderRadius: [30, 30, 0, 0],
      backgroundColor: Colors.white,
      child: SafeArea(
        top: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            /*
            CustomContainer(
              padding: EdgeInsets.all(25),
              height: 72,
              child: Row(
                children: [
                  Text('장소 추가', style: TextStyleTheme.subtitle),
                  Spacer(),
                  GestureDetector(
                    child: Image.asset('assets/images/icnsClose.png', color: Colors.black, width: 28, height: 28),
                    onTap: () => Navigator.of(context).pop(),
                  )
                ],
              ),
            ),
            */
            SizedBox(
              height: 10,
            ),
            CustomContainer(
              height: 60,
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, right: 25, left: 22),
              child: Row(
                children: [
                  Image.asset(
                    'assets/images/icnsLinedLink.png',
                    color: Colors.black,
                    width: 24,
                    height: 24,
                  ),
                  SizedBox(width: 8),
                  Text('링크로 추가하기',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 16, fontWeight: FontWeight.w600)),
                  Spacer(),
                  Image.asset('assets/images/icnsNext.png',
                      color: Colors.grey, width: 28, height: 28)
                ],
              ),
              onPressed: this.onPressedLink,
            ),
            CustomContainer(
              height: 60,
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, right: 25, left: 22),
              child: Row(
                children: [
                  Image.asset(
                    'assets/images/icnsCamera.png',
                    color: Colors.black,
                    width: 24,
                    height: 24,
                  ),
                  SizedBox(width: 8),
                  Text('직접 추가하기',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 16, fontWeight: FontWeight.w600)),
                  Spacer(),
                  Image.asset('assets/images/icnsNext.png',
                      color: Colors.grey, width: 28, height: 28)
                ],
              ),
              onPressed: this.onPressedPhoto,
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }
}
