class NoticeModel {
  String contentType;
  int participantCount;
  String title;
  String subtitle;
  int createdAt;
  String startDate;
  String endDate;

  NoticeModel(
      {this.contentType,
      this.participantCount,
      this.title,
      this.subtitle,
      this.createdAt,
      this.startDate,
      this.endDate});

  NoticeModel.fromJson(Map<String, dynamic> json) {
    contentType = json['content_type'];
    participantCount = json['participant_count'];
    title = json['title'];
    subtitle = json['subtitle'];
    createdAt = json['createdAt'];
    startDate = json['startDate'];
    endDate = json['endDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content_type'] = this.contentType;
    data['participant_count'] = this.participantCount;
    data['title'] = this.title;
    data['subtitle'] = this.subtitle;
    data['createdAt'] = this.createdAt;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    return data;
  }
}
