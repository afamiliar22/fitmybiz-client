import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/notice_model.dart';
import 'package:fitmybiz/extensions/extensions.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'notice_cubit.dart';
import 'notice_detail/notice_detail_page.dart';
import 'notice_state.dart';

class NoticePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => NoticeCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  NoticeCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<NoticeCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<NoticeCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case LoadedState:
            this.indicator.hide();
            setState(() {});
            break;
          case CompletedState:
            this.indicator.hide();
            Navigator.popUntil(context, (route) => route.isFirst);
            setState(() {});
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        title: Text('공지사항'),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        body: SafeArea(
          child: ListView.builder(
            itemBuilder: (context, index) {
              NoticeModel notice = this.cubit.notices[index];
              return Column(
                children: [
                  CustomContainer(
                    onPressed: () {
                      this.navigator.pushRoute(NoticeDetailPage(notice));
                    },
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              notice.noticeTitle,
                              style:
                                  TextStyleTheme.common.copyWith(fontSize: 15),
                            ),
                            SizedBox(height: 5),
                            Text(
                              notice.createdAt.toDateString() +
                                  ' ' +
                                  notice.createdAt.toTimeString(),
                              style: TextStyleTheme.common.copyWith(
                                fontSize: 13,
                                color: Color.fromRGBO(173, 181, 190, 1.0),
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        Image.asset('assets/images/icnsNext.png',
                            color: Colors.grey, width: 28, height: 28),
                      ],
                    ),
                  ),
                  Divider(
                      height: 0.5, thickness: 0.5, indent: 25, endIndent: 25),
                ],
              );
            },
            itemCount: this.cubit.notices.length,
          ),
        ),
      ),
    );
  }
}
