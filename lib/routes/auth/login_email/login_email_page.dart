import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_text_field2.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/routes/auth/forgot_password/forgot_password_page.dart';
import 'package:fitmybiz/routes/auth/sign_email/sign_email_page.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/auth/term/term_page.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'login_email_cubit.dart';
import 'login_email_state.dart';

class LoginEmailPage extends StatelessWidget {
  const LoginEmailPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LoginEmailCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  LoginEmailCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<LoginEmailCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case SignedState:
            this.indicator.hide();
            context.bloc<LoginEmailCubit>().checkUserTerm();
            break;
          case CheckedTermState:
            this.indicator.hide();
            if (state.result == true) {
              this.property.isSignIn = true;
              this.navigator.resetRoute(SplashPage());
            } else {
              this.navigator.pushRoute(TermPage(state.terms));
            }

            break;
        }
      },
      child: PlatformScaffold(
        title: Text('로그인'),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              Text('이메일', style: TextStyleTheme.textFieldLabel),
              SizedBox(height: 8),
              CustomTextField2(
                placeholder: "이메일을 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                inputType: TextInputType.emailAddress,
                onChanged: (email) => this.cubit.emailChanged(email),
              ),
              SizedBox(height: 40),
              Text('비밀번호', style: TextStyleTheme.textFieldLabel),
              SizedBox(height: 8),
              CustomTextField2(
                placeholder: "비밀번호를 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                secure: true,
                onChanged: (password) => this.cubit.passwordChanged(password),
              ),
              SizedBox(height: 90),
              PrimaryButton(
                text: Text('로그인'),
                onPressed: () => this.cubit.login(),
              ),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    child: Text('회원가입',
                        style: TextStyleTheme.common
                            .copyWith(fontSize: 13, color: ColorTheme.grayLv1)),
                    onTap: () => this.navigator.pushRoute(SignEmailPage()),
                  ),
                  SizedBox(width: 8),
                  Container(width: 0.5, height: 12, color: Colors.grey),
                  SizedBox(width: 8),
                  GestureDetector(
                    child: Text('비밀번호를 잊어버리셨나요?',
                        style: TextStyleTheme.common
                            .copyWith(fontSize: 13, color: ColorTheme.primary)),
                    onTap: () => this.navigator.pushRoute(ForgotPasswordPage()),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
