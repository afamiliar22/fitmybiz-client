import 'dart:convert';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/blog_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/google_place_model.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class PlaceDetailCubit extends BaseCubit<PlaceDetailState> {
  /// {@macro counter_cubit}
  PlaceDetailCubit() : super(InitialState());
  List<FollowModel> follows = [];

  FollowModel follow = FollowModel();
  void init() {
    emit(InitialState());
  }

  void getImageContents(Map<String, dynamic> data) {
    String method = 'get';
    String path = '/api/v1/content/image_content/list';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      log(response.data['data'].toString());
      this.facebook.addEvent(FacebookEventType.viewImageContent);
      List<ContentsModel> contents = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();
      print('contentInfo: ' + contents[0].contentInfo.googlePlaceId.toString());
      getGoogleReview(contents[0].contentInfo.googlePlaceId ?? '');
      getBlogContents(contents[0].title ?? '');

      emit(GetContents(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getGoogleReview(String pid) {
    String method = 'get';
    String path = '/api/v1/google_map/place/search/detail?place_id=$pid';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      log(response.data['result'].toString());

      GooglePlaceModel contents =
          GooglePlaceModel.fromJson(response.data['result']);

      emit(GetGoogleReview(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getBlogContents(String title) {
    String method = 'get';
    String path = '/api/v1/kakao/blog/search?query=$title';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      log(response.data['documents'].toString());
      //this.facebook.addEvent(FacebookEventType.viewImageContent);
      List<BlogModel> contents = (response.data['documents'] as List)
          .map((record) => BlogModel.fromJson(record))
          .toList();

      emit(GetBlogContents(true, contents.sublist(0, 10)));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void putUpdateFollow(String userNo) {
    String method = 'put';
    String path = '/api/v1/user/$userNo/follow';

    // emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      this.property.meProfileSubject.add(true);
      this.facebook.addEvent(FacebookEventType.following);
      this.follow = FollowModel.fromJson(response.data);

      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void insertImageContentReport(String imageContentNo, String description) {
    String method = 'post';
    String path = '/api/v1/report/image_content/$imageContentNo';
    Map<String, dynamic> data = {
      'report_title': '신고하기',
      'report_description': description
    };

    this.network.requestWithAuth(method, path, data).then((response) {
      emit(ToastState('신고되었습니다.'));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void deleteImageContent(String imageContentNo) {
    String method = 'delete';
    String path = '/api/v1/content/image_content/$imageContentNo';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      this.property.meCollectionSubject.add(true);
      emit(ToastState('콘텐츠가 삭제되었습니다.'));
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
