import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/link_data_model.dart';
import 'package:fitmybiz/tools/network_client.dart';

abstract class InsertPlaceState extends Equatable {}

class InitialState extends InsertPlaceState {
  @override
  List<Object> get props => [];
}

class SetState extends InsertPlaceState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadedState extends InsertPlaceState {
  @override
  List<Object> get props => [];
}

class ToastState extends InsertPlaceState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class CompletedState extends InsertPlaceState {
  final NetworkResponse response;

  CompletedState(this.response);

  @override
  List<Object> get props => [];
}

class LoadingState extends InsertPlaceState {
  @override
  List<Object> get props => [];
}

class LoadedLinkImageState extends InsertPlaceState {
  LoadedLinkImageState(this.linkData);

  final LinkDataModel linkData;

  @override
  List<Object> get props => [linkData];
}

class ErrorState extends InsertPlaceState {
  @override
  List<Object> get props => [];
}
