import 'dart:developer';

class LinkDataModel {
  final String destinationUrl;
  final String title;
  final String description;
  final List<String> images;

  LinkDataModel({this.destinationUrl, this.title, this.images, this.description});

  factory LinkDataModel.fromJson(Map<String, dynamic> json) {
    List<String> images = json.containsKey('item') ? (json['item'] as List).map((value) => value.toString()).toList() : [];

    return LinkDataModel(
      destinationUrl: json['destination_url'],
      title: json['title'],
      description: json['description'],
      images: images,
    );
  }
}
