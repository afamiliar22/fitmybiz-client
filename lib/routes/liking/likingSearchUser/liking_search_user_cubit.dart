import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

import 'liking_search_user_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class LikingSearchUserCubit extends BaseCubit<LikingSearchUserState> {
  /// {@macro counter_cubit}
  LikingSearchUserCubit() : super(InitialState());

  List<bool> selectedFollows = [];
  FollowModel follow = FollowModel();

  List<UserModel> users = [];
  void init() {
    emit(InitialState());
  }

  Future getSearchUser(String name, String take) {
    String method = 'get';
    String path = '/api/v1/user/search';
    Map<String, dynamic> data = {"nickname": name, 'take': take};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      users = (response.data['data'] as List)
          .map((record) => UserModel.fromJson(record))
          .toList();

      this.selectedFollows.clear();
      for (int i = 0; i < this.users.length; i += 1) {
        this.selectedFollows.add(this.users[i].isFollow);
      }

      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value();
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void putUpdateFollow(String userNo) {
    String method = 'put';
    String path = '/api/v1/user/$userNo/follow';

    // emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      this.follow = FollowModel.fromJson(response.data);
      this.property.meProfileSubject.add(true);
      this.facebook.addEvent(FacebookEventType.following);
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
