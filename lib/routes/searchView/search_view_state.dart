import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/keyword_model.dart';
import 'package:fitmybiz/models/user_model.dart';

abstract class SearchViewState extends Equatable {}

class InitialState extends SearchViewState {
  @override
  List<Object> get props => [];
}

class LoadingState extends SearchViewState {
  @override
  List<Object> get props => [];
}

class ErrorState extends SearchViewState {
  @override
  List<Object> get props => [];
}

class GetKeywords extends SearchViewState {
  GetKeywords(this.result, this.keywords);

  final bool result;
  final List<KeywordModel> keywords;

  @override
  List<Object> get props => [result, keywords];
}

class GetRecommendUser extends SearchViewState {
  GetRecommendUser(this.result, this.recommendUsers);

  final bool result;
  final List<UserModel> recommendUsers;

  @override
  List<Object> get props => [result, recommendUsers];
}

class GetLikingContents extends SearchViewState {
  GetLikingContents(this.result, this.contents);

  final bool result;
  final List<ContentsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class GetSearchImageContent extends SearchViewState {
  GetSearchImageContent(this.result, this.contents);

  final bool result;
  final List<ContentsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class GetSearchUser extends SearchViewState {
  GetSearchUser(this.result, this.users);

  final bool result;
  final List<UserModel> users;

  @override
  List<Object> get props => [result, users];
}

class PutUpdateFollow extends SearchViewState {
  PutUpdateFollow(this.result, this.follow);

  final bool result;
  final FollowModel follow;

  @override
  List<Object> get props => [result, follow];
}

class SetState extends SearchViewState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}
