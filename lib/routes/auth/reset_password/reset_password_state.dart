import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';

abstract class ResetPasswordState extends Equatable {}

class InitialState extends ResetPasswordState {
  @override
  List<Object> get props => [];
}

class SetState extends ResetPasswordState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadingState extends ResetPasswordState {
  @override
  List<Object> get props => [];
}

class ToastState extends ResetPasswordState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class CompletedState extends ResetPasswordState {
  @override
  List<Object> get props => [];
}

class ErrorState extends ResetPasswordState {
  @override
  List<Object> get props => [];
}
