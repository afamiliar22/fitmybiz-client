import 'dart:developer';

import 'package:fitmybiz/models/image_source_model.dart';

import 'contents_model.dart';

class UserModel {
  final String userNo;
  final String userUuid;
  final String email;
  final String nickname;
  final ImageSourceModel profileImage;
  final String introduce;
  final String followerCount;
  final String followingCount;
  final String imageContentCount;
  final bool isFollow;
  final String collectionCount;
  final List<ContentsModel> imageContent;
  final String state;

  UserModel(
      {this.userNo,
      this.userUuid,
      this.email,
      this.nickname,
      this.profileImage,
      this.introduce,
      this.followerCount,
      this.followingCount,
      this.imageContentCount,
      this.isFollow,
      this.collectionCount,
      this.imageContent,
      this.state});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    List<ContentsModel> a = json.containsKey('image_content')
        ? (json['image_content'] as List)
            .map((value) => ContentsModel.fromJson(value))
            .toList()
        : [];
    return UserModel(
        userNo: json['user_no'].toString(),
        userUuid: json['user_uuid'],
        email: json['email'],
        nickname: json['nickname'],
        profileImage:
            json.containsKey('profile_image') && json['profile_image'] != null
                ? ImageSourceModel.fromJson(json['profile_image'])
                : null,
        introduce: json['introduce'],
        followerCount: json['follower_count'].toString(),
        followingCount: json['following_count'].toString(),
        imageContentCount: json['image_content_count'].toString(),
        isFollow: json['is_follow'],
        collectionCount: json['collection_count'].toString(),
        imageContent: a,
        state: json['state'].toString());
  }
}
