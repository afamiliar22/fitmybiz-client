import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:io' show Platform;
import 'dart:convert';

class FirebaseClient {
  FirebaseClient._initialize() {
    print('AuthClient Initialize');
  }
  static final FirebaseClient _instance = FirebaseClient._initialize();

  static FirebaseClient get shared => _instance;

  final FirebaseMessaging messaging = FirebaseMessaging();
  final FirebaseDatabase database = FirebaseDatabase();

  Future<String> generateDynamicLink(
      {String path, String title, String description, String image}) {
    DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://deeplink.zzieut.com',
      link: Uri.parse('https://www.zzieut.net$path'),
      androidParameters: AndroidParameters(
        packageName: 'com.example.fitmybiz',
      ),
      iosParameters: IosParameters(
        bundleId: 'com.example.fitmybiz',
        appStoreId: '1451798919',
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
        title: title ?? '찜',
        description: description,
        imageUrl: image != null ? Uri.parse(image) : null,
      ),
      navigationInfoParameters: NavigationInfoParameters(
        forcedRedirectEnabled: true,
      ),
    );
    return parameters.buildUrl().then((dynamicLinkUri) {
      return DynamicLinkParameters.shortenUrl(
        dynamicLinkUri,
        DynamicLinkParametersOptions(),
      ).then((result) {
        return Future.value(result.shortUrl.toString());
      });
    });
  }

  void requestNotificationPermission() {
    messaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    messaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future<String> getMessagingToken() {
    return messaging.getToken();
  }

  convertPayloadToData(message) {
    var data = message;
    if (Platform.isAndroid) {
      if (message.containsKey('data')) {
        data = message['data'];
      } else {
        data = {};
      }
    }
    return data;
  }

  void configureNotification(Function(dynamic) onSelectNotification) {
    messaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
        FirebaseClient.showLocalNotification(message);
        return Future.value();
      },
      // onBackgroundMessage:
      // Platform.isIOS ? null : FirebaseClient.backgroundMessageHandler,
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
        var data = convertPayloadToData(message);
        onSelectNotification(data);
        return Future.value();
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
        var data = convertPayloadToData(message);
        onSelectNotification(data);
        return Future.value();
      },
    );

    var initAndroidSetting =
        AndroidInitializationSettings('@drawable/ic_notify');
    var initIOSSetting = IOSInitializationSettings();
    var initSetting = InitializationSettings(
        android: initAndroidSetting, iOS: initIOSSetting);

    FlutterLocalNotificationsPlugin().initialize(initSetting,
        onSelectNotification: (String payload) {
      if (payload == null) {
        return Future.value();
      }
      var message = json.decode(payload);
      var data = convertPayloadToData(message);
      onSelectNotification(data);
      return Future.value();
    });
  }

  static NotificationDetails platformOptions() {
    var android = AndroidNotificationDetails(
        'ZZIM_CHANNEL_ID', 'ZZIM_CHANNEL_NAME', 'ZZIM_CHANNEL_DESCRIPTION');
    var iOS = IOSNotificationDetails();
    return NotificationDetails(android: android, iOS: iOS);
  }

  static showLocalNotification(Map<String, dynamic> payload) {
    if (payload.containsKey('aps') && payload['aps'].containsKey('alert')) {
      var title = payload['aps']['alert']['title'];
      var body = payload['aps']['alert']['body'];
      var key = (DateTime.now().millisecondsSinceEpoch / 1000).round();
      FlutterLocalNotificationsPlugin().show(
          key, title, body, platformOptions(),
          payload: json.encode(payload));
    } else if (payload.containsKey('notification')) {
      var title = payload['notification']['title'];
      var body = payload['notification']['body'];
      var key = (DateTime.now().millisecondsSinceEpoch / 1000).round();
      FlutterLocalNotificationsPlugin().show(
          key, title, body, platformOptions(),
          payload: json.encode(payload));
    }
  }
}
