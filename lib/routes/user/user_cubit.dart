import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/user/user_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class UserCubit extends BaseCubit<UserState> {
  /// {@macro counter_cubit}
  UserCubit() : super(InitialState());

  List<CollectionModel> collections = [];
  UserModel user;
  String userNo;
  FollowModel follow = FollowModel();
  bool selectedFollow = false;
  void init() {
    emit(InitialState());
  }

  void userNoChanged(String userNo) {
    this.userNo = userNo;
    emit(LoadingState());
    this.getUser().then((value) {
      return this.getCollectionSaved();
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(DeletedUserState());
      emit(ErrorState());
    });
  }

  Future getUser() {
    String method = 'get';
    String path = '/api/v1/user/${this.userNo}';

    return this.network.requestWithAuth(method, path, null).then((response) {
      this.user = UserModel.fromJson(response.data);
      this.selectedFollow = user.isFollow;
      return Future.value();
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    });
  }

  Future getCollectionSaved() {
    String method = 'get';
    String path = '/api/v1/collection/${this.userNo}';

    return this.network.requestWithAuth(method, path, null).then((response) {
      this.collections = (response.data['data'] as List)
          .map((record) => CollectionModel.fromJson(record))
          .toList();
      return Future.value();
    });
  }

  void postCollection(String name) {
    String method = 'post';
    String path = '/api/v1/collection';
    Map data = {"name": name, "secret": false};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.facebook.addEvent(FacebookEventType.addMap);
      return this.getCollectionSaved();
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void deleteCollection(String collectionNo) {
    String method = 'delete';
    String path = '/api/v1/collection/delete';
    Map<String, dynamic> data = {"collection_no": collectionNo};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      emit(ToastState('지도가 삭제되었습니다.'));
      return this.getCollectionSaved();
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void putUpdateFollow(String userNo) {
    String method = 'put';
    String path = '/api/v1/user/$userNo/follow';

    // emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      this.property.meProfileSubject.add(true);
      this.facebook.addEvent(FacebookEventType.following);
      this.follow = FollowModel.fromJson(response.data);

      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void insertCollectionReport(String collectionNo, String description) {
    String method = 'post';
    String path = '/api/v1/report/collection/$collectionNo';
    Map<String, dynamic> data = {
      'report_title': '신고하기',
      'report_description': description
    };

    this.network.requestWithAuth(method, path, data).then((response) {
      emit(ToastState('신고되었습니다.'));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
