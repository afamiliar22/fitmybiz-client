import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/collection_item_model.dart';
import 'edit_collection_items_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class EditCollectionItemsCubit extends BaseCubit<EditCollectionItemsState> {
  /// {@macro counter_cubit}
  EditCollectionItemsCubit() : super(InitialState());

  String collectionNo;
  List<CollectionItemModel> collectionItems = [];
  Set<String> selectedCollectionItems = {};
  void init() {
    // {{host}}/api/v1/link/image?source_url=https://blog.naver.com/hjungeun27/222133877111
  }

  void collectionNoChanged(String collectionNo) {
    this.collectionNo = collectionNo;
  }

  void collectionItemsChanged(List<CollectionItemModel> collectionItems) {
    this.collectionItems = collectionItems;
  }

  void deleteCollectionItem(String collectionNo, String collectionItemNo) {
    String method = 'delete';
    String path = '/api/v1/collection/$collectionNo/item/delete';
    Map<String, dynamic> data = {"collection_item_no": collectionItemNo};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.property.meCollectionSubject.add(true);
      emit(ToastState('장소가 삭제되었습니다.'));
      emit(CompletedState());
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void copyCollectionItem(String collectionNo, String imageContentNo) {
    String method = 'post';
    String path = '/api/v1/collection/$collectionNo/copy/item';
    Map<String, dynamic> data = {"image_content_no": imageContentNo};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.property.meCollectionSubject.add(true);
      emit(ToastState('장소가 복사되었습니다.'));
      emit(CompletedState());
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
