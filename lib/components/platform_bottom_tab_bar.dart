import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

class PlatformBottomTabBar extends StatefulWidget {
  final List<Widget> children;
  final List<Image> tabs;
  final List<Image> tabs2;
  final Color selectedColor;
  PlatformBottomTabBar({this.children, this.tabs, this.tabs2, this.selectedColor});

  @override
  PlatformBottomTabBarState createState() => PlatformBottomTabBarState(this.children, this.tabs, this.tabs2, this.selectedColor);
}

class PlatformBottomTabBarState extends State<PlatformBottomTabBar> {
  List<Widget> children;
  List<Image> tabs;
  List<Image> tabs2;
  Color selectedColor;

  PlatformBottomTabBarState(this.children, this.tabs, this.tabs2, this.selectedColor);

  int currentIndex = 0;
  List<GlobalKey<NavigatorState>> tabBarKeys;

  @override
  void initState() {
    super.initState();
    this.tabBarKeys = List<GlobalKey<NavigatorState>>.generate(tabs.length, (index) => GlobalKey<NavigatorState>());
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      List<BottomNavigationBarItem> iosTabs = this.tabs.asMap().entries.map((record) {
        int index = record.key;

        Widget image = ColorFiltered(
          child: record.value,
          colorFilter: ColorFilter.mode(index == this.currentIndex ? selectedColor : Colors.grey, BlendMode.srcIn),
        );
        Widget image2 = ColorFiltered(
          child: tabs2[index],
          colorFilter: ColorFilter.mode(index == this.currentIndex ? selectedColor : Colors.grey, BlendMode.srcIn),
        );

        return BottomNavigationBarItem(icon: index == this.currentIndex ? image : image2);
      }).toList();

      return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          border: Border.all(color: Colors.white),
          backgroundColor: Colors.white,
          activeColor: selectedColor,
          items: iosTabs,
          onTap: (index) {
            if (this.currentIndex == index && tabBarKeys[index].currentState != null) {
              tabBarKeys[index].currentState.popUntil((route) => route.isFirst);
            }
            setState(() {
              this.currentIndex = index;
            });
          },
        ),
        tabBuilder: (context, index) {
          return CupertinoTabView(navigatorKey: tabBarKeys[index], builder: (context) => children[index]);
        },
      );
    } else {
      List<BottomNavigationBarItem> androidTabs = this.tabs.asMap().entries.map((record) {
        int index = record.key;

        Widget image = ColorFiltered(
          child: record.value,
          colorFilter: ColorFilter.mode(index == this.currentIndex ? selectedColor : Colors.grey, BlendMode.srcIn),
        );
        Widget image2 = ColorFiltered(
          child: tabs2[index],
          colorFilter: ColorFilter.mode(index == this.currentIndex ? selectedColor : Colors.grey, BlendMode.srcIn),
        );

        return BottomNavigationBarItem(icon: index == this.currentIndex ? image : image2, title: Text(""));
      }).toList();

      return Scaffold(
        body: IndexedStack(
          index: currentIndex,
          children: children,
        ),
        bottomNavigationBar: BottomNavigationBar(
          elevation: 0,
          selectedItemColor: selectedColor,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          currentIndex: currentIndex,
          onTap: (int index) => setState(() => this.currentIndex = index),
          items: androidTabs,
        ),
      );
    }
  }
}
