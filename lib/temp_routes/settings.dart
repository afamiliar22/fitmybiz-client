import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/panel_info_job.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  var height;
  var width;
  TabController _tabController;

  bool _value = false;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 5);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              this.navigator.popRoute(null);
            },
          ),
          title: Text('설정'),
          body: CustomContainer(
            child: Column(
              children: [
                ListTile(
                  leading: Image.asset(
                    'assets/images/icnsPush.png',
                    width: 26,
                    height: 26,
                  ),
                  title: Text('푸시알림'),
                  trailing: CupertinoSwitch(
                    activeColor: ColorTheme.primary2,
                    value: _value,
                    onChanged: (value) {
                      setState(() {
                        _value = value;
                      });
                    },
                  ),
                ),
                Divider(),
                ListTile(
                  leading: Image.asset(
                    'assets/images/icnsPrivacy.png',
                    width: 26,
                    height: 26,
                  ),
                  title: Text('개인정보처리방침'),
                  trailing: IconButton(
                    icon: Image.asset('assets/images/icnsNext.png'),
                    onPressed: () {},
                  ),
                ),
                Divider(),
                ListTile(
                  leading: Image.asset(
                    'assets/images/icnsTerm.png',
                    width: 26,
                    height: 26,
                  ),
                  title: Text('이용약관'),
                  trailing: IconButton(
                    icon: Image.asset('assets/images/icnsNext.png'),
                    onPressed: () {},
                  ),
                ),
                Divider(),
                ListTile(
                  leading: Image.asset(
                    'assets/images/icnsLogout2.png',
                    width: 26,
                    height: 26,
                  ),
                  title: Text('로그아웃'),
                  trailing: IconButton(
                    icon: Image.asset('assets/images/icnsNext.png'),
                    onPressed: () {},
                  ),
                ),
                Divider(),
                ListTile(
                    leading: Image.asset(
                      'assets/images/icnsWithdrawal2.png',
                      width: 26,
                      height: 26,
                    ),
                    title: Text('회원탈퇴'),
                    trailing: IconButton(
                      icon: Image.asset('assets/images/icnsNext.png'),
                      onPressed: () {},
                    ))
              ],
            ),
          )),
    );
  }
}
