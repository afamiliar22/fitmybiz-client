import 'package:equatable/equatable.dart';

abstract class MeState extends Equatable {}

class InitialState extends MeState {
  @override
  List<Object> get props => [];
}

class LoadingState extends MeState {
  @override
  List<Object> get props => [];
}

class SetState extends MeState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class ToastState extends MeState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends MeState {
  @override
  List<Object> get props => [];
}
