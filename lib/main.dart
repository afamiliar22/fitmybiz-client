import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:firebase_core/firebase_core.dart';

import 'app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  // 기기 회전 잠금
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  // 실행
  runApp(
    EasyLocalization(
        supportedLocales: [Locale('ko')],
        path: 'assets/translations',
        fallbackLocale: Locale('ko'),
        child: App()),
  );
}
