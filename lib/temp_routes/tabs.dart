import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/home.dart';
import 'package:fitmybiz/temp_routes/my_login.dart';
import 'package:fitmybiz/temp_routes/survey_logout.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TabsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    SurveyLogoutPage(),
    MyLoginPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  Widget buildItem(index) {
    return CustomContainer(
      padding: EdgeInsets.symmetric(vertical: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CustomContainer(
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  'law data',
                  style: TextStyle(color: ColorTheme.white),
                ),
                backgroundColor: ColorTheme.primary,
                borderRadius: [10, 10, 10, 10],
              ),
              Spacer(),
              Text(
                '1234명 참여',
                style: TextStyle(color: ColorTheme.primary),
              )
            ],
          ),
          SizedBox(
            height: 9,
          ),
          Text(
            '고양이 집사 이용성 테스트',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            '고양이를 키우는 사람들이 가장 이상적으로 생각하는 화장실 모양에 대한 내용입니다',
            style: TextStyle(color: ColorTheme.grayLv1),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return true
        ? PlatformScaffold(
            body: Column(
              children: [
                Expanded(
                  child: _widgetOptions.elementAt(_selectedIndex),
                ),
                BottomNavigationBar(
                  backgroundColor: Colors.white,
                  showSelectedLabels: false,
                  showUnselectedLabels: false,
                  unselectedItemColor: ColorTheme.grayLv1,
                  items: <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: ImageIcon(
                        AssetImage("assets/images/icnsHome.png"),
                        //color: ColorTheme.primary2,
                      ),
                      label: 'Home',
                    ),
                    BottomNavigationBarItem(
                      icon: ImageIcon(
                        AssetImage("assets/images/icnsSurvey.png"),
                        //color: ColorTheme.primary2,
                      ),
                      label: 'Home',
                    ),
                    BottomNavigationBarItem(
                      icon: ImageIcon(
                        AssetImage("assets/images/icnsMy.png"),
                        //color: ColorTheme.primary2,
                      ),
                      label: 'Home',
                    ),
                  ],
                  currentIndex: _selectedIndex,
                  selectedItemColor: Colors.amber[800],
                  onTap: _onItemTapped,
                )
              ],
            ),
          )
        : PlatformScaffold(
            body: SafeArea(
              child: Column(
                children: [
                  CustomContainer(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    backgroundColor: ColorTheme.primary,
                    width: width,
                    height: (width / 359) * 134,
                    borderRadius: [0, 0, 15, 15],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '서비스 평가하고\n포인트 받자!',
                          style: TextStyle(
                              color: ColorTheme.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 11,
                        ),
                        Text(
                          '서비스 안내 보러가기',
                          style: TextStyle(
                              color: ColorTheme.primary2, fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  CustomContainer(
                    child: Column(
                      children: [
                        CustomContainer(
                          child: Row(
                            children: [
                              Text(
                                '설문조사 결과',
                                style: TextStyle(
                                    color: ColorTheme.grayLv1, fontSize: 12),
                              ),
                              Spacer(),
                              Text(
                                '필터',
                                style: TextStyle(
                                    color: ColorTheme.primary, fontSize: 12),
                              ),
                              Image.asset(
                                'assets/images/icnsFilter.png',
                                width: 16,
                                height: 16,
                                color: ColorTheme.primary,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  ),
                  Expanded(
                    child: ListView.builder(
                      padding: EdgeInsets.all(22),
                      itemBuilder: (context, index) {
                        return buildItem(index);
                      },
                    ),
                  )
                ],
              ),
            ),
          );
  }
}
