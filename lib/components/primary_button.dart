import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/themes/themes.dart';

class PrimaryButton extends StatelessWidget {
  final Widget text;
  final VoidCallback onPressed;

  PrimaryButton({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Container(
        child: FlatButton(
          child: text,
          padding: EdgeInsets.all(16),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
          color: ColorTheme.primary,
          disabledColor: Color.fromRGBO(228, 230, 233, 0.9),
          textColor: Colors.white,
          disabledTextColor: Colors.white,
          onPressed: this.onPressed,
        ),
        decoration:
            this.onPressed != null ? this.buildActiveDecoration() : null,
      ),
    );
  }

  BoxDecoration buildActiveDecoration() {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: ColorTheme.primary.withOpacity(0.3),
          spreadRadius: 0.5,
          blurRadius: 12,
          offset: Offset(0, 0), // changes position of shadow
        ),
      ],
    );
  }
}
