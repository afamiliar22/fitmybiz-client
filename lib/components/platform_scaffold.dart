import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

class PlatformScaffold extends StatelessWidget {
  final Widget title;
  final GlobalKey<ScaffoldState> key;
  final Widget leading;
  final Widget trailing;
  final Widget body;
  final VoidCallback onPressed;
  final Color backgroundColor;
  final bool resizeToAvoidBottomInset;
  final Future<bool> Function() onWillPop;
  PlatformScaffold(
      {this.title,
      this.key,
      this.leading,
      this.trailing,
      this.backgroundColor,
      this.body,
      this.onPressed,
      this.resizeToAvoidBottomInset,
      this.onWillPop});

  @override
  Widget build(BuildContext context) {
    bool showNavigationBar = false;
    if (title != null || leading != null || trailing != null) {
      showNavigationBar = true;
    }

    if (Platform.isAndroid) {
      var appBar = AppBar(
        title: title,
        leading: leading,
        actions: trailing != null ? [trailing, SizedBox(width: 16)] : [],
      );
      return WillPopScope(
        child: Scaffold(
          backgroundColor: this.backgroundColor,
          key: key,
          appBar: showNavigationBar ? appBar : null,
          resizeToAvoidBottomInset: this.resizeToAvoidBottomInset ?? false,
          body: GestureDetector(onTap: onPressed, child: body),
        ),
        onWillPop: this.onWillPop,
      );
    } else {
      var navigationBar = CupertinoNavigationBar(
        middle: title,
        border: Border.all(color: Colors.transparent),
        backgroundColor: Colors.white,
        trailing: trailing,
        leading: leading,
        padding: EdgeInsetsDirectional.zero,
      );
      return WillPopScope(
        child: CupertinoPageScaffold(
          backgroundColor: this.backgroundColor,
          navigationBar: showNavigationBar ? navigationBar : null,
          resizeToAvoidBottomInset: this.resizeToAvoidBottomInset ?? false,
          child: GestureDetector(onTap: onPressed, child: body),
        ),
        onWillPop: this.onWillPop,
      );
    }
  }
}
