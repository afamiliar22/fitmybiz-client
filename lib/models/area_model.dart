import 'dart:developer';

class AreaModel {
  final int userActiveAreaNo;
  final String code;
  final int groupCode;
  final String region1depthName;
  final String region2depthName;
  final String region3depthName;

  AreaModel({this.userActiveAreaNo, this.groupCode, this.code, this.region1depthName, this.region2depthName, this.region3depthName});

  factory AreaModel.fromJson(Map<String, dynamic> json) {
    return AreaModel(
      userActiveAreaNo: json['user_active_area_no'],
      code: json['code'].toString(),
      groupCode: json['groupCode'],
      region1depthName: json['region_1depth_name'],
      region2depthName: json['region_2depth_name'],
      region3depthName: json['region_3depth_name'],
    );
  }
}
