class ResultModel {
  String contentType;
  int participantCount;
  String title;
  String subtitle;

  ResultModel(
      {this.contentType, this.participantCount, this.title, this.subtitle});

  ResultModel.fromJson(Map<String, dynamic> json) {
    contentType = json['content_type'];
    participantCount = json['participant_count'];
    title = json['title'];
    subtitle = json['subtitle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content_type'] = this.contentType;
    data['participant_count'] = this.participantCount;
    data['title'] = this.title;
    data['subtitle'] = this.subtitle;
    return data;
  }
}
