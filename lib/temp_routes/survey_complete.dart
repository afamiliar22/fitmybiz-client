import 'package:carousel_slider/carousel_slider.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/point.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
];

class SurveyCompletePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;
  VideoPlayerController _playerController;
  bool isVideo = false;
  int _current = 0;

  @override
  void initState() {
    _playerController = VideoPlayerController.network(
        'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');
    _playerController.initialize();
    _playerController.play();
    super.initState();
  }

  final List<Widget> imageSliders = imgList
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Image.network(item, fit: BoxFit.cover, width: 1000.0),
                      /*
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20.0),
                              
                          child: Text(
                            'No. ${imgList.indexOf(item)} image',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      */
                    ],
                  )),
            ),
          ))
      .toList();

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      top: true,
      child: PlatformScaffold(
        leading: CustomContainer(
          onPressed: () {
            this.navigator.popRoute(null);
          },
          borderRadius: [6, 6, 6, 6],
          child: Image.asset(
            'assets/images/icnsBack.png',
            width: 26,
            height: 26,
          ),
        ),
        title: Text('설문완료'),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 38),
          child: Column(
            children: [
              SizedBox(
                height: 42,
              ),
              Center(
                child: CustomContainer(
                  backgroundColor: ColorTheme.primary2.withOpacity(.37),
                  borderRadius: [55, 55, 55, 55],
                  width: 110,
                  height: 110,
                  child: Center(
                    child: Text.rich(
                      TextSpan(
                        children: <InlineSpan>[
                          const TextSpan(
                            text: '포인트 지급\n',
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          TextSpan(
                              text: '500 P',
                              style: TextStyle(fontWeight: FontWeight.bold))
                        ],
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              Text(
                '설문에 참여해 주셔서 감사드립니다',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 11,
              ),
              Text(
                """비성실한 답변의 경우 포인트 회수 조치가 될 수 있으니 참고해주십시오 """,
                style: TextStyle(fontSize: 12),
              ),
              SizedBox(
                height: 34,
              ),
              CustomContainer(
                onPressed: () {
                  this.navigator.popRoute(null);
                },
                backgroundColor: ColorTheme.primary2,
                borderRadius: [6, 6, 6, 6],
                width: 285,
                height: 40,
                child: Center(
                  child: Text(
                    '마이 페이지로 이동',
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Spacer(),
              Image.asset('assets/images/bg_ch_complete.png')
            ],
          ),
        ),
      ),
    );
  }
}
