import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:url_launcher/url_launcher.dart';

enum LaunchMapType { KAKAOMAP, NAVERMAP, TMAP }

class LaunchClient {
  LaunchClient._initialize() {
    print('LaunchClient Initialize');
  }

  static final LaunchClient _instance = LaunchClient._initialize();

  static LaunchClient get shared => _instance;

  moveToBrowser(String url) {
    canLaunch(url).then((value) {
      if (value) {
        launch(url, forceSafariVC: false, forceWebView: false);
      }
    });
  }

  Future moveToMap(
      {LaunchMapType type,
      double sourceLat,
      double sourceLng,
      String sourceName,
      double targetLat,
      double targetLng,
      String targetName}) {
    String url;
    switch (type) {
      case LaunchMapType.NAVERMAP:
        url = 'nmap://route/public?';
        url += 'slat=$sourceLat&slng=$sourceLng&sname=내위치&';
        url += 'dlat=$targetLat&dlng=$targetLng&';
        url += 'appname=com.example.fitmybiz';
        break;
      case LaunchMapType.TMAP:
        url = 'tmap://route?goalx=$targetLng&goaly=$targetLat';
        break;
      case LaunchMapType.KAKAOMAP:
        url =
            'kakaomap://route?sp=$sourceLat,$sourceLng&ep=$targetLat,$targetLng&by=PUBLICTRANSIT';
        break;
      default:
        break;
    }
    print(url);

    String encodedUrl = Uri.encodeFull(url);
    return canLaunch(encodedUrl).then((value) {
      print(value);
      if (value) {
        return launch(encodedUrl, forceSafariVC: false, forceWebView: false);
      } else {
        switch (type) {
          case LaunchMapType.NAVERMAP:
            return Future.error('네이버 지도를 열 수 없습니다.');
          case LaunchMapType.TMAP:
            return Future.error('티맵을 열 수 없습니다.');
          case LaunchMapType.KAKAOMAP:
            return Future.error('카카오맵을 열 수 없습니다.');
          default:
            return Future.error('알 수 없는 오류입니다.');
        }
      }
    });
  }
}
