import 'package:intl/intl.dart';

extension DateParsing on DateTime {
  String toTimeString() {
    final DateFormat formatter = DateFormat.jm();
    return formatter.format(this);
  }

  String toDateString() {
    final DateFormat formatter = DateFormat.yMMMd();
    return formatter.format(this);
  }

  String toDecimalDay() {
    DateTime startedAt = DateTime(this.year, this.month, this.day, 0);
    DateTime now = DateTime.now();
    int diff = now.difference(startedAt).inDays;

    if (startedAt.millisecondsSinceEpoch > now.millisecondsSinceEpoch) {
      return 'D-${diff}';
    } else {
      return 'D+${diff + 1}';
    }
  }

  String toBeforeString() {
    var ms = this.millisecondsSinceEpoch / 1000;
    var now = DateTime.now().millisecondsSinceEpoch / 1000;
    var diff = (now - ms).toInt();

    if (diff < 60) {
      return "방금";
    } else if (diff < 60 * 60) {
      return "${diff ~/ 60}분 전";
    } else if (diff < 24 * 60 * 60) {
      return "${diff ~/ 60 ~/ 60}시간 전";
    } else if (diff < 30 * 24 * 60 * 60) {
      return "${diff ~/ 24 ~/ 60 ~/ 60}일 전";
    } else if (diff < 365 * 24 * 60 * 60) {
      return "${diff ~/ 30 ~/ 24 ~/ 60 ~/ 60}개월 전";
    } else {
      return "${diff ~/ 365 ~/ 30 ~/ 24 ~/ 60 ~/ 60}년 전";
    }
  }
}
