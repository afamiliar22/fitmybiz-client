import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_text_field2.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/auth/term/term_page.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'sign_email_cubit.dart';
import 'sign_email_state.dart';

class SignEmailPage extends StatelessWidget {
  const SignEmailPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SignEmailCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  SignEmailCubit cubit;
  //
  TextEditingController _emailController = TextEditingController();
  TextEditingController _pwdController = TextEditingController();
  TextEditingController _repwdController = TextEditingController();

  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<SignEmailCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case SignedState:
            this.indicator.hide();
            context.bloc<SignEmailCubit>().checkUserTerm();
            break;
          case CheckedTermState:
            this.indicator.hide();
            if (state.result == true) {
              this.property.isSignIn = true;
              this.navigator.resetRoute(SplashPage());
            } else {
              this.navigator.pushRoute(TermPage(state.terms));
            }
            break;
        }
      },
      child: PlatformScaffold(
        title: Text('회원가입'),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        trailing: true
            ? CustomContainer(
                child: Text(
                  '다음',
                  style: TextStyle(color: ColorTheme.red),
                ),
                onPressed: () => this.cubit.sign(),
              )
            : FlatButton(
                onPressed: () => this.cubit.sign(),
                //margin: EdgeInsets.only(right: 24),
                child: Text(
                  '다음',
                  style: TextStyle(
                      color: _emailController.text.isEmpty ||
                              _pwdController.text.isEmpty ||
                              _repwdController.text.isEmpty
                          ? ColorTheme.grayDisable
                          : ColorTheme.primary),
                ),
              ),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              Text('이메일', style: TextStyleTheme.textFieldLabel),
              SizedBox(height: 8),
              CustomTextField2(
                suffix: CustomContainer(
                  onPressed: () {
                    _emailController.text = '';
                    setState(() {});
                  },
                  width: 28,
                  height: 28,
                  margin: EdgeInsets.all(4),
                  child: _emailController.text.isNotEmpty
                      ? Image.asset('assets/images/icnsDelete3.png')
                      : null,
                ),
                controller: _emailController,
                placeholder: "이메일을 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                inputType: TextInputType.emailAddress,
                onChanged: (email) {
                  setState(() {});
                  return this.cubit.emailChanged(email);
                },
              ),
              SizedBox(height: 40),
              Text('비밀번호', style: TextStyleTheme.textFieldLabel),
              SizedBox(height: 8),
              CustomTextField2(
                controller: _pwdController,
                placeholder: "비밀번호를 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                secure: true,
                onChanged: (password) {
                  setState(() {});
                  this.cubit.passwordChanged(password);
                },
              ),
              SizedBox(height: 16),
              CustomTextField2(
                controller: _repwdController,
                placeholder: "한번 더 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                secure: true,
                onChanged: (password2) {
                  setState(() {});
                  return this.cubit.password2Changed(password2);
                },
              ),
              /*
              SizedBox(height: 90),
              PrimaryButton(
                text: Text('다음'),
                onPressed: () => this.cubit.sign(),
              ),
              */
            ],
          ),
        ),
      ),
    );
  }
}
