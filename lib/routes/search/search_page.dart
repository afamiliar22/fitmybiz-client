import 'dart:io';

import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_search_bar.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/routes/liking/liking_page.dart';
import 'package:fitmybiz/routes/me/me_cubit.dart';
import 'package:fitmybiz/routes/search/search_cubit.dart';
import 'package:fitmybiz/routes/search/search_state.dart';
import 'package:fitmybiz/routes/searchView/search_view_page.dart';
import 'package:fitmybiz/routes/views/place_range_sheet.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'contentsView/search_content_view.dart';

//
import 'dart:async';

class SearchPage extends StatelessWidget {
  final List<CategoryModel> categories;
  SearchPage(this.categories);
  // const SearchPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => SearchCubit(),
        ),
        BlocProvider(
          create: (BuildContext context) => MeCubit(),
        ),
      ],
      child: _View(this.categories),
    );
  }
}

class _View extends BaseRoute {
  final List<CategoryModel> categories;
  _View(this.categories);
  @override
  _ViewState createState() => _ViewState(this.categories);
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  List<CategoryModel> categories = List<CategoryModel>();

  double radius;

  bool isGps;

  Position position;
  _ViewState(this.categories);
  final TextEditingController textEditingController = TextEditingController();
  final searchDebouncer = Debouncer(milliseconds: 500);
  bool isFocus = false;
  String searchText;

  List<ContentsModel> contents = List<ContentsModel>();
  List<BannerModel> banners = List<BannerModel>();
  int tabIndex = 0;
  TabController tabController;

  bool isSearchBar = true;

  final pageIndexNotifier = ValueNotifier<int>(0);

  List<Widget> pages = [];

  //
  MeCubit cubit;
  SearchCubit searchCubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.tabController =
        TabController(length: categories.length + 2, vsync: this);
    this.tabController.addListener(() {
      setState(() {
        this.tabIndex = this.tabController.index;
      });
    });
    //
    this.cubit = context.bloc<MeCubit>();
    this.searchCubit = context.bloc<SearchCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    context.bloc<SearchCubit>().getBanner();
    this.cubit.init();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<SearchCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();

            break;

          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case RefreshRadius:
            this.indicator.hide();
            this.radius = state.radius;
            pages.clear();
            this.searchCubit.getBanner();
            setState(() {});
            break;

          case GetBanner:
            this.indicator.hide();
            this.banners = state.banners;
            for (int i = 0; i < categories.length + 2; i += 1) {
              if (i == 0) {
                pages.add(SearchContentView(
                    null, SearchContentType.all, this.radius));
              } else if (i == 1) {
                pages.add(SearchContentView(
                    null, SearchContentType.follow, this.radius));
              } else {
                pages.add(SearchContentView(this.categories[i - 2].categoryNo,
                    SearchContentType.category, this.radius));
              }
            }

            setState(() {});
            break;
        }
      },
      child: PlatformScaffold(
        body: SafeArea(
          child: NestedScrollView(
            headerSliverBuilder: (context, innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  backgroundColor: Colors.white,
                  //title: searchBar(),
                  // floating 설정. SliverAppBar는 스크롤 다운되면 화면 위로 사라짐.
                  // true: 스크롤 업 하면 앱바가 바로 나타남. false: 리스트 최 상단에서 스크롤 업 할 때에만 앱바가 나타남
                  floating: false,

                  pinned: false,
                  bottom: PreferredSize(
                      child: Container(
                          padding: EdgeInsets.all(25),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text.rich(
                                        TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: "안녕하세요, ",
                                              style: TextStyle(fontSize: 15),
                                            ),
                                            TextSpan(
                                                text:
                                                    "${this.cubit.user?.nickname ?? ''}",
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            TextSpan(
                                                text: "님!",
                                                style: TextStyle(fontSize: 15)),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 17,
                                      ),
                                      Text(
                                        '내 지도에 찜 할\n장소를 찾아볼까요?',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 26),
                                      ),
                                    ],
                                  ),
                                  CustomContainer(
                                    width: 48,
                                    height: 48,
                                    padding: EdgeInsets.all(10),
                                    backgroundColor: Colors.white,
                                    shadowColor:
                                        ColorTheme.blackShadow.withOpacity(.1),
                                    borderRadius: [24, 24, 24, 24],
                                    child: Image.asset(
                                      'assets/images/icnsFriends.png',
                                      width: 24,
                                      height: 24,
                                    ),
                                    onPressed: () {
                                      this
                                          .navigator
                                          .fadePresentRoute(LikingPage());
                                    },
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              searchBar()
                            ],
                          )),
                      preferredSize: Size.fromHeight(250)),
                ),
                SliverPersistentHeader(
                  delegate: _SliverAppBarDelegate(this.getHorizontalMenu()),
                  pinned: true,
                ),
              ];
            },
            body: this.tabBarView(),
          ),
        ),
      ),
    );
  }

  Widget searchBar() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12, horizontal: 0),
      child: Stack(
        children: [
          CustomSearchBar(
            controller: this.textEditingController,
            placeholder: "검색어를 입력하세요",
            btnText: "검색",
            btnSearch: CustomContainer(
              child: Image.asset(
                'assets/images/icnsFriends.png',
                width: 24,
                height: 24,
              ),
              onPressed: () {
                this.navigator.fadePresentRoute(LikingPage());
              },
            ),
            onEnter: () {
              setState(() {
                this.isFocus = true;
              });
            },
            onLeave: () {
              setState(() {
                this.isFocus = false;
              });
            },
            onValueChange: (value) {
              this.searchText = value;
              if (value != "") {}
            },
          ),
          CustomContainer(
            width: MediaQuery.of(context).size.width - 100,
            height: 40,
            backgroundColor: Colors.transparent,
            onPressed: () {
              this.navigator.fadePresentRoute(SearchViewPage());
            },
          )
        ],
      ),
    );
  }

  Widget tab(String title, int index) {
    return true
        ? Tab(
            child: Container(
              child: Text(
                title,
                style: this.tabIndex == index
                    ? TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold)
                    : TextStyle(color: Colors.grey[400], fontSize: 16),
              ),
            ),
          )
        : Material(
            color: Colors.transparent,
            child: Tab(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /*
            Container(
              width: 6,
              height: 6,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: this.tabIndex == index
                    ? ColorTheme.primary
                    : Colors.transparent,
              ),
              margin: EdgeInsets.only(
                  top: this.tabIndex == index ? 4 : 10, left: 5.5),
            ),
            */
                  if (Platform.isIOS) SizedBox(height: 4),
                  Container(
                    child: Text(
                      title,
                      style: this.tabIndex == index
                          ? TextStyle(
                              color: true ? Colors.white : Colors.black,
                              fontSize: true ? 16 : 22,
                              fontWeight: FontWeight.bold)
                          : TextStyle(color: Colors.grey[400], fontSize: 15),
                    ),
                  )
                ],
              ),
            ),
          );
  }

  getHorizontalMenu() {
    List<Widget> tabs = [];
    tabs.add(tab("추천", 0));
    tabs.add(tab("팔로잉", 1));
    this.categories.asMap().forEach((key, value) {
      tabs.add(tab(value.categoryName, key + 2));
    });

    return true
        ? TabBar(
            tabs: tabs,
            isScrollable: true,
            controller: this.tabController,
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: new BubbleTabIndicator(
              indicatorRadius: 22.0,
              indicatorHeight: 36.0,
              padding: EdgeInsets.symmetric(horizontal: 0),
              indicatorColor: ColorTheme.red,
              tabBarIndicatorSize: TabBarIndicatorSize.label,
            ),
          )
        : TabBar(
            isScrollable: true,
            controller: this.tabController,
            indicatorColor: Colors.white,
            tabs: tabs,
          );
  }

  tabBarView() {
    return this.pages.length == 0
        ? Container()
        : Stack(
            children: [
              TabBarView(
                controller: this.tabController,
                children: pages,
              ),
              Column(
                children: [
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: FutureBuilder(
                            future: SharedPreferences.getInstance(),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                SharedPreferences prefs = snapshot.data;
                                radius = prefs.getDouble('radius') ?? 0.0;
                                return CustomContainer(
                                    onPressed: () {
                                      showModalBottomSheet(
                                          backgroundColor: Colors.transparent,
                                          context: context,
                                          builder: (context) {
                                            return PlaceRangeSheet(
                                              onPressed: (value) {
                                                radius = value;

                                                this.isGps =
                                                    radius == 0 ? false : true;
                                                if (!this.isGps) {
                                                  radius = null;
                                                }
                                                Geolocator
                                                        .getLastKnownPosition()
                                                    .then((value) {
                                                  this.position = value;

                                                  setState(() {});
                                                  this
                                                      .searchCubit
                                                      .refreshRadius(radius);
                                                  //this.onRefresh();
                                                });
                                                Geolocator.checkPermission()
                                                    .then((value) {
                                                  if (value ==
                                                      LocationPermission
                                                          .denied) {
                                                    this.isGps = false;
                                                    setState(() {});
                                                  }
                                                });
                                                setState(() {});
                                                Navigator.of(context).pop();
                                              },
                                            );
                                          });
                                    },
                                    shadowColor:
                                        ColorTheme.black.withOpacity(.2),
                                    backgroundColor: ColorTheme.black,
                                    borderRadius: [40, 40, 40, 40],
                                    padding: EdgeInsets.symmetric(
                                        vertical: 12, horizontal: 24),
                                    child: Row(
                                      children: [
                                        Text(
                                          radius == 0
                                              ? '내 주변 찾기'
                                              : '내 주변 ${radius.toInt()} KM',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Container(
                                          child: Image.asset(
                                              'assets/images/icnsDown2.png',
                                              width: 28,
                                              height: 28),
                                        )
                                      ],
                                    ));
                              } else {
                                return FloatingActionButton.extended(
                                    heroTag: null,
                                    icon: Icon(Icons.arrow_drop_down),
                                    backgroundColor: ColorTheme.black,
                                    onPressed: () {
                                      showModalBottomSheet(
                                          backgroundColor: Colors.transparent,
                                          context: context,
                                          builder: (context) {
                                            return PlaceRangeSheet(
                                              onPressed: (value) {
                                                Navigator.of(context).pop();
                                              },
                                            );
                                          });
                                    },
                                    label: Text('거리 설정 없음'));
                              }
                            }),
                      )
                    ],
                  )
                ],
              )
            ],
          );
  }

  PageViewIndicator _buildIndicator() {
    return PageViewIndicator(
      pageIndexNotifier: pageIndexNotifier,
      length: 3,
      normalBuilder: (animationController, index) => Circle(
        size: 8.0,
        color: Colors.black87,
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController,
          curve: Curves.ease,
        ),
        child: Circle(size: 10.0, color: Colors.white),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
