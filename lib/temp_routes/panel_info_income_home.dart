import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/panel_info_income_birth.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

const List<String> incomes = [
  '소득 없음',
  '월 100만 원 미만',
  '월 100만 원 이상 200 만 원 이하',
  '월 200만 원 이상 300 만 원 이하',
  '월 300만 원 이상 400 만 원 이하',
  '월 400만 원 이상 500 만 원 이하',
  '월 500만 원 이상 700 만 원 이하',
  '월 700만 원 이상 900 만 원 이하',
  '월 1000만 원 이상',
];

class PanelInfoHomeIncomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('패널 정보입력'),
        body: CustomContainer(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '직업을 알려주세요',
                style: TextStyle(color: ColorTheme.black, fontSize: 16),
              ),
              SizedBox(
                height: 18,
              ),
              CustomContainer(
                height: MediaQuery.of(context).size.height / 2,
                child: ListView.builder(
                  itemCount: incomes.length,
                  itemBuilder: (context, index) {
                    return CustomContainer(
                      //margin: EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        children: [
                          Radio(
                            value: true,
                            onChanged: (value) {},
                            groupValue: true,
                          ),
                          Text(incomes[index])
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  Spacer(),
                  CustomContainer(
                    margin: EdgeInsets.all(10),
                    width: 285,
                    height: 40,
                    onPressed: () {
                      this.navigator.pushRoute(PanelInfoBirthPage());
                    },
                    backgroundColor: ColorTheme.primary2,
                    borderRadius: [6, 6, 6, 6],
                    child: Center(
                      child: Text(
                        '다음',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
