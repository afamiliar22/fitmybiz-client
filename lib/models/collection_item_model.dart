import 'dart:developer';

import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'image_source_model.dart';

class CollectionItemModel {
  final String title;
  final String collectionItemNo;
  final String imageContentNo;
  final String userNo;
  final bool isWrite;
  final ImageSourceModel imageSrc;

  CollectionItemModel(
      {this.collectionItemNo,
      this.imageContentNo,
      this.userNo,
      this.imageSrc,
      this.isWrite,
      this.title});

  factory CollectionItemModel.fromJson(Map<String, dynamic> json) {
    return CollectionItemModel(
      title: json['title'],
      collectionItemNo: json['collection_item_no'].toString(),
      imageContentNo: json['image_content_no'].toString(),
      userNo: json['user_no'].toString(),
      imageSrc: json.containsKey('image_src') && json['image_src'] != null
          ? ImageSourceModel.fromJson(json['image_src'])
          : null,
      isWrite: json['is_write'] == '1',
    );
  }
}
