import 'dart:developer';

class AreaCodeModel {
  final List<String> si;
  final Map<String, dynamic> gungu;

  AreaCodeModel({this.si, this.gungu});

  factory AreaCodeModel.fromJson(Map<String, dynamic> json) {
    return AreaCodeModel(si: (json['si'] as List).map((record) => record.toString()).toList(), gungu: json['gungu']);
  }
}
