import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/google_place_model.dart';

abstract class SearchPlaceNameState extends Equatable {}

class InitialState extends SearchPlaceNameState {
  @override
  List<Object> get props => [];
}

class LoadingState extends SearchPlaceNameState {
  @override
  List<Object> get props => [];
}

class ErrorState extends SearchPlaceNameState {
  @override
  List<Object> get props => [];
}

class GetPlace extends SearchPlaceNameState {
  GetPlace(this.places);

  final List<GooglePlaceModel> places;

  @override
  List<Object> get props => [places];
}
