import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/panel_info_job.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class ServiceInfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  var height;
  var width;
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 5);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              this.navigator.popRoute(null);
            },
          ),
          title: Text('서비스 안내'),
          body: CustomContainer(
            child: Column(
              children: [
                TabBar(
                  isScrollable: true,
                  indicatorColor: ColorTheme.primary2,
                  labelColor: ColorTheme.primary2,
                  unselectedLabelColor: ColorTheme.grayLv1,
                  controller: _tabController,
                  tabs: [
                    Tab(
                      text: '카테고리명',
                    ),
                    Tab(
                      text: '카테고리명',
                    ),
                    Tab(
                      text: '카테고리명',
                    ),
                    Tab(
                      text: '카테고리명',
                    ),
                    Tab(
                      text: '카테고리명',
                    ),
                  ],
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text('1. 제목텍스트'),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                          '글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다글내용 설명에 대한글텍스트는 이렇게 노출됩니다'),
                      SizedBox(
                        height: 8,
                      ),
                      CustomContainer(
                        backgroundColor: ColorTheme.grayBg,
                        height: 244,
                        child: Center(
                          child: Text('이미지 가로너비 픽스\n세로로 길어지게 노출'),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
