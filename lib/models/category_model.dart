import 'dart:developer';

class CategoryModel {
  final int categoryNo;
  final String categoryName;
  final String categoryColor;

  CategoryModel({this.categoryNo, this.categoryName, this.categoryColor});

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
      categoryNo: json['category_no'],
      categoryName: json['category_name'],
      categoryColor: json['category_color'],
    );
  }
}
