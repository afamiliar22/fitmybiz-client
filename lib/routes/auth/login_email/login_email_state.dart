import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';

abstract class LoginEmailState extends Equatable {}

class InitialState extends LoginEmailState {
  @override
  List<Object> get props => [];
}

class LoadingState extends LoginEmailState {
  @override
  List<Object> get props => [];
}

class SignedState extends LoginEmailState {
  @override
  List<Object> get props => [];
}

class CheckedTermState extends LoginEmailState {
  CheckedTermState(this.result, this.terms);

  final bool result;
  final List<TermModel> terms;

  @override
  List<Object> get props => [result, terms];
}

class ToastState extends LoginEmailState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends LoginEmailState {
  @override
  List<Object> get props => [];
}
