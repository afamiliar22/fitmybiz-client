import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_models/result.dart';
import 'package:fitmybiz/temp_routes/service_info.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return PlatformScaffold(
      body: SafeArea(
        child: Column(
          children: [
            CustomContainer(
              padding: EdgeInsets.symmetric(horizontal: 25),
              backgroundColor: ColorTheme.primary,
              width: width,
              height: (width / 359) * 134,
              borderRadius: [0, 0, 15, 15],
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '서비스 평가하고\n포인트 받자!',
                    style: TextStyle(
                        color: ColorTheme.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 11,
                  ),
                  CustomContainer(
                    onPressed: () {
                      this.navigator.pushRoute(ServiceInfoPage());
                    },
                    child: Text(
                      '서비스 안내 보러가기',
                      style:
                          TextStyle(color: ColorTheme.primary3, fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
            CustomContainer(
              child: Column(
                children: [
                  CustomContainer(
                    child: Row(
                      children: [
                        Text(
                          '설문조사 결과',
                          style: TextStyle(
                              color: ColorTheme.grayLv1, fontSize: 12),
                        ),
                        Spacer(),
                        Text(
                          '필터',
                          style: TextStyle(
                              color: ColorTheme.primary, fontSize: 12),
                        ),
                        Image.asset(
                          'assets/images/icnsFilter.png',
                          width: 16,
                          height: 16,
                          color: ColorTheme.primary,
                        )
                      ],
                    ),
                  )
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            ),
            Expanded(
              child: StreamBuilder(
                stream:
                    FirebaseFirestore.instance.collection('tests').snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    QuerySnapshot query = snapshot.data;
                    var tests = query.docs;
                    return ListView.builder(
                      padding: EdgeInsets.all(22),
                      itemCount: 100,
                      itemBuilder: (context, index) {
                        return buildItem(index, tests.first);
                      },
                    );
                  } else {
                    return Center();
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildItem(index, DocumentSnapshot document) {
    var data = ResultModel(
        contentType: "law data",
        participantCount: 1234,
        title: "고양이 집사 이용성 테스트",
        subtitle: "고양이를 키우는 사람들이 가장 이상적으로 생각하는 화장실 모양");
    return CustomContainer(
      padding: EdgeInsets.symmetric(vertical: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CustomContainer(
                borderRadius: [10, 10, 10, 10],
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 7),
                backgroundColor: ColorTheme.primary,
                child: Text(
                  data.contentType,
                  style: TextStyle(color: ColorTheme.white),
                ),
              ),
              Spacer(),
              Text(
                '${data.participantCount} 명 참여',
                style: TextStyle(color: ColorTheme.primary),
              )
            ],
          ),
          SizedBox(
            height: 9,
          ),
          Text(
            data.title,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            data.subtitle,
            style: TextStyle(color: ColorTheme.grayLv1),
          )
        ],
      ),
    );
  }
}
