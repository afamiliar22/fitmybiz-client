import 'package:equatable/equatable.dart';

abstract class CreateMapState extends Equatable {}

class InitialState extends CreateMapState {
  @override
  List<Object> get props => [];
}

class LoadingState extends CreateMapState {
  @override
  List<Object> get props => [];
}

class ErrorState extends CreateMapState {
  @override
  List<Object> get props => [];
}

class ToastState extends CreateMapState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class PostCollection extends CreateMapState {
  PostCollection(this.result);

  final bool result;

  @override
  List<Object> get props => [result];
}
