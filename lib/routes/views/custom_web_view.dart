import 'package:flutter/material.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomWebView extends BaseRoute {
  final String url;
  final String title;
  final Function(WebViewController) cookie;

  CustomWebView(this.url, this.title, this.cookie);
  @override
  CustomWebViewState createState() =>
      CustomWebViewState(this.url, this.title, this.cookie);
}

class CustomWebViewState extends BaseRouteState {
  String url;
  String title;
  Function(WebViewController) cookie;
  CustomWebViewState(this.url, this.title, this.cookie);

  WebViewController controller;
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return PlatformScaffold(
      leading: IconButton(
        padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
        icon: Image.asset('assets/images/icnsBack.png',
            color: Colors.black, width: 28, height: 28),
        onPressed: () {
          this.navigator.popRoute(null);
        },
      ),
      title: Text(title ?? ""),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: WebView(
                javascriptMode: JavascriptMode.unrestricted,
                initialUrl: url,
                debuggingEnabled: true,
                onWebViewCreated: (controller) {
                  if (this.cookie != null) {
                    this.cookie(controller);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
