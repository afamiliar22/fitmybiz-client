import 'package:equatable/equatable.dart';

abstract class NoticeState extends Equatable {}

class InitialState extends NoticeState {
  @override
  List<Object> get props => [];
}

class SetState extends NoticeState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadedState extends NoticeState {
  @override
  List<Object> get props => [];
}

class CompletedState extends NoticeState {
  @override
  List<Object> get props => [];
}

class LoadingState extends NoticeState {
  @override
  List<Object> get props => [];
}

class ErrorState extends NoticeState {
  @override
  List<Object> get props => [];
}
