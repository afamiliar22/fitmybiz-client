import 'package:carousel_slider/carousel_slider.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/point.dart';
import 'package:fitmybiz/temp_routes/survey.dart';
import 'package:fitmybiz/temp_routes/survey_complete.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
];

class SurveyIntroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;
  VideoPlayerController _playerController;
  bool isVideo = false;
  int _current = 0;

  @override
  void initState() {
    _playerController = VideoPlayerController.network(
        'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');
    _playerController.initialize();
    _playerController.play();
    super.initState();
  }

  final List<Widget> imageSliders = imgList
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Image.network(item, fit: BoxFit.cover, width: 1000.0),
                      /*
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20.0),
                              
                          child: Text(
                            'No. ${imgList.indexOf(item)} image',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      */
                    ],
                  )),
            ),
          ))
      .toList();

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return PlatformScaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Stack(
              children: [
                CustomContainer(
                  height: 266,
                  child: Column(
                    children: [
                      CustomContainer(
                        padding: EdgeInsets.symmetric(horizontal: 25),
                        backgroundColor: Color(0xffec6c00).withOpacity(.34),
                        width: width,
                        height: true ? 242 : (width / 359) * 134,
                        borderRadius: [0, 0, 15, 15],
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CustomContainer(
                              height: kToolbarHeight,
                              child: Row(
                                children: [
                                  CustomContainer(
                                    onPressed: () {
                                      this.navigator.popRoute(null);
                                    },
                                    borderRadius: [6, 6, 6, 6],
                                    child: Image.asset(
                                      'assets/images/icnsBack.png',
                                      width: 26,
                                      height: 26,
                                    ),
                                  ),
                                  Spacer(),
                                  CustomContainer(
                                    borderRadius: [6, 6, 6, 6],
                                    child: Text('설문',
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: ColorTheme.black,
                                        )),
                                  ),
                                  Spacer(),
                                  CustomContainer(
                                    width: 26,
                                    height: 26,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 13,
                            ),
                            Text('설문조사 대제목 노출',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16)),
                            SizedBox(
                              height: 5,
                            ),
                            Text('설문조사 서브설명 노출 됩니다.',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    color: Color(0xff767676))),
                            SizedBox(
                              height: 19,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('예상 시간'),
                                    Text(
                                      '1분',
                                      style: TextStyle(
                                          color: ColorTheme.primary2,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 48,
                                ),
                                VerticalDivider(
                                  thickness: 1,
                                  color: Color(0xffe9e9e9),
                                ),
                                SizedBox(
                                  width: 48,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('포인트 지급'),
                                    Text(
                                      '500P',
                                      style: TextStyle(
                                          color: ColorTheme.primary2,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned.fill(
                  child: Column(
                    children: [
                      Spacer(),
                      CustomContainer(
                        onPressed: () {
                          this.navigator.pushRoute(SurveyPage());
                        },
                        backgroundColor: ColorTheme.primary2,
                        borderRadius: [6, 6, 6, 6],
                        width: 285,
                        height: 40,
                        child: Center(
                          child: Text(
                            '설문조사 시작하기',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 12,
            ),
            CustomContainer(
              padding: EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: [
                  Flexible(
                    child: Text(
                      """설문조사를 완료하시면, 포인트가 즉시 지급됩니다.\n 비성실한 답변의 경우 포인트가 회수 조치 될 수 있으니 참고해주십시오""",
                      style: TextStyle(color: ColorTheme.grayLv1, fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 18,
            ),
            Divider(
              thickness: 1,
              color: ColorTheme.grayLv1,
            ),
            SizedBox(
              height: 19,
            ),
            isVideo
                ? Center(
                    child: Text(
                      '필수시청 영상',
                      style:
                          TextStyle(color: ColorTheme.primary2, fontSize: 14),
                    ),
                  )
                : Center(
                    child: Text(
                      '필수시청 사진',
                      style:
                          TextStyle(color: ColorTheme.primary2, fontSize: 14),
                    ),
                  ),
            SizedBox(
              height: 19,
            ),
            isVideo
                ? VideoPlayer(_playerController)
                : Column(children: [
                    CarouselSlider(
                      items: imageSliders,
                      options: CarouselOptions(
                          //autoPlay: true,
                          //enlargeCenterPage: true,
                          aspectRatio: 2.0,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: imgList.map((url) {
                        int index = imgList.indexOf(url);
                        return Container(
                          width: 8.0,
                          height: 8.0,
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _current == index
                                ? Color.fromRGBO(0, 0, 0, 0.9)
                                : Color.fromRGBO(0, 0, 0, 0.4),
                          ),
                        );
                      }).toList(),
                    ),
                  ]),
          ],
        ),
      ),
    );
  }
}
