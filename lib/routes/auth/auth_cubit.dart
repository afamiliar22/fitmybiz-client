import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'auth_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class AuthCubit extends BaseCubit<AuthState> {
  GoogleSignIn googleSignIn = GoogleSignIn();

  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  /// {@macro counter_cubit}
  AuthCubit() : super(InitialState());

  void signInApple() {
    this.auth.signInWithApple().then((value) {
      String accessToken = value['authorizationCode'];
      String idToken = value['identityToken'];
      String method = 'post';
      String path = '/api/v1/user/apple/login';
      Map data = {'access_token': accessToken, 'id_token': idToken};
      emit(LoadingState());
      this.network.requestWithoutAuth(method, path, data).then((response) {
        LoginModel loginModel = LoginModel.fromJson(response.data);
        this.network.property.accessToken = loginModel.accessToken;
        this.network.property.refreshToken = loginModel.refreshToken;
        emit(SignedState());
      }).catchError((e) {
        emit(ErrorState());
      });
    });
  }

  Future<Null> signInGoogle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    GoogleSignInAccount googleUser = await googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    User firebaseUser =
        (await firebaseAuth.signInWithCredential(credential)).user;

    if (firebaseUser != null) {
      // Check is already sign up
      emit(LoadingState());
      final QuerySnapshot result = await FirebaseFirestore.instance
          .collection('users')
          .where('id', isEqualTo: firebaseUser.uid)
          .get();
      final List<DocumentSnapshot> documents = result.docs;
      if (documents.length == 0) {
        // Update data to server if new user
        FirebaseFirestore.instance
            .collection('users')
            .doc(firebaseUser.uid)
            .set({
          'nickname': firebaseUser.displayName,
          'photoUrl': firebaseUser.photoURL,
          'id': firebaseUser.uid,
          'createdAt': DateTime.now().millisecondsSinceEpoch.toString(),
          'chattingWith': null
        });

        // Write data to local
        var currentUser = firebaseUser;
        await prefs.setString('id', currentUser.uid);
        await prefs.setString('nickname', currentUser.displayName);
        await prefs.setString('photoUrl', currentUser.photoURL);
      } else {
        // Write data to local
        await prefs.setString('id', documents[0].data()['id']);
        await prefs.setString('nickname', documents[0].data()['nickname']);
        await prefs.setString('photoUrl', documents[0].data()['photoUrl']);
        await prefs.setString('aboutMe', documents[0].data()['aboutMe']);
      }
      emit(SignedState());
      return;
      String accessToken = googleAuth.accessToken;
      String idToken = googleAuth.idToken;
      String method = 'post';
      String path = '/api/v1/user/apple/login';
      Map data = {'access_token': accessToken, 'id_token': idToken};
      emit(LoadingState());
      this.network.requestWithoutAuth(method, path, data).then((response) {
        LoginModel loginModel = LoginModel.fromJson(response.data);
        this.network.property.accessToken = loginModel.accessToken;
        this.network.property.refreshToken = loginModel.refreshToken;
        emit(SignedState());
      }).catchError((e) {
        emit(ErrorState());
      });
    } else {
      emit(ErrorState());
    }
  }

  void signInKakao() {
    this.auth.signInWithKakao().then((value) {
      String accessToken = value['accessToken'];
      String method = 'post';
      String path = '/api/v1/user/kakao/login';
      Map data = {'access_token': accessToken};
      emit(LoadingState());
      this.network.requestWithoutAuth(method, path, data).then((response) {
        LoginModel loginModel = LoginModel.fromJson(response.data);
        this.network.property.accessToken = loginModel.accessToken;
        this.network.property.refreshToken = loginModel.refreshToken;
        emit(SignedState());
      }).catchError((e) {
        emit(ErrorState());
      });
    });
  }

  void checkUserTerm() {
    String method = 'get';
    String path = '/api/v1/user/term';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<TermModel> terms = (response.data['terms'] as List)
          .map((record) => TermModel.fromJson(record))
          .toList();
      emit(CheckedTermState(response.data['check'] == true, terms));
      // emit(CheckedTermState(true, []));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
