import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/follow/follow_page.dart';
import 'package:fitmybiz/tools/facebook_client.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'follow_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class FollowCubit extends BaseCubit<FollowState> {
  /// {@macro counter_cubit}
  FollowCubit() : super(InitialState());

  List<FollowModel> follows;
  List<UserModel> users;
  List<bool> selectedFollows = [];

  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  int page = 1;
  String userNo = '';
  String nickname = '';
  FollowType type;
  void init() {
    emit(InitialState());
  }

  void setUserNo(String userNo, FollowType type) {
    this.userNo = userNo;
    this.type = type;
  }

  void setNickname(String nickname) {
    this.nickname = nickname;
  }

  onRefresh() {
    this.page = 1;
    this.selectedFollows.clear();
    if (type == FollowType.follower) {
      this.getSearchFollower().then((followers) {
        this.follows = followers;
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        this.refreshController.refreshCompleted();
        this.refreshController.resetNoData();
      }).catchError((e) {
        this.follows = [];
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        this.refreshController.refreshCompleted();
        this.refreshController.resetNoData();
      });
    } else if (type == FollowType.following) {
      this.getSearchFollowing().then((followings) {
        this.follows = followings;
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        this.refreshController.refreshCompleted();
        this.refreshController.resetNoData();
      }).catchError((e) {
        this.follows = [];
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        this.refreshController.refreshCompleted();
        this.refreshController.resetNoData();
      });
    } else {
      this.getSearchUser().then((users) {
        this.users = users;
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        this.refreshController.refreshCompleted();
        this.refreshController.resetNoData();
      }).catchError((e) {
        this.users = [];
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        this.refreshController.refreshCompleted();
        this.refreshController.resetNoData();
      });
    }
  }

  onLoading() {
    this.page = this.page + 1;
    if (type == FollowType.follower) {
      this.getSearchFollower().then((followers) {
        this.follows.addAll(followers);

        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        if (followers.length == 0) {
          this.refreshController.loadNoData();
        } else {
          this.refreshController.loadComplete();
        }
      }).catchError((e) {
        this.refreshController.loadComplete();
      });
    } else if (type == FollowType.following) {
      this.getSearchFollowing().then((followings) {
        this.follows.addAll(followings);
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        if (followings.length == 0) {
          this.refreshController.loadNoData();
        } else {
          this.refreshController.loadComplete();
        }
      }).catchError((e) {
        this.refreshController.loadComplete();
      });
    } else {
      this.getSearchUser().then((users) {
        this.users.addAll(users);
        emit(SetState(DateTime.now().millisecondsSinceEpoch));
        if (users.length == 0) {
          this.refreshController.loadNoData();
        } else {
          this.refreshController.loadComplete();
        }
      }).catchError((e) {
        this.refreshController.loadComplete();
      });
    }
  }

  Future<List<FollowModel>> getSearchFollower() {
    String method = 'get';
    String path = '/api/v1/user/${this.userNo}/follower';
    Map<String, dynamic> data = {'take': '20', 'page': this.page.toString()};
    return this.network.requestWithAuth(method, path, data).then((response) {
      List<FollowModel> result = (response.data['data'] as List)
          .map((record) => FollowModel.fromJson(record))
          .toList();

      for (int i = 0; i < result.length; i += 1) {
        this.selectedFollows.add(result[i].isFollow);
      }
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value(result);
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future<List<FollowModel>> getSearchFollowing() {
    String method = 'get';
    String path = '/api/v1/user/${this.userNo}/following';
    Map<String, dynamic> data = {'take': '20', 'page': this.page.toString()};
    return this.network.requestWithAuth(method, path, data).then((response) {
      List<FollowModel> result = (response.data['data'] as List)
          .map((record) => FollowModel.fromJson(record))
          .toList();

      for (int i = 0; i < result.length; i += 1) {
        this.selectedFollows.add(result[i].isFollow);
      }
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value(result);
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future<List<UserModel>> getSearchUser() {
    String method = 'get';
    String path = '/api/v1/user/search';
    Map<String, dynamic> data = {"nickname": this.nickname, 'take': '20'};
    return this.network.requestWithAuth(method, path, data).then((response) {
      List<UserModel> result = (response.data['data'] as List)
          .map((record) => UserModel.fromJson(record))
          .toList();

      for (int i = 0; i < result.length; i += 1) {
        this.selectedFollows.add(result[i].isFollow);
      }

      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value(result);
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void putUpdateFollow(String userNo) {
    String method = 'put';
    String path = '/api/v1/user/$userNo/follow';

    this.network.requestWithAuth(method, path, null).then((response) {
      this.facebook.addEvent(FacebookEventType.following);
      this.property.meProfileSubject.add(true);
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
