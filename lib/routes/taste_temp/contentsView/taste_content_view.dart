import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_smart_refresher.dart';
import 'package:fitmybiz/models/banner_model.dart';

import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/routes/_cells/contents_cell.dart';
import 'package:fitmybiz/routes/my_place/my_place_page.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/user/user_page.dart';
import 'package:fitmybiz/routes/views/place_range_sheet.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/network_client.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';

enum TasteContentType { all, follow, category }

class TasteContentView extends BaseRoute {
  final int categoryNo;
  final TasteContentType type;
  final ValueChanged onZzim;
  final bool isReset;

  TasteContentView(this.categoryNo, this.type, this.isReset,
      {Key key, @required this.onZzim});
  @override
  SearchContentViewState createState() => SearchContentViewState(
      this.categoryNo, this.type, this.onZzim, this.isReset);
}

class SearchContentViewState extends BaseRouteState
    with AutomaticKeepAliveClientMixin {
  bool isReset;
  int categoryNo;
  TasteContentType type;

  SearchContentViewState(this.categoryNo, this.type, this.onZzim, this.isReset);
  bool isGps = false;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  var contents = <ContentsModel>[];
  var selectedContents = <ContentsModel>[];
  List<BannerModel> banners = List<BannerModel>();

  Position position;
  int page = 1;
  SharedPreferences prefs;
  //
  ValueChanged onZzim;

  @override
  void initState() {
    super.initState();
  }

  Future<List<ContentsModel>> selectImageContents() async {
    prefs = await SharedPreferences.getInstance();
    String method = 'get';
    String path = '/api/v1/content/image_content/list';
    Map<String, dynamic> data = {'take': '20', 'page': this.page.toString()};

    switch (this.type) {
      case TasteContentType.all:
        data["recommend"] = '1';
        break;
      case TasteContentType.follow:
        data["follow_search"] = '1';
        data["more_field"] = 'users';
        break;
      case TasteContentType.category:
        data["category_no"] = categoryNo.toString();
        break;
    }

    if (this.isGps) {
      data["lat"] = this.position.latitude.toString();
      data["lng"] = this.position.longitude.toString();
    }
    return NetworkClient.shared
        .requestWithAuth(method, path, data)
        .then((response) {
      List<ContentsModel> contentsList = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();
      return Future.value(contentsList);
    });
  }

  Future<List<BannerModel>> selectBanners() {
    String method = 'get';
    String path = '/api/v1/banner/list?now_processed=1';

    return NetworkClient.shared
        .requestWithAuth(method, path, null)
        .then((response) {
      List<BannerModel> bannerList = (response.data as List)
          .map((record) => BannerModel.fromJson(record))
          .toList();
      return Future.value(bannerList);
    });
  }

  onRefresh() {
    if (this.type == TasteContentType.all) {
      this.selectBanners().then((banners) {
        this.banners = banners;
        setState(() {});
      });
    }

    this.page = 1;
    this.selectImageContents().then((contents) {
      this.contents = contents;
      setState(() {});
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    }).catchError((e) {
      this.contents = [];
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    });
  }

  onLoading() {
    this.page = this.page + 1;
    this.selectImageContents().then((contents) {
      this.contents.addAll(contents);
      setState(() {});
      if (contents.length == 0) {
        this.refreshController.loadNoData();
      } else {
        this.refreshController.loadComplete();
      }
    }).catchError((e) {
      this.refreshController.loadComplete();
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.onRefresh();
  }

  Widget emptyView() {
    String title = "";
    String desc = "";

    title = "내 주변에 등록된 장소가 없어요.";
    desc = "지금 내 주변의 장소를 등록해 주세요.";

    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 100),
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              desc,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [
        RefreshConfiguration(
          enableLoadingWhenNoData: false,
          footerTriggerDistance: 200,
          child: SmartRefresher(
            controller: this.refreshController,
            enablePullDown: true,
            enablePullUp: true,
            header: CustomSmartRefresher.customHeader(),
            footer: CustomSmartRefresher.customFooter(),
            onRefresh: this.onRefresh,
            onLoading: this.onLoading,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 25),
              children: [
                this.contents == null
                    ? Shimmer.fromColors(
                        child: Container(
                          child: StaggeredGridView.countBuilder(
                            shrinkWrap: true,
                            primary: false,
                            crossAxisCount: 3,
                            itemCount: 20,
                            itemBuilder: (BuildContext context, int index) {
                              return ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: Container(
                                    color: Colors.red,
                                  ));
                            },
                            staggeredTileBuilder: (index) =>
                                StaggeredTile.count((index % 7 == 0) ? 2 : 1,
                                    (index % 7 == 0) ? 2 : 1),
                            mainAxisSpacing: 11.0,
                            crossAxisSpacing: 11.0,
                          ),
                        ),
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey[100])
                    : this.contents.length == 0
                        ? emptyView()
                        : Container(
                            margin: EdgeInsets.only(top: 30),
                            child: StaggeredGridView.countBuilder(
                              shrinkWrap: true,
                              primary: false,
                              crossAxisCount: 3,
                              itemCount: this.contents.length,
                              itemBuilder: (BuildContext context, int index) {
                                ContentsModel contents = this.contents[index];
                                return Stack(
                                  children: [
                                    ContentsCell(
                                      contents: contents,
                                      onPressedUser: () {
                                        if (contents.userInfo == null) return;

                                        this.navigator.pushRoute(
                                            UserPage(contents.userInfo.userNo));
                                      },
                                      onPressedMore: () {
                                        this.showPlaceMoreSheetOther(
                                            onReport: (value) {
                                          String method = 'post';
                                          String path =
                                              '/api/v1/report/image_content/${contents.imageContentNo}';
                                          Map<String, dynamic> data = {
                                            'report_title': '신고하기',
                                            'report_description': value
                                          };

                                          NetworkClient.shared
                                              .requestWithAuth(
                                                  method, path, data)
                                              .then((response) {
                                            this.showToast('신고되었습니다.');
                                          }).catchError((e) {});
                                        });
                                      },
                                    ),
                                    Column(
                                      children: [
                                        Spacer(),
                                        Row(
                                          children: [
                                            Spacer(),
                                            CustomContainer(
                                              margin: EdgeInsets.only(
                                                  right: 12, bottom: 12),
                                              height: 44,
                                              width: 44,
                                              // decoration: BoxDecoration(borderRadius: BorderRadius.circular(36), color: this.isZzim ? ColorTheme.primary2 : Colors.grey),
                                              backgroundColor: selectedContents
                                                      .contains(contents)
                                                  ? ColorTheme.primary2
                                                  : Color.fromRGBO(
                                                      205, 210, 216, 1.0),
                                              borderRadius: [36, 36, 36, 36],
                                              child: Container(
                                                margin: EdgeInsets.all(10),
                                                child: Image.asset(
                                                  "assets/images/bookmark.png",
                                                  height: 28,
                                                  width: 28,
                                                ),
                                              ),
                                              onPressed: () {
                                                // this.isZzim = !this.isZzim;
                                                this.onZzim([
                                                  contents,
                                                  selectedContents
                                                ]);
                                                selectedContents
                                                        .contains(contents)
                                                    ? selectedContents
                                                        .remove(contents)
                                                    : selectedContents
                                                        .add(contents);
                                                setState(() {});
                                              },
                                            )
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                );
                              },
                              staggeredTileBuilder: (index) =>
                                  StaggeredTile.count((index % 7 == 0) ? 2 : 1,
                                      (index % 7 == 0) ? 2 : 1),
                              mainAxisSpacing: 11.0,
                              crossAxisSpacing: 11.0,
                            ),
                          ),
              ],
            ),
          ),
        ),
        Column(
          children: [
            Spacer(),
            selectedContents.length < 5
                ? Container()
                : CustomContainer(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.all(35),
                    backgroundColor: ColorTheme.primary2,
                    height: 52,
                    borderRadius: [18, 18, 18, 18],
                    shadowColor: ColorTheme.primary.withOpacity(.5),
                    onPressed: () {
                      if (this.isReset == true) {
                        this.navigator.resetRoute(SplashPage());
                      } else {
                        this.navigator.popRoute(null);
                      }
                    },
                    child: Center(
                      child: Text(
                        '찜 시작하기',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  )
          ],
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
