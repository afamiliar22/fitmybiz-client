import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/themes/themes.dart';

class CircleCheckBox extends StatelessWidget {
  final double size;
  final Color color;
  final bool value;
  CircleCheckBox({
    this.value,
    this.size,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Row(
      children: [
        Container(
          decoration: this.value ? checkedDecoration() : uncheckedDecoration(),
          child: Padding(
            padding: EdgeInsets.all(2),
            child: this.value ? checkedIcon() : uncheckedIcon(),
          ),
        ),
      ],
    ));
  }

  BoxDecoration checkedDecoration() {
    return BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        border: Border.all(color: this.color ?? Colors.blue, width: 2));
  }

  BoxDecoration uncheckedDecoration() {
    return BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        border: Border.all(color: Colors.grey.withOpacity(0.2), width: 2));
  }

  Widget checkedIcon() {
    return Icon(
      Icons.circle,
      size: this.size ?? 16,
      color: this.color ?? Colors.blue,
    );
  }

  Widget uncheckedIcon() {
    return Icon(
      Icons.check,
      size: this.size ?? 16,
      color: Colors.transparent,
    );
  }
}
