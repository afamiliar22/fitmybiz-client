import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/routes/auth/auth_page_temp.dart';
import 'package:fitmybiz/routes/splash/splash_cubit.dart';
import 'package:fitmybiz/routes/splash/splash_state.dart';
import 'package:fitmybiz/routes/tabs/tabs_page.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SplashCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  List<CategoryModel> categories = List<CategoryModel>();

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    context.bloc<SplashCubit>().init();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<SplashCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case GetCategory:
            categories = state.categories;

            this.navigator.resetRoute(TabsPage(categories));
            this.indicator.hide();

            break;
          case AuthState:
            if (state.result == true) {
              context.bloc<SplashCubit>().getCategories(0, '', 0, 0);
            } else {
              this.navigator.resetRoute(AuthPage());
            }
            break;
        }
      },
      child: PlatformScaffold(
        body: Container(),
      ),
    );
  }
}
