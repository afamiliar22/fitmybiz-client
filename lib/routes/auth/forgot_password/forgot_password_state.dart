import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';

abstract class ForgotPasswordState extends Equatable {}

class InitialState extends ForgotPasswordState {
  @override
  List<Object> get props => [];
}

class SetState extends ForgotPasswordState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class LoadingState extends ForgotPasswordState {
  @override
  List<Object> get props => [];
}

class ToastState extends ForgotPasswordState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class RequestedState extends ForgotPasswordState {
  @override
  List<Object> get props => [];
}

class CompletedState extends ForgotPasswordState {
  @override
  List<Object> get props => [];
}

class ErrorState extends ForgotPasswordState {
  @override
  List<Object> get props => [];
}
