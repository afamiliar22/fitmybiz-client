import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_button.dart';

class CustomSearchBar extends StatefulWidget {
  final Function() onEnter;
  final Function() onLeave;
  final Function() onClear;
  final Function(String) onSubmitted;
  final Function(String) onValueChange;
  final TextEditingController controller;
  final String placeholder;
  final String btnText;
  final Widget btnSearch;
  final bool isFocus;
  CustomSearchBar(
      {this.onEnter,
      this.onLeave,
      this.onClear,
      this.onSubmitted,
      this.onValueChange,
      this.placeholder,
      this.controller,
      this.btnText,
      this.btnSearch,
      this.isFocus});

  @override
  CustomSearchBarState createState() => CustomSearchBarState(
      this.onEnter,
      this.onLeave,
      this.onClear,
      this.onSubmitted,
      this.onValueChange,
      this.placeholder,
      this.controller,
      this.btnText,
      this.btnSearch,
      this.isFocus);
}

class CustomSearchBarState extends State<CustomSearchBar> {
  Function() onEnter;
  Function() onLeave;
  Function(String) onValueChange;
  Function() onClear;
  Function(String) onSubmitted;
  TextEditingController controller;
  String placeholder;
  String btnText;
  Widget btnSearch;
  bool isFocus;

  CustomSearchBarState(
      this.onEnter,
      this.onLeave,
      this.onClear,
      this.onSubmitted,
      this.onValueChange,
      this.placeholder,
      this.controller,
      this.btnText,
      this.btnSearch,
      this.isFocus);

  bool isEnter = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 40,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 4,
                    spreadRadius: 2,
                    offset: Offset(0, 1)),
              ],
              borderRadius: BorderRadius.circular(8), // if you need this
            ),
            child: Row(
              children: [
                SizedBox(width: 8),
                SizedBox(
                  height: 32,
                  width: 32,
                  child: Icon(
                    Icons.search,
                    color: Colors.black.withAlpha(50),
                  ),
                ),
                Flexible(
                  child: CupertinoTextField(
                    autofocus: this.isFocus ?? false,
                    textAlignVertical: TextAlignVertical.center,
                    placeholder: this.placeholder ?? "",
                    controller: this.controller,
                    onChanged: (value) {
                      this.onTextChange();
                    },
                    maxLines: 1,
                    style: TextStyle(fontSize: 14),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.transparent),
                    ),
                    onTap: () {
                      this.onEnterAction();
                    },
                    onSubmitted: (str) {
                      onSubmitted(str);
                    },
                  ),
                ),
                Visibility(
                  visible: this.controller.text.length > 0,
                  child: SizedBox(
                    height: 32,
                    width: 32,
                    child: GestureDetector(
                      onTap: () {
                        this.onClearAction();
                      },
                      child: Icon(CupertinoIcons.clear_thick_circled,
                          color: Colors.black.withAlpha(100)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        /*
        CupertinoButton(
          padding: EdgeInsets.only(left: 8),
          child: this.btnText == null ? this.btnSearch : Text(this.btnText ?? ''),
          onPressed: () {
            this.onCancelAction();
          },
        )
        */
      ],
    );
  }

  void onEnterAction() {
    setState(() {
      this.isEnter = true;
      this.onEnter();
    });
  }

  void onClearAction() {
    setState(() {
      this.controller.text = "";
      this.onClear();
    });
  }

  void onCancelAction() {
    this.controller.text = "";
    FocusScope.of(context).unfocus();
    this.isEnter = false;
    this.onLeave();
  }

  void onTextChange() {
    setState(() {});
    onValueChange(this.controller.text);
  }
}
