import 'dart:math';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/extensions/color_extension.dart';
import 'package:fitmybiz/models/blog_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/google_place_model.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_cubit.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_state.dart';
import 'package:fitmybiz/routes/place_detail/view/place_detail_header.dart';
import 'package:fitmybiz/routes/place_detail/zzim/place_zzim_page.dart';
import 'package:fitmybiz/routes/place_management/update_place/update_place_page.dart';
import 'package:fitmybiz/routes/user/user_page.dart';
import 'package:fitmybiz/routes/views/custom_image_view.dart';
import 'package:fitmybiz/routes/views/search_route_sheet.dart';
import 'package:fitmybiz/routes/views/custom_web_view.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/network_client.dart';
import 'package:share/share.dart';
//
import 'package:html/parser.dart';
import 'package:url_launcher/url_launcher.dart';

class PlaceDetailPage extends StatelessWidget {
  // const PlaceDetailPage({Key key}) : super(key: key);

  final String imageContentNo;
  PlaceDetailPage(this.imageContentNo);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => PlaceDetailCubit(),
      child: _View(this.imageContentNo),
    );
  }
}

class _View extends BaseRoute {
  final String imageContentNo;
  _View(this.imageContentNo);
  @override
  _ViewState createState() => _ViewState(this.imageContentNo);
}

class _ViewState extends BaseRouteState {
  String imageContentNo;
  _ViewState(this.imageContentNo);
  ScrollController _controller;
  ContentsModel content;
  GooglePlaceModel gContent;

  String interval = "";

  List<Widget> cts = [];
  bool isZzim;
  bool selectedFollow = false;
  PlaceDetailCubit cubit;
  List<BlogModel> blogs = [];
  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    this.cubit = context.bloc<PlaceDetailCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    Map<String, dynamic> data = {
      "image_content_no": this.imageContentNo.toString(),
      "more_field": true ? "*" : "is_follow,is_zzim,categories,contents,users",
    };
    context.bloc<PlaceDetailCubit>().getImageContents(data);
  }

  //here goes the function
  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<PlaceDetailCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();

            break;
          case ToastState:
            this.showToast(state.message);
            break;

          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case GetGoogleReview:
            this.indicator.hide();
            this.gContent = state.contents;
            setState(() {});
            break;
          case GetBlogContents:
            this.indicator.hide();
            this.blogs = state.contents;

            setState(() {});
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            Navigator.of(context).popUntil((route) => route.isFirst);
            break;
          case GetContents:
            this.indicator.hide();
            this.content = state.contents[0];

            if (this.content.imageSrc.height > this.content.imageSrc.width) {
              this._controller = ScrollController(initialScrollOffset: 0);
            } else {
              this._controller =
                  ScrollController(initialScrollOffset: true ? 0 : 300);
            }

            this.selectedFollow = this.content.isFollow;
            this.cts.clear();

            for (int i = 0; i < this.content.categories.length; i += 1) {
              this.cts.add(Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: true
                            ? ColorTheme.categoryColors
                                .where((e) =>
                                    ColorTheme.categoryColors.indexOf(e) + 1 ==
                                    this.content.categories[i].categoryNo)
                                .first
                            : false
                                ? HexColor.fromHex(
                                    this.content.categories[i].categoryColor)
                                : Color.fromRGBO(255, 157, 81, 1.0)),
                    child: Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                        child: Text(
                          this.content.categories[i].categoryName,
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                        )),
                  ));
            }

            Geolocator.getLastKnownPosition().then((value) {
              double a = Geolocator.distanceBetween(value.latitude,
                  value.longitude, this.content.lat, this.content.lng);

              this.interval = (a / 1000).toStringAsFixed(1);
              this.interval += "KM";
              setState(() {});
            });

            this.isZzim = this.content.isZzim;
            setState(() {});
            break;
        }
      },
      child: Stack(
        children: [
          this.content == null
              ? Container(
                  color: Colors.white,
                )
              : PlatformScaffold(
                  body: Column(
                    children: [
                      Flexible(
                        child: CustomScrollView(
                          physics: BouncingScrollPhysics(),
                          controller: this._controller,
                          slivers: [
                            SliverPersistentHeader(
                              delegate: PlaceDetailHeader(
                                  contents: this.content,
                                  placeId: '1',
                                  isFind: true,
                                  isZzim: this.content.isZzim,
                                  onZzim: () {
                                    this
                                        .navigator
                                        .presentRoute(PlaceZzimPage(
                                            this.content.imageContentNo))
                                        .then((value) {
                                      if (value == 'refresh') {
                                        Map<String, dynamic> data = {
                                          "image_content_no":
                                              this.imageContentNo.toString(),
                                          "more_field":
                                              "is_follow,is_zzim,categories,contents,users",
                                        };
                                        context
                                            .bloc<PlaceDetailCubit>()
                                            .getImageContents(data);
                                      }
                                    });
                                    setState(() {});
                                  },
                                  onPressed: () {
                                    if (this.content.resourceUrl != null) {
                                      this.navigator.pushRoute(CustomWebView(
                                          this.content.resourceUrl,
                                          null,
                                          null));
                                    } else {
                                      this.navigator.pushRoute(CustomImageView(
                                          this.content.imageSrc.sm, null));
                                    }
                                  }),
                            ),
                            SliverList(
                              delegate: SliverChildListDelegate(
                                [
                                  Container(
                                    color: Colors.transparent,
                                    /*
                                    height: this.content.description == null
                                        ? MediaQuery.of(context).size.height
                                        : (this.content.description.length /
                                                (MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    110)) +
                                            MediaQuery.of(context).size.height,
                                            */
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 20),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 20),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                  margin: EdgeInsets.only(
                                                    bottom: 16,
                                                  ),
                                                  child: Wrap(
                                                      direction:
                                                          Axis.horizontal,
                                                      alignment:
                                                          WrapAlignment.start,
                                                      spacing: 10,
                                                      runSpacing: 10,
                                                      children: cts)),
                                              Text(
                                                this.content.title ?? '',
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: TextStyle(
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              SizedBox(height: 15),
                                              Container(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Expanded(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Container(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Image.asset(
                                                                  'assets/images/iconMapFilled.png',
                                                                  width: 18,
                                                                ),
                                                                SizedBox(
                                                                    width: 4),
                                                                Expanded(
                                                                  child: Text(
                                                                    this.content.contentInfo ==
                                                                            null
                                                                        ? ""
                                                                        : this
                                                                            .content
                                                                            .contentInfo
                                                                            .formattedAddress,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    maxLines: 1,
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            13,
                                                                        color: Colors
                                                                            .grey[500]),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 4),
                                                            child: Row(
                                                                children: [
                                                                  Image.asset(
                                                                    'assets/images/iconHunt.png',
                                                                    width: 18,
                                                                  ),
                                                                  SizedBox(
                                                                      width: 4),
                                                                  Text(
                                                                      this
                                                                          .interval,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              13,
                                                                          color:
                                                                              Colors.grey[500])),
                                                                ]),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    CustomContainer(
                                                      backgroundColor:
                                                          ColorTheme.grayBg,
                                                      margin: EdgeInsets.only(
                                                          left: 4),
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              vertical: 4,
                                                              horizontal: 8),
                                                      borderWidth: 1,
                                                      borderColor:
                                                          Colors.grey[200],
                                                      borderRadius: [
                                                        30,
                                                        30,
                                                        30,
                                                        30
                                                      ],
                                                      child: Container(
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                vertical: 4,
                                                                horizontal: 8),
                                                        child: Row(
                                                          children: [
                                                            Visibility(
                                                              visible: true,
                                                              child: Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        right:
                                                                            8),
                                                                child:
                                                                    CustomButton(
                                                                  image: Image
                                                                      .asset(
                                                                    "assets/images/information.png",
                                                                    height: 24,
                                                                    width: 24,
                                                                    color: ColorTheme
                                                                        .grayLv1,
                                                                  ),
                                                                  onPressed:
                                                                      () {
                                                                    this.navigator.pushRoute(CustomWebView(
                                                                        'https://www.google.com/maps/place/?q=place_id:${this.content.contentInfo.googlePlaceId}',
                                                                        null,
                                                                        null));
                                                                    return;
                                                                    if (this
                                                                            .content
                                                                            .contentInfo
                                                                            .resourceUrl !=
                                                                        null) {
                                                                      this.navigator.pushRoute(CustomWebView(
                                                                          this
                                                                              .content
                                                                              .contentInfo
                                                                              .resourceUrl,
                                                                          null,
                                                                          null));
                                                                    }
                                                                  },
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              child:
                                                                  Image.asset(
                                                                'assets/images/iconDirectionOutline.png',
                                                                width: 24,
                                                                color: ColorTheme
                                                                    .grayLv1,
                                                              ),
                                                            ),

                                                            /*
                                                            Container(
                                                              child: Text(
                                                                "길 찾기",
                                                                style: TextStyle(fontSize: 13, color: Colors.black, fontWeight: FontWeight.w600),
                                                              ),
                                                            )
                                                            */
                                                          ],
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        showModalBottomSheet(
                                                          useRootNavigator:
                                                              true,
                                                          backgroundColor:
                                                              Colors
                                                                  .transparent,
                                                          context: context,
                                                          builder: (context) =>
                                                              SearchRouteSheet(
                                                            onPressed: (type) {
                                                              Geolocator
                                                                      .getCurrentPosition()
                                                                  .then(
                                                                      (position) {
                                                                return this.launch.moveToMap(
                                                                    type: type,
                                                                    sourceLat:
                                                                        position
                                                                            .latitude,
                                                                    sourceLng:
                                                                        position
                                                                            .longitude,
                                                                    targetLat: this
                                                                        .content
                                                                        .lat,
                                                                    targetLng: this
                                                                        .content
                                                                        .lng);
                                                              }).catchError(
                                                                      (e) {
                                                                if (e
                                                                    is String) {
                                                                  this.showToast(
                                                                      e);
                                                                }
                                                              });
                                                            },
                                                          ),
                                                        );
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        SizedBox(height: 25),
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 14),
                                          child: Html(
                                            data:
                                                this.content.description ?? "",
                                            style: {
                                              "html": Style(
                                                color: Colors.grey[500],
                                                fontSize: FontSize(14),
                                              )
                                            },
                                          ),
                                        ),
                                        SizedBox(
                                          height: 40,
                                        ),
                                        CustomContainer(
                                          backgroundColor: ColorTheme.grayBg,
                                          height: 12,
                                          width:
                                              MediaQuery.of(context).size.width,
                                        ),
                                        //구글리뷰
                                        SizedBox(
                                          height: 40,
                                        ),
                                        CustomContainer(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 25),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    '구글 리뷰',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color:
                                                            ColorTheme.black),
                                                  ),
                                                  Spacer(),
                                                  Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        gContent == null
                                                            ? '5.0'
                                                            : '${gContent?.rating ?? 5.0}',
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            color: ColorTheme
                                                                .black,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      RatingBar(
                                                        ignoreGestures: true,
                                                        initialRating: gContent ==
                                                                null
                                                            ? 5.0
                                                            : gContent
                                                                    ?.rating ??
                                                                5.0,
                                                        direction:
                                                            Axis.horizontal,
                                                        allowHalfRating: true,
                                                        itemCount: 5,
                                                        ratingWidget:
                                                            RatingWidget(
                                                          full: Image.asset(
                                                              'assets/images/icnsStar.png'),
                                                          half: Image.asset(
                                                              'assets/images/icnsStarHalf.png'),
                                                          empty: Image.asset(
                                                            'assets/images/icnsStar.png',
                                                            color: ColorTheme
                                                                .point
                                                                .withOpacity(
                                                                    .5),
                                                          ),
                                                        ),
                                                        onRatingUpdate:
                                                            (rating) {
                                                          print(rating);
                                                        },
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),

                                              /*
                                              Row(
                                                children: [
                                                  Text(
                                                    '인스타그램 검색 결과',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Spacer(),
                                                  CustomContainer(
                                                    child: Text(
                                                      '더보기',
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          color: ColorTheme
                                                              .grayLv1),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              SizedBox(
                                                height: 101,
                                                child: ListView.builder(
                                                    itemCount: 10,
                                                    shrinkWrap: true,
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return CustomContainer(
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                horizontal:
                                                                    5.5),
                                                        borderRadius: [
                                                          20,
                                                          20,
                                                          20,
                                                          20
                                                        ],
                                                        height: 101,
                                                        width: 101,
                                                        child: ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20),
                                                            child:
                                                                CustomCachedNetworkImage(
                                                              preloadImage:
                                                                  'https://images.unsplash.com/photo-1606851094655-b2593a9af63f?ixid=MXwxMjA3fDF8MHxzZWFyY2h8MXx8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60',
                                                              image:
                                                                  'https://images.unsplash.com/photo-1606851094655-b2593a9af63f?ixid=MXwxMjA3fDF8MHxzZWFyY2h8MXx8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60',
                                                            )),
                                                      );
                                                    }),
                                              ),
                                              */
                                              //블로그
                                              SizedBox(
                                                height: 40,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    '블로그 검색 결과',
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Column(
                                                children: blogs
                                                    .map((item) =>
                                                        CustomContainer(
                                                          onPressed: () {
                                                            this.navigator.pushRoute(
                                                                CustomWebView(
                                                                    item.url,
                                                                    item.blogname,
                                                                    null));
                                                          },
                                                          margin: EdgeInsets
                                                              .symmetric(
                                                                  vertical: 6),
                                                          height: 60,
                                                          child: Row(
                                                            children: [
                                                              SizedBox(
                                                                height: 60,
                                                                width: 60,
                                                                child:
                                                                    ClipRRect(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              12),
                                                                  child:
                                                                      Material(
                                                                    child:
                                                                        CachedNetworkImage(
                                                                      imageUrl:
                                                                          item.thumbnail,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 15,
                                                              ),
                                                              Expanded(
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Row(
                                                                      children: [
                                                                        Flexible(
                                                                            child: true
                                                                                ? Text(
                                                                                    _parseHtmlString(item.title),
                                                                                    overflow: TextOverflow.ellipsis,
                                                                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: ColorTheme.black.withOpacity(1)),
                                                                                  )
                                                                                : Html(
                                                                                    data: item.title ?? "",
                                                                                    style: {
                                                                                      "html": Style(
                                                                                        color: Colors.grey[500],
                                                                                        fontSize: FontSize(14),
                                                                                      )
                                                                                    },
                                                                                  ))
                                                                      ],
                                                                    ),
                                                                    SizedBox(
                                                                      height: 2,
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Flexible(
                                                                            child: true
                                                                                ? Text(
                                                                                    _parseHtmlString(item.contents),
                                                                                    overflow: TextOverflow.ellipsis,
                                                                                    style: TextStyle(fontSize: 15, color: ColorTheme.black.withOpacity(.7)),
                                                                                  )
                                                                                : Html(
                                                                                    data: item.contents ?? "",
                                                                                    style: {
                                                                                      "html": Style(
                                                                                        color: Colors.grey[500],
                                                                                        fontSize: FontSize(14),
                                                                                      )
                                                                                    },
                                                                                  ))
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ))
                                                    .toList(),
                                              ),
                                              SizedBox(
                                                height: 40,
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /*
                      if (this.content.userInfo.userNo ==
                          this.property.user.userNo)
                        Container(
                          child: SafeArea(
                            top: false,
                            child: Container(),
                          ),
                        ),
                      if (this.content.userInfo.userNo !=
                          this.property.user.userNo)
                        Container(
                          color: Color.fromRGBO(244, 246, 248, 1.0),
                          child: SafeArea(
                            top: false,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Row(
                                  children: [
                                    CustomContainer(
                                      margin: EdgeInsets.only(
                                          left: 25,
                                          top: 16,
                                          bottom: 16,
                                          right: 10),
                                      width: 44,
                                      height: 44,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(26),
                                        child: this
                                                    .content
                                                    .userInfo
                                                    .profileImage !=
                                                null
                                            ? CustomCachedNetworkImage(
                                                preloadImage: this
                                                    .content
                                                    .userInfo
                                                    .profileImage
                                                    .sm,
                                                image: this
                                                    .content
                                                    .userInfo
                                                    .profileImage
                                                    .sm,
                                              )
                                            : Image.asset(
                                                'assets/images/userProfileImg.png',
                                                fit: BoxFit.cover,
                                              ),
                                      ),
                                      onPressed: () {
                                        this.navigator.pushRoute(UserPage(
                                            this.content.userInfo.userNo));
                                      },
                                    ),
                                    CustomContainer(
                                      child: Text(
                                        this.content.userInfo.nickname,
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      onPressed: () {
                                        this.navigator.pushRoute(UserPage(
                                            this.content.userInfo.userNo));
                                      },
                                    ),
                                    Spacer(),
                                    this.content.userInfo.state == '0'
                                        ? Container()
                                        : this.selectedFollow
                                            ? Container(
                                                margin:
                                                    EdgeInsets.only(right: 20),
                                                child: CustomContainer(
                                                  backgroundColor:
                                                      Color.fromRGBO(
                                                          244, 246, 248, 1.0),
                                                  borderRadius: [
                                                    10,
                                                    10,
                                                    10,
                                                    10
                                                  ],
                                                  borderWidth: 1,
                                                  borderColor: Color.fromRGBO(
                                                      173, 181, 190, 1),
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 2,
                                                      horizontal: 4),
                                                  child: Container(
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 8,
                                                            horizontal: 16),
                                                    child: Text(
                                                      "팔로잉",
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              173, 181, 190, 1),
                                                          fontSize: 13),
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    this.selectedFollow =
                                                        !this.selectedFollow;
                                                    setState(() {});
                                                    this.cubit.putUpdateFollow(
                                                        this.content.userNo);
                                                  },
                                                ))
                                            : Container(
                                                margin:
                                                    EdgeInsets.only(right: 20),
                                                child: CustomContainer(
                                                  backgroundColor:
                                                      Color.fromRGBO(
                                                          31, 31, 36, 1),
                                                  borderRadius: [
                                                    10,
                                                    10,
                                                    10,
                                                    10
                                                  ],
                                                  borderColor: Color.fromRGBO(
                                                      31, 31, 36, 1),
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 2,
                                                      horizontal: 4),
                                                  child: Container(
                                                    margin:
                                                        EdgeInsets.symmetric(
                                                            vertical: 8,
                                                            horizontal: 16),
                                                    child: Text(
                                                      "팔로우",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 13),
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    this.selectedFollow =
                                                        !this.selectedFollow;
                                                    setState(() {});
                                                    this.cubit.putUpdateFollow(
                                                        this.content.userNo);
                                                  },
                                                ),
                                              )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        */
                    ],
                  ),
                ),
          Positioned(
            right: 0,
            left: 0,
            top: 0,
            child: SafeArea(
              child: Row(
                children: [
                  IconButton(
                    padding:
                        EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
                    icon: Image.asset('assets/images/icnsClose.png',
                        color: Colors.white),
                    onPressed: () {
                      this.navigator.popRoute(null);
                    },
                  ),
                  Spacer(),
                  /*
                  Visibility(
                    visible: true,
                    child: Container(
                      margin: EdgeInsets.only(right: 8),
                      child: CustomButton(
                        image: Image.asset(
                          "assets/images/information.png",
                          height: 28,
                          width: 28,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          if (this.content.resourceUrl != null) {
                            this.navigator.pushRoute(CustomWebView(this.content.resourceUrl, null, null));
                          } else {
                            this.navigator.pushRoute(CustomImageView(this.content.imageSrc.sm, null));
                          }
                        },
                      ),
                    ),
                  ),
                  */
                  Container(
                    //margin: EdgeInsets.only(left: 8, right: 8),
                    child: CustomButton(
                      image: Image.asset(
                        "assets/images/iconShare.png",
                        height: 28,
                        width: 28,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        this.indicator.show();
                        this
                            .firebase
                            .generateDynamicLink(
                                path: '/place/${this.content.imageContentNo}',
                                title: this.content.title,
                                description: this.content.description,
                                image: this.content.imageSrc.l)
                            .then((value) {
                          this.indicator.hide();
                          Share.share(
                            value,
                          );
                        }).catchError((e) {
                          this.indicator.hide();
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Container(
                    child: CustomButton(
                      image: Image.asset(
                        "assets/images/icnsMore.png",
                        height: 28,
                        width: 28,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        // this.showPlaceMoreSheet(this.content.imageContentNo);
                        if (this.content.userNo == this.property.user.userNo) {
                          this.showPlaceMoreSheetMe(onUpdate: () {
                            this
                                .navigator
                                .presentRoute(UpdatePlacePage(
                                    this.content.imageContentNo))
                                .then((value) {
                              if (value == true) {
                                Map<String, dynamic> data = {
                                  "image_content_no":
                                      this.imageContentNo.toString(),
                                  "more_field":
                                      "is_follow,is_zzim,categories,contents,users",
                                };
                                context
                                    .bloc<PlaceDetailCubit>()
                                    .getImageContents(data);
                              }
                            });
                          }, onDelete: () {
                            this.cubit.deleteImageContent(this.imageContentNo);
                            return;
                            String method = 'delete';
                            String path =
                                '/api/v1/report/image_content/${this.imageContentNo}';
                            /*
                            Map<String, dynamic> data = {
                              "image_content_no": this.imageContentNo.toString()
                            };
                            */
                            NetworkClient.shared
                                .requestWithAuth(method, path, null)
                                .then((response) {
                              this.navigator.popRoute(null);
                            }).catchError((e) {});
                            /*
                            this.cubit.deleteCollectionItem(
                                  this.cubit.collectionNo.toString(),
                                  collectionItem.collectionItemNo,
                                );
                                */
                          });
                        } else {
                          this.showPlaceMoreSheetOther(onReport: (value) {
                            this.cubit.insertImageContentReport(
                                imageContentNo, value);
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  this.content == null
                      ? Container(
                          color: Colors.white,
                        )
                      : CustomContainer(
                          backgroundColor: Colors.white,
                          borderRadius: [18.5, 18.5, 18.5, 18.5],
                          /*
                          margin: EdgeInsets.only(
                              left: 25, top: 16, bottom: 16, right: 10),
                              */
                          width: 37,
                          height: 37,
                          child: Center(
                            child: CustomContainer(
                              width: 36,
                              height: 36,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(18),
                                child:
                                    this.content.userInfo.profileImage != null
                                        ? CustomCachedNetworkImage(
                                            preloadImage: this
                                                .content
                                                .userInfo
                                                .profileImage
                                                .sm,
                                            image: this
                                                .content
                                                .userInfo
                                                .profileImage
                                                .sm,
                                          )
                                        : Image.asset(
                                            'assets/images/userProfileImg.png',
                                            fit: BoxFit.cover,
                                          ),
                              ),
                            ),
                          ),
                          onPressed: () {
                            this.navigator.pushRoute(
                                UserPage(this.content.userInfo.userNo));
                          },
                        ),
                  SizedBox(width: 25),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
