import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_smart_refresher.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_item_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/_cells/collection_item_cell.dart';
import 'package:fitmybiz/routes/edit_collection_items/edit_collection_items_page.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';
import 'package:fitmybiz/routes/place_management/update_place/update_place_page.dart';
import 'package:fitmybiz/routes/views/place_range_sheet.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'my_place_cubit.dart';
import 'my_place_state.dart';

class MyPlacePage extends StatelessWidget {
  final String collectionNo;
  final String collectionName;
  final bool isEdit;
  final bool useIsWrite;
  final UserModel user;
  MyPlacePage(
      this.collectionNo, this.collectionName, this.isEdit, this.useIsWrite,
      {Key key, this.user})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MyPlaceCubit(),
      child: _View(
        this.collectionNo,
        this.collectionName,
        this.isEdit,
        this.useIsWrite,
        user: user,
      ),
    );
  }
}

class _View extends BaseRoute {
  final String collectionNo;
  final String collectionName;
  final bool isEdit;
  final bool useIsWrite;
  final UserModel user;
  _View(this.collectionNo, this.collectionName, this.isEdit, this.useIsWrite,
      {this.user});
  @override
  _ViewState createState() => _ViewState(
      this.collectionNo, this.collectionName, this.isEdit, this.useIsWrite,
      user: user);
}

class _ViewState extends BaseRouteState {
  String collectionNo;
  String collectionName;
  bool isEdit;
  bool useIsWrite;
  final UserModel user;

  bool isGps = false;

  Position position;
  _ViewState(
      this.collectionNo, this.collectionName, this.isEdit, this.useIsWrite,
      {this.user});

  MyPlaceCubit cubit;
  //double radius;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<MyPlaceCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
    this.cubit.collectionNoChanged(collectionNo);
    // this.cubit.selectCollectionItems();
    this.cubit.onRefresh();
  }

  List<String> areas = [];
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadedState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text(
          this.collectionName ?? '',
        ),
        /*
        trailing: this.user == null
            ? Container()
            : Padding(
                padding: EdgeInsets.only(right: 25),
                child: CustomAvatar(
                  size: 36,
                  image: CustomCachedNetworkImage(
                    image: this.user.profileImage.sm,
                    preloadImage: this.user.profileImage.sm,
                  ),
                ),
              ),
              */
        // trailing: this.isEdit ? buildEditButton() : null,
        body: SafeArea(
          child: Stack(
            children: [
              Column(
                children: [
                  //if (this.useIsWrite) this.buildIsWriteSwitch(),
                  Expanded(
                    child: RefreshConfiguration(
                      enableLoadingWhenNoData: false,
                      footerTriggerDistance: 200,
                      child: SmartRefresher(
                        controller: this.cubit.refreshController,
                        enablePullDown: true,
                        enablePullUp: true,
                        header: CustomSmartRefresher.customHeader(),
                        footer: CustomSmartRefresher.customFooter(),
                        onRefresh: this.cubit.onRefresh,
                        onLoading: this.cubit.onLoading,
                        child: true
                            ? StaggeredGridView.countBuilder(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 12),
                                shrinkWrap: true,
                                primary: false,
                                crossAxisCount: 3,
                                itemCount: this.cubit.collectionItems.length,
                                itemBuilder: (BuildContext context, int index) {
                                  CollectionItemModel collectionItem =
                                      this.cubit.collectionItems[index];
                                  return CollectionItemCell(
                                    collectionItem: collectionItem,
                                    onPressed: () {
                                      this.navigator.pushRoute(PlaceDetailPage(
                                          collectionItem.imageContentNo));
                                    },
                                    onPressedMore: () {
                                      if (collectionItem.isWrite == true) {
                                        this.showPlaceMoreSheetMe(
                                          onDelete: () {
                                            this.cubit.deleteCollectionItem(
                                                  this
                                                      .cubit
                                                      .collectionNo
                                                      .toString(),
                                                  collectionItem
                                                      .collectionItemNo,
                                                );
                                          },
                                          onUpdate: () {
                                            this.navigator.presentRoute(
                                                UpdatePlacePage(collectionItem
                                                    .imageContentNo));
                                          },
                                        );
                                      } else {
                                        this.showPlaceMoreSheetOther(
                                          onReport: (value) {
                                            this.cubit.insertImageContentReport(
                                                collectionItem.imageContentNo,
                                                value);
                                          },
                                        );
                                      }
                                    },
                                  );
                                },
                                staggeredTileBuilder: (index) =>
                                    StaggeredTile.count(
                                        (index % 7 == 0) ? 2 : 1,
                                        (index % 7 == 0) ? 2 : 1),
                                mainAxisSpacing: 8.0,
                                crossAxisSpacing: 8.0,
                              )
                            : StaggeredGridView.countBuilder(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 25, vertical: 12),
                                crossAxisCount: 4,
                                itemCount: this.cubit.collectionItems.length,
                                itemBuilder: (BuildContext context, int index) {
                                  CollectionItemModel collectionItem =
                                      this.cubit.collectionItems[index];
                                  return CollectionItemCell(
                                    collectionItem: collectionItem,
                                    onPressed: () {
                                      this.navigator.pushRoute(PlaceDetailPage(
                                          collectionItem.imageContentNo));
                                    },
                                    onPressedMore: () {
                                      if (collectionItem.isWrite == true) {
                                        this.showPlaceMoreSheetMe(
                                          onDelete: () {
                                            this.cubit.deleteCollectionItem(
                                                  this
                                                      .cubit
                                                      .collectionNo
                                                      .toString(),
                                                  collectionItem
                                                      .collectionItemNo,
                                                );
                                          },
                                          onUpdate: () {
                                            this.navigator.presentRoute(
                                                UpdatePlacePage(collectionItem
                                                    .imageContentNo));
                                          },
                                        );
                                      } else {
                                        this.showPlaceMoreSheetOther(
                                          onReport: (value) {
                                            this.cubit.insertImageContentReport(
                                                collectionItem.imageContentNo,
                                                value);
                                          },
                                        );
                                      }
                                    },
                                  );
                                },
                                staggeredTileBuilder: (int index) =>
                                    StaggeredTile.count(
                                        2,
                                        this
                                                .cubit
                                                .collectionItems[index]
                                                .imageSrc
                                                .height /
                                            this
                                                .cubit
                                                .collectionItems[index]
                                                .imageSrc
                                                .width *
                                            2),
                                mainAxisSpacing: 8.0,
                                crossAxisSpacing: 8.0,
                              ),
                      ),
                    ),
                  ),
                ],
              ),
              if (this.isEdit) this.buildEditButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildIsWriteSwitch() {
    return Container(
      margin: EdgeInsets.only(top: 8, left: 12),
      child: Row(
        children: [
          Container(
            child: Transform.scale(
              scale: 0.7,
              child: CupertinoSwitch(
                activeColor: ColorTheme.primary,
                value: this.cubit.isWrite,
                onChanged: (value) => this.cubit.isWriteChanged(value),
              ),
            ),
          ),
          Container(
            child: Text.rich(
              TextSpan(children: [
                TextSpan(
                    text: '내가 올린 장소',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 13)),
                TextSpan(
                    text: '만 보기',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 13,
                        color: Colors.grey))
              ]),
            ),
          )
        ],
      ),
    );
  }

  Widget buildEditButton() {
    return Positioned(
      bottom: 25,
      left: 25,
      child: Row(
        children: [
          CustomContainer(
            backgroundColor: ColorTheme.black,
            borderRadius: [40, 40, 40, 40],
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Row(
              children: [
                GestureDetector(
                  child: Image.asset('assets/images/icnsOrganize.png',
                      width: 28, height: 28, color: Colors.white),
                  onTap: () {
                    this
                        .navigator
                        .presentRoute(EditCollectionItemsPage(
                            this.cubit.collectionNo,
                            this.cubit.collectionItems))
                        .then(
                      (value) {
                        if (value == true) {
                          this.navigator.popRoute(true);
                        }
                      },
                    );
                  },
                ),
              ],
            ),
          ),
          /*
          SizedBox(
            width: MediaQuery.of(context).size.width / 2 - 180,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.0),
            child: FutureBuilder(
                future: SharedPreferences.getInstance(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    SharedPreferences prefs = snapshot.data;
                    radius = prefs.getDouble('radius') ?? 0.0;
                    return FloatingActionButton.extended(
                        heroTag: null,
                        icon: Icon(Icons.arrow_drop_down),
                        backgroundColor: ColorTheme.black,
                        onPressed: () {
                          showModalBottomSheet(
                              backgroundColor: Colors.transparent,
                              context: context,
                              builder: (context) {
                                return PlaceRangeSheet(
                                  onPressed: (value) {
                                    radius = value;

                                    this.isGps = radius == 0 ? false : true;
                                    if (!this.isGps) {
                                      radius = null;
                                    }
                                    Geolocator.getLastKnownPosition()
                                        .then((value) {
                                      this.position = value;
                                      setState(() {});
                                      this.cubit.onRefresh(radius);
                                    });
                                    Geolocator.checkPermission().then((value) {
                                      if (value == LocationPermission.denied) {
                                        this.isGps = false;
                                        setState(() {});
                                      }
                                    });
                                    setState(() {});
                                    Navigator.of(context).pop();
                                  },
                                );
                              });
                        },
                        label: Text(radius == 0
                            ? '거리 설정 없음'
                            : '내 주변 ${radius.toInt()} KM'));
                  } else {
                    return FloatingActionButton.extended(
                        heroTag: null,
                        icon: Icon(Icons.arrow_drop_down),
                        backgroundColor: ColorTheme.black,
                        onPressed: () {
                          showModalBottomSheet(
                              backgroundColor: Colors.transparent,
                              context: context,
                              builder: (context) {
                                return PlaceRangeSheet(
                                  onPressed: (value) {
                                    Navigator.of(context).pop();
                                  },
                                );
                              });
                        },
                        label: Text('거리 설정 없음'));
                  }
                }),
          )
          */
        ],
      ),
    );
  }
}
