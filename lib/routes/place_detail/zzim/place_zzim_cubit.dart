import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/routes/place_detail/zzim/place_zzim_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class PlaceZzimCubit extends BaseCubit<PlaceZzimState> {
  /// {@macro counter_cubit}
  PlaceZzimCubit() : super(InitialState());

  void init() {
    emit(InitialState());
  }

  void getCollectionSaved(String imageContentNo) {
    String method = 'get';
    String path =
        '/api/v1/collection/check/saved/item?image_content_no=$imageContentNo';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<CollectionModel> collections = (response.data as List)
          .map((record) => CollectionModel.fromJson(record))
          .toList();

      emit(GetCollections(true, collections));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void postCollectionSet(String imgContentNo, String collectionNo) {
    String method = 'post';
    String path = '/api/v1/collection/set';

    Map data = {
      "image_content_no": imgContentNo.toString(),
      "collection_no": collectionNo
    };
    if (collectionNo == null) {
      data = {"image_content_no": imgContentNo.toString()};
    }
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.facebook.addEvent(FacebookEventType.wishImageContent);

      this.property.meCollectionSubject.add(true);

      emit(PostCollectionSet(true));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
