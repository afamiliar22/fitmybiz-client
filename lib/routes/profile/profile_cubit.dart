import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/area_code_model.dart';
import 'package:fitmybiz/models/area_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/profile/profile_state.dart';
import 'package:fitmybiz/tools/network_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class ProfileCubit extends BaseCubit<ProfileState> {
  /// {@macro counter_cubit}
  ProfileCubit() : super(InitialState());

  String nickname = '';
  Uint8List imageBytes;
  UserModel user;
  List<Map<String, dynamic>> areas;

  void init() {
    emit(InitialState());

    this.selectUser().then((_) {
      this.selectUserAreas();
    });
    this.selectAreas();
  }

  void nicknameChanged(String nickname) {
    this.nickname = nickname;
  }

  void imageBytesChanged(Uint8List imageBytes) {
    this.imageBytes = imageBytes;
  }

  void areasChanged(List<Map<String, dynamic>> areas) {
    this.areas = areas;
  }

  void selectAreas() {
    String method = 'get';
    String path = '/api/v1/area_code';

    this.network.requestWithAuth(method, path, null).then((response) {
      AreaCodeModel areaCode = AreaCodeModel.fromJson(response.data);
      emit(LoadedAreaState(areaCode));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future selectUser() {
    String method = 'get';
    String path = '/api/v1/user/me';

    emit(LoadingState());
    return this.network.requestWithAuth(method, path, null).then((response) {
      this.user = UserModel.fromJson(response.data);
      this.nickname = this.user.nickname ?? '';
      emit(LoadedUserState(this.user));
      return Future.value();
    }).catchError((e) {
      emit(ErrorState());
      return Future.value();
    });
  }

  void selectCurrentLocation(int index) {
    emit(LoadingState());
    Geolocator.getLastKnownPosition().then((value) {
      String method = 'get';
      String path = '/api/v1/area_code/now/area';
      var data = {
        'lat': value.latitude.toString(),
        'lng': value.longitude.toString(),
      };
      this.network.requestWithAuth(method, path, data).then((response) {
        AreaModel area = AreaModel.fromJson(response.data);
        this.areas[index] = {
          'isUpdate': true,
          'areaName1': area.region1depthName,
          'areaName2': area.region2depthName,
          'code': area.code
        };
        emit(LoadedUserAreaState(this.areas));
      }).catchError((e) {
        emit(ErrorState());
      });
    });
  }

  void selectUserAreas() {
    String method = 'get';
    String path = '/api/v1/user/${this.user.userNo}/area';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<AreaModel> userAreas = (response.data['data'] as List)
          .map((record) => AreaModel.fromJson(record))
          .toList();
      this.areas = userAreas
          .map((element) => {
                'isUpdate': false,
                'areaName1': element.region1depthName,
                'areaName2': element.region2depthName,
                'userActiveAreaNo': element.userActiveAreaNo,
                'code': element.code
              })
          .toList();
      emit(LoadedUserAreaState(areas));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void updateUser() {
    if (this.nickname.isEmpty) {
      emit(ToastState('닉네임을 입력해주세요.'));
      return;
    }
    // this.updateUserProfile();
    emit(LoadingState());
    this.updateUserProfile().then((response) {
      this.property.meProfileSubject.add(true);
      return this.saveUserAreas();
    }).then((value) {
      emit(CompleteState());
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future<NetworkResponse> updateUserProfile() {
    String method = 'post';
    String path = '/api/v1/user/me';
    Map data = {"_method": "PUT", "nickname": this.nickname};

    Map<String, Uint8List> files = {};

    return Future.value().then((value) {
      if (this.imageBytes != null) {
        return resizeFuture(this.imageBytes).then((value) {
          files = {"image": value};
          return Future.value();
        });
      } else {
        return Future.value();
      }
    }).then((value) {
      return this.network.requestMultipart(method, path, data, files);
    });
  }

  Future saveUserAreas() {
    if (this.areas == null) {
      return Future.value();
    }
    List<Map<String, dynamic>> updateList = this
        .areas
        .where((element) =>
            element['isUpdate'] == true &&
            element['userActiveAreaNo'] != null &&
            element['code'] != null)
        .toList();
    List<Map<String, dynamic>> insertList = this
        .areas
        .where((element) =>
            element['isUpdate'] == true &&
            element['userActiveAreaNo'] == null &&
            element['code'] != null)
        .toList();
    List<Future> futureUpdateList = updateList
        .map((e) =>
            this.updateUserArea(e['userActiveAreaNo'], e['code'].toString()))
        .toList();
    List<Future> futureInsertList = insertList
        .map((e) => this.insertUserArea(e['code'].toString()))
        .toList();

    return Future.wait(
        [Future.wait(futureUpdateList), Future.wait(futureInsertList)]);
  }

  void deleteUserArea(int userActiveAreaNo) {
    String method = 'delete';
    String path = '/api/v1/user/me/area/$userActiveAreaNo';
    this.network.requestWithAuth(method, path, null).then((value) {
      this.selectUserAreas();
    });
  }

  Future insertUserArea(String code) {
    String method = 'post';
    String path = '/api/v1/user/me/area/$code';
    return this.network.requestWithAuth(method, path, null);
  }

  Future updateUserArea(int userActiveAreaNo, String code) {
    String method = 'put';
    String path = '/api/v1/user/me/area/$userActiveAreaNo/$code';
    return this.network.requestWithAuth(method, path, null);
  }
}
