import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/routes/_cells/collection_cell.dart';
import 'package:fitmybiz/routes/_cells/map_collection_cell.dart';
import 'package:fitmybiz/themes/themes.dart';

class DeleteCollectionView extends BaseRoute {
  final List<CollectionModel> collections;
  DeleteCollectionView({this.collections});

  @override
  DeleteCollectionViewState createState() =>
      DeleteCollectionViewState(this.collections);
}

class DeleteCollectionViewState extends BaseRouteState {
  List<CollectionModel> collections;
  DeleteCollectionViewState(this.collections);
  Set<String> selectedCollections = {};
  @override
  Widget build(BuildContext context) {
    Widget selectButton = CustomContainer(
      backgroundColor: ColorTheme.black,
      borderRadius: [10, 10, 10, 10],
      borderWidth: 1.0,
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
      child: Text(
        '전체 선택',
        style:
            TextStyleTheme.common.copyWith(fontSize: 13, color: Colors.white),
      ),
      onPressed: () {
        this.selectedCollections =
            this.collections.map((e) => e.collectionNo.toString()).toSet();
        setState(() {});
      },
    );

    Widget deselectButton = CustomContainer(
      backgroundColor: Colors.white,
      borderRadius: [10, 10, 10, 10],
      borderColor: ColorTheme.black,
      borderWidth: 1.0,
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
      child: Text(
        '선택 해제',
        style: TextStyleTheme.common
            .copyWith(fontSize: 13, color: ColorTheme.black),
      ),
      onPressed: () {
        this.selectedCollections = Set<String>();
        setState(() {});
      },
    );

    return PlatformScaffold(
      body: SafeArea(
        child: Column(
          children: [
            CustomContainer(
              height: 48,
              child: Row(
                children: [
                  SizedBox(width: 25),
                  GestureDetector(
                    //padding: EdgeInsets.all(8),
                    child: Image.asset('assets/images/icnsClose.png',
                        color: Colors.black, width: 28, height: 28),
                    onTap: () {
                      Navigator.of(context).popUntil((route) => route.isFirst);
                    },
                  ),
                  Spacer(),
                  SizedBox(
                    width: 29,
                  ),
                  Text('지도 정리', style: TextStyleTheme.subtitle),
                  Spacer(),
                  GestureDetector(
                    //padding: EdgeInsets.all(8),
                    child: Text(
                      this.selectedCollections.length != this.collections.length
                          ? '전체 선택'
                          : '선택 해제',
                      style: TextStyle(
                          fontSize: 16,
                          color: this.selectedCollections.length !=
                                  this.collections.length
                              ? ColorTheme.primary2
                              : ColorTheme.grayPlaceholder),
                    ),
                    onTap: () {
                      if (selectedCollections.length !=
                          this.collections.length) {
                        this.selectedCollections = this
                            .collections
                            .map((e) => e.collectionNo.toString())
                            .toSet();
                      } else {
                        this.selectedCollections = Set<String>();
                      }
                      setState(() {});
                    },
                  ),
                  /*
                  if (this.selectedCollections.length !=
                      this.collections.length)
                    selectButton,
                  if (this.selectedCollections.length ==
                      this.collections.length)
                    deselectButton,
                    */
                  SizedBox(width: 25),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  buildCollections(this.collections),
                  Positioned(
                    bottom: 24,
                    left: 0,
                    right: 0,
                    child: Row(
                      children: [
                        Spacer(),
                        CupertinoButton(
                          padding: EdgeInsets.all(16),
                          borderRadius: BorderRadius.circular(30),
                          color: ColorTheme.primary,
                          child: Image.asset('assets/images/icnsDelete2.png',
                              width: 28, height: 28, color: Colors.white),
                          onPressed: () {
                            if (selectedCollections.length <= 0) {
                              this.showToast('삭제할 지도를 선택해주세요.');
                              return;
                            }
                            this
                                .showConfirm(
                                    title: '정말 삭제하시겠습니까?',
                                    description: '삭제하신 지도는 복구되지 않아요',
                                    positiveButtonText: '삭제',
                                    negativeButtonText: '취소')
                                .then((value) {
                              if (value == true) {
                                this
                                    .navigator
                                    .popRoute(selectedCollections.toList());
                              }
                            });
                          },
                        ),
                        Spacer(),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCollections(List<CollectionModel> collections) {
    return ListView.builder(
      padding: EdgeInsets.only(left: 25, right: 25, top: 16, bottom: 72),
      /*
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.70,
          crossAxisSpacing: 10,
          mainAxisSpacing: 20),
          */
      itemBuilder: ((BuildContext c, int i) {
        CollectionModel collection = this.collections[i];

        return Container(
          margin: EdgeInsets.symmetric(vertical: 5.5),
          child: MapCollectionCell(
            isForDelete: true,
            collection: collection,
            isEditMode: true,
            isSelect: this
                .selectedCollections
                .contains(collection.collectionNo.toString()),
            onPressed: () {
              if (this
                  .selectedCollections
                  .contains(collection.collectionNo.toString())) {
                this
                    .selectedCollections
                    .remove(collection.collectionNo.toString());
              } else {
                this
                    .selectedCollections
                    .add(collection.collectionNo.toString());
              }
              setState(() {});
            },
          ),
        );
      }),
      itemCount: this.collections.length,
    );
  }
}
