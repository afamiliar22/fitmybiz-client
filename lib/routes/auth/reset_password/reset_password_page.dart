import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_text_field2.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'reset_password_cubit.dart';
import 'reset_password_state.dart';

class ResetPasswordPage extends StatelessWidget {
  final String email;
  final String passwordToken;
  ResetPasswordPage(this.email, this.passwordToken);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => ResetPasswordCubit(),
      child: _View(this.email, this.passwordToken),
    );
  }
}

class _View extends BaseRoute {
  final String email;
  final String passwordToken;
  _View(this.email, this.passwordToken);

  @override
  _ViewState createState() => _ViewState(this.email, this.passwordToken);
}

class _ViewState extends BaseRouteState {
  ResetPasswordCubit cubit;
  String email;
  String passwordToken;
  _ViewState(this.email, this.passwordToken);
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<ResetPasswordCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.emailChanged(this.email);
    this.cubit.passwordTokenChanged(this.passwordToken);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case CompletedState:
            this.indicator.hide();
            this.navigator.popToRootRoute();
            break;
          case SetState:
            setState(() {});
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
        }
      },
      child: PlatformScaffold(
        title: Text('비밀번호 재설정'),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              Text('비밀번호', style: TextStyleTheme.textFieldLabel),
              SizedBox(height: 8),
              CustomTextField2(
                placeholder: "비밀번호를 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                secure: true,
                onChanged: (password) => this.cubit.passwordChanged(password),
              ),
              SizedBox(height: 16),
              CustomTextField2(
                placeholder: "한번 더 입력해주세요",
                placeholderStyle: TextStyleTheme.placeholder,
                secure: true,
                onChanged: (password2) =>
                    this.cubit.password2Changed(password2),
              ),
              SizedBox(height: 90),
              PrimaryButton(
                text: Text('인증 완료'),
                onPressed: () => this.cubit.resetPassword(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
