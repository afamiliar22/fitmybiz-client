import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/routes/_cells/collection_cell.dart';
import 'package:fitmybiz/routes/_cells/collection_item_cell.dart';
import 'package:fitmybiz/routes/_cells/map_collection_cell.dart';
import 'package:fitmybiz/routes/my_place/my_place_page.dart';

import 'my_map_cubit.dart';
import 'my_map_state.dart';

class MyMapPage extends StatelessWidget {
  final String userNo;
  MyMapPage(this.userNo);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MyMapCubit(),
      child: _View(this.userNo),
    );
  }
}

class _View extends BaseRoute {
  final String userNo;
  _View(this.userNo);
  @override
  _ViewState createState() => _ViewState(this.userNo);
}

class _ViewState extends BaseRouteState {
  String userNo;
  _ViewState(this.userNo);

  MyMapCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<MyMapCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init(this.userNo);
  }

  List<String> areas = [];
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        body: SafeArea(
          child: Column(
            children: [
              Container(
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 0),
                child: Row(
                  children: [
                    GestureDetector(
                        child: Image.asset('assets/images/icnsClose.png',
                            color: Colors.black, width: 28, height: 28),
                        onTap: () {
                          this.navigator.popRoute(null);
                        }),
                    Spacer(),
                    Container(
                      child: Text(
                        "지도 리스트",
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Spacer(),
                    SizedBox(
                      width: 28,
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: this.buildCollections(this.cubit.collections),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildCollections(List<CollectionModel> collections) {
    return true
        ? ListView.separated(
            separatorBuilder: (context, index) {
              return SizedBox(
                height: 12,
              );
            },
            padding: EdgeInsets.symmetric(vertical: 10),
            shrinkWrap: true,
            primary: false,
            itemBuilder: ((BuildContext c, int i) {
              CollectionModel collection = this.cubit.collections[i];
              return MapCollectionCell(
                isForDelete: false,
                collection: collection,
                onPressed: () {
                  this.navigator.pushRoute(MyPlacePage(
                      collection.collectionNo.toString(),
                      collection.name,
                      false,
                      true));
                },
                onPressedMore: () {
                  if (collection.userNo.toString() ==
                      this.property.user.userNo) {
                    this.showCollectionMoreSheetMe(
                      onDelete: () {
                        this.cubit.deleteCollection(
                            collection.collectionNo.toString());
                      },
                    );
                  } else {
                    this.showCollectionMoreSheetOther(
                      onReport: (description) {
                        this.cubit.insertCollectionReport(
                            collection.collectionNo.toString(), description);
                      },
                    );
                  }
                },
              );
            }),
            itemCount: cubit.collections.length,
          )
        : ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 10),
            shrinkWrap: true,
            primary: false,
            /*
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 0.75,
                crossAxisSpacing: 10,
                mainAxisSpacing: 20),
                */
            itemBuilder: ((BuildContext c, int i) {
              CollectionModel collection = this.cubit.collections[i];
              return CollectionCell(
                isMine: true,
                collection: collection,
                onPressed: () {
                  this.navigator.pushRoute(MyPlacePage(
                      collection.collectionNo.toString(),
                      collection.name,
                      false,
                      true));
                },
                onPressedMore: () {
                  if (collection.userNo.toString() ==
                      this.property.user.userNo) {
                    this.showCollectionMoreSheetMe(
                      onDelete: () {
                        this.cubit.deleteCollection(
                            collection.collectionNo.toString());
                      },
                    );
                  } else {
                    this.showCollectionMoreSheetOther(
                      onReport: (description) {
                        this.cubit.insertCollectionReport(
                            collection.collectionNo.toString(), description);
                      },
                    );
                  }
                },
              );
            }),
            itemCount: cubit.collections.length,
          );
  }
}
