import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

class BaseNavigator {
  BuildContext context;

  BaseNavigator(BuildContext context) {
    this.context = context;
  }

  Future pushRoute(Widget route) {
    return Navigator.of(context, rootNavigator: true).push(
      CupertinoPageRoute(builder: (context) => route),
    );
  }

  Future presentRoute(Widget route) {
    return Navigator.of(context, rootNavigator: true).push(
      CupertinoPageRoute(fullscreenDialog: true, builder: (context) => route),
    );
  }

  Future replaceRoute(Widget route) {
    return Navigator.of(context).pushReplacement(
      PageRouteBuilder(
        opaque: false,
        pageBuilder: (context, animation, secondaryAnimation) {
          return route;
        },
        transitionDuration: Duration(milliseconds: 100),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return FadeTransition(
            opacity: animation,
            child: child,
          );
        },
      ),
    );
  }

  Future fadePresentRoute(Widget route) {
    return Navigator.of(context, rootNavigator: true).push(
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) {
          return route;
        },
        transitionDuration: Duration(milliseconds: 100),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return FadeTransition(
            opacity: animation,
            child: child,
          );
        },
      ),
    );
  }

  Future modalRoute(Widget route) {
    return Navigator.of(context).push(
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation1, animation2) => route),
    );
  }

  Future resetRoute(Widget route) {
    if (Platform.isIOS) {
      return Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
          PageRouteBuilder(
              fullscreenDialog: true,
              pageBuilder: (context, animation1, animation2) =>
                  CupertinoTabView(builder: (context) => route)),
          (route) => false);
    } else {
      return Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
          PageRouteBuilder(
              fullscreenDialog: true,
              pageBuilder: (context, animation1, animation2) => route),
          (route) => false);
    }
  }

  void popRoute(var result) {
    Navigator.of(context).pop(result);
  }

  void popToRootRoute() {
    Navigator.popUntil(context, (route) => route.isFirst);
  }
}
