import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/me/me_state.dart';
import 'package:fitmybiz/routes/views/insert_collection_sheet.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class MeCubit extends BaseCubit<MeState> {
  /// {@macro counter_cubit}
  MeCubit() : super(InitialState());

  List<CollectionModel> collections = [];
  UserModel user;

  String point;

  void init() {
    this.user = this.property.user;

    this.property.meProfileSubject.listen((value) {
      this.refreshUser();
    });

    this.property.meCollectionSubject.listen((value) {
      this.refreshCollection();
    });

    emit(LoadingState());
    this.selectUser().then((value) {
      return this.getCollectionSaved();
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void refreshUser() {
    this.selectUser().then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void refreshCollection() {
    this.getCollectionSaved().then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future selectUser() {
    String method = 'get';
    String path = '/api/v1/user/me';

    return this.network.requestWithAuth(method, path, null).then((response) {
      this.user = UserModel.fromJson(response.data);
      return Future.value();
    });
  }

  Future getPoint() {
    String method = 'get';
    String path = '/api/v1/user/me/point';

    return this.network.requestWithAuth(method, path, null).then((response) {
      print(response);
      this.point = response.data['point'].toString();
      return Future.value();
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future getCollectionSaved() {
    String method = 'get';
    String path =
        '/api/v1/collection/${this.property.user.userNo}?page=1&take=20';

    return this.network.requestWithAuth(method, path, null).then((response) {
      this.collections = (response.data['data'] as List)
          .map((record) => CollectionModel.fromJson(record))
          .toList();
      return Future.value();
    });
  }

  void postCollection(String name) {
    String method = 'post';
    String path = '/api/v1/collection';
    Map data = {"name": name, "secret": false};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.facebook.addEvent(FacebookEventType.addMap);
      this.property.meCollectionSubject.add(true);
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void deleteCollection(String collectionNo) {
    String method = 'delete';
    String path = '/api/v1/collection/delete';
    Map<String, dynamic> data = {"collection_no": collectionNo};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.property.meCollectionSubject.add(true);
      emit(ToastState('지도가 삭제되었습니다.'));
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
