import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

class PointAccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  //

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('계좌번호 변경'),
        body: CustomContainer(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '예금주',
                style: TextStyle(color: ColorTheme.grayLv1),
              ),
              SizedBox(
                height: 7,
              ),
              TextFormField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.verified_user),
                    hintText: '이름을 입력해주세요',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: ColorTheme.grayLv1),
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                '은행명',
                style: TextStyle(color: ColorTheme.grayLv1),
              ),
              SizedBox(
                height: 7,
              ),
              TextFormField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.verified_user),
                    hintText: '은행명을 입력해주세요',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: ColorTheme.grayLv1),
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                '계좌번호',
                style: TextStyle(color: ColorTheme.grayLv1),
              ),
              SizedBox(
                height: 7,
              ),
              TextFormField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.verified_user),
                    hintText: '계좌번호 입력해주세요',
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: ColorTheme.grayLv1),
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  Spacer(),
                  CustomContainer(
                    margin: EdgeInsets.all(10),
                    width: 285,
                    height: 40,
                    child: CustomButton(
                      onPressed: () {
                        this.navigator.popRoute(null);
                      },
                      backgroundColor: ColorTheme.primary2,
                      borderRadius: 6,
                      text: Text(
                        '저장하기',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
