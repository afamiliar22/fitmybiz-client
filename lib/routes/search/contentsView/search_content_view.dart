import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_smart_refresher.dart';
import 'package:fitmybiz/models/banner_model.dart';

import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/routes/_cells/contents_cell.dart';
import 'package:fitmybiz/routes/my_place/my_place_page.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';
import 'package:fitmybiz/routes/user/user_page.dart';
import 'package:fitmybiz/routes/views/place_range_sheet.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/network_client.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';

enum SearchContentType { all, follow, category }

class SearchContentView extends BaseRoute {
  final int categoryNo;
  //
  final SearchContentType type;
  final double radius;

  SearchContentView(this.categoryNo, this.type, this.radius);
  @override
  SearchContentViewState createState() =>
      SearchContentViewState(this.categoryNo, this.type, this.radius);
}

class SearchContentViewState extends BaseRouteState
    with AutomaticKeepAliveClientMixin {
  int categoryNo;
  SearchContentType type;
  //
  SearchContentViewState(this.categoryNo, this.type, this.radius);
  bool isGps = false;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  List<ContentsModel> contents;
  List<BannerModel> banners = List<BannerModel>();

  Position position;
  int page = 1;
  SharedPreferences prefs;
  //주변 반경
  final double radius;
  @override
  void initState() {
    super.initState();
  }

  Future<List<ContentsModel>> selectImageContents() async {
    prefs = await SharedPreferences.getInstance();
    String method = 'get';
    String path = '/api/v1/content/image_content/list';
    Map<String, dynamic> data = {'take': '20', 'page': this.page.toString()};

    switch (this.type) {
      case SearchContentType.all:
        data["recommend"] = '1';
        break;
      case SearchContentType.follow:
        data["follow_search"] = '1';
        data["more_field"] = 'users';
        break;
      case SearchContentType.category:
        data["category_no"] = categoryNo.toString();
        break;
    }

    if (this.isGps) {
      data["lat"] = this.position.latitude.toString();
      data["lng"] = this.position.longitude.toString();
    }
    //radius = prefs.getDouble('radius') ?? 0.0;
    if (radius != null) {
      data['radius'] = radius.toString();
    }
    return NetworkClient.shared
        .requestWithAuth(method, path, data)
        .then((response) {
      List<ContentsModel> contentsList = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();
      return Future.value(contentsList);
    });
  }

  Future<List<BannerModel>> selectBanners() {
    String method = 'get';
    String path = '/api/v1/banner/list?now_processed=1';

    return NetworkClient.shared
        .requestWithAuth(method, path, null)
        .then((response) {
      List<BannerModel> bannerList = (response.data as List)
          .map((record) => BannerModel.fromJson(record))
          .toList();
      return Future.value(bannerList);
    });
  }

  onRefresh() {
    if (this.type == SearchContentType.all) {
      this.selectBanners().then((banners) {
        this.banners = banners;
        setState(() {});
      });
    }

    this.page = 1;
    this.selectImageContents().then((contents) {
      this.contents = contents;
      setState(() {});
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    }).catchError((e) {
      this.contents = [];
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    });
  }

  onLoading() {
    this.page = this.page + 1;
    this.selectImageContents().then((contents) {
      this.contents.addAll(contents);
      setState(() {});
      if (contents.length == 0) {
        this.refreshController.loadNoData();
      } else {
        this.refreshController.loadComplete();
      }
    }).catchError((e) {
      this.refreshController.loadComplete();
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.onRefresh();
  }

  Widget emptyView() {
    String title = "";
    String desc = "";

    title = "내 주변에 등록된 장소가 없어요.";
    desc = "지금 내 주변의 장소를 등록해 주세요.";

    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 100),
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              desc,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [
        RefreshConfiguration(
          enableLoadingWhenNoData: false,
          footerTriggerDistance: 200,
          child: SmartRefresher(
            controller: this.refreshController,
            enablePullDown: true,
            enablePullUp: true,
            header: CustomSmartRefresher.customHeader(),
            footer: CustomSmartRefresher.customFooter(),
            onRefresh: this.onRefresh,
            onLoading: this.onLoading,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 25),
              children: [
                if (this.type == SearchContentType.all)
                  CustomContainer(
                    height: 120,
                    child: PageView.builder(
                      itemBuilder: (context, index) {
                        return CustomContainer(
                          onPressed: () {
                            this.navigator.pushRoute(MyPlacePage(
                                this.banners[index].collectionNo,
                                this.banners[index].title,
                                false,
                                false));
                          },
                          child: Stack(
                            children: [
                              Positioned.fill(
                                child: Container(
                                  child: CustomCachedNetworkImage(
                                    preloadImage: this
                                        .banners[index]
                                        .contentBannerImage
                                        .sm,
                                    image: this
                                        .banners[index]
                                        .contentBannerImage
                                        .sm,
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  /*
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 25,
                                      ),
                                      child: Text(this.banners[index].subTitle,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15)),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 25,
                                      ),
                                      child: Text(this.banners[index].title,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                    */
                                  Spacer(),
                                  Row(
                                    children: [
                                      Spacer(),
                                      Container(
                                          margin: EdgeInsets.only(
                                              top: 8, bottom: 17, right: 21),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              color: Colors.black54),
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                vertical: 2, horizontal: 4),
                                            child: Container(
                                              margin: EdgeInsets.symmetric(
                                                horizontal: 8,
                                              ),
                                              child: Text(
                                                  '${index + 1} / ' +
                                                      this
                                                          .banners
                                                          .length
                                                          .toString(),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 13)),
                                            ),
                                          )),
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                      itemCount: this.banners.length,
                    ),
                  ),
                SizedBox(
                  height: 25,
                ),
                this.contents == null
                    ? Shimmer.fromColors(
                        child: Container(
                          child: StaggeredGridView.countBuilder(
                            shrinkWrap: true,
                            primary: false,
                            crossAxisCount: 3,
                            itemCount: 20,
                            itemBuilder: (BuildContext context, int index) {
                              return ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: Container(
                                    color: Colors.red,
                                  ));
                            },
                            staggeredTileBuilder: (index) =>
                                StaggeredTile.count((index % 7 == 0) ? 2 : 1,
                                    (index % 7 == 0) ? 2 : 1),
                            mainAxisSpacing: 11.0,
                            crossAxisSpacing: 11.0,
                          ),
                        ),
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey[100])
                    : this.contents.length == 0
                        ? emptyView()
                        : Container(
                            child: StaggeredGridView.countBuilder(
                              shrinkWrap: true,
                              primary: false,
                              crossAxisCount: 3,
                              itemCount: this.contents.length,
                              itemBuilder: (BuildContext context, int index) {
                                ContentsModel contents = this.contents[index];
                                return ContentsCell(
                                  contents: contents,
                                  onPressed: () {
                                    this.navigator.pushRoute(PlaceDetailPage(
                                          contents.imageContentNo,
                                        ));
                                  },
                                  onPressedUser: () {
                                    if (contents.userInfo == null) return;

                                    this.navigator.pushRoute(
                                        UserPage(contents.userInfo.userNo));
                                  },
                                  onPressedMore: () {
                                    this.showPlaceMoreSheetOther(
                                        onReport: (value) {
                                      String method = 'post';
                                      String path =
                                          '/api/v1/report/image_content/${contents.imageContentNo}';
                                      Map<String, dynamic> data = {
                                        'report_title': '신고하기',
                                        'report_description': value
                                      };

                                      NetworkClient.shared
                                          .requestWithAuth(method, path, data)
                                          .then((response) {
                                        this.showToast('신고되었습니다.');
                                      }).catchError((e) {});
                                    });
                                  },
                                );
                              },
                              staggeredTileBuilder: (index) =>
                                  StaggeredTile.count((index % 7 == 0) ? 2 : 1,
                                      (index % 7 == 0) ? 2 : 1),
                              mainAxisSpacing: 11.0,
                              crossAxisSpacing: 11.0,
                            ),
                          ),
              ],
            ),
          ),
        ),
        /*
        Column(
          children: [
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20.0),
                  child: FutureBuilder(
                      future: SharedPreferences.getInstance(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          SharedPreferences prefs = snapshot.data;
                          radius = prefs.getDouble('radius') ?? 0.0;
                          return FloatingActionButton.extended(
                              heroTag: null,
                              icon: Icon(Icons.arrow_drop_down),
                              backgroundColor: ColorTheme.black,
                              onPressed: () {
                                showModalBottomSheet(
                                    backgroundColor: Colors.transparent,
                                    context: context,
                                    builder: (context) {
                                      return PlaceRangeSheet(
                                        onPressed: (value) {
                                          radius = value;

                                          this.isGps =
                                              radius == 0 ? false : true;
                                          if (!this.isGps) {
                                            radius = null;
                                          }
                                          Geolocator.getLastKnownPosition()
                                              .then((value) {
                                            this.position = value;
                                            setState(() {});
                                            this.onRefresh();
                                          });
                                          Geolocator.checkPermission()
                                              .then((value) {
                                            if (value ==
                                                LocationPermission.denied) {
                                              this.isGps = false;
                                              setState(() {});
                                            }
                                          });
                                          setState(() {});
                                          Navigator.of(context).pop();
                                        },
                                      );
                                    });
                              },
                              label: Text(radius == 0
                                  ? '거리 설정 없음'
                                  : '내 주변 ${radius.toInt()} KM'));
                        } else {
                          return FloatingActionButton.extended(
                              heroTag: null,
                              icon: Icon(Icons.arrow_drop_down),
                              backgroundColor: ColorTheme.black,
                              onPressed: () {
                                showModalBottomSheet(
                                    backgroundColor: Colors.transparent,
                                    context: context,
                                    builder: (context) {
                                      return PlaceRangeSheet(
                                        onPressed: (value) {
                                          Navigator.of(context).pop();
                                        },
                                      );
                                    });
                              },
                              label: Text('거리 설정 없음'));
                        }
                      }),
                )
              ],
            )
          ],
        )
        */
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
