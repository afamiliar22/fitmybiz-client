import 'package:flutter/material.dart';

class CustomAvatar extends StatelessWidget {
  final Widget image;
  final double size;
  final double radius;
  final VoidCallback onPressed;
  final BoxFit fit;
  final Color borderColor;

  CustomAvatar({this.image, this.size, this.onPressed, this.radius, this.fit, this.borderColor});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: size,
        height: size,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(this.radius ?? (size / 2)),
          child: image,
        ),
      ),
    );
  }
}
