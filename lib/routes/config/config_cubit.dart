import 'package:bloc/bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';

import 'config_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class ConfigCubit extends BaseCubit<ConfigState> {
  /// {@macro counter_cubit}
  ConfigCubit() : super(InitialState());

  void deleteUser() {
    String method = 'delete';
    String path = '/api/v1/user/me';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      emit(ToastState('탈퇴 처리되었습니다.'));
      emit(CompleteDeleteUserState());
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void getCategories(int categoryNo, String categoryName, int limit, int page) {
    String method = 'get';
    String path = '/api/v1/content/category/list';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<CategoryModel> category = (response.data['data'] as List)
          .map((record) => CategoryModel.fromJson(record))
          .toList();
      emit(GetCategory(true, category));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
