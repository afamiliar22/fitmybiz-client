import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/models/google_place_model.dart';

import 'search_place_name_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class SearchPlaceNameCubit extends BaseCubit<SearchPlaceNameState> {
  /// {@macro counter_cubit}
  SearchPlaceNameCubit() : super(InitialState());

  final searchDebouncer = Debouncer(milliseconds: 500);

  String query = '';
  void init() {
    this.property.isSignIn.then((value) {});
  }

  void searchChanged(String query) {
    this.query = query;
    this.searchDebouncer.run(() => searchPlace());
  }

  void searchPlace() {
    Geolocator.getLastKnownPosition().then((position) {
      String method = 'get';
      String path = '/api/v1/google_map/place/search';
      Map<String, dynamic> data = {'query': this.query};
      if (position != null) {
        data['location'] = '${position.latitude},${position.longitude}';
      }
      emit(LoadingState());
      return this.network.requestWithAuth(method, path, data).then((response) {
        List<GooglePlaceModel> places = (response.data['results'] as List)
            .map((record) => GooglePlaceModel.fromJson(record))
            .toList();
        emit(GetPlace(places));
      });
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
