import 'dart:developer';

import 'image_source_model.dart';

class BannerModel {
  final String contentBannerNo;
  final String contentBannerId;
  final String contentBannerOrder;
  final String collectionNo;

  final ImageSourceModel contentBannerImage;
  final String subTitle;
  final String title;
  final String buttonTitle;
  final String state;

  BannerModel({
    this.contentBannerNo,
    this.contentBannerId,
    this.contentBannerOrder,
    this.collectionNo,
    this.contentBannerImage,
    this.subTitle,
    this.title,
    this.buttonTitle,
    this.state,
  });

  factory BannerModel.fromJson(Map<String, dynamic> json) {
    return BannerModel(
      contentBannerNo: json['content_banner_no'].toString(),
      contentBannerId: json['content_banner_id'].toString(),
      contentBannerOrder: json['content_banner_order'].toString(),
      collectionNo: json['collection_no'].toString(),
      contentBannerImage: ImageSourceModel.fromJson(json['content_banner_image']),
      title: json['title'].toString(),
      subTitle: json['sub_title'].toString(),
      buttonTitle: json['button_title'].toString(),
      state: json['state'].toString(),
    );
  }
}
