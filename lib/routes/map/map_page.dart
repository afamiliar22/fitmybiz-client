import 'dart:async';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kakao_flutter_sdk/link.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';

import 'package:fitmybiz/components/custom_container.dart';

import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/extensions/color_extension.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/map_model.dart';
import 'package:fitmybiz/models/user_model.dart';

import 'package:fitmybiz/routes/map/map_cubit.dart';
import 'package:fitmybiz/routes/myMap/my_map_page.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';
import 'package:fitmybiz/routes/views/search_route_sheet.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/property_client.dart';
import 'package:share/share.dart';
import 'package:transparent_image/transparent_image.dart';

import 'map_state.dart';
import 'dart:ui' as ui;

class MapPage extends StatelessWidget {
  final bool isUser;
  final String userNo;
  final String latitude;
  final String longitude;
  MapPage({this.isUser, this.userNo, this.latitude, this.longitude});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MapCubit(),
      child: _View(this.isUser, this.userNo, this.latitude, this.longitude),
    );
  }
}

class _View extends BaseRoute {
  final bool isUser;
  final String userNo;
  final String latitude;
  final String longitude;
  _View(this.isUser, this.userNo, this.latitude, this.longitude);
  @override
  _ViewState createState() =>
      _ViewState(this.isUser, this.userNo, this.latitude, this.longitude);
}

class _ViewState extends BaseRouteState {
  bool isUser;
  String userNo;
  String latitude;
  String longitude;
  _ViewState(this.isUser, this.userNo, this.latitude, this.longitude);
  MapCubit cubit;
  UserModel user;
  List<CollectionModel> collections = [];
  List<MapModel> maps = [];
  List<CategoryModel> categories = [];
  List<ContentsModel> contents = [];

  bool isFilter = false; //필터를 열었는지 체크

  int selectedIndex = 1; //1이 최소값 콜렉션 클릭
  int filterSelectedIndex = 0; // 필터 카테고리 클릭

  MarkerId selectedMarkerId;

  GoogleMapController controller;

  CameraPosition cameraPosition =
      CameraPosition(target: LatLng(37.532600, 127.024612), zoom: 12);

  CameraPosition userPosition;
  CameraPosition currentPosition =
      CameraPosition(target: LatLng(37.532600, 127.024612), zoom: 12);

  List<Marker> _markers = [];
  List<String> intervals = [];

  BitmapDescriptor sourceIcon;
  BitmapDescriptor sourceIconSelected;
  BitmapDescriptor zzimIcon;
  BitmapDescriptor zzimIconSelected;

  var pinPosition;
  //
  bool isFolder = true;

  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<MapCubit>();

    Geolocator.getCurrentPosition().then((value) {
      if (value != null) {
        this.userPosition = CameraPosition(
            target: LatLng(value.latitude, value.longitude), zoom: 12);
        this.currentPosition = CameraPosition(
            target: LatLng(value.latitude, value.longitude), zoom: 12);
      } else {
        this.userPosition =
            CameraPosition(target: LatLng(37.532600, 127.024612), zoom: 12);
        this.currentPosition =
            CameraPosition(target: LatLng(37.532600, 127.024612), zoom: 12);
      }

      if (this.latitude != null && this.longitude != null) {
        this.currentPosition = CameraPosition(
            target: LatLng(
                double.parse(this.latitude), double.parse(this.longitude)),
            zoom: 12);
      }

      setState(() {});
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
    if (this.isUser) {
      this.cubit.userNoChanged(this.property.user.userNo);
    } else {
      this.cubit.userNoChanged(this.userNo);
      this.cubit.getUser();
    }
    this.cubit.getUserMaps();
    this.cubit.getCollection();

    this.cubit.getCategories();

    setCustomMapPin().then((value) {
      this.reloadMarker();
      setState(() {});
    });
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  Future setCustomMapPin() {
    Future<Uint8List> markerImgFuture =
        getBytesFromAsset('assets/images/elementMarkerMyplace.png', 100);
    Future<Uint8List> markerImgSelectedFuture =
        getBytesFromAsset('assets/images/elementMarkerMyplaceFocused.png', 175);
    Future<Uint8List> zzimImgFuture =
        getBytesFromAsset('assets/images/elementMarkerZzim.png', 100);
    Future<Uint8List> zzimImgSelectedFuture =
        getBytesFromAsset('assets/images/elementMarkerZzimFocused.png', 175);

    return Future.wait([
      markerImgFuture,
      markerImgSelectedFuture,
      zzimImgFuture,
      zzimImgSelectedFuture
    ]).then((results) {
      sourceIcon = BitmapDescriptor.fromBytes(results[0]);
      sourceIconSelected = BitmapDescriptor.fromBytes(results[1]);
      zzimIcon = BitmapDescriptor.fromBytes(results[2]);
      zzimIconSelected = BitmapDescriptor.fromBytes(results[3]);
      return Future.value();
    });
  }

  Marker getMarker2(MapModel model) {
    bool isSelected = MarkerId(model.contentNo) == this.selectedMarkerId;
    return Marker(
      markerId: MarkerId(model.contentNo),
      position: LatLng(model.lat, model.lng),
      icon: model.tempMarkerIcon == 0
          ? isSelected
              ? zzimIconSelected
              : zzimIcon
          : isSelected
              ? sourceIconSelected
              : sourceIcon,
      onTap: () {
        if (this.selectedMarkerId == MarkerId(model.contentNo)) {
          this.selectedMarkerId = null;
        } else {
          this.selectedMarkerId = MarkerId(model.contentNo);
        }
        this.reloadMarker();
        this.cubit.getImageContents(model.contentNo);
      },
    );
  }

  void reloadMarker() {
    List<MapModel> filteredMaps;
    if (this.filterSelectedIndex != 0) {
      String selectCategoryNo =
          this.categories[filterSelectedIndex - 1].categoryNo.toString();

      filteredMaps = this
          .maps
          .where((element) => (element.categoryNoList ?? '')
              .split(',')
              .contains(selectCategoryNo))
          .toList();
    } else {
      filteredMaps = this.maps;
    }
    this._markers = filteredMaps.map((e) => this.getMarker2(e)).toList();
  }

  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<MapCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case GetCollection:
            this.indicator.hide();

            this.collections = state.collections;
            setState(() {});
            break;

          case GetUser:
            this.indicator.hide();
            this.user = state.result;
            setState(() {});
            break;
          case GetCollectionMap:
            this.indicator.hide();
            this.maps = state.maps;
            this.reloadMarker();
            setState(() {});
            break;
          case GetUserMap:
            this.indicator.hide();
            this.maps = state.maps;

            this.reloadMarker();
            setState(() {});
            break;
          case GetImageContents:
            this.indicator.hide();
            this.contents = state.contents;

            this.intervals.clear();
            this.contents.forEach((element) {
              String interval = "";
              double a = Geolocator.distanceBetween(
                  this.userPosition.target.latitude,
                  this.userPosition.target.longitude,
                  element.lat,
                  element.lng);

              interval = (a / 1000).toStringAsFixed(1);
              interval += "KM";
              this.intervals.add(interval);
              setState(() {});
            });
            break;
          case GetCategory:
            this.indicator.hide();
            this.categories = state.categories;
            setState(() {});
            break;
        }
      },
      child: this.isUser
          ? PlatformScaffold(
              body: Stack(
                children: [
                  getPage(),
                  getHorizontalMenu(),
                  mapButtons(),
                  this.isFilter ? filterCategoryList() : Container(),
                ],
              ),
            )
          : PlatformScaffold(
              body: Stack(
                children: [
                  getPage(),
                  Column(
                    children: [
                      getUserBar(),
                      getHorizontalMenu(),
                    ],
                  ),
                  mapButtons(),
                  this.isFilter ? filterCategoryList() : Container(),
                ],
              ),
            ),
    );
  }

  mapButtons() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                CustomContainer(
                  margin: EdgeInsets.only(right: 25, bottom: 20),
                  height: 44,
                  width: 44,
                  borderRadius: [15, 15, 15, 15],
                  shadowColor: ColorTheme.black.withOpacity(.2),
                  backgroundColor: Colors.white,
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Image.asset(
                      "assets/images/icnsShare.png",
                      height: 28,
                    ),
                  ),
                  onPressed: () {
                    String userNo;
                    String userName;
                    String image;
                    String defaultImage =
                        'https://s3.ap-northeast-2.amazonaws.com/www.zzieut.com/icon.png';
                    if (this.isUser == false) {
                      userNo = this.user.userNo;
                      userName = this.user.nickname;
                      image = this.user.profileImage != null
                          ? this.user.profileImage.s
                          : defaultImage;
                    } else {
                      userNo = this.property.user.userNo;
                      userName = this.property.user.nickname;
                      image = this.property.user.profileImage != null
                          ? this.property.user.profileImage.s
                          : defaultImage;
                    }

                    double latitude = this.cameraPosition.target.latitude;
                    double longitude = this.cameraPosition.target.longitude;

                    this.indicator.show();
                    this
                        .firebase
                        .generateDynamicLink(
                            path: '/map/$userNo/$latitude/$longitude',
                            title: userName,
                            description: '${userName}님의 찜 지도입니다.',
                            image: image)
                        .then((value) {
                      this.indicator.hide();
                      Share.share(value);
                    }).catchError((e) {
                      this.indicator.hide();
                    });
                  },
                ),
                CustomContainer(
                  margin: EdgeInsets.only(right: 25, bottom: 20),
                  height: 44,
                  width: 44,
                  borderRadius: [15, 15, 15, 15],
                  shadowColor: ColorTheme.black.withOpacity(.2),
                  backgroundColor: Colors.white,
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Image.asset(
                      "assets/images/icnsLocationHunt.png",
                      height: 28,
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      Geolocator.getLastKnownPosition().then((value) {
                        if (value != null) {
                          this.userPosition = CameraPosition(
                              target: LatLng(value.latitude, value.longitude),
                              zoom: this.cameraPosition.zoom);
                          this.currentPosition = CameraPosition(
                              target: LatLng(value.latitude, value.longitude),
                              zoom: this.cameraPosition.zoom);
                        } else {
                          this.userPosition = CameraPosition(
                              target: LatLng(37.532600, 127.024612),
                              zoom: this.cameraPosition.zoom);
                          this.currentPosition = CameraPosition(
                              target: LatLng(37.532600, 127.024612),
                              zoom: this.cameraPosition.zoom);
                        }
                        this.controller.animateCamera(
                            CameraUpdate.newCameraPosition(
                                this.currentPosition));
                      });
                    });
                  },
                ),
                CustomContainer(
                  margin: EdgeInsets.only(right: 25, bottom: 40),
                  height: 44,
                  width: 44,
                  borderRadius: [15, 15, 15, 15],
                  shadowColor: ColorTheme.black.withOpacity(.2),
                  backgroundColor: Colors.white,
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Image.asset(
                      "assets/images/icnsFilter.png",
                      height: 28,
                    ),
                  ),
                  onPressed: () {
                    this.isFilter = !this.isFilter;
                    this.selectedMarkerId = null;
                    this.reloadMarker();
                    setState(() {});
                  },
                ),
              ],
            )
          ],
        ),
        this.isFilter
            ? SizedBox(
                height: 110,
              )
            : Visibility(
                visible: this.selectedMarkerId != null,
                child: Container(
                  height: 200,
                  child: PageView.builder(
                    controller:
                        PageController(viewportFraction: 0.9, initialPage: 0),
                    itemBuilder: (context, i) {
                      return Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white),
                        margin: EdgeInsets.only(bottom: 50, left: 5, right: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: CustomContainer(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 40,
                                      child: ListView.builder(
                                        padding: EdgeInsets.only(left: 10),
                                        primary: false,
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemCount:
                                            this.contents[i].categories.length,
                                        itemBuilder: (context, j) {
                                          return CustomContainer(
                                            backgroundColor:
                                                Color.fromRGBO(255, 157, 81, 1),
                                            margin: EdgeInsets.only(
                                                left: 10, top: 20),
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10),
                                            borderRadius: [10, 10, 10, 10],
                                            child: Center(
                                              child: Text(
                                                this
                                                    .contents[i]
                                                    .categories[j]
                                                    .categoryName,
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: Colors.white),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 20,
                                          top: 8,
                                          bottom: 4,
                                          right: 8),
                                      child: Text(
                                        this.contents[i].title ?? "",
                                        style: TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    Spacer(),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 20, bottom: 4, right: 8),
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Image.asset(
                                              'assets/images/iconMapFilled.png',
                                              width: 18,
                                            ),
                                            SizedBox(width: 4),
                                            Flexible(
                                                child: Container(
                                              child: Text(
                                                this
                                                    .contents[i]
                                                    .contentInfo
                                                    .formattedAddress,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: Colors.grey[500]),
                                              ),
                                            )),
                                          ]),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 20, bottom: 18, right: 8),
                                      child: Row(children: [
                                        Image.asset(
                                          'assets/images/iconHunt.png',
                                          width: 18,
                                        ),
                                        SizedBox(width: 4),
                                        Text(this.intervals[i],
                                            style: TextStyle(
                                                fontSize: 13,
                                                color: Colors.grey[500])),
                                      ]),
                                    )
                                  ],
                                ),
                                onPressed: () {
                                  this.navigator.pushRoute(PlaceDetailPage(
                                        this.contents[i].imageContentNo,
                                      ));
                                },
                              ),
                            ),
                            Container(
                              child: Column(
                                children: [
                                  CustomContainer(
                                    height: 81,
                                    width: 81,
                                    margin: EdgeInsets.only(
                                        left: 12, top: 16, right: 20),
                                    onPressed: () {
                                      this.navigator.pushRoute(PlaceDetailPage(
                                            this.contents[i].imageContentNo,
                                          ));
                                    },
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: CustomCachedNetworkImage(
                                        preloadImage:
                                            this.contents[i].imageSrc.s,
                                        image: this.contents[i].imageSrc.s,
                                      ),
                                    ),
                                  ),
                                  CustomContainer(
                                    onPressed: () {
                                      showModalBottomSheet(
                                        useRootNavigator: true,
                                        backgroundColor: Colors.transparent,
                                        context: context,
                                        builder: (context) => SearchRouteSheet(
                                          onPressed: (type) {
                                            Geolocator.getCurrentPosition()
                                                .then((position) {
                                              this.launch.moveToMap(
                                                  type: type,
                                                  sourceLat: position.latitude,
                                                  sourceLng: position.longitude,
                                                  targetLat:
                                                      this.contents[i].lat,
                                                  targetLng:
                                                      this.contents[i].lng);
                                            });
                                          },
                                        ),
                                      );
                                    },
                                    margin: EdgeInsets.only(
                                        left: 12,
                                        top: 10,
                                        bottom: 10,
                                        right: 20),
                                    borderRadius: [14, 14, 14, 14],
                                    borderWidth: 1,
                                    borderColor:
                                        Color.fromRGBO(228, 230, 233, 1),
                                    width: 82,
                                    height: 30,
                                    backgroundColor: ColorTheme.black,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        /*
                                        Container(
                                          margin: EdgeInsets.only(left: 8),
                                          child: Image.asset(
                                            'assets/images/iconDirectionBlack.png',
                                            width: 22,
                                            height: 22,
                                          ),
                                        ),
                                        Spacer(),
                                        */
                                        Container(
                                          //margin: EdgeInsets.only(right: 10),
                                          child: Text(
                                            "길찾기",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    itemCount: contents.length,
                  ),
                ),
              ),
      ],
    );
  }

  getUserBar() {
    return Container(
      padding: EdgeInsets.only(bottom: 12),
      color: Colors.white,
      child: SafeArea(
        bottom: false,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          IconButton(
            padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
            icon: Image.asset('assets/images/icnsClose.png',
                color: Colors.black, width: 28, height: 28),
            onPressed: () {
              this.navigator.popRoute(null);
            },
          ),
          Container(
            child: Text(
              this.user != null ? this.user.nickname : '',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 20),
            width: 44,
            height: 44,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(22),
              child: this.user != null && this.user.profileImage != null
                  ? CustomCachedNetworkImage(
                      preloadImage: this.user.profileImage.s ?? '',
                      image: this.user.profileImage.m ?? '',
                    )
                  : Image.asset("assets/images/userProfileImg.png"),
            ),
          ),
        ]),
      ),
    );
  }

  getHorizontalMenu() {
    return SafeArea(
        top: this.isUser,
        child: Column(
          children: [
            CustomContainer(
              height: 40,
              margin: EdgeInsets.only(
                top: 20,
              ),
              child: Row(
                children: [
                  CustomContainer(
                    shadowColor: ColorTheme.blackShadow.withOpacity(.2),
                    margin: EdgeInsets.only(left: 8, right: 8),
                    borderRadius: [10, 10, 10, 10],
                    backgroundColor: Colors.white,
                    child: CustomContainer(
                      width: 28,
                      height: 28,
                      margin: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                      child: Image.asset(
                        'assets/images/icnsFolder.png',
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        isFolder = true;
                      });
                      if (this.isUser) {
                        this
                            .navigator
                            .presentRoute(MyMapPage(this.property.user.userNo));
                      } else {
                        this
                            .navigator
                            .presentRoute(MyMapPage(this.user.userNo));
                      }
                    },
                  ),
                  Expanded(
                    child: ListView.builder(
                        padding: EdgeInsets.only(
                          top: 2,
                          bottom: 2,
                          left: 20,
                          right: 20,
                        ),
                        scrollDirection: Axis.horizontal,
                        itemBuilder: ((BuildContext c, int i) {
                          Widget child;
                          switch (i) {
                            case 0:
                              child = CustomContainer(
                                margin: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 20),
                                child: Center(
                                  child: Text(
                                    "전체",
                                    style: TextStyle(
                                        color: this.selectedIndex == i &&
                                                !isFolder
                                            ? Colors.white
                                            : Color.fromRGBO(138, 148, 159, 1),
                                        fontSize: 15),
                                  ),
                                ),
                              );

                              break;
                            default:
                              child = CustomContainer(
                                margin: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 20),
                                child: Center(
                                  child: Text(
                                    this.collections[i - 1].name ?? "",
                                    style: TextStyle(
                                        color: this.selectedIndex == i &&
                                                !isFolder
                                            ? Colors.white
                                            : Color.fromRGBO(138, 148, 159, 1),
                                        fontSize: 15),
                                  ),
                                ),
                              );
                              break;
                          }

                          return CustomContainer(
                              shadowColor:
                                  ColorTheme.blackShadow.withOpacity(.2),
                              margin: EdgeInsets.only(left: 8, right: 8),
                              borderRadius: [10, 10, 10, 10],
                              backgroundColor:
                                  this.selectedIndex == i && !isFolder
                                      ? Colors.black
                                      : Colors.white,
                              child: child,
                              onPressed: () {
                                isFolder = false;
                                this.selectedIndex = i;
                                if (this.selectedIndex == 0) {
                                  this.cubit.getUserMaps();
                                } else {
                                  this.cubit.getMaps(this
                                      .collections[i - 1]
                                      .collectionNo
                                      .toString());
                                }

                                this.selectedMarkerId = null;
                                setState(() {});
                              });
                        }),
                        shrinkWrap: true,
                        itemCount: this.collections.length + 1),
                  )
                ],
              ),
            )
          ],
        ));
  }

  getPage() {
    return Container(
      child: GoogleMap(
        mapType: MapType.normal,
        markers: _markers.toSet(),
        myLocationEnabled: true,
        myLocationButtonEnabled: false,
        zoomControlsEnabled: false,
        mapToolbarEnabled: false,
        liteModeEnabled: false,
        initialCameraPosition: this.currentPosition,
        onMapCreated: (GoogleMapController controller) {
          this.controller = controller;

          this.controller.animateCamera(
              CameraUpdate.newCameraPosition(this.currentPosition));
        },
        onCameraMove: (cameraPosition) {
          this.cameraPosition = cameraPosition;
        },
        onTap: (value) {
          this.selectedMarkerId = null;
          this.reloadMarker();

          setState(() {});
        },
        compassEnabled: true,
        zoomGesturesEnabled: true,
        rotateGesturesEnabled: false,
        scrollGesturesEnabled: true,
        tiltGesturesEnabled: false,
      ),
    );
  }

  filterCategoryList() {
    return CustomContainer(
      height: MediaQuery.of(context).size.height,
      backgroundColor: Color.fromRGBO(0, 0, 0, 0.5),
      onPressed: () {
        this.isFilter = false;
        setState(() {});
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomContainer(
                margin: EdgeInsets.only(right: 25, bottom: 40),
                height: 44,
                width: 44,
                borderRadius: [15, 15, 15, 15],
                backgroundColor: Colors.black,
                child: Container(
                  margin: EdgeInsets.all(8),
                  child: Image.asset(
                    "assets/images/icnsFilter.png",
                    height: 28,
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
          Container(
            height: 60,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(bottom: 50),
            child: ListView.builder(
              padding: EdgeInsets.only(left: 20, right: 20),
              scrollDirection: Axis.horizontal,
              itemBuilder: ((BuildContext c, int i) {
                Widget textWidget;
                if (i == 0) {
                  textWidget = Text(
                    "전체",
                    style: TextStyle(
                        fontSize: 13,
                        color: this.filterSelectedIndex == i
                            ? Colors.white
                            : Color.fromRGBO(138, 148, 148, 1)),
                  );
                } else {
                  textWidget = Text(
                    this.categories[i - 1].categoryName,
                    style: TextStyle(
                        fontSize: 13,
                        color: this.filterSelectedIndex == i
                            ? Colors.white
                            : Color.fromRGBO(138, 148, 148, 1)),
                  );
                }

                Color selectedColor;
                if (this.filterSelectedIndex == i && i == 0) {
                  selectedColor = Colors.black;
                } else if (this.filterSelectedIndex == i) {
                  selectedColor = true
                      ? HexColor.fromHex(categories[i - 1].categoryColor)
                      : Color.fromRGBO(255, 157, 81, 1);
                } else {
                  selectedColor = Colors.white;
                }
                return CustomContainer(
                  height: 60,
                  width: 60,
                  margin: EdgeInsets.only(left: 8, right: 8),
                  borderRadius: [30, 30, 30, 30],
                  backgroundColor: selectedColor,
                  child: Center(
                    child: textWidget,
                  ),
                  onPressed: () {
                    this.filterSelectedIndex = i;
                    this.reloadMarker();
                    setState(() {});
                  },
                );
              }),
              itemCount: this.categories.length + 1,
            ),
          )
        ],
      ),
    );
  }
}
