import 'dart:convert';
import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_radar_chart/flutter_radar_chart.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/result_model.dart';
import 'package:fitmybiz/routes/character/result/character_result_cubit.dart';
import 'package:fitmybiz/routes/character/result/character_result_state.dart';
import 'package:fitmybiz/routes/character/test/character_test_page.dart';
import 'package:fitmybiz/routes/me/me_cubit.dart';
import 'package:fitmybiz/themes/color_theme.dart';
//
import 'package:flutter/services.dart' show rootBundle;
import 'package:validators/sanitizers.dart';

class CharacterResultPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => CharacterResultCubit(),
        ),
        BlocProvider(
          create: (_) => MeCubit(),
        )
      ],
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  final Duration animDuration = const Duration(milliseconds: 250);
  int touchedIndex;
  //radar
  bool useSides = true;
  double numberOfFeatures = 6;
  var hexaco = <List<int>>[];
  var ticks = <int>[];

  ResultModel result;

  List<String> features;
  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    context.bloc<CharacterResultCubit>().getResult();
  }

  Future<String> getJson() {
    return rootBundle.loadString('assets/json/generated.json');
  }

  @override
  Widget build(BuildContext context) {
    features = ["정직성", "정서성", "외향성", "원만성", "성실성", "개방성"];
    features = features.sublist(0, numberOfFeatures.floor());
    hexaco = hexaco
        .map((graph) => graph.sublist(0, numberOfFeatures.floor()))
        .toList();
    return BlocListener(
      cubit: context.bloc<CharacterResultCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();

            break;
          case GetResult:
            this.indicator.hide();
            this.result = state.contents;
            var data = [
              this.result.hexaco.h,
              this.result.hexaco.e,
              this.result.hexaco.x,
              this.result.hexaco.a,
              this.result.hexaco.c,
              this.result.hexaco.o,
            ];
            hexaco.add(data);
            var maxTick = [
              result.data.h,
              result.data.e,
              result.data.x,
              result.data.a,
              result.data.c,
              result.data.o
            ].reduce(max);
            ticks = true
                ? [
                    20,
                    40,
                    60,
                    80,
                    100,
                  ]
                : true
                    ? [
                        (maxTick * .3).toInt(),
                        (maxTick * .6).toInt(),
                        (maxTick * .9).toInt(),
                        (maxTick * 1.2).toInt(),
                        (maxTick * 1.5).toInt()
                      ]
                    : [
                        this.result.hexaco.h,
                        this.result.hexaco.e,
                        this.result.hexaco.x,
                        this.result.hexaco.a,
                        this.result.hexaco.c,
                        this.result.hexaco.o
                      ];
            setState(() {});
            break;
        }
      },
      child: PlatformScaffold(
        leading: CustomContainer(
          onPressed: () {
            this.navigator.popRoute(null);
          },
          margin: EdgeInsets.only(left: 25),
          width: 28,
          height: 28,
          child: Image.asset('assets/images/icnsClose.png'),
        ),
        title: Text('결과 분석'),
        trailing: true
            ? CustomContainer(
                margin: EdgeInsets.symmetric(horizontal: 24),
                onPressed: () {
                  this.navigator.popRoute(null);
                  this.navigator.replaceRoute(CharacterTestPage());
                },
                child: Text(
                  '다시하기',
                  style: TextStyle(
                      color: ColorTheme.primary,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              )
            : FlatButton(
                onPressed: () {
                  this.navigator.popRoute(null);
                  this.navigator.replaceRoute(CharacterTestPage());
                },
                padding: EdgeInsets.only(right: 24),
                child: Text(
                  '다시하기',
                  style: TextStyle(
                      color: ColorTheme.primary,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
        body: ListView(
          padding: EdgeInsets.all(25),
          children: [
            AspectRatio(
              aspectRatio: 325 / 239,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: this.result != null
                    ? CachedNetworkImage(
                        imageUrl: this.result.data?.imageUrl,
                        fit: BoxFit.cover,
                      )
                    : Container(),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            //title section
            Text(
              this.result != null ? '${this.result.data?.title ?? ''}' : '',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 26,
              ),
            ),

            SizedBox(
              height: 12,
            ),

            //subs section
            Text(
              this.result != null
                  ? '${this.result.data?.description ?? ''}'
                  : '',
              style: TextStyle(
                color: ColorTheme.black.withOpacity(.7),
                fontSize: 15,
              ),
            ),
            SizedBox(
              height: 30,
            ),

            //tag section
            Row(
              children: this.result != null
                  ? (this.result.data?.tags ?? [])
                      .toList()
                      .map((e) => CustomContainer(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            borderRadius: [20, 20, 20, 20],
                            backgroundColor: ColorTheme.grayBg,
                            margin: EdgeInsets.symmetric(horizontal: 5.5),
                            height: 40,
                            child: Center(
                              child: Text(
                                '# $e',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: ColorTheme.black),
                              ),
                            ),
                          ))
                      .toList()
                  : [],
            ),

            SizedBox(
              height: 40,
            ),
            Text(
              '분야별 점수',
              style: TextStyle(
                  color: ColorTheme.black,
                  fontSize: 22,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 15,
            ),
            CustomContainer(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              borderRadius: [30, 30, 30, 30],
              backgroundColor: ColorTheme.grayBg,
              child: BarChart(
                mainBarData(),
                swapAnimationDuration: animDuration,
              ),
            ),
            SizedBox(
              height: 28,
            ),
            CustomContainer(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              borderRadius: [30, 30, 30, 30],
              backgroundColor: ColorTheme.grayBg,
              child: RadarChart.light(
                ticks: ticks,
                features: features,
                data: hexaco,
                reverseAxis: false,
                useSides: useSides,
              ),
            )
          ],
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(
    int x,
    double y, {
    bool isTouched = false,
    Color barColor,
    double width = 38,
    List<int> showTooltips = const [],
  }) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          y: isTouched ? y + 1 : y,
          colors: isTouched ? [Colors.yellow] : [barColor],
          width: width,
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            y: 20,
            colors: [Colors.transparent],
          ),
        ),
      ],
      showingTooltipIndicators: showTooltips,
    );
  }

  List<BarChartGroupData> showingGroups() => List.generate(6, (i) {
        switch (i) {
          case 0:
            return makeGroupData(
                0, this.result != null ? this.result.data.h.toDouble() : 0.0,
                barColor: ColorTheme.colorActivity,
                isTouched: i == touchedIndex);
          case 1:
            return makeGroupData(
                1, this.result != null ? this.result.data.e.toDouble() : 0.0,
                barColor: ColorTheme.colorTrip, isTouched: i == touchedIndex);
          case 2:
            return makeGroupData(
                2, this.result != null ? this.result.data.x.toDouble() : 0.0,
                barColor: ColorTheme.colorCafe, isTouched: i == touchedIndex);
          case 3:
            return makeGroupData(
                3, this.result != null ? this.result.data.a.toDouble() : 0.0,
                barColor: ColorTheme.colorTrip, isTouched: i == touchedIndex);
          case 4:
            return makeGroupData(
                4, this.result != null ? this.result.data.c.toDouble() : 0.0,
                barColor: ColorTheme.colorShopping,
                isTouched: i == touchedIndex);
          case 5:
            return makeGroupData(
                5, this.result != null ? this.result.data.o.toDouble() : 0.0,
                barColor: ColorTheme.colorReports,
                isTouched: i == touchedIndex);
          default:
            return null;
        }
      });

  BarChartData mainBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: Colors.blueGrey,
            getTooltipItem: (group, groupIndex, rod, rodIndex) {
              String weekDay;
              weekDay = features[group.x.toInt()];
              return BarTooltipItem(weekDay + '\n' + (rod.y - 1).toString(),
                  TextStyle(color: Colors.yellow));
            }),
        touchCallback: (barTouchResponse) {
          setState(() {
            if (barTouchResponse.spot != null &&
                barTouchResponse.touchInput is! FlPanEnd &&
                barTouchResponse.touchInput is! FlLongPressEnd) {
              touchedIndex = barTouchResponse.spot.touchedBarGroupIndex;
            } else {
              touchedIndex = -1;
            }
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),
          margin: 16,
          getTitles: (double value) {
            return features[value.toInt()];
          },
        ),
        leftTitles: SideTitles(
          showTitles: false,
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: showingGroups(),
    );
  }
}
