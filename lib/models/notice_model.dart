import 'dart:developer';

import 'package:intl/intl.dart';

class NoticeModel {
  final String noticeTitle;
  final String noticeContent;
  final DateTime createdAt;

  NoticeModel({this.noticeTitle, this.noticeContent, this.createdAt});

  factory NoticeModel.fromJson(Map<String, dynamic> json) {
    return NoticeModel(
      noticeTitle: json['notice_title'],
      noticeContent: json['notice_content'],
      createdAt: json.containsKey('created_at') ? DateFormat("yyyy-MM-dd HH:mm:ss").parse(json['created_at'], true) : null,
    );
  }
}
