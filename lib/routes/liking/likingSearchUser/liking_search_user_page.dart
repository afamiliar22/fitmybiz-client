import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/liking/zzimWithUser/liking_zzim_page.dart';

import 'liking_search_user_cubit.dart';
import 'liking_search_user_state.dart';

enum FollowType { user, follower, following }

class LikingSearchUserPage extends StatelessWidget {
  final String search;

  LikingSearchUserPage(this.search);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LikingSearchUserCubit(),
      child: _View(this.search),
    );
  }
}

class _View extends BaseRoute {
  final String search;

  _View(this.search);
  @override
  _ViewState createState() => _ViewState(this.search);
}

class _ViewState extends BaseRouteState {
  String search;

  _ViewState(this.search);

  String titleText;
  LikingSearchUserCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<LikingSearchUserCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    cubit.getSearchUser(search, "10");
    this.titleText = "유저 검색 결과";
    setState(() {});
  }

  List<String> areas = [];
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<LikingSearchUserCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        title: Text(
          this.titleText,
          style: TextStyle(
              fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),
        ),
        body: SafeArea(
          child: cubit.users.length == 0
              ? Center(
                  child: Container(
                    margin: EdgeInsets.only(top: 100),
                    child: Column(
                      children: [
                        Text(
                          "검색된 유저가 없어요.",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                      ],
                    ),
                  ),
                )
              : Container(
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemBuilder: ((BuildContext c, int i) {
                      return CustomContainer(
                        onPressed: () {
                          this
                              .navigator
                              .pushRoute(LikingZzimPage(cubit.users[i].userNo));
                        },
                        child: Row(
                          children: [
                            this.cubit.users[i].profileImage == null
                                ? Container(
                                    margin: EdgeInsets.only(top: 8, bottom: 8),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(22),
                                      child: Image.asset(
                                        'assets/images/userProfileImg.png',
                                        fit: BoxFit.cover,
                                        height: 44,
                                        width: 44,
                                      ),
                                    ),
                                  )
                                : Container(
                                    margin:
                                        EdgeInsets.only(top: 15, bottom: 15),
                                    child: CustomAvatar(
                                      size: 44,
                                      image: CustomCachedNetworkImage(
                                        preloadImage:
                                            this.cubit.users[i].profileImage.sm,
                                        image:
                                            this.cubit.users[i].profileImage.sm,
                                      ),
                                    ),
                                  ),
                            Container(
                              margin: EdgeInsets.only(left: 8),
                              child: Text(
                                this.cubit.users[i].nickname,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Spacer(),
                            this.cubit.selectedFollows[i]
                                ? Container(
                                    child: CustomContainer(
                                    backgroundColor: Colors.white,
                                    borderRadius: [10, 10, 10, 10],
                                    borderWidth: 1,
                                    borderColor:
                                        Color.fromRGBO(173, 181, 190, 1),
                                    child: Container(
                                      margin: EdgeInsets.symmetric(
                                          vertical: 6, horizontal: 16),
                                      child: Text(
                                        "팔로잉",
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                173, 181, 190, 1),
                                            fontSize: 13),
                                      ),
                                    ),
                                    onPressed: () {
                                      this.cubit.selectedFollows[i] =
                                          !this.cubit.selectedFollows[i];
                                      setState(() {});
                                      this.cubit.putUpdateFollow(
                                          this.cubit.users[i].userNo);
                                    },
                                  ))
                                : Container(
                                    child: CustomButton(
                                    backgroundColor:
                                        Color.fromRGBO(31, 31, 36, 1),
                                    borderRadius: 10,
                                    borderColor: Color.fromRGBO(31, 31, 36, 1),
                                    text: Container(
                                      margin: EdgeInsets.symmetric(
                                          vertical: 6, horizontal: 16),
                                      child: Text(
                                        "팔로우",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 13),
                                      ),
                                    ),
                                    onPressed: () {
                                      this.cubit.selectedFollows[i] =
                                          !this.cubit.selectedFollows[i];
                                      setState(() {});
                                      this.cubit.putUpdateFollow(
                                          this.cubit.users[i].userNo);
                                    },
                                  )),
                          ],
                        ),
                      );
                    }),
                    itemCount: this.cubit.users.length,
                  ),
                ),
        ),
      ),
    );
  }
}
