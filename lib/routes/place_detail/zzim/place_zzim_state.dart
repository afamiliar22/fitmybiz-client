import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/collection_model.dart';

abstract class PlaceZzimState extends Equatable {}

class InitialState extends PlaceZzimState {
  @override
  List<Object> get props => [];
}

class LoadingState extends PlaceZzimState {
  @override
  List<Object> get props => [];
}

class ErrorState extends PlaceZzimState {
  @override
  List<Object> get props => [];
}

class GetCollections extends PlaceZzimState {
  GetCollections(this.result, this.collections);

  final bool result;
  final List<CollectionModel> collections;

  @override
  List<Object> get props => [result, collections];
}

class PostCollectionSet extends PlaceZzimState {
  PostCollectionSet(this.result);

  final bool result;

  @override
  List<Object> get props => [result];
}
