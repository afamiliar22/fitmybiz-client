import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';

abstract class AuthState extends Equatable {}

class InitialState extends AuthState {
  @override
  List<Object> get props => [];
}

class LoadingState extends AuthState {
  @override
  List<Object> get props => [];
}

class SignedState extends AuthState {
  @override
  List<Object> get props => [];
}

class CheckedTermState extends AuthState {
  CheckedTermState(this.result, this.terms);

  final bool result;
  final List<TermModel> terms;

  @override
  List<Object> get props => [result, terms];
}

class ErrorState extends AuthState {
  @override
  List<Object> get props => [];
}
