import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/tools/facebook_client.dart';
import 'select_collection_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class SelectCollectionCubit extends BaseCubit<SelectCollectionState> {
  /// {@macro counter_cubit}
  SelectCollectionCubit() : super(InitialState());

  List<CollectionModel> collections = [];
  Set<String> selectedCollectionItems = {};
  void init() {
    // {{host}}/api/v1/link/image?source_url=https://blog.naver.com/hjungeun27/222133877111
  }

  void getCollectionSaved() {
    String method = 'get';
    String path = '/api/v1/collection/check/saved/item?image_content_no=1';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      this.collections = (response.data as List)
          .map((record) => CollectionModel.fromJson(record))
          .toList();
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void deleteCollectionItem(String collectionNo, String collectionItemNo) {
    String method = 'delete';
    String path = '/api/v1/collection/$collectionNo/item/delete';
    Map<String, dynamic> data = {"collection_item_no": collectionItemNo};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      emit(ToastState('장소가 삭제되었습니다.'));
      emit(CompletedState());
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void copyCollectionItem(String collectionNo, String imageContentNo) {
    String method = 'post';
    String path = '/api/v1/collection/$collectionNo/copy/item';
    Map<String, dynamic> data = {"image_content_no": imageContentNo};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.property.meCollectionSubject.add(true);
      emit(ToastState('장소가 복사되었습니다.'));
      emit(CompletedState());
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void postCollectionSet(String imgContentNo, String collectionNo) {
    String method = 'post';
    String path = '/api/v1/collection/set';

    Map data = {
      "image_content_no": imgContentNo.toString(),
      "collection_no": collectionNo
    };
    if (collectionNo == null) {
      data = {"image_content_no": imgContentNo.toString()};
    }
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.facebook.addEvent(FacebookEventType.wishImageContent);

      this.property.meCollectionSubject.add(true);

      emit(PostCollectionSet(true));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
