class ContentInfoModel {
  String contentId;
  int contentNo;
  int placeNo;
  String contentUuid;
  String title;
  String summary;
  Null overview;
  String addrParcel;
  String addrRoad;
  Null homepage;
  double lat;
  double lng;
  String areaCode;
  String groupCode;
  Null userNo;
  String check;
  Null video;
  Null reservation;
  int bookmarkCount;
  int reportCount;
  String state;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String googlePlaceId;
  String formattedAddress;
  String formattedPhoneNumber;
  Null internationalPhoneNumber;
  String administrativeAreaLevel1;
  Null locality;
  String sublocalityLevel1;
  String sublocalityLevel2;

  ContentInfoModel(
      {this.contentId,
      this.contentNo,
      this.placeNo,
      this.contentUuid,
      this.title,
      this.summary,
      this.overview,
      this.addrParcel,
      this.addrRoad,
      this.homepage,
      this.lat,
      this.lng,
      this.areaCode,
      this.groupCode,
      this.userNo,
      this.check,
      this.video,
      this.reservation,
      this.bookmarkCount,
      this.reportCount,
      this.state,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.googlePlaceId,
      this.formattedAddress,
      this.formattedPhoneNumber,
      this.internationalPhoneNumber,
      this.administrativeAreaLevel1,
      this.locality,
      this.sublocalityLevel1,
      this.sublocalityLevel2});

  ContentInfoModel.fromJson(Map<String, dynamic> json) {
    contentId = json['content_id'];
    contentNo = json['content_no'];
    placeNo = json['place_no'];
    contentUuid = json['content_uuid'];
    title = json['title'];
    summary = json['summary'];
    overview = json['overview'];
    addrParcel = json['addr_parcel'];
    addrRoad = json['addr_road'];
    homepage = json['homepage'];
    lat = json['lat'];
    lng = json['lng'];
    areaCode = json['area_code'];
    groupCode = json['group_code'];
    userNo = json['user_no'];
    check = json['check'];
    video = json['video'];
    reservation = json['reservation'];
    bookmarkCount = json['bookmark_count'];
    reportCount = json['report_count'];
    state = json['state'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    googlePlaceId = json['google_place_id'];
    formattedAddress = json['formatted_address'];
    formattedPhoneNumber = json['formatted_phone_number'];
    internationalPhoneNumber = json['international_phone_number'];
    administrativeAreaLevel1 = json['administrative_area_level_1'];
    locality = json['locality'];
    sublocalityLevel1 = json['sublocality_level_1'];
    sublocalityLevel2 = json['sublocality_level_2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content_id'] = this.contentId;
    data['content_no'] = this.contentNo;
    data['place_no'] = this.placeNo;
    data['content_uuid'] = this.contentUuid;
    data['title'] = this.title;
    data['summary'] = this.summary;
    data['overview'] = this.overview;
    data['addr_parcel'] = this.addrParcel;
    data['addr_road'] = this.addrRoad;
    data['homepage'] = this.homepage;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['area_code'] = this.areaCode;
    data['group_code'] = this.groupCode;
    data['user_no'] = this.userNo;
    data['check'] = this.check;
    data['video'] = this.video;
    data['reservation'] = this.reservation;
    data['bookmark_count'] = this.bookmarkCount;
    data['report_count'] = this.reportCount;
    data['state'] = this.state;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['google_place_id'] = this.googlePlaceId;
    data['formatted_address'] = this.formattedAddress;
    data['formatted_phone_number'] = this.formattedPhoneNumber;
    data['international_phone_number'] = this.internationalPhoneNumber;
    data['administrative_area_level_1'] = this.administrativeAreaLevel1;
    data['locality'] = this.locality;
    data['sublocality_level_1'] = this.sublocalityLevel1;
    data['sublocality_level_2'] = this.sublocalityLevel2;
    return data;
  }
}
