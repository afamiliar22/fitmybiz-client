import 'package:equatable/equatable.dart';

abstract class MyMapState extends Equatable {}

class InitialState extends MyMapState {
  @override
  List<Object> get props => [];
}

class LoadingState extends MyMapState {
  @override
  List<Object> get props => [];
}

class SetState extends MyMapState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class ToastState extends MyMapState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends MyMapState {
  @override
  List<Object> get props => [];
}
