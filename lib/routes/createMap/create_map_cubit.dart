import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/routes/createMap/create_map_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class CreateMapCubit extends BaseCubit<CreateMapState> {
  /// {@macro counter_cubit}
  CreateMapCubit() : super(InitialState());

  void init() {
    emit(InitialState());
  }

  void postCollection(String name, bool secret) {
    if (name.isEmpty) {
      emit(ToastState('지도 제목륵 입력 해 주세요'));
      emit(ErrorState());
      return;
    }
    String method = 'post';
    String path = '/api/v1/collection';
    Map data = {"name": name, "secret": false};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.facebook.addEvent(FacebookEventType.addMap);
      this.property.meCollectionSubject.add(true);
      emit(PostCollection(true));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
