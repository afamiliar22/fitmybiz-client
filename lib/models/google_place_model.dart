import 'dart:developer';

class GooglePlaceModel {
  final String formattedAddress;
  final String placeId;
  final String name;
  final double latitude;
  final double longitude;
  final double rating;

  GooglePlaceModel(
      {this.formattedAddress,
      this.placeId,
      this.name,
      this.latitude,
      this.longitude,
      this.rating});

  factory GooglePlaceModel.fromJson(Map<String, dynamic> json) {
    return GooglePlaceModel(
      formattedAddress: json['formatted_address'],
      placeId: json['place_id'],
      name: json['name'],
      latitude: json['geometry']['location']['lat'],
      longitude: json['geometry']['location']['lng'],
      rating: json['rating'],
    );
  }
}
