import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/routes/auth/forgot_password/forgot_password_page.dart';
import 'package:fitmybiz/routes/character/test/character_test_page.dart';
import 'package:fitmybiz/routes/config/config_cubit.dart';
import 'package:fitmybiz/routes/me/me_cubit.dart';
import 'package:fitmybiz/routes/notice/notice_page.dart';
import 'package:fitmybiz/routes/profile/profile_page.dart';
import 'package:fitmybiz/routes/splash/splash_cubit.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/tabs/tabs_page.dart';
import 'package:fitmybiz/routes/taste_temp/taste_page.dart';
import 'package:fitmybiz/routes/views/custom_web_view.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'config_state.dart';

class ConfigPage extends StatelessWidget {
  const ConfigPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => MeCubit(),
        ),
        BlocProvider(
          create: (_) => ConfigCubit(),
        )
      ],
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  ConfigCubit cubit;
  MeCubit meCubit;
  //
  List<CategoryModel> categories = [];

  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<ConfigCubit>();
    this.meCubit = context.bloc<MeCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.getCategories(0, '', 0, 0);
    this.meCubit.init();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case GetCategory:
            categories = state.categories;
            this.indicator.hide();
            setState(() {});

            break;

          case CompleteDeleteUserState:
            this.property.isSignIn = false;
            this.navigator.resetRoute(SplashPage());

            break;
          case ToastState:
            this.showToast(state.message);
            break;
        }
      },
      child: PlatformScaffold(
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('설정'),
        /*
        trailing: CustomContainer(
          onPressed: () {
            this.navigator.pushRoute(
                TastePage(categories, false, this.meCubit.user.userNo));
          },
          child: Image.asset('assets/images/elementButtonsBookmark.png'),
        ),
        */
        body: ListView(
          children: [
            this.buildCell(
              text: Text('프로필 설정',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                this.navigator.pushRoute(ProfilePage(
                      false,
                    ));
              },
            ),
            this.buildCell(
              text: Text('성향 검사',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                this.navigator.pushRoute(CharacterTestPage());
              },
            ),
            this.buildCell(
              text: Text('비밀번호 재설정',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                this.navigator.pushRoute(ForgotPasswordPage());
              },
            ),
            Divider(height: 12, thickness: 12, color: ColorTheme.grayBg),
            this.buildCell(
              text: Text('공지사항',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                this.navigator.pushRoute(NoticePage());
              },
            ),
            this.buildCell(
              text: Text('이용약관',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                String url =
                    "https://s3.ap-northeast-2.amazonaws.com/www.zzieut.com/real_zzim_term.html";
                this.navigator.pushRoute(CustomWebView(url, null, null));
              },
            ),
            this.buildCell(
              text: Text('개인정보 취급방침',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                String url =
                    "https://s3.ap-northeast-2.amazonaws.com/www.zzieut.com/real_zzim_personal_data.html";
                this.navigator.pushRoute(CustomWebView(url, null, null));
              },
            ),
            this.buildCell(
              text: Text('문의하기',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                String url = "https://pf.kakao.com/_esUeC";
                this.launch.moveToBrowser(url);
              },
            ),
            this.buildCell(
              text: Text('카카오톡으로 친구 초대하기',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                this.indicator.show();

                String defaultImage =
                    'https://s3.ap-northeast-2.amazonaws.com/www.zzieut.com/icon.png';
                this
                    .firebase
                    .generateDynamicLink(
                      path: '/',
                      title: '찜',
                      description:
                          '${this.property.user.nickname}님이 찜에 초대 했어요.',
                      image: defaultImage,
                    )
                    .then((value) {
                  this.indicator.hide();
                  Share.share(value);
                }).catchError((e) {
                  this.indicator.hide();
                });
              },
            ),
            Divider(height: 12, thickness: 12, color: ColorTheme.grayBg),
            this.buildCell(
              text: Text('로그아웃',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.black)),
              onPressed: () {
                this.property.isSignIn = false;
                this.navigator.resetRoute(SplashPage());
              },
            ),
            this.buildCell(
              text: Text('탈퇴하기',
                  style: TextStyleTheme.common
                      .copyWith(fontSize: 15, color: ColorTheme.red)),
              onPressed: () {
                this
                    .showConfirm(
                        title: '탈퇴하기',
                        description: '정말로 찜을 떠나실건가요?...??',
                        positiveButtonText: '네.',
                        negativeButtonText: '아니요~')
                    .then((value) {
                  if (value == true) {
                    this.cubit.deleteUser();
                  }
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCell({Text text, VoidCallback onPressed}) {
    return Column(
      children: [
        CustomContainer(
          child: Row(
            children: [
              text,
              Spacer(),
              Image.asset('assets/images/icnsNext.png',
                  color: Colors.grey, width: 28, height: 28),
            ],
          ),
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25),
          onPressed: onPressed,
        ),
        Divider(
            height: 0.5,
            thickness: 0.5,
            indent: 25,
            endIndent: 25,
            color: ColorTheme.grayBg),
      ],
    );
  }
}
