import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/login_model.dart';
import 'package:fitmybiz/models/term_model.dart';

abstract class SignEmailState extends Equatable {}

class InitialState extends SignEmailState {
  @override
  List<Object> get props => [];
}

class LoadingState extends SignEmailState {
  @override
  List<Object> get props => [];
}

class SignedState extends SignEmailState {
  @override
  List<Object> get props => [];
}

class CheckedTermState extends SignEmailState {
  CheckedTermState(this.result, this.terms);

  final bool result;
  final List<TermModel> terms;

  @override
  List<Object> get props => [result, terms];
}

class ToastState extends SignEmailState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends SignEmailState {
  @override
  List<Object> get props => [];
}
