import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';

class BaseModalRoute extends BaseRoute {
  @override
  BaseModalRouteState createState() => BaseModalRouteState();
}

class BaseModalRouteState extends BaseRouteState {
  Color _color;
  @override
  void initState() {
    super.initState();
    _color = Colors.transparent;
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    _color = Colors.black.withOpacity(0.5);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }

  Widget modallyScaffold({Widget title, Widget body}) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      // color: _color,
      curve: Curves.easeIn,
      child: PlatformScaffold(
        onPressed: () {
          FocusScope.of(context).unfocus();
        },
        backgroundColor: Colors.transparent,
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              CustomContainer(
                height: 72,
                borderRadius: [30, 30, 0, 0],
                backgroundColor: Colors.white,
                child: title,
              ),
              Expanded(
                child: CustomContainer(
                  backgroundColor: Colors.white,
                  child: SafeArea(child: body),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
