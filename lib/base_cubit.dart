import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:fitmybiz/tools/auth_client.dart';
import 'package:fitmybiz/tools/facebook_client.dart';
import 'package:fitmybiz/tools/network_client.dart';
import 'package:fitmybiz/tools/property_client.dart';
import 'package:image/image.dart' as IMG;

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class BaseCubit<State> extends Cubit<State> {
  NetworkClient network = NetworkClient.shared;
  PropertyClient property = PropertyClient.shared;
  AuthClient auth = AuthClient.shared;
  FacebookClient facebook = FacebookClient.shared;

  BaseCubit(State state) : super(state);
}

Future<Uint8List> resizeFuture(Uint8List bytes) {
  return compute(resize, bytes);
}

Uint8List resize(Uint8List bytes) {
  int maxSize = 1024;
  IMG.Image img = IMG.decodeImage(bytes);
  if (img.width >= maxSize || img.height >= maxSize) {
    double widthRatio = img.width / img.height;
    double heightRatio = img.height / img.width;
    int width =
        img.width > img.height ? maxSize : (widthRatio * maxSize).round();
    int height =
        img.width > img.height ? (heightRatio * maxSize).round() : maxSize;

    IMG.Image resized = IMG.copyResize(img, width: width, height: height);
    Uint8List resizedData = IMG.encodeJpg(resized);
    return resizedData;
  } else {
    return bytes;
  }
}
