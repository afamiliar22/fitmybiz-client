import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/questions_model.dart';
import 'package:fitmybiz/routes/character/test/character_test_cubit.dart';
import 'package:fitmybiz/routes/character/result/character_result_page.dart';
import 'package:fitmybiz/routes/character/test/character_test_state.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:percent_indicator/percent_indicator.dart';

class CharacterTestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => CharacterTestCubit(),
        ),
      ],
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var _options = <String>[
    '전혀 그렇지 않다',
    '그렇지 않은 편이다',
    '보통이다',
    '그런 편이다',
    '매우 그렇다'
  ];

  int questionCount = 1;
  var solvedItems = <int, int>{};
  bool isBack = false;
  var questions = <QuestionsModel>[];

  var selectedItems = List<Map<String, int>>.generate(24, (i) => {});

  PageController _pageController;

  CharacterTestCubit cubit;

  _ViewState();

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    context.bloc<CharacterTestCubit>().getQuestions();

    this.cubit.init();
  }

  @override
  void dispose() {
    selectedItems.clear();

    super.dispose();
  }

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    this.cubit = context.bloc<CharacterTestCubit>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<CharacterTestCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();

            break;
          case CompletedState:
            this.indicator.hide();
            this.navigator.popRoute(null);
            this.navigator.replaceRoute(CharacterResultPage());
            break;
          case GetQuestions:
            this.indicator.hide();
            this.questions = List<QuestionsModel>.from(state.contents);

            setState(() {});
            break;
        }
      },
      child: PlatformScaffold(
        title: Text.rich(
          TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: "$questionCount",
                style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    color: ColorTheme.black),
              ),
              TextSpan(
                  text: " / 24",
                  style: TextStyle(
                      fontSize: 26, color: ColorTheme.grayPlaceholder)),
            ],
          ),
        ),
        leading: CustomContainer(
          onPressed: () {
            this.navigator.popRoute(null);
          },
          margin: EdgeInsets.only(left: 19),
          width: 30,
          height: 30,
          child: Image.asset('assets/images/icnsClose.png'),
        ),
        body: Stack(
          children: [
            Column(
              children: [
                SizedBox(
                  height: 12,
                ),
                Padding(
                  padding: EdgeInsets.all(25),
                  child: LinearPercentIndicator(
                    percent: questionCount / 24,
                    backgroundColor: ColorTheme.grayBg,
                  ),
                ),
                Expanded(
                    child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: PageView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 24,
                      onPageChanged: (page) {
                        var swipingRight = page > _pageController.page;
                        if (swipingRight) {
                          isBack = false;
                        } else {}
                        questionCount = page + 1;
                        setState(() {});
                      },
                      controller: _pageController,
                      itemBuilder: (context, index) {
                        return Column(
                          children: [
                            Container(
                              height: 196,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Flexible(
                                      child: Text(
                                    questions.isEmpty
                                        ? ""
                                        : this
                                            .questions[questionCount - 1]
                                            .question,
                                    style: TextStyle(
                                        fontSize: 26,
                                        fontWeight: FontWeight.w500),
                                  ))
                                ],
                              ),
                            ),
                            Expanded(
                                child: Column(
                              children: _options
                                  .map((option) => CustomContainer(
                                        onPressed: () {
                                          var data = {
                                            'personality_test_question_no':
                                                index + 1,
                                            'score':
                                                _options.indexOf(option) + 1
                                          };
                                          selectedItems[index] = data;
                                          solvedItems[index] =
                                              _options.indexOf(option);
                                          if (!isBack)
                                            _pageController.nextPage(
                                                duration:
                                                    Duration(milliseconds: 200),
                                                curve: Curves.easeIn);
                                          setState(() {});
                                        },
                                        borderRadius: [15, 15, 15, 15],
                                        padding: EdgeInsets.only(
                                            left: 20, top: 15, bottom: 15),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height: 52,
                                        margin:
                                            EdgeInsets.symmetric(vertical: 6),
                                        backgroundColor: solvedItems[index] ==
                                                _options.indexOf(option)
                                            ? ColorTheme.primary
                                            : ColorTheme.grayBg,
                                        child: Text(
                                          '$option',
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500,
                                              color: solvedItems[index] ==
                                                      _options.indexOf(option)
                                                  ? ColorTheme.white
                                                  : ColorTheme.grayLv1),
                                        ),
                                      ))
                                  .toList(),
                            ))
                          ],
                        );
                      }),
                ))
              ],
            ),
            Positioned(
                child: Column(
              children: [
                Spacer(),
                Container(
                  margin: EdgeInsets.all(25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Visibility(
                          visible: questionCount - 1 != 0,
                          child: Container(
                            width: 60,
                            height: 60,
                            child: FloatingActionButton(
                                heroTag: null,
                                elevation: 0,
                                backgroundColor: ColorTheme.grayBg,
                                onPressed: () {
                                  _pageController
                                      .previousPage(
                                          duration: Duration(
                                            milliseconds: 200,
                                          ),
                                          curve: Curves.easeIn)
                                      .then((value) {
                                    setState(() {
                                      isBack = true;
                                    });
                                  });
                                },
                                child: Image.asset(
                                  'assets/images/icnsPrev.png',
                                  width: 28,
                                  height: 28,
                                )),
                          )),
                      solvedItems.length == 24 && questionCount == 24
                          ? CustomContainer(
                              onPressed: () {
                                this.cubit.postResult(selectedItems);
                              },
                              borderRadius: [30, 30, 30, 30],
                              backgroundColor: ColorTheme.primary2,
                              shadowColor: ColorTheme.primary.withOpacity(.5),
                              width: 159,
                              height: 60,
                              child: Center(
                                child: Text(
                                  '결과보기',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                ),
                              ),
                            )
                          : Visibility(
                              visible: isBack,
                              child: Container(
                                width: 60,
                                height: 60,
                                child: FloatingActionButton(
                                    heroTag: null,
                                    elevation: 0,
                                    backgroundColor: ColorTheme.grayBg,
                                    onPressed: () {
                                      _pageController.nextPage(
                                          duration: Duration(
                                            milliseconds: 100,
                                          ),
                                          curve: Curves.easeIn);
                                      setState(() {
                                        isBack = false;
                                      });
                                    },
                                    child: Image.asset(
                                      'assets/images/icnsNext2.png',
                                      width: 28,
                                      height: 28,
                                    )),
                              )),
                    ],
                  ),
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}
