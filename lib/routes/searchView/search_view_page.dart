import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/custom_search_bar.dart';
import 'package:fitmybiz/components/custom_smart_refresher.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/keyword_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/_cells/contents_cell.dart';
import 'package:fitmybiz/routes/follow/follow_page.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';

import 'package:fitmybiz/routes/searchView/search_view_state.dart';
import 'package:fitmybiz/routes/searchView/search_view_cubit.dart';
import 'package:fitmybiz/routes/user/user_page.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/network_client.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';

class SearchViewPage extends StatelessWidget {
  const SearchViewPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SearchViewCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  final TextEditingController textEditingController = TextEditingController();
  final searchDebouncer = Debouncer(milliseconds: 500);
  bool isFocus = true;
  String searchText;

  final pageIndexNotifier = ValueNotifier<int>(0);

  List<KeywordModel> keywords;
  List<Widget> k = [];

  List<UserModel> recommendUsers = [];
  List<ContentsModel> likingContents = [];

  List<ContentsModel> searchContents = [];

  List<String> recentSearch = [];
  List<UserModel> searchUsers = [];
  List<bool> recommendFollows = [false, false, false];
  List<bool> searchUserFollows = [false, false, false];

  FollowModel follow = FollowModel();

  SearchViewCubit cubit;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // this.searchDebouncer.run(() => searchPlace());
    this.cubit = context.bloc<SearchViewCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.textEditingController.text = "";
    context.bloc<SearchViewCubit>().getKeyword();
    context.bloc<SearchViewCubit>().getRecommendUser(null, null);
    context.bloc<SearchViewCubit>().getLikingImageContents();

    print("2222");
    this.property.getStringList("search").then((value) {
      print("1111");
      print(value);
      this.recentSearch = value;
      if (value == null) {
        this.recentSearch = [];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        cubit: context.bloc<SearchViewCubit>(),
        listener: (context, state) {
          switch (state.runtimeType) {
            case InitialState:
              this.indicator.hide();

              break;

            case ErrorState:
              this.indicator.hide();
              break;
            case LoadingState:
              this.indicator.show();
              break;

            case GetKeywords:
              this.indicator.hide();

              this.keywords = state.keywords;

              for (int i = 0; i < this.keywords.length; i += 1) {
                this.k.add(Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          border:
                              Border.all(width: 1, color: Colors.grey[400])),
                      child: CustomContainer(
                        margin:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        child: Text("#" + this.keywords[i].keyword,
                            style: TextStyleTheme.common.copyWith(
                                color: Color.fromRGBO(31, 31, 36, 1.0),
                                fontSize: 15)),
                        onPressed: () {
                          this.textEditingController.text =
                              this.keywords[i].keyword;
                          this.searchText = this.keywords[i].keyword;
                          this.textEditingController.selection =
                              TextSelection.fromPosition(TextPosition(
                                  offset: textEditingController.text.length));

                          this.cubit.setQuery(this.keywords[i].keyword);
                          this.cubit.onRefresh();
                          setState(() {
                            FocusManager.instance.primaryFocus.unfocus();
                          });
                        },
                      ),
                    ));
              }

              setState(() {});
              break;

            case GetRecommendUser:
              this.indicator.hide();
              this.recommendUsers = state.recommendUsers;
              recommendFollows.clear();
              this.recommendUsers.forEach((element) {
                recommendFollows.add(element.isFollow);
              });

              setState(() {});
              break;
            case GetLikingContents:
              this.indicator.hide();
              this.likingContents = state.contents;

              setState(() {});
              break;
            case SetState:
              this.indicator.hide();
              setState(() {});
              break;

            case GetSearchUser:
              this.indicator.hide();

              this.searchUsers = state.users;

              searchUserFollows.clear();
              this.searchUsers.forEach((element) {
                searchUserFollows.add(element.isFollow);
              });

              setState(() {});

              break;
            case PutUpdateFollow:
              this.follow = state.follow;
              this.indicator.hide();

              setState(() {});
              break;
          }
        },
        child: PlatformScaffold(
          onPressed: () {
            FocusManager.instance.primaryFocus.unfocus();
            this.isFocus = false;
            setState(() {});
          },
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                searchBar(),
                this.textEditingController.text == "" ||
                        this.textEditingController.text.isEmpty
                    ? defaultView()
                    : searchView()
              ],
            ),
          ),
        ));
  }

  Widget searchBar() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Row(
        children: [
          CustomButton(
            image: Image.asset(
              'assets/images/icnsClose.png',
              width: 28,
              height: 28,
            ),
            onPressed: () {
              this.navigator.popRoute(null);
            },
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: CustomSearchBar(
              isFocus: isFocus,
              controller: this.textEditingController,
              placeholder: "검색어를 입력하세요",
              btnSearch: CustomContainer(
                child: Image.asset('assets/images/icnsClose.png',
                    color: Colors.black, width: 28, height: 28),
                onPressed: () {
                  this.navigator.popRoute(null);
                },
              ),
              onEnter: () {
                setState(() {
                  this.isFocus = true;
                });
              },
              onLeave: () {
                setState(() {
                  this.isFocus = false;
                });
              },
              onValueChange: (value) {
                this.searchText = value;

                this.searchDebouncer.run(() {
                  if (this.searchText.length > 1) {
                    this.recentSearch.insert(0, this.searchText);
                    this.property.setStringList("search", this.recentSearch);

                    this.cubit.setQuery(this.searchText);
                    this.cubit.onRefresh();
                    context
                        .bloc<SearchViewCubit>()
                        .getSearchUser(this.searchText, "20");
                  }
                  setState(() {});
                });
              },
              onClear: () {
                setState(() {});
              },
            ),
          )
        ],
      ),
    );
  }

  searchView() {
    Widget divider = SizedBox(
      height: 12,
      child: Container(
        color: Colors.grey[200],
      ),
    );
    return Expanded(
      child: RefreshConfiguration(
        enableLoadingWhenNoData: false,
        footerTriggerDistance: 200,
        child: SmartRefresher(
            controller: this.cubit.refreshController,
            enablePullDown: true,
            enablePullUp: true,
            header: CustomSmartRefresher.customHeader(),
            footer: CustomSmartRefresher.customFooter(),
            onRefresh: this.cubit.onRefresh,
            onLoading: this.cubit.onLoading,
            child: ListView(children: [
              setTitle(title: "유저 검색 결과", isKakao: true),
              searchUser(),
              this.searchUsers.length == 0
                  ? Container()
                  : SizedBox(
                      height: 16,
                    ),
              setTitle(title: "장소 검색 결과"),
              searchPlace()
            ])),
      ),
    );
  }

  defaultView() {
    Widget divider = SizedBox(
      height: 12,
      child: Container(
        color: Colors.grey[200],
      ),
    );
    return Expanded(
      child: ListView(children: [
        this.recentSearch.isEmpty ? Container() : recentSearchTitle(),
        this.recentSearch.isEmpty ? Container() : recentSearchList(),
        this.recentSearch.length == 0 ? Container() : divider,
        setTitle(title: "추천 키워드"),
        recommendationKeyword(),
        setTitle(title: "내 취향 저격!"),
        likingView(),
        setTitle(title: "주변 추천 유저"),
        recommendationUser(),
      ]),
    );
  }

  recentSearchTitle() {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25, top: 16, bottom: 0),
      child: Row(
        children: [
          Text(
            "최근 검색어",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Spacer(),
          CustomContainer(
            child: Text(
              "전체삭제",
              style: TextStyle(
                fontSize: 13,
                color: ColorTheme.primary,
              ),
            ),
            onPressed: () {
              this.recentSearch.clear();

              setState(() {});
            },
          )
        ],
      ),
    );
  }

  setTitle({String title, bool isKakao = false}) {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25, top: 16, bottom: 0),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Spacer(),
          isKakao
              ? CustomContainer(
                  child: Row(children: [
                    Image.asset(
                      "assets/images/icnsKakao.png",
                      height: 16,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      "카카오톡 친구 초대하기",
                      style: TextStyle(
                          fontSize: 13, color: Color.fromRGBO(254, 189, 38, 1)),
                    ),
                  ]),
                  onPressed: () {
                    String defaultImage =
                        'https://s3.ap-northeast-2.amazonaws.com/www.zzieut.com/icon.png';
                    this
                        .firebase
                        .generateDynamicLink(
                          path: '/',
                          title: '찜 - 하고 싶은 그것',
                          description: '우리 찜 할래?',
                          image: defaultImage,
                        )
                        .then((value) {
                      this.indicator.hide();
                      Share.share(value);
                    }).catchError((e) {
                      this.indicator.hide();
                    });
                  },
                )
              : Container()
        ],
      ),
    );
  }

  recentSearchList() {
    print("xzzzzxzxzx");
    print(recentSearch[0]);
    return Container(
      margin: EdgeInsets.only(left: 25, right: 8, top: 10, bottom: 14),
      child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemBuilder: ((BuildContext c, int i) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomContainer(
                  margin: EdgeInsets.symmetric(vertical: 4),
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          this.recentSearch[i],
                          style: TextStyle(fontSize: 15, color: Colors.black87),
                        ),
                      ),
                      Spacer(),
                      CustomButton(
                        image: Image.asset(
                          'assets/images/icnsDelete.png',
                          height: 28,
                        ),
                        onPressed: () {
                          this.recentSearch.removeAt(i);
                          this
                              .property
                              .setStringList("search", this.recentSearch);
                          setState(() {});
                        },
                      )
                    ],
                  ),
                  onPressed: () {
                    this.textEditingController.text = this.recentSearch[i];
                    this.searchText = this.recentSearch[i];
                    this.textEditingController.selection =
                        TextSelection.fromPosition(TextPosition(
                            offset: textEditingController.text.length));
                    FocusManager.instance.primaryFocus.unfocus();

                    this.cubit.setQuery(this.recentSearch[i]);
                    this.cubit.onRefresh();
                    context
                        .bloc<SearchViewCubit>()
                        .getSearchUser(this.recentSearch[i], "20");

                    setState(() {});
                  },
                ),
              ],
            );
          }),
          itemCount:
              this.recentSearch.length > 4 ? 5 : this.recentSearch.length),
    );
  }

  recommendationKeyword() {
    return Container(
      margin: EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 16),
      child: Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.start,
          spacing: 10,
          runSpacing: 10,
          children: this.k),
    );
  }

  likingView() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 16, horizontal: 10),
        height: 157,
        width: 157,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: ((BuildContext c, int i) {
              return CustomContainer(
                onPressed: () {
                  this.navigator.pushRoute(PlaceDetailPage(
                        this.likingContents[i].imageContentNo,
                      ));
                },
                borderRadius: [10, 10, 10, 10],
                backgroundColor: ColorTheme.grayBg,
                width: 157,
                margin: EdgeInsets.symmetric(horizontal: 8),
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10)),
                      width: 157,
                      height: 157,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: CustomCachedNetworkImage(
                          preloadImage: this.likingContents[i].imageSrc.sm,
                          image: this.likingContents[i].imageSrc.sm,
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Color.fromRGBO(31, 31, 36, 0.0),
                            Color.fromRGBO(0, 0, 0, 0.5),
                          ],
                        ),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Flexible(
                                child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 11, vertical: 8),
                              child: Text(
                                this.likingContents[i].title ?? "",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ))
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              );
            }),
            itemCount: this.likingContents.length));
  }

  recommendationUser() {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25, bottom: 35),
      child: ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemBuilder: ((BuildContext c, int i) {
          return Container(
            margin: EdgeInsets.only(top: 16, left: 0, right: 0),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    blurRadius: 10,
                    spreadRadius: 2,
                    offset: Offset(0, 1)),
              ],
              borderRadius: BorderRadius.circular(16), // if you need this
            ),
            child: Column(
              children: [
                CustomContainer(
                  child: Row(
                    children: [
                      this.recommendUsers[i].profileImage == null
                          ? Container(
                              margin: EdgeInsets.only(left: 15, top: 15),
                              child: CustomAvatar(
                                size: 44,
                                image: Image.asset(
                                  'assets/images/userProfileImg.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.only(left: 15, top: 15),
                              child: CustomAvatar(
                                size: 44,
                                image: CustomCachedNetworkImage(
                                  preloadImage:
                                      this.recommendUsers[i].profileImage.sm,
                                  image: this.recommendUsers[i].profileImage.sm,
                                ),
                              ),
                            ),
                      Container(
                        margin: EdgeInsets.only(left: 10, top: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(this.recommendUsers[i].nickname ?? '',
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black87)),
                            Text(
                                "찜 ${this.recommendUsers[i].imageContentCount}개 | 지도 ${this.recommendUsers[i].collectionCount}개",
                                style: TextStyle(
                                    fontSize: 13, color: Colors.grey[400])),
                          ],
                        ),
                      ),
                      Spacer(),
                      this.recommendFollows[i]
                          ? Container(
                              margin: EdgeInsets.only(right: 15, top: 15),
                              child: CustomContainer(
                                backgroundColor: Colors.white,
                                borderRadius: [10, 10, 10, 10],
                                borderWidth: 1,
                                borderColor: Color.fromRGBO(173, 181, 190, 1),
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 16),
                                  child: Text(
                                    "팔로잉",
                                    style: TextStyle(
                                        color: Color.fromRGBO(173, 181, 190, 1),
                                        fontSize: 13),
                                  ),
                                ),
                                onPressed: () {
                                  this.recommendFollows[i] =
                                      !this.recommendFollows[i];
                                  setState(() {});
                                  context
                                      .bloc<SearchViewCubit>()
                                      .putUpdateFollow(
                                          this.recommendUsers[i].userNo);
                                },
                              ))
                          : Container(
                              margin: EdgeInsets.only(right: 15, top: 15),
                              child: CustomContainer(
                                backgroundColor: Color.fromRGBO(31, 31, 36, 1),
                                borderRadius: [10, 10, 10, 10],
                                borderColor: Color.fromRGBO(31, 31, 36, 1),
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 16),
                                  child: Text(
                                    "팔로우",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 13),
                                  ),
                                ),
                                onPressed: () {
                                  this.recommendFollows[i] =
                                      !this.recommendFollows[i];
                                  setState(() {});
                                  context
                                      .bloc<SearchViewCubit>()
                                      .putUpdateFollow(
                                          this.recommendUsers[i].userNo);
                                },
                              ))
                    ],
                  ),
                  onPressed: () {
                    this
                        .navigator
                        .pushRoute(UserPage(this.recommendUsers[i].userNo));
                  },
                ),
                Container(
                  margin: EdgeInsets.all(12),
                  child: Row(
                    children: [
                      Expanded(
                        child: this.recommendUsers[i].imageContent.length > 0
                            ? CustomContainer(
                                onPressed: () {
                                  this.navigator.pushRoute(PlaceDetailPage(
                                        this
                                            .recommendUsers[i]
                                            .imageContent[0]
                                            .imageContentNo,
                                      ));
                                },
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                child: AspectRatio(
                                  aspectRatio: 1,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: CustomCachedNetworkImage(
                                      preloadImage: this
                                          .recommendUsers[i]
                                          .imageContent[0]
                                          .imageSrc
                                          .sm,
                                      image: this
                                          .recommendUsers[i]
                                          .imageContent[0]
                                          .imageSrc
                                          .sm,
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                      ),
                      Expanded(
                        child: this.recommendUsers[i].imageContent.length > 1
                            ? CustomContainer(
                                onPressed: () {
                                  this.navigator.pushRoute(PlaceDetailPage(
                                        this
                                            .recommendUsers[i]
                                            .imageContent[1]
                                            .imageContentNo,
                                      ));
                                },
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                child: AspectRatio(
                                  aspectRatio: 1,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: CustomCachedNetworkImage(
                                      preloadImage: this
                                          .recommendUsers[i]
                                          .imageContent[1]
                                          .imageSrc
                                          .sm,
                                      image: this
                                          .recommendUsers[i]
                                          .imageContent[1]
                                          .imageSrc
                                          .sm,
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                      ),
                      Expanded(
                        child: this.recommendUsers[i].imageContent.length > 2
                            ? CustomContainer(
                                onPressed: () {
                                  this.navigator.pushRoute(PlaceDetailPage(
                                        this
                                            .recommendUsers[i]
                                            .imageContent[2]
                                            .imageContentNo,
                                      ));
                                },
                                margin: EdgeInsets.symmetric(horizontal: 4),
                                child: AspectRatio(
                                  aspectRatio: 1,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: CustomCachedNetworkImage(
                                      preloadImage: this
                                          .recommendUsers[i]
                                          .imageContent[2]
                                          .imageSrc
                                          .sm,
                                      image: this
                                          .recommendUsers[i]
                                          .imageContent[2]
                                          .imageSrc
                                          .sm,
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }),
        itemCount: this.recommendUsers.length,
      ),
    );
  }

  searchUser() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      height: 220,
      color: Color.fromRGBO(244, 246, 248, 1),
      child: this.searchUsers.length == 0
          ? Center(
              child: Container(
                margin: EdgeInsets.only(top: 80),
                child: Column(
                  children: [
                    Text(
                      "검색 결과가 없어요",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "카카오톡에서 친구를 초대해보세요.",
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            )
          : Stack(
              children: [
                Column(
                  children: [
                    Flexible(
                        child: ListView.builder(
                      padding:
                          EdgeInsets.symmetric(horizontal: 5, vertical: 20),
                      scrollDirection: Axis.horizontal,
                      itemBuilder: ((context, i) {
                        Widget imageWidget;
                        if (searchUsers[i].profileImage == null) {
                          imageWidget =
                              Image.asset("assets/images/userProfileImg.png");
                        } else {
                          imageWidget = CustomCachedNetworkImage(
                            preloadImage: this.searchUsers[i].profileImage.sm,
                            image: this.searchUsers[i].profileImage.sm,
                          );
                        }

                        return CustomContainer(
                          margin: EdgeInsets.symmetric(horizontal: 15),
                          child: Column(
                            children: [
                              Container(
                                height: 84,
                                width: 84,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(42),
                                  child: imageWidget,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8),
                                height: 20,
                                child: Text(
                                  this.searchUsers[i].nickname,
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              this.searchUserFollows[i]
                                  ? Container(
                                      margin: EdgeInsets.only(top: 11),
                                      child: CustomContainer(
                                        backgroundColor:
                                            Color.fromRGBO(244, 246, 248, 1),
                                        borderRadius: [10, 10, 10, 10],
                                        borderWidth: 1,
                                        borderColor:
                                            Color.fromRGBO(173, 181, 190, 1),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 8, horizontal: 20),
                                        child: Text(
                                          "팔로잉",
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  173, 181, 190, 1),
                                              fontSize: 13),
                                        ),
                                        onPressed: () {
                                          this.searchUserFollows[i] =
                                              !this.searchUserFollows[i];
                                          setState(() {});
                                          context
                                              .bloc<SearchViewCubit>()
                                              .putUpdateFollow(
                                                  this.searchUsers[i].userNo);
                                        },
                                      ))
                                  : Container(
                                      margin: EdgeInsets.only(top: 11),
                                      child: CustomContainer(
                                        backgroundColor:
                                            Color.fromRGBO(31, 31, 36, 1),
                                        borderRadius: [10, 10, 10, 10],
                                        borderWidth: 1,
                                        borderColor:
                                            Color.fromRGBO(31, 31, 36, 1),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 8, horizontal: 20),
                                        child: Text(
                                          "팔로우",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 13),
                                        ),
                                        onPressed: () {
                                          this.searchUserFollows[i] =
                                              !this.searchUserFollows[i];
                                          setState(() {});
                                          context
                                              .bloc<SearchViewCubit>()
                                              .putUpdateFollow(
                                                  this.searchUsers[i].userNo);
                                        },
                                      ))
                            ],
                          ),
                          onPressed: () {
                            this.navigator.pushRoute(
                                UserPage(this.searchUsers[i].userNo));
                          },
                        );
                      }),
                      itemCount: this.searchUsers.length,
                    )),
                    Container(
                      height: 20,
                      color: Colors.white,
                    )
                  ],
                ),
                Visibility(
                  visible: this.searchUsers.length > 10,
                  child: Positioned(
                    right: 20,
                    bottom: 0,
                    child: CustomContainer(
                      height: 40,
                      backgroundColor: Color.fromRGBO(173, 181, 190, 1),
                      borderRadius: [20, 20, 20, 20],
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 20, right: 5),
                            child: Text(
                              "검색 결과 더보기",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 13),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Image.asset('assets/images/icnsNext.png',
                                color: Colors.white, width: 20, height: 20),
                          )
                        ],
                      ),
                      onPressed: () {
                        this.navigator.pushRoute(
                            FollowPage(this.searchText, FollowType.user));
                      },
                    ),
                  ),
                )
              ],
            ),
    );
  }

  searchPlace() {
    return this.cubit.contents.length == 0
        ? Center(
            child: Container(
              margin: EdgeInsets.only(top: 80),
              child: Column(
                children: [
                  Text(
                    "검색 결과가 없어요",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "카카오톡에서 친구를 초대해보세요.",
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
          )
        : Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            child: StaggeredGridView.countBuilder(
              shrinkWrap: true,
              primary: false,
              crossAxisCount: 4,
              itemCount: this.cubit.contents.length,
              itemBuilder: (BuildContext context, int index) {
                ContentsModel contents = this.cubit.contents[index];
                return ContentsCell(
                  contents: contents,
                  onPressed: () {
                    this.navigator.pushRoute(PlaceDetailPage(
                          contents.imageContentNo,
                        ));
                  },
                  onPressedMore: () {
                    this.showPlaceMoreSheetOther(onReport: (value) {
                      String method = 'post';
                      String path =
                          '/api/v1/report/image_content/${contents.imageContentNo}';
                      Map<String, dynamic> data = {
                        'report_title': '신고하기',
                        'report_description': value
                      };

                      NetworkClient.shared
                          .requestWithAuth(method, path, data)
                          .then((response) {
                        this.showToast('신고되었습니다.');
                      }).catchError((e) {});
                    });
                  },
                );
              },
              staggeredTileBuilder: (int index) {
                return StaggeredTile.count(
                    2,
                    this.cubit.contents[index].imageSrc.height /
                        this.cubit.contents[index].imageSrc.width *
                        2);
              },
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
            ),
          );
  }
}
