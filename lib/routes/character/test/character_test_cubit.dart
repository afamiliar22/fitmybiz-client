import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/questions_model.dart';
import 'package:fitmybiz/routes/character/test/character_test_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class CharacterTestCubit extends BaseCubit<CharacterTestState> {
  /// {@macro counter_cubit}
  CharacterTestCubit() : super(InitialState());

  void init() {
    emit(InitialState());
  }

  void getQuestions() {
    String method = 'get';
    String path = '/api/v1/personality/questions';

    emit(LoadingState());

    this.network.requestWithAuth(method, path, null).then((response) {
      List<QuestionsModel> contents = (response.data as List)
          .map((record) => QuestionsModel.fromJson(record))
          .toList();
      emit(GetQuestions(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void postResult(List questions) {
    String method = 'post';
    String path = '/api/v1/personality/result';

    Map data = {'questions': questions};
    print('$data');

    emit(LoadingState());

    this.network.requestWithAuth(method, path, data).then((response) {
      emit(CompletedState());
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
