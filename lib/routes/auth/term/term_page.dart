import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/circle_check_box.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/term_model.dart';
import 'package:fitmybiz/routes/profile/profile_page.dart';
import 'package:fitmybiz/routes/auth/term/term_cubit.dart';
import 'package:fitmybiz/routes/auth/term/term_state.dart';
import 'package:fitmybiz/routes/views/custom_web_view.dart';
import 'package:fitmybiz/themes/themes.dart';

class TermPage extends StatelessWidget {
  final List<TermModel> terms;

  TermPage(this.terms);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => TermCubit(),
      child: _View(
        this.terms,
      ),
    );
  }
}

class _View extends BaseRoute {
  final List<TermModel> terms;

  _View(this.terms);

  @override
  _ViewState createState() => _ViewState(this.terms);
}

class _ViewState extends BaseRouteState {
  List<TermModel> terms = [];
  List<TermModel> selectedTerms = [];
  _ViewState(this.terms);

  @override
  void initState() {
    super.initState();
    this.terms.forEach((term) {
      if (term.necessary == true) {
        this.requireAgrees[term.termNo] = false;
      } else {
        this.optionAgrees[term.termNo] = false;
      }
    });
  }

  bool isAgreeAll = false;
  Map<int, bool> requireAgrees = {};
  Map<int, bool> optionAgrees = {};

  @override
  Widget build(BuildContext context) {
    List<Widget> termWidgets = [
      CustomContainer(
        padding: EdgeInsets.all(20),
        child: Row(
          children: [
            CircleCheckBox(
              value: this.isAgreeAll,
              color: ColorTheme.primary,
            ),
            SizedBox(width: 10),
            Text('전체 약관 동의',
                style: TextStyleTheme.common.copyWith(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: this.isAgreeAll
                        ? ColorTheme.black
                        : ColorTheme.grayLv2))
          ],
        ),
        onPressed: () {
          this.isAgreeAll = !this.isAgreeAll;
          this.requireAgrees.keys.forEach((key) {
            this.requireAgrees[key] = this.isAgreeAll;
          });
          this.optionAgrees.keys.forEach((key) {
            this.optionAgrees[key] = this.isAgreeAll;
          });
          setState(() {});
        },
      ),
      Divider(height: 1)
    ];

    this.terms.forEach((term) {
      Widget termWidget = CustomContainer(
        padding: EdgeInsets.all(20),
        child: Row(
          children: [
            CircleCheckBox(
              value: term.necessary == true
                  ? this.requireAgrees[term.termNo]
                  : this.optionAgrees[term.termNo],
              color: ColorTheme.primary,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Text(
                '[${term.necessary == true ? "필수" : "선택"}] ${term.termTitle}',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: term.necessary == true
                    ? TextStyleTheme.common.copyWith(
                        fontSize: 16,
                        color: this.requireAgrees[term.termNo]
                            ? ColorTheme.black
                            : ColorTheme.grayLv2)
                    : TextStyleTheme.common.copyWith(
                        fontSize: 16,
                        color: this.optionAgrees[term.termNo]
                            ? ColorTheme.black
                            : ColorTheme.grayLv2),
              ),
            ),
            GestureDetector(
              child: Image.asset('assets/images/icnsNext.png',
                  color: Colors.grey, width: 28, height: 28),
              onTap: () {
                String url = term.urlSrc;
                this.navigator.pushRoute(CustomWebView(url, null, null));
              },
            )
          ],
        ),
        onPressed: () {
          if (term.necessary == true) {
            this.requireAgrees[term.termNo] = !this.requireAgrees[term.termNo];
          } else {
            this.optionAgrees[term.termNo] = !this.optionAgrees[term.termNo];
          }

          List requires = this
              .requireAgrees
              .entries
              .where((element) => element.value == true)
              .toList();
          List options = this
              .optionAgrees
              .entries
              .where((element) => element.value == true)
              .toList();

          if (requires.length == this.requireAgrees.entries.length &&
              options.length == this.optionAgrees.entries.length) {
            this.isAgreeAll = true;
          } else {
            this.isAgreeAll = false;
          }
          setState(() {});
        },
      );
      termWidgets.add(termWidget);
    });

    List requires = this
        .requireAgrees
        .entries
        .where((element) => element.value == true)
        .toList();
    return BlocListener(
      cubit: context.bloc<TermCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case CompleteState:
            this.indicator.hide();
            this.property.isSignIn = true;
            this.navigator.pushRoute(ProfilePage(
                  true,
                ));
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        title: Text('이용약관'),
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          icon: Image.asset('assets/images/icnsBack.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        trailing: true
            ? CustomContainer(
                child: Text(
                  '다음',
                  style: requires.length == this.requireAgrees.entries.length
                      ? TextStyleTheme.txtBtnNextEnable
                      : TextStyleTheme.txtBtnNextDisable,
                ),
                onPressed: () {
                  if (requires.length == this.requireAgrees.entries.length) {
                    List<int> termNo = [];
                    this.requireAgrees.entries.forEach((element) {
                      if (element.value == true) {
                        termNo.add(element.key);
                      }
                    });
                    this.optionAgrees.entries.forEach((element) {
                      if (element.value == true) {
                        termNo.add(element.key);
                      }
                    });
                    context.bloc<TermCubit>().updateUserTerm(termNo);
                  } else {
                    this.showToast('필수 약관에 동의해주세요.');
                  }
                },
              )
            : FlatButton(
                padding: EdgeInsets.only(right: 24),
                child: Text(
                  '다음',
                  style: requires.length == this.requireAgrees.entries.length
                      ? TextStyleTheme.txtBtnNextEnable
                      : TextStyleTheme.txtBtnNextDisable,
                ),
                onPressed: () {
                  if (requires.length == this.requireAgrees.entries.length) {
                    List<int> termNo = [];
                    this.requireAgrees.entries.forEach((element) {
                      if (element.value == true) {
                        termNo.add(element.key);
                      }
                    });
                    this.optionAgrees.entries.forEach((element) {
                      if (element.value == true) {
                        termNo.add(element.key);
                      }
                    });
                    context.bloc<TermCubit>().updateUserTerm(termNo);
                  } else {
                    this.showToast('필수 약관에 동의해주세요.');
                  }
                },
              ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: termWidgets,
                ),
              ),
              /*
              CustomContainer(
                padding: EdgeInsets.all(25),
                child: PrimaryButton(
                  text: Text('다음'),
                  onPressed: () {
                    if (requires.length == this.requireAgrees.entries.length) {
                      List<int> termNo = [];
                      this.requireAgrees.entries.forEach((element) {
                        if (element.value == true) {
                          termNo.add(element.key);
                        }
                      });
                      this.optionAgrees.entries.forEach((element) {
                        if (element.value == true) {
                          termNo.add(element.key);
                        }
                      });
                      context.bloc<TermCubit>().updateUserTerm(termNo);
                    } else {
                      this.showToast('필수 약관에 동의해주세요.');
                    }
                  },
                ),
              ),
              */
            ],
          ),
        ),
      ),
    );
  }
}
