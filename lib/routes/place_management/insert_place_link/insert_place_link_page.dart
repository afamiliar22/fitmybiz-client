import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/models/link_data_model.dart';
import 'package:fitmybiz/routes/place_management/insert_place/insert_place_page.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:validators/validators.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'insert_place_link_cubit.dart';
import 'insert_place_link_state.dart';

class InsertPlaceLinkPage extends StatelessWidget {
  final String link;
  InsertPlaceLinkPage(this.link);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => InsertPlaceLinkCubit(),
      child: _View(this.link),
    );
  }
}

class _View extends BaseRoute {
  final String link;
  _View(this.link);
  @override
  _ViewState createState() => _ViewState(this.link);
}

class _ViewState extends BaseRouteState {
  String link;

  String uri;
  _ViewState(this.link);

  final FocusNode focusNode = FocusNode();
  InsertPlaceLinkCubit cubit;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<InsertPlaceLinkCubit>();
    this.cubit.init();
    if (this.link != null) {
      this.cubit.textEditingController.text = this.link;
      this.cubit.linkChanged(this.link);
    }

    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case SetState:
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case LoadedState:
            this.indicator.hide();
            LinkDataModel linkData = state.linkData;
            this.navigator.replaceRoute(InsertPlacePage(linkData, null));
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        leading: Container(
          margin: EdgeInsets.symmetric(horizontal: 19),
          child: GestureDetector(
            child: Image.asset('assets/images/icnsClose.png',
                color: Colors.black, width: 28, height: 28),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        title: Text('링크로 추가하기'),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomContainer(
              margin: EdgeInsets.symmetric(horizontal: 24),
              onPressed: () => this.cubit.selectLinkImage(),
              child: Text(
                '다음',
                style: TextStyle(
                    fontSize: 16,
                    color: this.cubit.textEditingController.text.isEmpty
                        ? ColorTheme.grayDisable
                        : ColorTheme.primary2),
              ),
            )
          ],
        ),
        body: SafeArea(
          child: Column(
            children: [
              CustomContainer(
                padding: EdgeInsets.symmetric(vertical: 14, horizontal: 25),
                child: Row(
                  children: [
                    Expanded(
                      child: CustomContainer(
                        borderRadius: [8, 8, 8, 8],
                        shadowColor: Colors.grey.withOpacity(0.1),
                        padding:
                            EdgeInsets.symmetric(horizontal: 14, vertical: 6),
                        backgroundColor: Colors.white,
                        child: CupertinoTextField(
                          prefix: CustomContainer(
                            width: 28,
                            height: 28,
                            child: Image.asset(
                              'assets/images/icnsLinedLink.png',
                              color: ColorTheme.grayPlaceholder,
                            ),
                          ),
                          autofocus: true,
                          focusNode: this.focusNode,
                          controller: this.cubit.textEditingController,
                          decoration: BoxDecoration(),
                          style: TextStyleTheme.common.copyWith(fontSize: 15),
                          onChanged: (text) {
                            this.cubit.linkChanged(text);
                            setState(() {});
                          },
                          onSubmitted: (text) {
                            this.focusNode.unfocus();
                            if (isURL(text)) {
                              uri = Uri.encodeFull(text);
                              setState(() {});
                              cubit.linkChanged(text);
                              this.cubit.webViewController.loadUrl(uri);
                            } else {}
                          },
                          placeholder: "링크를 입력하세요",
                        ),
                      ),
                    ),
                    /*
                    SizedBox(width: 16),
                    GestureDetector(
                      child: Image.asset('assets/images/icnsClose.png',
                          color: Colors.black, width: 28, height: 28),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    )
                    */
                  ],
                ),
              ),
              uri == null
                  ? Container()
                  : Container(
                      margin: EdgeInsets.fromLTRB(25, 0, 0, 15),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/icnsDescription.png',
                            width: 28,
                            height: 28,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '장소명을 확인하세요',
                            style: TextStyle(
                                color: ColorTheme.primary, fontSize: 14),
                          ),
                        ],
                      ),
                    ),
              Expanded(
                child: WebView(
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController webViewController) {
                    this.cubit.webViewController = webViewController;
                  },
                ),
              ),
              /*
              CustomContainer(
                backgroundColor: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                child: PrimaryButton(
                  text: Text('다음'),
                  onPressed: () => this.cubit.selectLinkImage(),
                ),
              ),
              */
            ],
          ),
        ),
      ),
    );
  }
}
