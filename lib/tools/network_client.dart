import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:retry/retry.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkProperty {
  Future<SharedPreferences> preferenceInstance =
      SharedPreferences.getInstance();

  Future<String> getString(String key) {
    return preferenceInstance.then((preference) {
      String accessToken = preference.getString(key);
      return Future.value(accessToken);
    });
  }

  Future setString(String key, String value) {
    return preferenceInstance.then((preference) {
      preference.setString(key, value);
      return Future.value();
    });
  }

  Future<String> get accessToken => getString("accessToken");
  set accessToken(value) => setString("accessToken", value);

  Future<String> get refreshToken => getString("refreshToken");
  set refreshToken(value) => setString("refreshToken", value);
}

class NetworkClient {
  NetworkProperty property;
  NetworkClient._initialize() {
    print('NetworkClient Initialize');
    this.property = NetworkProperty();
  }

  static final NetworkClient _instance = NetworkClient._initialize();

  static NetworkClient get shared => _instance;

  final host = 'https://beta-app.zzieut.com';
  // final host = 'http://10.0.0.27:8080';
  final clientId = 'e16052aa-de38-4d4c-a6d0-20ce5a051ffc';
  final clientSecret = 'yJpEtRs8nnXY5mVhMKP6Yst5s3xlvlKxQcAiGn3n';

  Future<dynamic> request(String method, String path, var headers, var data) {
    var url = '$host$path';
    log(method);
    log(url);
    log(data.toString());
    var request;
    switch (method) {
      case 'post':
        var body = json.encode(data ?? Map());
        request = http.post(url, headers: headers, body: body);
        break;
      case 'put':
        var body = json.encode(data ?? Map());
        request = http.put(url, headers: headers, body: body);
        break;
      case 'delete':
        if (data != null) {
          var query = Uri(queryParameters: data).query;
          request = http.delete('$url?$query', headers: headers);
        } else {
          request = http.delete('$url', headers: headers);
        }
        break;
      case 'get':
        if (data != null) {
          var query = Uri(queryParameters: data).query;
          request = http.get('$url?$query', headers: headers);
        } else {
          request = http.get('$url', headers: headers);
        }
        break;
    }
    return retry(() => request,
        retryIf: (e) => e is SocketException || e is TimeoutException);
  }

  Future<NetworkResponse> requestWithoutAuth(
      String method, String path, var data) {
    var headers = {
      'Accept': '*/*',
      'Content-Type': 'application/json',
      'client_id': clientId,
      'client_secret': clientSecret
    };
    return this.request(method, path, headers, data).then((response) {
      log(response.statusCode.toString());
      if (response.statusCode >= 200 && response.statusCode < 300) {
        return Future.value(
            NetworkResponse.success(response.statusCode, response.body));
      } else {
        return Future.error(
            NetworkResponse.failure(response.statusCode, response.body));
      }
    });
  }

  Future<NetworkResponse> requestWithAuth(
      String method, String path, var data) {
    return this.property.accessToken.then((accessToken) {
      log(accessToken);
      var headers = {
        "authorization": 'Bearer $accessToken',
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'client_id': clientId,
        'client_secret': clientSecret,
      };
      return this.request(method, path, headers, data);
    }).then((response) {
      log(response.request.toString());
      log(response.statusCode.toString());
      if (response.statusCode >= 200 && response.statusCode < 300) {
        return Future.value(
            NetworkResponse.success(response.statusCode, response.body));
      } else if (response.statusCode == 401) {
        return this.requestToken().then((response) {
          return this.requestWithAuth(method, path, data);
        });
      } else {
        return Future.error(
            NetworkResponse.failure(response.statusCode, response.body));
      }
    });
  }

  Future<NetworkResponse> requestToken() {
    String path = '/api/v1/oauth/token';
    String method = 'put';
    return this.property.refreshToken.then((refreshToken) {
      var headers = {
        "refresh_token": refreshToken,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'client_id': clientId,
        'client_secret': clientSecret
      };
      return this.request(method, path, headers, null);
    }).then((response) {
      print(response.statusCode);
      print(response.body);
      if (response.statusCode >= 200 && response.statusCode < 300) {
        NetworkResponse networkResponse =
            NetworkResponse.success(response.statusCode, response.body);
        this.property.accessToken = networkResponse.data['access_token'];
        print(networkResponse.data['access_token']);
        this.property.refreshToken = networkResponse.data['refresh_token'];
        print(networkResponse.data['refresh_token']);
        return Future.value(
            NetworkResponse.success(response.statusCode, response.body));
      } else {
        return Future.error(
            NetworkResponse.failure(response.statusCode, response.body));
      }
    });
  }

  Future<NetworkResponse> requestMultipart(
      String method, String path, Map data, Map<String, Uint8List> files) {
    return this.property.accessToken.then((accessToken) {
      print(accessToken);
      var uri = Uri.parse('${host}${path}');
      // var uri = Uri.parse('http://10.0.0.5:8080${path}');

      Map<String, String> headers = {
        "authorization": 'Bearer $accessToken',
      };

      var request = http.MultipartRequest(method, uri);
      headers.entries.forEach((element) {
        request.headers[element.key] = element.value;
      });
      data.entries.forEach((element) {
        request.fields[element.key] = element.value;
      });
      var futures = files.entries.map((element) {
        MultipartFile file = http.MultipartFile.fromBytes(
            element.key, element.value,
            filename: 'photo.jpg');
        request.files.add(file);
        return Future.value();
      });
      return Future.wait(futures).then((_) {
        return request.send();
      });
    }).then((result) {
      return http.Response.fromStream(result);
    }).then((response) {
      print(response.statusCode);
      if (response.statusCode >= 200 && response.statusCode < 300) {
        return Future.value(
            NetworkResponse.success(response.statusCode, response.body));
      } else {
        return Future.error(
            NetworkResponse.failure(response.statusCode, response.body));
      }
    });
  }
}

class NetworkResponse {
  var statusCode;
  var success;
  var data;
  var error;

  NetworkResponse();

  factory NetworkResponse.empty() {
    var response = NetworkResponse();
    response.success = false;
    return response;
  }

  factory NetworkResponse.success(int statusCode, String body) {
    var response = NetworkResponse();
    response.success = true;
    response.statusCode = statusCode;
    response.data = json.decode(body);
    return response;
  }
  factory NetworkResponse.failure(int statusCode, String body) {
    var response = NetworkResponse();
    response.success = false;
    response.statusCode = statusCode;
    response.error = body;
    return response;
  }
}
