import 'dart:io';
import 'dart:typed_data';

import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/components/primary_button.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/link_data_model.dart';
import 'package:fitmybiz/routes/createMap/create_map_page.dart';
import 'package:fitmybiz/routes/place_detail/place_detail_page.dart';
import 'package:fitmybiz/routes/place_management/insert_place_link/insert_place_link_page.dart';
import 'package:fitmybiz/routes/place_management/search_place_name/search_place_name_page.dart';
import 'package:fitmybiz/routes/views/custom_web_view.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/extensions/extensions.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'insert_place_cubit.dart';
import 'insert_place_state.dart';

class InsertPlacePage extends StatelessWidget {
  final LinkDataModel linkData;
  final Uint8List imageBytes;
  InsertPlacePage(this.linkData, this.imageBytes);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => InsertPlaceCubit(),
      child: _View(this.linkData, this.imageBytes),
    );
  }
}

class _View extends BaseRoute {
  final LinkDataModel linkData;
  final Uint8List imageBytes;
  _View(this.linkData, this.imageBytes);

  @override
  _ViewState createState() => _ViewState(this.linkData, this.imageBytes);
}

class _ViewState extends BaseRouteState {
  final LinkDataModel linkData;
  final Uint8List imageBytes;
  _ViewState(this.linkData, this.imageBytes);

  final FocusNode focusNode = FocusNode();

  WebViewController webViewController;
  InsertPlaceCubit cubit;

  bool isValidated = false;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<InsertPlaceCubit>();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);

    this.cubit.init();
    if (this.linkData != null) {
      this.cubit.linkDataChanged(this.linkData);
    } else if (this.imageBytes != null) {
      this.cubit.imageBytesChanged(this.imageBytes);
    } else {
      //not supported
    }
  }

  @override
  Widget build(BuildContext context) {
    Color focusedColor = ColorTheme.black;
    Color unfocusedColor = Color.fromRGBO(205, 210, 216, 1.0);

    Widget avatarImage;

    if (cubit.imageBytes != null) {
      avatarImage = Image.memory(cubit.imageBytes, fit: BoxFit.cover);
    } else if (cubit.image != null) {
      avatarImage = FadeInImage.memoryNetwork(
          placeholder: kTransparentImage,
          image: cubit.image,
          fit: BoxFit.cover);
    } else {
      avatarImage = Container(
        color: Colors.grey.withOpacity(0.5),
        child: Center(
            child: Icon(Icons.image_outlined, color: Colors.white, size: 32)),
      );
    }

    Widget placeNameWidget;

    if (context.bloc<InsertPlaceCubit>().place != null) {
      placeNameWidget = Text(cubit.place.name,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyleTheme.textFieldValue);
    } else {
      placeNameWidget = Text("장소명, 상호명 등을 입력하세요",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyleTheme.placeholder);
    }

    Widget categoriesWidget;
    String categoriesWidgetText = cubit.selectedCategories
        .toList()
        .map((e) => cubit.categories
            .where((element) => element.categoryNo == e)
            .first
            .categoryName)
        .join(', ');

    if (categoriesWidgetText.isNotEmpty) {
      categoriesWidget = Text(categoriesWidgetText,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyleTheme.textFieldValue);
    } else {
      categoriesWidget = Text("카테고리를 선택해주세요 (중복선택 가능)",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyleTheme.placeholder);
    }
    Widget collectionsWidget;
    String collectionsWidgetText = cubit.selectedCollections
        .toList()
        .map((e) => cubit.collections
            .where((element) => element.collectionNo == e)
            .first
            .name)
        .join(', ');

    if (collectionsWidgetText.isNotEmpty) {
      collectionsWidget = Text(collectionsWidgetText,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyleTheme.textFieldValue);
    } else {
      collectionsWidget = Text("지도를 선택해주세요 (중복선택 가능)",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyleTheme.placeholder);
    }

    return BlocListener(
      cubit: context.bloc<InsertPlaceCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case SetState:
            this.isValidated = this.cubit.validateInsertContent(true);
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ToastState:
            this.showToast(state.message);

            break;
          case LoadedState:
            this.indicator.hide();
            setState(() {});
            break;
          case CompletedState:
            this.indicator.hide();
            Navigator.popUntil(context, (route) => route.isFirst);
            this.navigator.pushRoute(PlaceDetailPage(
                  '${state.response.data[0]['image_content_no']}',
                ));
            setState(() {});
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        onPressed: () {
          FocusScope.of(context).unfocus();
        },
        body: SafeArea(
          child: Column(
            children: [
              CustomContainer(
                padding: EdgeInsets.symmetric(vertical: 14, horizontal: 25),
                child: Row(
                  children: [
                    GestureDetector(
                      child: Image.asset('assets/images/icnsClose.png',
                          color: Colors.black, width: 28, height: 28),
                      onTap: () {
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                      },
                    ),
                    Spacer(),
                    Text(
                      '장소 추가',
                      style: TextStyleTheme.subtitle,
                    ),
                    Spacer(),
                    CustomContainer(
                      onPressed: () => this.cubit.requestInsertContent(),
                      backgroundColor: Colors.white,
                      child: Text(
                        '완료',
                        style: TextStyle(
                            color: isValidated
                                ? ColorTheme.primary
                                : ColorTheme.grayDisable),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  children: [
                    Row(
                      children: [
                        CustomContainer(
                          child: CustomAvatar(
                            image: avatarImage,
                            size: 60,
                            radius: 12,
                          ),
                          onPressed: () {
                            this.showImagePicker().then((value) {
                              this.cubit.imageBytesChanged(value);
                            });
                          },
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: Text(
                            //context.bloc<InsertPlaceCubit>().title ?? '',
                            context.bloc<InsertPlaceCubit>().link ?? '',
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyleTheme.common.copyWith(
                                fontSize: 15,
                                color: Color.fromRGBO(31, 31, 36, 1.0),
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        CustomContainer(
                          onPressed: () {
                            if (context.bloc<InsertPlaceCubit>().link != null)
                              this.navigator.pushRoute(CustomWebView(
                                  context.bloc<InsertPlaceCubit>().link,
                                  null,
                                  null));
                          },
                          width: 28,
                          height: 28,
                          child: Image.asset(
                            'assets/images/icnsNext.png',
                            color: ColorTheme.grayLv1,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 26),
                    Text('장소명', style: TextStyleTheme.textFieldLabel),
                    SizedBox(height: 16),
                    CustomContainer(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          placeNameWidget,
                          SizedBox(height: 8),
                          Divider(
                              height: 0.5,
                              thickness: 0.5,
                              color: ColorTheme.grayPlaceholder),
                        ],
                      ),
                      onPressed: () {
                        this
                            .navigator
                            .pushRoute(SearchPlaceNamePage())
                            .then((place) {
                          if (place != null) {
                            this
                                .context
                                .bloc<InsertPlaceCubit>()
                                .placeChanged(place);
                          }
                          setState(() {});
                        });
                      },
                    ),
                    SizedBox(height: 40),
                    Text('카테고리', style: TextStyleTheme.textFieldLabel),
                    SizedBox(height: 8),
                    ExpandablePanel(
                      header: categoriesWidget,
                      headerAlignment: ExpandablePanelHeaderAlignment.center,
                      collapsed: Divider(
                          height: 0.5,
                          thickness: 0.5,
                          color: ColorTheme.grayPlaceholder),
                      expanded: Column(
                        children: [
                          GridView.builder(
                            primary: false,
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3),
                            itemBuilder: ((BuildContext c, int i) {
                              CategoryModel category = cubit.categories[i];

                              Color backgroundColor = cubit.selectedCategories
                                          .contains(category.categoryNo) ==
                                      true
                                  ? HexColor.fromHex(category.categoryColor)
                                  : ColorTheme.grayBg;
                              Color textColor = cubit.selectedCategories
                                          .contains(category.categoryNo) ==
                                      true
                                  ? Colors.white
                                  : Color.fromRGBO(138, 148, 159, 1.0);
                              return CustomContainer(
                                onPressed: () => cubit
                                    .categoriesChanged(category.categoryNo),
                                child: Container(
                                  margin: EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    color: backgroundColor,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: Text(
                                      category.categoryName,
                                      style: TextStyleTheme.common.copyWith(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w700,
                                        color: textColor,
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }),
                            itemCount: cubit.categories.length,
                          ),
                          SizedBox(height: 16),
                          Divider(
                              height: 0.5,
                              thickness: 0.5,
                              color: ColorTheme.grayPlaceholder),
                        ],
                      ),
                    ),
                    SizedBox(height: 40),
                    Text('지도', style: TextStyleTheme.textFieldLabel),
                    SizedBox(height: 16),
                    ExpandablePanel(
                      header: collectionsWidget,
                      headerAlignment: ExpandablePanelHeaderAlignment.center,
                      collapsed: Divider(
                          height: 0.5,
                          thickness: 0.5,
                          color: ColorTheme.grayPlaceholder),
                      expanded: Column(
                        children: [
                          ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemBuilder: ((BuildContext c, int i) {
                              CollectionModel collection = cubit.collections[i];
                              Widget radio = cubit.selectedCollections
                                      .contains(collection.collectionNo)
                                  ? Icon(
                                      Icons.radio_button_checked,
                                      color: ColorTheme.primary2,
                                    )
                                  : Icon(
                                      Icons.radio_button_unchecked,
                                      color: Color.fromRGBO(205, 210, 216, 1),
                                    );

                              Widget imageWidget;
                              if (collection.imageSrc != null) {
                                // imageWidget = FadeInImage.memoryNetwork(placeholder: kTransparentImage, image: collection.imageSrc.s, fit: BoxFit.cover);
                                imageWidget = CustomCachedNetworkImage(
                                  preloadImage: collection.imageSrc.s,
                                  image: collection.imageSrc.s,
                                );
                              } else {
                                imageWidget =
                                    Container(color: ColorTheme.grayBg);
                              }
                              return CustomContainer(
                                onPressed: () => cubit.collectionsChanged(
                                    collection.collectionNo),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(vertical: 12),
                                      width: 60,
                                      height: 60,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: imageWidget,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          vertical: 20, horizontal: 16),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            child: Text(
                                              cubit.collections[i].name,
                                              style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 4,
                                          ),
                                          Container(
                                            child: Text(
                                              "${cubit.collections[i].itemCount}개의 장소",
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  color: Color.fromRGBO(
                                                      138, 148, 159, 1)),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Spacer(),
                                    radio,
                                  ],
                                ),
                              );
                            }),
                            itemCount: cubit.collections.length,
                          ),
                          SizedBox(height: 16),
                          CustomContainer(
                            padding: EdgeInsets.symmetric(vertical: 12),
                            child: Row(
                              children: [
                                Container(
                                  child: Image.asset(
                                      'assets/images/icnsAddMaps.png',
                                      width: 28,
                                      height: 28),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 16),
                                  child: Text("새 지도 만들기",
                                      style: TextStyleTheme.common.copyWith(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700)),
                                ),
                                Spacer(),
                                Container(
                                  child: Image.asset(
                                      'assets/images/icnsNext.png',
                                      color: Colors.grey,
                                      width: 28,
                                      height: 28),
                                ),
                              ],
                            ),
                            onPressed: () {
                              this
                                  .navigator
                                  .pushRoute(CreateMapPage())
                                  .then((value) {
                                if (value == 'refresh') {
                                  context
                                      .bloc<InsertPlaceCubit>()
                                      .getCollectionSaved();
                                }
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 40),
                    Text('메모', style: TextStyleTheme.textFieldLabel),
                    SizedBox(height: 16),
                    CustomContainer(
                      padding: EdgeInsets.all(20),
                      borderRadius: [12, 12, 12, 12],
                      backgroundColor:
                          Color.fromRGBO(244, 246, 248, 1.0).withOpacity(0.5),
                      child: CupertinoTextField(
                        controller: cubit.descriptionEditingController,
                        padding: EdgeInsets.zero,
                        placeholder: '이 장소에 대해 더 알려주세요',
                        decoration: BoxDecoration(),
                        minLines: 3,
                        maxLines: 3,
                        onChanged: (text) => context
                            .bloc<InsertPlaceCubit>()
                            .descriptionChanged(text),
                      ),
                    ),
                    SizedBox(height: 40),
                    Row(
                      children: [
                        Text('공개 범위', style: TextStyleTheme.textFieldLabel),
                        Spacer(),
                        CustomButton(
                          borderRadius: 12,
                          padding:
                              EdgeInsets.symmetric(horizontal: 17, vertical: 9),
                          backgroundColor:
                              context.bloc<InsertPlaceCubit>().secret == false
                                  ? focusedColor
                                  : Colors.transparent,
                          borderColor:
                              context.bloc<InsertPlaceCubit>().secret == false
                                  ? focusedColor
                                  : ColorTheme.grayLv2,
                          text: Text('전체 공개',
                              style: TextStyleTheme.common.copyWith(
                                  fontSize: 13,
                                  color:
                                      context.bloc<InsertPlaceCubit>().secret ==
                                              false
                                          ? Colors.white
                                          : ColorTheme.grayLv2)),
                          onPressed: () => context
                              .bloc<InsertPlaceCubit>()
                              .secretChanged(false),
                        ),
                        SizedBox(width: 8),
                        CustomButton(
                          borderRadius: 10,
                          padding:
                              EdgeInsets.symmetric(horizontal: 17, vertical: 9),
                          backgroundColor:
                              context.bloc<InsertPlaceCubit>().secret == true
                                  ? focusedColor
                                  : Colors.transparent,
                          borderColor:
                              context.bloc<InsertPlaceCubit>().secret == true
                                  ? focusedColor
                                  : ColorTheme.grayLv2,
                          text: Text('나만 보기',
                              style: TextStyleTheme.common.copyWith(
                                  fontSize: 13,
                                  color:
                                      context.bloc<InsertPlaceCubit>().secret ==
                                              true
                                          ? Colors.white
                                          : ColorTheme.grayLv2)),
                          onPressed: () => context
                              .bloc<InsertPlaceCubit>()
                              .secretChanged(true),
                        ),
                      ],
                    ),
                    /*
                    SizedBox(height: 40),
                    CustomContainer(
                      backgroundColor: Colors.white,
                      child: PrimaryButton(
                        text: Text('완료'),
                        onPressed: () => this.cubit.requestInsertContent(),
                      ),
                    ),
                    SizedBox(height: 16),
                    */
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
