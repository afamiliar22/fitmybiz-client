import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/collection_item_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'my_place_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class MyPlaceCubit extends BaseCubit<MyPlaceState> {
  /// {@macro counter_cubit}
  MyPlaceCubit() : super(InitialState());

  String collectionNo;
  List<CollectionItemModel> collectionItems = [];
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  bool isWrite = false;
  int page = 1;
  void init() {}

  void collectionNoChanged(String collectionNo) {
    this.collectionNo = collectionNo;
  }

  void isWriteChanged(bool isWrite) {
    this.isWrite = isWrite;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
    this.onRefresh();
  }

  onRefresh() {
    this.page = 1;
    this.selectCollectionItems().then((collectionItems) {
      this.collectionItems = collectionItems;
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    }).catchError((e) {
      this.collectionItems = [];
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    });
  }

  onLoading() {
    this.page = this.page + 1;
    this.selectCollectionItems().then((collectionItems) {
      this.collectionItems.addAll(collectionItems);
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      if (collectionItems.length == 0) {
        this.refreshController.loadNoData();
      } else {
        this.refreshController.loadComplete();
      }
    }).catchError((e) {
      this.refreshController.loadComplete();
    });
  }

  Future<List<CollectionItemModel>> selectCollectionItems() {
    String method = 'get';
    String path = '/api/v1/collection/item/list';
    Map<String, String> data = {
      "collection_no": this.collectionNo.toString(),
      "collection_item_take": '20',
      "page": this.page.toString(),
    };
    if (this.isWrite) {
      data['is_write'] = '1';
    }

    // emit(LoadingState());
    return this.network.requestWithAuth(method, path, data).then((response) {
      List<CollectionItemModel> results = (response.data['data'] as List)
          .map((record) => CollectionItemModel.fromJson(record))
          .toList();
      return Future.value(results);
    });
  }

  void deleteCollectionItem(String collectionNo, String collectionItemNo) {
    String method = 'delete';
    String path = '/api/v1/collection/$collectionNo/item/delete';
    Map<String, dynamic> data = {"collection_item_no": collectionItemNo};
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.property.meCollectionSubject.add(true);
      emit(ToastState('장소가 삭제되었습니다.'));
      this.onRefresh();
    }).then((value) {
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void insertImageContentReport(String imageContentNo, String description) {
    String method = 'post';
    String path = '/api/v1/report/image_content/$imageContentNo';
    Map<String, dynamic> data = {
      'report_title': '신고하기',
      'report_description': description
    };

    this.network.requestWithAuth(method, path, data).then((response) {
      emit(ToastState('신고되었습니다.'));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
