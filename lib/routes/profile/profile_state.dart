import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/area_code_model.dart';
import 'package:fitmybiz/models/area_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/user_model.dart';

abstract class ProfileState extends Equatable {}

class InitialState extends ProfileState {
  @override
  List<Object> get props => [];
}

class LoadingState extends ProfileState {
  @override
  List<Object> get props => [];
}

class CompleteState extends ProfileState {
  @override
  List<Object> get props => [];
}

class LoadedUserState extends ProfileState {
  LoadedUserState(this.user);

  final UserModel user;

  @override
  List<Object> get props => [user];
}

class LoadedAreaState extends ProfileState {
  LoadedAreaState(this.areaCode);

  final AreaCodeModel areaCode;

  @override
  List<Object> get props => [areaCode];
}

class LoadedUserAreaState extends ProfileState {
  LoadedUserAreaState(this.areas);

  final List<Map<String, dynamic>> areas;

  @override
  List<Object> get props => [areas];
}

class ToastState extends ProfileState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends ProfileState {
  @override
  List<Object> get props => [];
}

class GetCategory extends ProfileState {
  GetCategory(this.result, this.categories);

  final bool result;
  final List<CategoryModel> categories;

  @override
  List<Object> get props => [result, categories];
}
