import 'package:bloc/bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/contents_model.dart';

import 'package:fitmybiz/routes/search/search_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class SearchCubit extends BaseCubit<SearchState> {
  /// {@macro counter_cubit}
  SearchCubit() : super(InitialState());

  void init() {
    emit(InitialState());
  }

/*
  void getImageContents(double lat, double lng) {
    String method = 'get';
    String path = '/api/v1/content/image_content/list?recommend=1';

    emit(LoadingState());
    Map<String, dynamic> data = {'lat': lat.toString(), 'lng': lng.toString()};

    this
        .network
        .requestWithAuth(method, path, lat != null ? data : null)
        .then((response) {
      List<ContentsModel> contents = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();
      emit(GetContents(true, contents));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
  */

  void getBanner() {
    String method = 'get';
    String path = '/api/v1/banner/list?now_processed=1';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<BannerModel> banners = (response.data as List)
          .map((record) => BannerModel.fromJson(record))
          .toList();

      emit(GetBanner(true, banners));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void refreshRadius(double radius) {
    emit(RefreshRadius(radius));
  }
}
