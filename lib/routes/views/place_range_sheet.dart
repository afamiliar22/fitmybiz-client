import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PlaceRangeSheet extends StatelessWidget {
  //final VoidCallback onPressedRange;
  final ValueChanged<double> onPressed;

  PlaceRangeSheet({this.onPressed});

  @override
  Widget build(BuildContext context) {
    var ranges = [0.0, 1.0, 3.0, 5.0, 10.0];
    return FutureBuilder(
        future: SharedPreferences.getInstance(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            SharedPreferences prefs = snapshot.data;
            double radius = prefs.getDouble('radius') ?? 0.0;

            return CustomContainer(
              borderRadius: [30, 30, 0, 0],
              backgroundColor: Colors.white,
              child: SafeArea(
                top: false,
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: ranges
                        .map((range) => CustomContainer(
                              margin: EdgeInsets.only(
                                  top: ranges.indexOf(range) == 0 ? 20 : 0),
                              height: 60,
                              padding: EdgeInsets.only(
                                  top: 16, bottom: 16, right: 25, left: 22),
                              child: Row(
                                children: [
                                  SizedBox(width: 8),
                                  Text(
                                      ranges.indexOf(range) == 0
                                          ? '내 주변 찾기'
                                          : '내 주변 ${range.toInt()} km 이내',
                                      style: TextStyleTheme.common.copyWith(
                                          color: radius == range
                                              ? ColorTheme.red
                                              : ColorTheme.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600)),
                                  Spacer(),
                                  Icon(
                                    Icons.check,
                                    size: 24,
                                    color: radius == range
                                        ? ColorTheme.red
                                        : Colors.transparent,
                                  )
                                ],
                              ),
                              onPressed: () {
                                prefs.setDouble('radius', range);
                                this.onPressed(range);
                              },
                            ))
                        .toList()),
              ),
            );
          } else {
            return Center();
          }
        });
  }
}
