import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';

import 'package:fitmybiz/themes/themes.dart';

class ReportView extends BaseRoute {
  @override
  ReportViewState createState() => ReportViewState();
}

class ReportViewState extends BaseRouteState {
  TextEditingController controller;
  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
  }

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Text(
                      "신고하기",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Spacer(),
                  CustomButton(
                    image: Image.asset('assets/images/icnsClose.png',
                        color: Colors.black, width: 28, height: 28),
                    onPressed: () {
                      this.navigator.popRoute(null);
                    },
                  ),
                  SizedBox(width: 20),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
              child: Text(
                "신고사유",
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(138, 148, 159, 1)),
              ),
            ),
            Expanded(
              child: CustomContainer(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                borderRadius: [12, 12, 12, 12],
                backgroundColor: ColorTheme.grayBg,
                padding: EdgeInsets.all(8),
                child: CupertinoTextField(
                  decoration: BoxDecoration(),
                  textInputAction: TextInputAction.newline,
                  controller: this.controller,
                  maxLines: null,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: CustomButton(
                text: Text('완료'),
                padding: EdgeInsets.all(16),
                borderRadius: 18,
                backgroundColor: ColorTheme.primary,
                shadowColor: ColorTheme.primary.withOpacity(0.1),
                onPressed: () {
                  this.navigator.popRoute(this.controller.text);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
