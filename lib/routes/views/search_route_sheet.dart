import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/launch_client.dart';

class SearchRouteSheet extends StatelessWidget {
  final Function(LaunchMapType) onPressed;

  SearchRouteSheet({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return CustomContainer(
      borderRadius: [30, 30, 0, 0],
      backgroundColor: Colors.white,
      child: SafeArea(
        top: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            /*
            CustomContainer(
              padding: EdgeInsets.all(25),
              height: 72,
              child: Row(
                children: [
                  Text('길찾기', style: TextStyleTheme.subtitle),
                  Spacer(),
                  GestureDetector(
                    child: Image.asset('assets/images/icnsClose.png', color: Colors.black, width: 28, height: 28),
                    onTap: () => Navigator.of(context).pop(),
                  )
                ],
              ),
            ),
            */
            CustomContainer(
              margin: EdgeInsets.only(top: 20),
              height: 60,
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, right: 25, left: 22),
              child: Row(
                children: [
                  Text('카카오맵으로 길찾기',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 16, fontWeight: FontWeight.w600)),
                  Spacer(),
                  Image.asset('assets/images/icnsNext.png',
                      color: Colors.grey, width: 28, height: 28)
                ],
              ),
              onPressed: () => this.onPressed(LaunchMapType.KAKAOMAP),
            ),
            CustomContainer(
              height: 60,
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, right: 25, left: 22),
              child: Row(
                children: [
                  Text('티맵으로 길찾기',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 16, fontWeight: FontWeight.w600)),
                  Spacer(),
                  Image.asset('assets/images/icnsNext.png',
                      color: Colors.grey, width: 28, height: 28)
                ],
              ),
              onPressed: () => this.onPressed(LaunchMapType.TMAP),
            ),
            CustomContainer(
              height: 60,
              padding:
                  EdgeInsets.only(top: 16, bottom: 16, right: 25, left: 22),
              child: Row(
                children: [
                  Text('네이버 지도로 길찾기',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 16, fontWeight: FontWeight.w600)),
                  Spacer(),
                  Image.asset('assets/images/icnsNext.png',
                      color: Colors.grey, width: 28, height: 28)
                ],
              ),
              onPressed: () => this.onPressed(LaunchMapType.NAVERMAP),
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }
}
