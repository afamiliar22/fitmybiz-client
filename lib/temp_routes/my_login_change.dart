import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/temp_routes/panel_info_account.dart';
import 'package:fitmybiz/temp_routes/point.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyLoginChangePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  bool isEntered = false;

  @override
  void initState() {
    super.initState();
  }

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return PlatformScaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Column(
                  children: [
                    CustomContainer(
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      backgroundColor: ColorTheme.primary2,
                      width: width,
                      height: (width / 359) * 134,
                      borderRadius: [0, 0, 15, 15],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.arrow_back),
                                onPressed: () {
                                  this.navigator.popRoute(null);
                                },
                              ),
                              Spacer(),
                              Text('내 정보'),
                              Spacer(),
                            ],
                          ),
                          Row(
                            children: [
                              CustomContainer(
                                borderRadius: [6, 6, 6, 6],
                                child: Image.asset(
                                  'assets/images/icnsMypage.png',
                                  width: 26,
                                  height: 26,
                                ),
                              ),
                              SizedBox(
                                width: 1,
                              ),
                              CustomContainer(
                                borderRadius: [6, 6, 6, 6],
                                child: Text('김수윤님 반갑습니다',
                                    style: TextStyle(
                                      color: ColorTheme.white,
                                    )),
                              ),
                              Spacer(),
                            ],
                          ),
                          CustomContainer(
                            borderRadius: [6, 6, 6, 6],
                            child: Text('sdfewe@gmail.com',
                                style: TextStyle(
                                  color: ColorTheme.white,
                                )),
                          ),
                          CustomContainer(
                            borderRadius: [6, 6, 6, 6],
                            child: Text('카카오로 가입',
                                style: TextStyle(
                                  color: ColorTheme.white,
                                )),
                          ),
                          SizedBox(
                            height: 13,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 13,
                    ),
                  ],
                ),
              ],
            ),
            CustomContainer(
              child: Column(
                children: [
                  CustomContainer(
                    child: Row(
                      children: [
                        Text(
                          '비밀번호 변경',
                          style: TextStyle(
                              color: ColorTheme.grayLv1, fontSize: 14),
                        ),
                        Spacer(),
                        Image.asset(
                          'assets/images/icnsNext.png',
                          width: 21,
                          height: 26,
                          color: ColorTheme.primary2,
                        ),
                      ],
                    ),
                  )
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            ),
            CustomContainer(
              backgroundColor: ColorTheme.grayBg,
              height: 21,
            ),
            isEntered
                ? CustomContainer(
                    padding: EdgeInsets.symmetric(horizontal: 22),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 13,
                        ),
                        Row(
                          children: [
                            Text('패널 정보'),
                            Spacer(),
                            CustomContainer(
                              onPressed: () {},
                              child: Text(
                                '패널정보 변경하기',
                                style: TextStyle(
                                    fontSize: 12, color: ColorTheme.primary),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 35,
                        ),
                        Text(
                          '본인확인',
                          style: TextStyle(fontSize: 12),
                        ),
                        Text('본인 확인 완료'),
                        SizedBox(
                          height: 21,
                        ),
                        Text(
                          '핸드폰 번호',
                          style: TextStyle(fontSize: 12),
                        ),
                        Text('01022212432'),
                        SizedBox(
                          height: 21,
                        ),
                        Text(
                          '주소',
                          style: TextStyle(fontSize: 12),
                        ),
                        Text('서울 마포구 어울마당로'),
                      ],
                    ),
                  )
                : CustomContainer(
                    padding: EdgeInsets.symmetric(horizontal: 22),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 13,
                        ),
                        Row(
                          children: [
                            Text('패널 정보'),
                            Spacer(),
                            CustomContainer(
                              onPressed: () {
                                this
                                    .navigator
                                    .pushRoute(PanelInfoAccountPage());
                              },
                              child: Text(
                                '패널정보 변경하기',
                                style: TextStyle(
                                    fontSize: 12, color: ColorTheme.primary),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 35,
                        ),
                        Center(
                          child: Text(
                            '패널 정보가 입력되지 않았어요',
                            style: TextStyle(fontSize: 14),
                          ),
                        ),
                        SizedBox(
                          height: 38,
                        ),
                      ],
                    ),
                  ),
            SizedBox(
              height: 24,
            ),
            CustomContainer(
              backgroundColor: ColorTheme.grayBg,
              height: 21,
            ),
            CustomContainer(
              padding: EdgeInsets.symmetric(vertical: 19),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomContainer(
                    onPressed: () {
                      this.property.isSignIn = false;
                      this.navigator.resetRoute(SplashPage());
                    },
                    padding: EdgeInsets.symmetric(vertical: 19),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/icnsLogout.png',
                          width: 36,
                          height: 36,
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text('로그아웃'),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 37,
                  ),
                  CustomContainer(
                    padding: EdgeInsets.symmetric(vertical: 19),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/icnsWithdrawal.png',
                          width: 36,
                          height: 36,
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text('탈퇴하기'),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
