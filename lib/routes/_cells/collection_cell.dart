import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:fitmybiz/extensions/extensions.dart';

class CollectionCell extends StatelessWidget {
  final CollectionModel collection;
  final VoidCallback onPressed;
  final VoidCallback onPressedMore;
  final bool isEditMode;
  final bool isSelect;
  final bool isMine;
  CollectionCell({
    this.collection,
    this.isEditMode,
    this.isSelect,
    this.onPressed,
    this.onPressedMore,
    @required this.isMine,
  });

  @override
  Widget build(BuildContext context) {
    Widget squareWidget;
    if (this.isEditMode == true) {
      Widget radio = Icon(
        Icons.radio_button_unchecked,
        color: Color.fromRGBO(205, 210, 216, 1),
      );

      Color coverColor = Colors.transparent;
      if (this.isSelect == true) {
        radio = Icon(
          Icons.radio_button_checked,
          color: ColorTheme.primary2,
        );
        coverColor = ColorTheme.primary.withOpacity(0.3);
      }
      squareWidget = Stack(
        children: [
          Positioned.fill(child: this.buildCollectionImages(collection)),
          Positioned(top: 12, left: 12, child: radio),
          CustomContainer(
            backgroundColor: coverColor,
            borderRadius: [20, 20, 20, 20],
          )
        ],
      );
    } else {
      squareWidget = this.buildCollectionImages(collection);
    }

    return CustomContainer(
      onPressed: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AspectRatio(
            aspectRatio: 1,
            child: squareWidget,
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: Text(collection.name,
                    maxLines: 1, overflow: TextOverflow.ellipsis),
              ),
              if (isMine)
                if (this.isEditMode != true) this.buildMore(),
            ],
          ),
          SizedBox(height: 2),
          Row(
            children: [
              Text(
                '${collection.itemCount}개의 장소',
                style: TextStyleTheme.common
                    .copyWith(fontSize: 13, color: ColorTheme.grayLv1),
              ),
              Spacer(),
              Text(
                collection.updatedAt.toBeforeString(),
                style: TextStyleTheme.common
                    .copyWith(fontSize: 13, color: ColorTheme.grayLv1),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildMore() {
    return CustomButton(
      image: Image.asset(
        'assets/images/iconMore.png',
        color: ColorTheme.black,
        height: 20,
        width: 20,
      ),
      onPressed: this.onPressedMore,
    );
  }

  Widget buildCollectionImages(CollectionModel collection) {
    if (collection.collectionItems.length >= 4) {
      return GridView.count(
        primary: false,
        childAspectRatio: 1,
        crossAxisCount: 2,
        crossAxisSpacing: 5,
        mainAxisSpacing: 5,
        children: [
          AspectRatio(
            aspectRatio: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: CustomCachedNetworkImage(
                preloadImage: collection.collectionItems[0].sm,
                image: collection.collectionItems[0].sm,
              ),
            ),
          ),
          AspectRatio(
            aspectRatio: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: CustomCachedNetworkImage(
                preloadImage: collection.collectionItems[1].sm,
                image: collection.collectionItems[1].sm,
              ),
            ),
          ),
          AspectRatio(
            aspectRatio: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: CustomCachedNetworkImage(
                preloadImage: collection.collectionItems[2].sm,
                image: collection.collectionItems[2].sm,
              ),
            ),
          ),
          AspectRatio(
            aspectRatio: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: CustomCachedNetworkImage(
                preloadImage: collection.collectionItems[3].sm,
                image: collection.collectionItems[3].sm,
              ),
            ),
          ),
        ],
      );
    } else if (collection.collectionItems.length >= 1) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: CustomCachedNetworkImage(
          preloadImage: collection.collectionItems[0].sm,
          image: collection.collectionItems[0].sm,
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(color: ColorTheme.grayBg),
      );
    }
  }
}
