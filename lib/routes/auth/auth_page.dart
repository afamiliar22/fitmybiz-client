import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/routes/auth/auth_cubit.dart';
import 'package:fitmybiz/routes/auth/auth_page.dart';
import 'package:fitmybiz/routes/auth/auth_state.dart';
import 'package:fitmybiz/routes/auth/sign_email/sign_email_page.dart';
import 'package:fitmybiz/routes/splash/splash_cubit.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/tabs/tabs_page.dart';
import 'package:fitmybiz/routes/auth/term/term_page.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'login_email/login_email_page.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AuthCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<AuthCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case SignedState:
            this.indicator.hide();
            context.bloc<AuthCubit>().checkUserTerm();
            // this.navigator.resetRoute(TabsPage());
            break;
          case CheckedTermState:
            this.indicator.hide();
            if (state.result == true) {
              this.property.isSignIn = true;
              this.navigator.resetRoute(SplashPage());
            } else {
              this.navigator.pushRoute(TermPage(state.terms));
            }

            break;
        }
      },
      child: PlatformScaffold(
          onWillPop: this.onWillPop,
          body: SafeArea(
            top: true,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: (MediaQuery.of(context).size.height / 2) - 300,
                  ),
                  Text.rich(
                    TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: "SNS 인플루언서들의\n",
                          style: TextStyle(fontSize: 28),
                        ),
                        TextSpan(
                            text: "핫플 집합소,", style: TextStyle(fontSize: 28)),
                        TextSpan(
                            text: "찜!",
                            style: TextStyle(
                                fontSize: 28,
                                color: ColorTheme.primary2,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  SizedBox(height: 90),
                  Text('⚡️ 빠르게 회원가입하고 찜하기',
                      style: TextStyleTheme.common.copyWith(fontSize: 15)),
                  SizedBox(height: 20),
                  CustomContainer(
                    backgroundColor: Color.fromRGBO(254, 207, 54, 1.0),
                    width: MediaQuery.of(context).size.width - 40,
                    borderRadius: [10, 10, 10, 10],
                    padding: EdgeInsets.all(13),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/images/icnsKakao.png',
                          width: 24,
                          height: 24,
                        ),
                        Spacer(),
                        Container(
                          child: Text("카카오톡으로 로그인",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w400)),
                        ),
                        Spacer()
                      ],
                    ),
                    onPressed: () {
                      context.bloc<AuthCubit>().signInKakao();
                    },
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  if (Platform.isIOS)
                    CustomContainer(
                      backgroundColor: Colors.black,
                      width: MediaQuery.of(context).size.width - 40,
                      borderRadius: [10, 10, 10, 10],
                      padding: EdgeInsets.all(13),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/icnsApple.png',
                            width: 24,
                            height: 24,
                          ),
                          Spacer(),
                          Container(
                            child: Text("Apple로 로그인",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white)),
                          ),
                          Spacer()
                        ],
                      ),
                      onPressed: () {
                        context.bloc<AuthCubit>().signInApple();
                      },
                    ),
                  if (Platform.isIOS)
                    SizedBox(
                      height: 12,
                    ),
                  CustomContainer(
                    backgroundColor: ColorTheme.primary2,
                    width: MediaQuery.of(context).size.width - 40,
                    borderRadius: [10, 10, 10, 10],
                    padding: EdgeInsets.all(13),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/images/icnsEmail.png',
                          width: 24,
                          height: 24,
                        ),
                        Spacer(),
                        Container(
                          child: Text("이메일로 회원가입",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white)),
                        ),
                        Spacer()
                      ],
                    ),
                    onPressed: () {
                      this.navigator.pushRoute(SignEmailPage());
                    },
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () => this.navigator.pushRoute(LoginEmailPage()),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom:
                                      BorderSide(color: ColorTheme.primary2))),
                          child: Text('이미 찜 계정이 있으신가요?',
                              textAlign: TextAlign.end,
                              style: TextStyleTheme.common.copyWith(
                                  color: ColorTheme.primary, fontSize: 13)),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                ],
              ),
            ),
          )),
    );
  }
}
