import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/questions_model.dart';
import 'package:fitmybiz/models/result_model.dart';

abstract class CharacterResultState extends Equatable {}

class InitialState extends CharacterResultState {
  @override
  List<Object> get props => [];
}

class LoadingState extends CharacterResultState {
  @override
  List<Object> get props => [];
}

class ErrorState extends CharacterResultState {
  @override
  List<Object> get props => [];
}

class GetResult extends CharacterResultState {
  GetResult(this.result, this.contents);

  final bool result;
  final ResultModel contents;

  @override
  List<Object> get props => [result, contents];
}
