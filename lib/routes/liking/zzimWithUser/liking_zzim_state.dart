import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/keyword_model.dart';

abstract class LikingZzimState extends Equatable {}

class InitialState extends LikingZzimState {
  @override
  List<Object> get props => [];
}

class LoadingState extends LikingZzimState {
  @override
  List<Object> get props => [];
}

class ErrorState extends LikingZzimState {
  @override
  List<Object> get props => [];
}

class ToastState extends LikingZzimState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class SetState extends LikingZzimState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}
