import 'package:equatable/equatable.dart';

abstract class NoticeDetailState extends Equatable {}

class InitialState extends NoticeDetailState {
  @override
  List<Object> get props => [];
}
