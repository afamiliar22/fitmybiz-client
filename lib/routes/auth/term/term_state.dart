import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/login_model.dart';

abstract class TermState extends Equatable {}

class InitialState extends TermState {
  @override
  List<Object> get props => [];
}

class LoadingState extends TermState {
  @override
  List<Object> get props => [];
}

class CompleteState extends TermState {
  @override
  List<Object> get props => [];
}

class ErrorState extends TermState {
  @override
  List<Object> get props => [];
}
