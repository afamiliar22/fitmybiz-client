import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/taste_temp/taste_state.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class TasteCubit extends BaseCubit<TasteState> {
  /// {@macro counter_cubit}
  TasteCubit() : super(InitialState());
  UserModel user;

  void init() {
    emit(InitialState());
  }

  void getBanner() {
    String method = 'get';
    String path = '/api/v1/banner/list?now_processed=1';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<BannerModel> banners = (response.data as List)
          .map((record) => BannerModel.fromJson(record))
          .toList();

      emit(GetBanner(true, banners));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void refreshRadius(double radius) {
    emit(RefreshRadius(radius));
  }

  void getCategories(int categoryNo, String categoryName, int limit, int page) {
    String method = 'get';
    String path = '/api/v1/content/category/list';
    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      List<CategoryModel> category = (response.data['data'] as List)
          .map((record) => CategoryModel.fromJson(record))
          .toList();

      emit(GetCategory(true, category));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void postCollectionSet(String imgContentNo, String collectionNo) {
    String method = 'post';
    String path = '/api/v1/collection/set';

    Map data = {
      "image_content_no": imgContentNo,
      "collection_no": collectionNo
    };
    if (collectionNo == null) {
      data = {"image_content_no": imgContentNo.toString()};
    }
    emit(LoadingState());
    this.network.requestWithAuth(method, path, data).then((response) {
      this.facebook.addEvent(FacebookEventType.wishImageContent);

      this.property.meCollectionSubject.add(true);

      emit(PostCollectionSet(true));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  Future selectUser() {
    String method = 'get';
    String path = '/api/v1/user/me';

    return this.network.requestWithAuth(method, path, null).then((response) {
      this.user = UserModel.fromJson(response.data);
      emit(SelectUser(user));
      return Future.value();
    });
  }

  Future getCollection(String userNo) {
    String method = 'get';
    String path = '/api/v1/collection/$userNo';

    return this.network.requestWithAuth(method, path, null).then((response) {
      List<CollectionModel> collections = (response.data['data'] as List)
          .map((record) => CollectionModel.fromJson(record))
          .toList();

      emit(GetCollection(true, collections));
      return Future.value();
    });
  }
}
