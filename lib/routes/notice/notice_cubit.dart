import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/notice_model.dart';

import 'notice_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class NoticeCubit extends BaseCubit<NoticeState> {
  /// {@macro counter_cubit}
  NoticeCubit() : super(InitialState());
  List<NoticeModel> notices = [];

  void init() {
    String method = 'get';
    String path = '/api/v1/notice?take=20&page=1';

    emit(LoadingState());
    this.network.requestWithAuth(method, path, null).then((response) {
      this.notices = (response.data['data'] as List)
          .map((record) => NoticeModel.fromJson(record))
          .toList();
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
