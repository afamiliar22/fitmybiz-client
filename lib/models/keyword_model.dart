import 'dart:developer';

class KeywordModel {
  final String recommendKeywordNo;
  final String keyword;
  final String searchCount;
  final String isAdmin;
  final String state;

  KeywordModel({this.recommendKeywordNo, this.keyword, this.searchCount, this.isAdmin, this.state});

  factory KeywordModel.fromJson(Map<String, dynamic> json) {
    return KeywordModel(
      recommendKeywordNo: json['recommend_keyword_no'].toString(),
      keyword: json['keyword'].toString(),
      searchCount: json['search_count'].toString(),
      isAdmin: json['is_admin'].toString(),
      state: json['state'].toString(),
    );
  }
}
