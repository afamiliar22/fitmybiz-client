import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;
  final double width;
  final double height;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final double borderWidth;
  final List<double> borderRadius;
  final Color borderColor;
  final Color backgroundColor;
  final Color shadowColor;

  CustomContainer(
      {this.onPressed,
      this.child,
      this.width,
      this.height,
      this.margin,
      this.padding,
      this.borderWidth,
      this.borderRadius,
      this.borderColor,
      this.backgroundColor,
      this.shadowColor});

  @override
  Widget build(BuildContext context) {
    BorderRadiusGeometry borderRadiusGeometry;
    if (borderRadius != null && borderRadius.length == 4) {
      borderRadiusGeometry = BorderRadius.only(
          topLeft: Radius.circular(borderRadius[0]),
          topRight: Radius.circular(borderRadius[1]),
          bottomLeft: Radius.circular(borderRadius[2]),
          bottomRight: Radius.circular(borderRadius[3]));
    }

    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: this.width,
        height: this.height,
        child: Container(
          margin: this.padding,
          child: child,
        ),
        margin: margin,
        decoration: BoxDecoration(
          color: this.backgroundColor,
          border: Border.all(width: this.borderWidth ?? 0, color: this.borderColor ?? Colors.transparent),
          borderRadius: borderRadiusGeometry,
          boxShadow: [
            if (this.shadowColor != null)
              BoxShadow(
                color: this.shadowColor,
                spreadRadius: 4,
                blurRadius: 8,
                offset: Offset(0, 0), // changes position of shadow
              ),
          ],
        ),
      ),
    );
  }
}
