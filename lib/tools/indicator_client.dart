import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class IndicatorClient {
  IndicatorClient._initialize() {
    print('IndicatorClient Initialize');
    EasyLoading.instance.userInteractions = false;
    EasyLoading.instance.maskType = EasyLoadingMaskType.black;
    EasyLoading.instance.loadingStyle = EasyLoadingStyle.custom;

    EasyLoading.instance.backgroundColor = Colors.transparent;
    EasyLoading.instance.indicatorColor = Colors.transparent;
    EasyLoading.instance.textColor = Colors.transparent;
    EasyLoading.instance.progressColor = Colors.transparent;
  }

  static final IndicatorClient _instance = IndicatorClient._initialize();

  static IndicatorClient get shared => _instance;

  show() {
    EasyLoading.show(indicator: Lottie.asset('assets/lotties/loading.json'));
  }

  hide() {
    EasyLoading.dismiss();
  }
}
