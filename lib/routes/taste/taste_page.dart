import 'dart:io';

import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/routes/me/me_cubit.dart';
import 'package:fitmybiz/routes/splash/splash_page.dart';
import 'package:fitmybiz/routes/taste/contentsView/taste_content_view.dart';
import 'package:fitmybiz/routes/taste/taste_cubit.dart';
import 'package:fitmybiz/routes/taste/taste_state.dart';

import 'package:fitmybiz/themes/themes.dart';
import 'package:page_view_indicator/page_view_indicator.dart';

class TastePage extends StatelessWidget {
  final List<CategoryModel> categories;
  // const TabsPage({Key key}) : super(key: key);
  TastePage(this.categories);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => TasteCubit(),
        ),
        BlocProvider(
          create: (BuildContext context) => MeCubit(),
        ),
      ],
      child: _View(this.categories),
    );
  }
}

class _View extends BaseRoute {
  final List<CategoryModel> categories;
  _View(this.categories);

  @override
  _ViewState createState() => _ViewState(this.categories);
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  List<CategoryModel> categories;

  var zzimItems = <ContentsModel>[];
  _ViewState(this.categories);

  final TextEditingController textEditingController = TextEditingController();
  final searchDebouncer = Debouncer(milliseconds: 500);
  bool isFocus = false;
  String searchText;

  List<ContentsModel> contents = List<ContentsModel>();
  int tabIndex = 0;
  TabController tabController;

  bool isSearchBar = true;

  final pageIndexNotifier = ValueNotifier<int>(0);

  List<Widget> pages = [];

  //
  MeCubit cubit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.cubit = context.bloc<MeCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
    context.bloc<TasteCubit>().getImageContents(null, null);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: context.bloc<TasteCubit>(),
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            this.indicator.hide();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;

          case GetContents:
            this.indicator.hide();
            //TODO: type.category 에서 type.all 변경
            pages.add(TasteContentView(
              3,
              TasteContentType.all,
              onZzim: (value) {
                ContentsModel contents = value[0];
                zzimItems = value[1];
                if (zzimItems.contains(contents)) {
                  context
                      .bloc<TasteCubit>()
                      .postCollectionSet(contents.imageContentNo, '');
                } else {
                  if (this.cubit.collections != null) {
                    context.bloc<TasteCubit>().postCollectionSet(
                        contents.imageContentNo,
                        this.cubit.collections[0].collectionNo.toString());
                  }
                }
                setState(() {});
              },
            ));
            setState(() {});
            break;

          case CompleteState:
            this.indicator.hide();
            this.navigator.resetRoute(SplashPage());
            break;
        }
      },
      child: PlatformScaffold(
        leading: CustomContainer(
          onPressed: () {
            this.navigator.popRoute(null);
          },
          margin: EdgeInsets.only(left: 20),
          width: 28,
          height: 28,
          child: Image.asset('assets/images/icnsBack.png'),
        ),
        title: Text.rich(
          TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: "${this.zzimItems.length}",
                style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    color: ColorTheme.black),
              ),
              TextSpan(
                  text: " / 5",
                  style: TextStyle(
                      fontSize: 26, color: ColorTheme.grayPlaceholder)),
            ],
          ),
        ),
        body: SafeArea(
          child: NestedScrollView(
            headerSliverBuilder: (context, innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  backgroundColor: Colors.white,
                  //title: searchBar(),
                  // floating 설정. SliverAppBar는 스크롤 다운되면 화면 위로 사라짐.
                  // true: 스크롤 업 하면 앱바가 바로 나타남. false: 리스트 최 상단에서 스크롤 업 할 때에만 앱바가 나타남
                  floating: false,

                  pinned: false,
                  bottom: PreferredSize(
                      child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 25),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Spacer(),
                                  Image.asset(
                                    'assets/images/elementButtonsBookmark.png',
                                    width: 112,
                                    height: 113,
                                    fit: BoxFit.cover,
                                  )
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                          text:
                                              "${this.cubit.user?.nickname ?? ''}",
                                          style: TextStyle(
                                              fontSize: 26,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        TextSpan(
                                            text: " 님의\n취향에 딱 맞는 곳들을\n찜! 해보세요",
                                            style: TextStyle(fontSize: 26)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )),
                      preferredSize: Size.fromHeight(220 - kToolbarHeight)),
                ),
                /*
                SliverPersistentHeader(
                  delegate: _SliverAppBarDelegate(this.getHorizontalMenu()),
                  pinned: true,
                ),
                */
              ];
            },
            body: this.tabBarView(),
          ),
        ),
      ),
    );
  }

  Widget tab(String title, int index) {
    return Material(
      color: Colors.transparent,
      child: Tab(
        child: Container(
          child: Text(
            title,
            style: this.tabIndex == index
                ? TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)
                : TextStyle(color: Colors.grey[400], fontSize: 16),
          ),
        ),
      ),
    );
  }

  getHorizontalMenu() {
    List<Widget> tabs = [];
    tabs.add(tab("추천", 0));
    tabs.add(tab("팔로잉", 1));
    return true
        ? TabBar(
            tabs: tabs,
            isScrollable: true,
            controller: this.tabController,
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: new BubbleTabIndicator(
              indicatorRadius: 22.0,
              indicatorHeight: 36.0,
              padding: EdgeInsets.symmetric(horizontal: 0),
              indicatorColor: ColorTheme.red,
              tabBarIndicatorSize: TabBarIndicatorSize.label,
            ),
          )
        : TabBar(
            isScrollable: true,
            controller: this.tabController,
            indicatorColor: Colors.white,
            tabs: tabs,
          );
  }

  tabBarView() {
    return pages.first;
  }

  PageViewIndicator _buildIndicator() {
    return PageViewIndicator(
      pageIndexNotifier: pageIndexNotifier,
      length: 3,
      normalBuilder: (animationController, index) => Circle(
        size: 8.0,
        color: Colors.black87,
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController,
          curve: Curves.ease,
        ),
        child: Circle(size: 10.0, color: Colors.white),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
