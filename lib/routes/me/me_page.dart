import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/Notification/notification_page.dart';
import 'package:fitmybiz/routes/_cells/collection_cell.dart';
import 'package:fitmybiz/routes/config/config_page.dart';
import 'package:fitmybiz/routes/follow/follow_page.dart';
import 'package:fitmybiz/routes/me/me_cubit.dart';
import 'package:fitmybiz/routes/me/me_state.dart';
import 'package:fitmybiz/routes/my_place/my_place_page.dart';
import 'package:fitmybiz/routes/views/custom_web_view.dart';
import 'package:fitmybiz/routes/views/delete_collection_view.dart';
import 'package:fitmybiz/routes/views/insert_collection_sheet_temp.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:fitmybiz/tools/network_client.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_cookie_manager/webview_cookie_manager.dart';

class MePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MeCubit(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  MeCubit cubit;

  WebviewCookieManager cookieManager = WebviewCookieManager();
  Future<List<Cookie>> gotCookies;
  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<MeCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
    this.cubit.getPoint();
  }

  List<String> areas = [];
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        title: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: Row(
            children: [
              CustomContainer(
                child: Row(
                  children: [
                    Text.rich(
                      TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: "포인트",
                            style: TextStyle(
                                color: Color.fromRGBO(138, 148, 159, 1),
                                fontSize: 13),
                          ),
                          TextSpan(text: ' '),
                          TextSpan(
                              text: this.cubit.point ?? "",
                              style: TextStyle(
                                  fontSize: 13,
                                  color: ColorTheme.primary,
                                  fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: 'P',
                              style: TextStyle(
                                  fontSize: 13,
                                  color: ColorTheme.primary,
                                  fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                    SizedBox(width: 8),
                    CustomContainer(
                      onPressed: () {
                        bool enableSlideOff = true;
                        bool hideCloseButton = true;
                        bool onlyOne = true;
                        bool crossPage = true;
                        int seconds = 3;
                        int animationMilliseconds = 200;
                        int animationReverseMilliseconds = 200;
                        BackButtonBehavior backButtonBehavior =
                            BackButtonBehavior.none;
                        BotToast.showSimpleNotification(
                            borderRadius: 12,
                            title: "매번 찜할 때마다 5point 적립!",
                            subTitle: "모인 포인트는 찜스토어에서 현금처럼 쓰실 수 있어요",
                            titleStyle: TextStyle(color: Colors.white),
                            subTitleStyle: TextStyle(color: Colors.white),
                            enableSlideOff: enableSlideOff,
                            hideCloseButton: hideCloseButton,
                            onlyOne: onlyOne,
                            backgroundColor: ColorTheme.black.withOpacity(.9),
                            crossPage: crossPage,
                            backButtonBehavior: backButtonBehavior,
                            onTap: () {
                              BotToast.showText(text: 'Tap toast');
                            },
                            onLongPress: () {
                              BotToast.showText(text: 'Long press toast');
                            },
                            animationDuration:
                                Duration(milliseconds: animationMilliseconds),
                            animationReverseDuration: Duration(
                                milliseconds: animationReverseMilliseconds),
                            duration: Duration(seconds: seconds));
                      },
                      margin: EdgeInsets.only(top: 2),
                      child: Icon(Icons.help,
                          color: ColorTheme.grayPlaceholder, size: 18),
                    )
                  ],
                ),
                onPressed: () {
                  this.navigator.pushRoute(CustomWebView(
                          'https://www.zzimstore.com', '찜 스토어', (wvc) {
                        NetworkClient.shared.property.accessToken.then((value) {
                          cookieManager.clearCookies().then((_) {
                            cookieManager.setCookies([
                              Cookie('APP-TOKEN', value)
                                ..domain = 'www.zzimstore.com'
                                ..httpOnly = false
                            ]);
                          });
                        });
                      }));
                },
              ),
              Spacer(),
              GestureDetector(
                child: Image.asset('assets/images/icnsNotice.png',
                    width: 28, height: 28, color: Colors.black),
                onTap: () {
                  this.navigator.pushRoute(NotificationPage());
                },
              ),
              SizedBox(width: 16),
              GestureDetector(
                child: Image.asset('assets/images/icnsSetting.png',
                    width: 28, height: 28, color: Colors.black),
                onTap: () {
                  this.navigator.pushRoute(ConfigPage());
                },
              ),
            ],
          ),
        ),
        body: SafeArea(
          child: Stack(
            children: [
              ListView(
                padding: EdgeInsets.all(25),
                children: [
                  this.buildUser(this.cubit.user),
                  SizedBox(height: 40),
                  this.buildCollections(this.cubit.collections),
                  SizedBox(height: 85),
                ],
              ),
              Positioned(
                bottom: 25,
                left: 25,
                child: CustomContainer(
                  backgroundColor: ColorTheme.black,
                  borderRadius: [40, 40, 40, 40],
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                  child: Row(
                    children: [
                      GestureDetector(
                        child: Image.asset('assets/images/icnsAddMaps.png',
                            width: 28, height: 28, color: Colors.white),
                        onTap: () => false
                            ? this.navigator.pushRoute(InsertCollectionSheet(
                                onCompleted: (name) {
                                  Navigator.of(context).pop();
                                  this.cubit.postCollection(name);
                                },
                              ))
                            : showInsertCollectionSheet(),
                      ),
                      SizedBox(width: 30),
                      GestureDetector(
                        child: Image.asset('assets/images/icnsOrganize.png',
                            width: 28, height: 28, color: Colors.white),
                        onTap: () {
                          this
                              .navigator
                              .presentRoute(DeleteCollectionView(
                                  collections: this.cubit.collections))
                              .then((value) {
                            if (value != null) {
                              cubit.deleteCollection(value.join(','));
                            }
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showInsertCollectionSheet() {
    showModalBottomSheet(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      useRootNavigator: true,
      isScrollControlled: true,
      context: context,
      builder: (context) => Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: InsertCollectionSheet(
          onCompleted: (name) {
            Navigator.of(context).pop();
            this.cubit.postCollection(name);
          },
        ),
      ),
    );
  }

  Widget buildUser(UserModel user) {
    if (user == null) {
      return CustomContainer(
        height: 96,
      );
    }

    Widget avatarImage;

    if (user.profileImage == null) {
      avatarImage =
          Image.asset('assets/images/userProfileImg.png', fit: BoxFit.cover);
    } else {
      avatarImage = CustomCachedNetworkImage(
        preloadImage: user.profileImage.sm,
        image: user.profileImage.sm,
      );
    }

    return CustomContainer(
      height: 96,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CustomContainer(
            margin: EdgeInsets.only(right: 20),
            child: CustomAvatar(
              image: avatarImage,
              size: 96,
              radius: 48,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                this.cubit.user.nickname ?? '',
                textAlign: TextAlign.center,
                style: TextStyleTheme.common
                    .copyWith(fontSize: 20, fontWeight: FontWeight.w600),
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  /*
                  Text('장소',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 13, color: ColorTheme.grayLv1)),
                  SizedBox(width: 6),
                  Text(this.cubit.user.imageContentCount,
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 13, color: ColorTheme.black)),
                  SizedBox(width: 15),
                  */
                  CustomContainer(
                    child: Text(
                      '팔로워',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 13, color: ColorTheme.grayLv1),
                    ),
                    onPressed: () {
                      this.navigator.pushRoute(FollowPage(
                          this.property.user.userNo, FollowType.follower));
                    },
                  ),
                  SizedBox(width: 6),
                  Text(this.cubit.user.followerCount,
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 13, color: ColorTheme.black)),
                  SizedBox(width: 15),
                  CustomContainer(
                    child: Text('팔로잉',
                        style: TextStyleTheme.common
                            .copyWith(fontSize: 13, color: ColorTheme.grayLv1)),
                    onPressed: () {
                      this.navigator.pushRoute(FollowPage(
                          this.property.user.userNo, FollowType.following));
                    },
                  ),
                  SizedBox(width: 6),
                  Text(this.cubit.user.followingCount,
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 13, color: ColorTheme.black)),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildCollections(List<CollectionModel> collections) {
    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.70,
          crossAxisSpacing: 10,
          mainAxisSpacing: 20),
      itemBuilder: ((BuildContext c, int i) {
        CollectionModel collection = this.cubit.collections[i];
        return CollectionCell(
          collection: collection,
          isEditMode: false,
          isMine: true,
          onPressed: () {
            this.navigator.pushRoute(MyPlacePage(
                  collection.collectionNo.toString(),
                  collection.name,
                  true,
                  true,
                ));
          },
          onPressedMore: () {
            this.showCollectionMoreSheetMe(
              onDelete: () {
                this.cubit.deleteCollection(collection.collectionNo.toString());
              },
            );
          },
        );
      }),
      itemCount: cubit.collections.length,
    );
  }
}
