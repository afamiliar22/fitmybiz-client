import 'dart:async';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/google_place_model.dart';
import 'package:fitmybiz/models/link_data_model.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

import 'insert_place_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class InsertPlaceCubit extends BaseCubit<InsertPlaceState> {
  /// {@macro counter_cubit}
  InsertPlaceCubit() : super(InitialState());

  String description = '';
  bool secret = false;
  String link;
  String image;
  String title;
  Uint8List imageBytes;
  GooglePlaceModel place;
  LinkDataModel linkData;
  List<CategoryModel> categories = [];
  List<CollectionModel> collections = [];
  Set<int> selectedCategories = {};
  Set<int> selectedCollections = {};

  bool categoryDisabled = false;

  final TextEditingController descriptionEditingController =
      TextEditingController();
  void init() {
    // {{host}}/api/v1/link/image?source_url=https://blog.naver.com/hjungeun27/222133877111
    emit(LoadingState());
    Future.wait([this.selectCategories(), this.getCollectionSaved()])
        .then((value) {
      emit(LoadedState());
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void linkDataChanged(LinkDataModel linkData) {
    this.title = linkData.title;
    this.description = linkData.description;
    this.descriptionEditingController.text = this.description;
    this.link = linkData.destinationUrl;
    this.image = linkData.images.first;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }

  void imageBytesChanged(Uint8List imageBytes) {
    this.imageBytes = imageBytes;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }

  Future selectCategories() {
    String method = 'get';
    String path = '/api/v1/content/category/list';
    return this.network.requestWithAuth(method, path, null).then((response) {
      this.categories = (response.data['data'] as List)
          .map((record) => CategoryModel.fromJson(record))
          .toList();
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value();
    });
  }

  Future getCollectionSaved() {
    String method = 'get';
    String path = '/api/v1/collection/check/saved/item?image_content_no=1';

    return this.network.requestWithAuth(method, path, null).then((response) {
      this.collections = (response.data as List)
          .map((record) => CollectionModel.fromJson(record))
          .toList();
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value();
    });
  }

  bool validateInsertContent(bool isInit) {
    if (this.place == null) {
      if (!isInit) emit(ToastState('장소명, 상호명 등을 입력하세요'));
      return false;
    }
    if (this.selectedCategories.length <= 0) {
      if (!isInit) emit(ToastState('카테고리를 선택해주세요'));
      return false;
    }
    if (this.selectedCollections.length <= 0) {
      if (!isInit) emit(ToastState('지도를 선택해주세요'));
      return false;
    }

    return true;
  }

  requestInsertContent() {
    if (validateInsertContent(false) == false) {
      emit(ErrorState());
    }
    bool secret = this.secret;
    String title = this.title;
    String collectionNo = this.selectedCollections.toList().join(',');
    String categoryNo = this.selectedCategories.toList().join(',');
    String description = this.description;
    String placeName = this.place?.name ?? '';
    String placeId = this.place?.placeId ?? '';
    String lat = this.place.latitude.toString();
    String lng = this.place.longitude.toString();
    String image = this.image;
    String link = this.link;
    Uint8List imageBytes = this.imageBytes;

    String method = 'post';
    String path = '/api/v1/content/image_content';
    // String path = '/file';
    Map<String, dynamic> data = {
      "category_no": categoryNo,
      'collection_no': collectionNo,
      'place_id': placeId,
      'lat': lat,
      'lng': lng,
      'secret': secret ? 'true' : 'false'
    };

    if (title != null) {
      data['title'] = title;
    } else {
      data['title'] = placeName;
    }
    if (description != null) {
      data['description'] = description;
    }
    if (image != null) {
      data['image_url[]'] = image;
    }
    if (link != null) {
      data['resource_url'] = link;
    }

    emit(LoadingState());

    Map<String, Uint8List> files = {};
    Future.value().then((value) {
      if (this.imageBytes != null) {
        return resizeFuture(this.imageBytes).then((value) {
          files = {"image[]": value};
          return Future.value();
        });
      } else {
        return Future.value();
      }
    }).then((value) {
      return this.network.requestMultipart(method, path, data, files);
    }).then((response) {
      this.property.meProfileSubject.add(true);
      this.property.meCollectionSubject.add(true);
      if (this.imageBytes != null) {
        this.facebook.addEvent(FacebookEventType.addPlacePicture);
      } else {
        this.facebook.addEvent(FacebookEventType.addPlaceLink);
      }

      emit(CompletedState(response));
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void descriptionChanged(String description) {
    this.description = description;
  }

  void placeChanged(GooglePlaceModel place) {
    this.place = place;
    this.selectedCategories.clear();
    String method = 'get';
    String path = '/api/v1/place/check/category';
    Map<String, dynamic> data = {'google_place_id': this.place.placeId};

    this.network.requestWithAuth(method, path, data).then((response) {
      this.categoryDisabled = true;
      this
          .selectedCategories
          .addAll((response.data as List).map((e) => e['category_no']));
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    }).catchError((e) {
      this.categoryDisabled = false;
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
    });
  }

  void secretChanged(bool secret) {
    this.secret = secret;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }

  void titleChanged(String title) {
    this.title = title;
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }

  void categoriesChanged(int categoryNo) {
    if (this.categoryDisabled == true) {
      emit(ToastState('카테고리가 이미 지정된 장소입니다.'));
      return;
    }
    if (this.selectedCategories.contains(categoryNo)) {
      this.selectedCategories.remove(categoryNo);
    } else {
      this.selectedCategories.add(categoryNo);
    }
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }

  void collectionsChanged(int collectionNo) {
    if (this.selectedCollections.contains(collectionNo)) {
      this.selectedCollections.remove(collectionNo);
    } else {
      this.selectedCollections.add(collectionNo);
    }
    emit(SetState(DateTime.now().millisecondsSinceEpoch));
  }
}
