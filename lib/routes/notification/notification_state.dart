import 'package:equatable/equatable.dart';

abstract class NotificationState extends Equatable {}

class InitialState extends NotificationState {
  @override
  List<Object> get props => [];
}

class LoadingState extends NotificationState {
  @override
  List<Object> get props => [];
}

class SetState extends NotificationState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class ToastState extends NotificationState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends NotificationState {
  @override
  List<Object> get props => [];
}
