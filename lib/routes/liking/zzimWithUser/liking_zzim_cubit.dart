import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/models/contents_model.dart';

import 'package:fitmybiz/models/follow_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/tools/facebook_client.dart';

import 'liking_zzim_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class LikingZzimCubit extends BaseCubit<LikingZzimState> {
  /// {@macro counter_cubit}
  LikingZzimCubit() : super(InitialState());
  final searchDebouncer = Debouncer(milliseconds: 500);
  LikingZzimCubit cubit;
  List<FollowModel> follows = [];
  UserModel other = UserModel();
  List<ContentsModel> contents = [];
  String query = '';

  void init() {
    emit(InitialState());
    this.facebook.addEvent(FacebookEventType.searchTogetherWish);
  }

  void searchChanged(String query, String take) {
    this.query = query;
    // this.searchDebouncer.run(() => getSearchImageContent(query, take));
  }

  Future selectUser(String userNo) {
    String method = 'get';
    String path = '/api/v1/user/$userNo';

    emit(LoadingState());
    return this.network.requestWithAuth(method, path, null).then((response) {
      this.other = UserModel.fromJson(response.data);

      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value();
    }).catchError((e) {
      emit(ErrorState());
      return Future.value();
    });
  }

  Future getImageContents(String userNo, int page, int take) {
    String method = 'get';
    String path = '/api/v1/content/image_content/taste/$userNo';
    Map<String, dynamic> data = {
      "page": page.toString(),
      "take": take.toString()
    };
    emit(LoadingState());
    return this.network.requestWithAuth(method, path, data).then((response) {
      contents = (response.data['data'] as List)
          .map((record) => ContentsModel.fromJson(record))
          .toList();
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      return Future.value();
    }).catchError((e) {
      emit(ErrorState());
    });
  }

  void insertImageContentReport(String imageContentNo, String description) {
    String method = 'post';
    String path = '/api/v1/report/image_content/$imageContentNo';
    Map<String, dynamic> data = {
      'report_title': '신고하기',
      'report_description': description
    };

    this.network.requestWithAuth(method, path, data).then((response) {
      emit(ToastState('신고되었습니다.'));
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
