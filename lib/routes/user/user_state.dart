import 'package:equatable/equatable.dart';

abstract class UserState extends Equatable {}

class InitialState extends UserState {
  @override
  List<Object> get props => [];
}

class LoadingState extends UserState {
  @override
  List<Object> get props => [];
}

class DeletedUserState extends UserState {
  @override
  List<Object> get props => [];
}

class SetState extends UserState {
  SetState(this.timestamp);

  final int timestamp;

  @override
  List<Object> get props => [timestamp];
}

class ToastState extends UserState {
  ToastState(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class ErrorState extends UserState {
  @override
  List<Object> get props => [];
}
