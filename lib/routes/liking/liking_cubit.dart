import 'package:bloc/bloc.dart';
import 'package:fitmybiz/base_cubit.dart';
import 'package:fitmybiz/components/debouncer.dart';
import 'package:fitmybiz/models/follow_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'liking_state.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class LikingCubit extends BaseCubit<LikingState> {
  /// {@macro counter_cubit}
  LikingCubit() : super(InitialState());
  final searchDebouncer = Debouncer(milliseconds: 500);
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  LikingCubit cubit;
  String userNo;
  int page = 1;

  List<FollowModel> follows;
  String query = '';
  void init() {
    emit(InitialState());
  }

  void setUserNo(String userNo) {
    this.userNo = userNo;
  }

  void searchChanged(String query, String take) {
    this.query = query;
  }

  onRefresh() {
    this.page = 1;
    this.getSearchFollowing().then((f) {
      this.follows = f;
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    }).catchError((e) {
      this.follows = [];
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      this.refreshController.refreshCompleted();
      this.refreshController.resetNoData();
    });
  }

  onLoading() {
    this.page = this.page + 1;
    this.getSearchFollowing().then((f) {
      this.follows.addAll(f);
      emit(SetState(DateTime.now().millisecondsSinceEpoch));
      if (follows.length == 0) {
        this.refreshController.loadNoData();
      } else {
        this.refreshController.loadComplete();
      }
    }).catchError((e) {
      this.refreshController.loadComplete();
    });
  }

  Future<List<FollowModel>> getSearchFollowing() {
    String method = 'get';
    String path = '/api/v1/user/${this.userNo}/following';
    Map<String, dynamic> data = {'take': '20', 'page': this.page.toString()};
    emit(LoadingState());
    return this.network.requestWithAuth(method, path, data).then((response) {
      List<FollowModel> result = (response.data['data'] as List)
          .map((record) => FollowModel.fromJson(record))
          .toList();

      return Future.value(result);
    }).catchError((e) {
      emit(ErrorState());
    });
  }
}
