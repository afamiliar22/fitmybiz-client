import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_avatar.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/routes/_cells/collection_cell.dart';
import 'package:fitmybiz/routes/follow/follow_page.dart';
import 'package:fitmybiz/routes/liking/liking_page.dart';
import 'package:fitmybiz/routes/liking/zzimWithUser/liking_zzim_page.dart';
import 'package:fitmybiz/routes/map/map_page.dart';
import 'package:fitmybiz/routes/my_place/my_place_page.dart';
import 'package:fitmybiz/routes/user/user_cubit.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'user_state.dart';

class UserPage extends StatelessWidget {
  final String userNo;
  // const UserPage({Key key}) : super(key: key);
  UserPage(this.userNo);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => UserCubit(),
      child: _View(this.userNo),
    );
  }
}

class _View extends BaseRoute {
  final String userNo;
  _View(this.userNo);
  @override
  _ViewState createState() => _ViewState(this.userNo);
}

class _ViewState extends BaseRouteState {
  String userNo;
  _ViewState(this.userNo);
  UserCubit cubit;

  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<UserCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.init();
    this.cubit.userNoChanged(this.userNo);
  }

  List<String> areas = [];
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case DeletedUserState:
            this.showToast('탈퇴된 회원입니다.');
            this.navigator.popRoute(null);
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            setState(() {});
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        body: SafeArea(
          child: Stack(
            children: [
              ListView(
                padding: EdgeInsets.all(25),
                children: [
                  Row(
                    children: [
                      IconButton(
                        padding: EdgeInsets.only(
                            left: 12, top: 8, right: 0, bottom: 8),
                        icon: Image.asset('assets/images/icnsBack.png',
                            color: Colors.black, width: 28, height: 28),
                        onPressed: () {
                          this.navigator.popRoute(null);
                        },
                      ),
                      Spacer(),
                      this.cubit.user != null
                          ? CustomContainer(
                              width: 48,
                              height: 48,
                              padding: EdgeInsets.all(10),
                              backgroundColor: Colors.white,
                              shadowColor:
                                  ColorTheme.blackShadow.withOpacity(.1),
                              borderRadius: [24, 24, 24, 24],
                              child: Image.asset(
                                'assets/images/icnsFriends.png',
                                width: 24,
                                height: 24,
                              ),
                              onPressed: () {
                                this.navigator.pushRoute(
                                    LikingZzimPage(this.cubit.user.userNo));
                              },
                            )
                          : this.buildFollowButton(),
                    ],
                  ),
                  this.buildUser(this.cubit.user),
                  SizedBox(height: 40),
                  this.buildCollections(this.cubit.collections),
                  SizedBox(height: 85),
                ],
              ),
              Positioned(
                bottom: 25,
                left: 25,
                child: CustomContainer(
                  backgroundColor: ColorTheme.black,
                  borderRadius: [40, 40, 40, 40],
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  onPressed: () {
                    this.navigator.pushRoute(MapPage(
                          isUser: false,
                          userNo: this.cubit.user.userNo,
                        ));
                  },
                  child: Image.asset('assets/images/icnsMapView.png',
                      width: 28, height: 28, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildFollowButton() {
    if (this.cubit.user.userNo == this.property.user.userNo) {
      return Container();
    } else if (this.cubit.selectedFollow) {
      return Container(
        child: CustomContainer(
          backgroundColor: ColorTheme.grayLv1,
          borderRadius: [14, 14, 14, 14],
          borderWidth: 1,
          borderColor: Color.fromRGBO(173, 181, 190, 1),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
            child: Text(
              "팔로잉",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          onPressed: () {
            cubit.selectedFollow = !cubit.selectedFollow;
            setState(() {});
            this.cubit.putUpdateFollow(this.cubit.userNo);
          },
        ),
      );
    } else {
      return Container(
        child: CustomContainer(
          backgroundColor: Color.fromRGBO(31, 31, 36, 1),
          borderRadius: [14, 14, 14, 14],
          borderWidth: 1,
          borderColor: Color.fromRGBO(31, 31, 36, 1),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
            child: Text(
              "팔로우",
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
          onPressed: () {
            cubit.selectedFollow = !cubit.selectedFollow;
            setState(() {});
            this.cubit.putUpdateFollow(this.cubit.userNo);
          },
        ),
      );
    }
  }

  Widget buildUser(UserModel user) {
    if (user == null) {
      return CustomContainer(
        height: 96,
      );
    }

    Widget avatarImage;

    if (user.profileImage == null) {
      avatarImage =
          Image.asset('assets/images/userProfileImg.png', fit: BoxFit.cover);
    } else {
      // avatarImage = FadeInImage.memoryNetwork(placeholder: kTransparentImage, image: user.profileImage.l, fit: BoxFit.cover);
      avatarImage = CustomCachedNetworkImage(
        preloadImage: user.profileImage.sm,
        image: user.profileImage.sm,
      );
    }

    return CustomContainer(
      height: 96,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CustomContainer(
            margin: EdgeInsets.only(right: 20),
            child: CustomAvatar(
              image: avatarImage,
              size: 96,
              radius: 48,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                this.cubit.user.nickname,
                textAlign: TextAlign.center,
                style: TextStyleTheme.common
                    .copyWith(fontSize: 20, fontWeight: FontWeight.w600),
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  CustomContainer(
                    child: Text(
                      '팔로워',
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 14, color: ColorTheme.grayLv1),
                    ),
                    onPressed: () {
                      this.navigator.pushRoute(FollowPage(
                          this.cubit.user.userNo, FollowType.follower));
                    },
                  ),
                  SizedBox(width: 6),
                  Text(this.cubit.user.followerCount,
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 14, color: ColorTheme.black)),
                  SizedBox(width: 15),
                  CustomContainer(
                    child: Text('팔로잉',
                        style: TextStyleTheme.common
                            .copyWith(fontSize: 14, color: ColorTheme.grayLv1)),
                    onPressed: () {
                      this.navigator.pushRoute(FollowPage(
                          this.cubit.user.userNo, FollowType.following));
                    },
                  ),
                  SizedBox(width: 6),
                  Text(this.cubit.user.followingCount,
                      style: TextStyleTheme.common
                          .copyWith(fontSize: 14, color: ColorTheme.black)),
                ],
              ),
              SizedBox(
                height: 12,
              ),
              buildFollowButton()
            ],
          )
        ],
      ),
    );
  }

  Widget buildCollections(List<CollectionModel> collections) {
    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.70,
          crossAxisSpacing: 10,
          mainAxisSpacing: 20),
      itemBuilder: ((BuildContext c, int i) {
        CollectionModel collection = this.cubit.collections[i];
        return CollectionCell(
          collection: collection,
          isMine: false,
          onPressed: () {
            this
                .navigator
                .pushRoute(MyPlacePage(
                  collection.collectionNo.toString(),
                  collection.name,
                  false,
                  false,
                  user: this.cubit.user,
                ))
                .then((value) {
              if (value == true) {
                this.cubit.init();
              }
            });
          },
          onPressedMore: () {
            this.showCollectionMoreSheetOther(
              onReport: (description) {
                this.cubit.insertCollectionReport(
                    collection.collectionNo.toString(), description);
              },
            );
          },
        );
      }),
      itemCount: cubit.collections.length,
    );
  }
}
