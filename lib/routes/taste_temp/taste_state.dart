import 'package:equatable/equatable.dart';
import 'package:fitmybiz/models/banner_model.dart';
import 'package:fitmybiz/models/category_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/user_model.dart';

abstract class TasteState extends Equatable {}

class InitialState extends TasteState {
  @override
  List<Object> get props => [];
}

class LoadingState extends TasteState {
  @override
  List<Object> get props => [];
}

class ErrorState extends TasteState {
  @override
  List<Object> get props => [];
}

class GetBanner extends TasteState {
  GetBanner(this.result, this.banners);

  final bool result;
  final List<BannerModel> banners;

  @override
  List<Object> get props => [result, banners];
}

class GetContents extends TasteState {
  GetContents(this.result, this.contents);

  final bool result;
  final List<ContentsModel> contents;

  @override
  List<Object> get props => [result, contents];
}

class RefreshRadius extends TasteState {
  RefreshRadius(this.radius);

  final double radius;

  @override
  List<Object> get props => [radius];
}

class GetCategory extends TasteState {
  GetCategory(this.result, this.categories);

  final bool result;
  final List<CategoryModel> categories;

  @override
  List<Object> get props => [result, categories];
}

class PostCollectionSet extends TasteState {
  PostCollectionSet(this.result);

  final bool result;

  @override
  List<Object> get props => [result];
}

class GetCollection extends TasteState {
  GetCollection(this.result, this.collections);

  final bool result;
  final List<CollectionModel> collections;

  @override
  List<Object> get props => [result, collections];
}

class SelectUser extends TasteState {
  SelectUser(this.user);

  final UserModel user;

  @override
  List<Object> get props => [user];
}
