import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/models/collection_item_model.dart';
import 'package:fitmybiz/models/collection_model.dart';
import 'package:fitmybiz/routes/createMap/create_map_page.dart';
import 'package:fitmybiz/themes/themes.dart';

import 'select_collection_cubit.dart';
import 'select_collection_state.dart';

class SelectCollectionPage extends StatelessWidget {
  final List<CollectionItemModel> selectedItems;

  SelectCollectionPage({Key key, @required this.selectedItems})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SelectCollectionCubit(),
      child: _View(selectedItems),
    );
  }
}

class _View extends BaseRoute {
  final List<CollectionItemModel> selectedItems;

  _View(this.selectedItems);

  @override
  _ViewState createState() => _ViewState(selectedItems);
}

class _ViewState extends BaseRouteState {
  final List<CollectionItemModel> selectedItems;

  _ViewState(this.selectedItems);

  SelectCollectionCubit cubit;

  List<CollectionModel> selectedCollections = [];

  @override
  void initState() {
    super.initState();
    this.cubit = context.bloc<SelectCollectionCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    this.cubit.getCollectionSaved();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: this.cubit,
      listener: (context, state) {
        switch (state.runtimeType) {
          case InitialState:
            break;
          case ToastState:
            this.showToast(state.message);
            break;
          case SetState:
            this.indicator.hide();
            /*
            selectedItems =
                List.generate(this.cubit.collections.length, (i) => null);
                */

            setState(() {});
            break;
          case LoadedState:
            this.indicator.hide();
            break;
          case LoadingState:
            this.indicator.show();
            break;
          case ErrorState:
            this.indicator.hide();
            break;
        }
      },
      child: PlatformScaffold(
        leading: IconButton(
          padding: EdgeInsets.only(left: 12, top: 8, right: 0, bottom: 8),
          //padding: EdgeInsets.only(left: 0, top: 8, right: 12, bottom: 8),
          icon: Image.asset('assets/images/icnsClose.png',
              color: Colors.black, width: 28, height: 28),
          onPressed: () {
            Navigator.of(context).popUntil((route) => route.isFirst);
          },
        ),
        title: Text('지도 변경하기'),
        trailing: false
            ? CustomButton(
                text: Text(
                  '완료',
                  style: TextStyle(color: ColorTheme.red),
                ),
                onPressed: () {
                  for (var item in selectedCollections) {
                    this.cubit.copyCollectionItem(item.collectionNo.toString(),
                        selectedItems.map((e) => e.imageContentNo).join(','));
                  }
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
              )
            : CustomContainer(
                margin: EdgeInsets.only(right: 24),
                child: Text(
                  '완료',
                  style: TextStyle(fontSize: 16, color: ColorTheme.primary),
                ),
              ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 16),
              CustomContainer(
                margin: EdgeInsets.symmetric(horizontal: 25),
                child: Text(
                  true ? '지도를 선택해주세요 (중복선택 가능)' : '어떤 지도에 추가하실건가요?',
                  style: true
                      ? TextStyle(color: ColorTheme.grayLv1, fontSize: 14)
                      : TextStyleTheme.subtitle,
                ),
              ),
              SizedBox(height: 16),
              Expanded(
                child: Stack(
                  children: [
                    ListView(
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemBuilder: ((BuildContext c, int i) {
                            CollectionModel collection =
                                this.cubit.collections[i];

                            Widget imageWidget;
                            if (collection.imageSrc != null) {
                              imageWidget = CustomCachedNetworkImage(
                                preloadImage: collection.imageSrc.sm,
                                image: collection.imageSrc.sm,
                              );
                            } else {
                              imageWidget = Container(color: ColorTheme.grayBg);
                            }
                            return CustomContainer(
                              margin: EdgeInsets.symmetric(horizontal: 25),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Stack(
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsets.symmetric(vertical: 8),
                                        width: 60,
                                        height: 60,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(12),
                                          child: imageWidget,
                                        ),
                                      ),
                                      this
                                              .selectedCollections
                                              .contains(collection)
                                          ? CustomContainer(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 8),
                                              width: 60,
                                              height: 60,
                                              child: Center(
                                                child: Image.asset(
                                                  'assets/images/icnsCheck.png',
                                                  width: 28,
                                                  height: 28,
                                                ),
                                              ),
                                              borderRadius: [12, 12, 12, 12],
                                              backgroundColor: ColorTheme
                                                  .primary
                                                  .withOpacity(.5),
                                            )
                                          : Container()
                                    ],
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 16),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          child: Text(
                                            collection.name,
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Container(
                                          child: Text(
                                            "${collection.itemCount}개의 장소",
                                            style: TextStyle(
                                                fontSize: 13,
                                                color: Color.fromRGBO(
                                                    138, 148, 159, 1)),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                      child: this
                                                  .selectedCollections
                                                  .contains(collection) ??
                                              false
                                          ? Icon(
                                              Icons.radio_button_checked,
                                              color: ColorTheme.primary2,
                                            )
                                          : Icon(
                                              Icons.radio_button_unchecked,
                                              color: Color.fromRGBO(
                                                  205, 210, 216, 1),
                                            )),
                                ],
                              ),
                              onPressed: () {
                                this.selectedCollections.contains(collection)
                                    ? this
                                        .selectedCollections
                                        .remove(collection)
                                    : this.selectedCollections.add(collection);
                                setState(() {});
                                return;
                                this
                                    .navigator
                                    .popRoute(collection.collectionNo);
                              },
                            );
                          }),
                          itemCount: this.cubit.collections.length,
                        ),
                        /*
                    CustomContainer(
                      padding:
                          EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                      child: Row(
                        children: [
                          Container(
                            child: Image.asset('assets/images/icnsAddMaps.png',
                                width: 28, height: 28),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 12),
                            child: Text(
                              "새 지도 만들기",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            child: Image.asset('assets/images/icnsNext.png',
                                color: Colors.grey, width: 28, height: 28),
                          ),
                        ],
                      ),
                      onPressed: () {
                        this.navigator.pushRoute(CreateMapPage()).then((value) {
                          if (value == 'refresh') {
                            context
                                .bloc<SelectCollectionCubit>()
                                .getCollectionSaved();
                          }
                        });
                      },
                    )
                    */
                      ],
                    ),
                    Positioned(
                      bottom: 24,
                      left: 0,
                      right: 0,
                      child: Row(
                        children: [
                          Spacer(),
                          CustomContainer(
                              shadowColor: ColorTheme.black.withOpacity(.2),
                              backgroundColor: ColorTheme.black,
                              borderRadius: [40, 40, 40, 40],
                              padding: EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 30),
                              child: GestureDetector(
                                child: Row(
                                  children: [
                                    Image.asset('assets/images/icnsAddMaps.png',
                                        width: 28,
                                        height: 28,
                                        color: Colors.white),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    Text(
                                      '새 지도 만들기',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    )
                                  ],
                                ),
                                onTap: () {
                                  this
                                      .navigator
                                      .pushRoute(CreateMapPage())
                                      .then((value) {
                                    if (value == 'refresh') {
                                      context
                                          .bloc<SelectCollectionCubit>()
                                          .getCollectionSaved();
                                    }
                                  });
                                  /*
                                    if (this
                                            .cubit
                                            .selectedCollectionItems
                                            .length <=
                                        0) {
                                      this.showToast('복사할 장소를 선택해주세요.');
                                      return;
                                    }
                                    this
                                        .navigator
                                        .pushRoute(SelectCollectionPage())
                                        .then((value) {
                                      if (value == null) return;
                                      List<CollectionItemModel>
                                          selectedCollections = this
                                              .cubit
                                              .collectionItems
                                              .where((element) => this
                                                  .cubit
                                                  .selectedCollectionItems
                                                  .contains(
                                                      element.collectionItemNo))
                                              .toList();
                                      this.cubit.copyCollectionItem(
                                          value.toString(),
                                          selectedCollections
                                              .map((e) => e.imageContentNo)
                                              .join(','));
                                    });
                                    */
                                  //
                                },
                              )),
                          Spacer(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              /*
              Container(
                height: 54,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                child: CustomButton(
                  text: Text('완료'),
                  padding: EdgeInsets.all(16),
                  borderRadius: 18,
                  backgroundColor: ColorTheme.primary,
                  shadowColor: ColorTheme.primary.withOpacity(0.1),
                  onPressed: () {},
                ),
              )
              */
            ],
          ),
        ),
      ),
    );
  }
}
