import 'package:fitmybiz/base_route.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/components/platform_scaffold.dart';
import 'package:fitmybiz/temp_routes/panel_info_income_personal.dart';
import 'package:fitmybiz/themes/themes.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';

const List<String> jobs = [
  '무직',
  '농임어축산업',
  '자영업',
  '중학생',
  '판매영업서비스직',
  '고등학생',
  '초등학생',
  '기능 작업직',
  '사무 기술직',
  '경영 관리직',
  '자유 전문직',
  '대학생',
  '대학원생',
  '전업주부'
];

class PanelInfoJobPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  var height;
  var width;

  @override
  void initState() {
    super.initState();
  }

  Widget buildSettings() {
    return CustomContainer(
      padding: EdgeInsets.symmetric(horizontal: 34),
      child: Column(
        children: [
          Row(
            children: [
              Icon(CupertinoIcons.bell),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Text('공지사항'),
              ),
              Icon(CupertinoIcons.arrow_right),
            ],
          ),
          Divider()
        ],
      ),
    );
  }

  //

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: PlatformScaffold(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            this.navigator.popRoute(null);
          },
        ),
        title: Text('패널 정보입력'),
        body: CustomContainer(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '직업을 알려주세요',
                style: TextStyle(color: ColorTheme.black, fontSize: 16),
              ),
              SizedBox(
                height: 18,
              ),
              CustomContainer(
                height: MediaQuery.of(context).size.height / 2,
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 143 / 22, crossAxisCount: 2),
                  itemCount: jobs.length,
                  itemBuilder: (context, index) {
                    return CustomContainer(
                      //margin: EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        children: [
                          Radio(
                            value: true,
                            onChanged: (value) {},
                            groupValue: true,
                          ),
                          Text(jobs[index])
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  Spacer(),
                  CustomContainer(
                    margin: EdgeInsets.all(10),
                    width: 285,
                    height: 40,
                    onPressed: () {
                      this.navigator.pushRoute(PanelInfoPersonalIncomePage());
                    },
                    backgroundColor: ColorTheme.primary2,
                    borderRadius: [6, 6, 6, 6],
                    child: Center(
                      child: Text(
                        '다음',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
