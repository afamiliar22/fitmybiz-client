import 'package:flutter/material.dart';
import 'package:fitmybiz/components/custom_button.dart';
import 'package:fitmybiz/components/custom_cached_network_image.dart';
import 'package:fitmybiz/components/custom_container.dart';
import 'package:fitmybiz/models/contents_model.dart';
import 'package:fitmybiz/models/user_model.dart';
import 'package:fitmybiz/themes/themes.dart';

class PlaceDetailHeader extends SliverPersistentHeaderDelegate {
  ContentsModel contents;
  String placeId;
  bool isFind = false;
  bool isZzim = false;
  VoidCallback onPressed;

  final Function() onZzim;
  PlaceDetailHeader(
      {this.contents,
      this.placeId,
      this.isFind,
      this.isZzim,
      this.onZzim,
      this.onPressed});
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        Container(
          child: Hero(
            tag: "place-$placeId",
            // child: Image.network(image, fit: BoxFit.cover) ?? Container(),
            child: CustomCachedNetworkImage(
              preloadImage: this.contents.imageSrc.sm,
              image: this.contents.imageSrc.sm,
            ),
          ),
        ),
        GestureDetector(
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  Colors.black12,
                  Colors.transparent,
                  Colors.black26
                ])),
          ),
          onTap: onPressed,
        ),
        Positioned(
          right: 0,
          left: 0,
          bottom: -1,
          child: Container(
            height: 100,
            child: Stack(
              children: [
                this.contents.resourceUrl == null
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(left: 20, bottom: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            /*
                                CustomContainer(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 4, horizontal: 12),
                                  margin: EdgeInsets.only(bottom: 8),
                                  borderColor: Colors.white,
                                  borderRadius: [10, 10, 10, 10],
                                  child: Text("원본보기",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 14)),
                                  onPressed: () {
                                    onPressed();
                                  },
                                ),
                                  */
                            this.contents.providerName == null
                                ? Container(
                                    width: 200,
                                    child: Text(
                                      "${this.contents.resourceUrl}",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 14),
                                    ))
                                : Text(
                                    "by. ${this.contents.providerName}"
                                    // : ${this.contents.authorName}
                                    ,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 14),
                                  ),
                            SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      ),
                Column(
                  children: [
                    Container(
                      height: 60,
                      color: Colors.transparent,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              //topLeft: Radius.circular(30),
                              //topRight: Radius.circular(30),
                              )),
                      height: 40,
                      // color: Colors.white,
                    ),
                  ],
                ),
                Row(
                  children: [
                    /*
                    this.contents.resourceUrl == null
                        ? Container()
                        : Container(
                            margin: EdgeInsets.only(left: 20, bottom: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomContainer(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 4, horizontal: 12),
                                  margin: EdgeInsets.only(bottom: 8),
                                  borderColor: Colors.white,
                                  borderRadius: [10, 10, 10, 10],
                                  child: Text("원본보기",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 14)),
                                  onPressed: () {
                                    onPressed();
                                  },
                                ),
                                this.contents.providerName == null
                                    ? Container(
                                        width: 200,
                                        child: Text(
                                          "${this.contents.resourceUrl}",
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14),
                                        ))
                                    : Text(
                                        "by. ${this.contents.providerName} : ${this.contents.authorName}",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ),
                          */
                    CustomContainer(
                      margin: EdgeInsets.only(left: 25, top: 20),
                      height: 44,
                      width: 88,
                      // decoration: BoxDecoration(borderRadius: BorderRadius.circular(36), color: this.isZzim ? ColorTheme.primary2 : Colors.grey),
                      backgroundColor: ColorTheme.black,
                      borderRadius: [36, 36, 36, 36],
                      child: Container(
                        //margin: EdgeInsets.all(22),
                        child: Center(
                          child: Text(
                            '링크 방문',
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                      ),
                      onPressed: () {
                        onPressed();
                      },
                    ),
                    Spacer(),
                    CustomContainer(
                      margin: EdgeInsets.only(right: 25, top: 20),
                      height: 72,
                      width: 72,
                      shadowColor: this.isZzim
                          ? ColorTheme.primary2.withOpacity(.5)
                          : Colors.transparent,
                      // decoration: BoxDecoration(borderRadius: BorderRadius.circular(36), color: this.isZzim ? ColorTheme.primary2 : Colors.grey),
                      backgroundColor: this.isZzim
                          ? ColorTheme.primary2
                          : ColorTheme.grayLv2,
                      borderRadius: [36, 36, 36, 36],
                      child: Container(
                        margin: EdgeInsets.all(22),
                        child: Image.asset(
                          "assets/images/bookmark.png",
                          height: 28,
                          width: 28,
                        ),
                      ),
                      onPressed: () {
                        // this.isZzim = !this.isZzim;
                        this.onZzim();
                      },
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => 500;

  @override
  double get minExtent => 200;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
